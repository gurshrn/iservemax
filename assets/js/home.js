/*============Show frontend register==============*/


	jQuery(document).on('click','#frontend-register',function(){
		var datattr = jQuery(this).attr('data-attr');
		if(datattr == 'login')
		{
			jQuery('#pills-login-tab').attr('aria-selected',true);
			jQuery('#pills-register-tab').attr('aria-selected',false);
			jQuery('#pills-login-tab').addClass('active');
			jQuery('#pills-register-tab').removeClass('active');
			jQuery('#pills-login').addClass('active show');
			jQuery('#pills-register').removeClass('active show');
		}
		if(datattr == 'sign-up')
		{
			jQuery('#pills-register-tab').attr('aria-selected',true);
			jQuery('#pills-login-tab').attr('aria-selected',false);
			jQuery('#pills-register-tab').addClass('active');
			jQuery('#pills-login-tab').removeClass('active');
			jQuery('#pills-register').addClass('active show');
			jQuery('#pills-login').removeClass('active show');

		}
		$("#login").modal({ backdrop: 'static',keyboard: false});
		$('#login-form')[0].reset();
		$('#register-form')[0].reset();
		var validator = $("#register-form").validate();
		var validator1 = $("#login-form").validate();
		validator.resetForm();
		validator1.resetForm();
	});
	jQuery(document).on('click','#pills-PublishOffer-tab',function(){
		jQuery('#publish-credit-offer')[0].reset();
		var validator = $("#publish-credit-offer").validate();
		validator.resetForm();
	});
	jQuery(document).on('click','#pills-internetOffer-tab',function(){
		jQuery('#internet-offer')[0].reset();
		var validator = $("#internet-offer").validate();
		validator.resetForm();
	});
		

/*=================Calculate promotype=========================*/

	jQuery(document).on('change','.calculateVal',function(){


		if($(this).val().charAt(0) == '.')
		{
			var str = $(this).val();
			str = '0' + str;
			$(this).val(str);
		}

		var creditVal = jQuery('.creditVal').val();
		var sellingPrice = jQuery('.sellingPrice').val();
		
		if(creditVal != '' &&  sellingPrice != '')
		{
			var subtractVal = parseFloat(creditVal)-parseFloat(sellingPrice);
			var promotionType = parseFloat(subtractVal)/parseFloat(sellingPrice);
			var total = parseFloat(promotionType).toFixed(2);
			var totalPromtion = total*parseFloat(100);
			jQuery('.promotionType').val(totalPromtion);
		}
		else
		{
			jQuery('.promotionType').val('');
			jQuery('.sellingPriceError').html('');
			jQuery('.sellingPriceError').css('display','none');
		}
	});
	jQuery(document).on('change','.netCalculateVal',function(){


		if($(this).val().charAt(0) == '.')
		{
			var str = $(this).val();
			str = '0' + str;
			$(this).val(str);
		}

		var creditVal = jQuery('.internetVol').val();
		var sellingPrice = jQuery('.internetSellingPrice').val();
		
		if(creditVal != '' &&  sellingPrice != '')
		{
			var subtractVal = parseFloat(creditVal)-parseFloat(sellingPrice);
			var promotionType = parseFloat(subtractVal)/parseFloat(sellingPrice);
			var total = parseFloat(promotionType).toFixed(2);
			var totalPromtion = total*parseFloat(100);
			jQuery('.netPromotionType').val(totalPromtion);
		}
		else
		{
			jQuery('.netPromotionType').val('');
			jQuery('.networkSellingPriceError').html('');
			jQuery('.networkSellingPriceError').css('display','none');
		}
	});

/*===================Validate credit Offer========================*/

	jQuery(document).on('click','.validate-offer',function(){
		var isvalid = $("#publish-credit-offer").valid();
		if (isvalid) 
		{
			var creditVal = jQuery('.creditVal').val();
			var sellingPrice = jQuery('.sellingPrice').val();
			var nintyPer = parseInt(90)*parseFloat(creditVal);
			var totalPer = parseFloat(nintyPer)/100;
			if(sellingPrice >= totalPer)
			{
				jQuery('.sellingPriceError').css('display','block');
				jQuery('.sellingPriceError').html('Selling price less than or equal to 90% of credit value');
				toastr.error('Offer not valid', 'Error Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			}
			else
			{
				jQuery('.sellingPriceError').html('');
				toastr.success('Offer Valid publish your offer', 'Success Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
				jQuery('.publish-credit-offer').attr('disabled',false);
			}
			
			
		}
		else
		{
			toastr.error('Validate your offer', 'Error Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			jQuery('.publish-credit-offer').attr('disabled',true);
		}
	});

/*===================Validate internet Offer========================*/

	jQuery(document).on('click','.internet-validate-offer',function(){
		
		var isvalid = $("#internet-offer").valid();
		 
		if (isvalid) 
		{
			var creditVal = jQuery('.internetVol').val();
			var sellingPrice = jQuery('.internetSellingPrice').val();
			var nintyPer = parseInt(90)*parseFloat(creditVal);
			var totalPer = parseFloat(nintyPer)/100;
			if(sellingPrice >= totalPer)
			{
				jQuery('.networkSellingPriceError').css('display','block');
				jQuery('.networkSellingPriceError').html('Selling price less than or equal to 90% of credit value');
				toastr.error('Offer not valid', 'Error Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			}
			else
			{
				jQuery('.networkSellingPriceError').html('');
				toastr.success('Offer Valid publish your internet offer', 'Success Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
				jQuery('.internet-publish-offer').attr('disabled',false);
			}
		}
		else
		{
			toastr.error('Validate your offer', 'Error Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			jQuery('.internet-publish-offer').attr('disabled',true);
		}
	});

/*================Publish internet Offer===============================*/

	jQuery(document).on('click','.internet-publish-offer',function(){
		var isvalid = $("#internet-offer").valid();
		if (isvalid) 
		{
			var form = $('#internet-offer');
			$.ajax({
				type: form.attr('method'),
				url: form.attr('action'),
				data: form.serialize(),
				beforeSend: function() {
					$('.overlay').css('display','block');
				},
				dataType:"json",
				success: function(response) {
					if(response.success == true)
					{
						toastr.success(response.success_message, 'Success Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						$('.disabledButton').prop('disabled', true);
						
							setTimeout(
								function() 
								{
									if(response.type == "backend")
									{
										window.location = site_url+'admin/offers';
									}
									else
									{
										window.location = site_url+'dashboard';
									}
									
										
								}, 2000);
					}
					if(response.success == false)
					{
						toastr.error(response.error_message, 'Error!', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						$('.disabledButton').prop('disabled', false);
					}
				}
			});
			jQuery('.internet-publish-offer').attr('disabled',false);
		}
		else
		{
			toastr.error('Validate your offer', 'Error Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			jQuery('.internet-publish-offer').attr('disabled',true);
		}
	});

/*================Publish credit Offer===============================*/

	jQuery(document).on('click','.publish-credit-offer',function(){
		var isvalid = $("#publish-credit-offer").valid();
		if (isvalid) 
		{
			var form = $('#publish-credit-offer');
			$.ajax({
				type: form.attr('method'),
				url: form.attr('action'),
				data: form.serialize(),
				beforeSend: function() {
					$('.overlay').css('display','block');
				},
				dataType:"json",
				success: function(response) {
					if(response.success == true)
					{
						toastr.success(response.success_message, 'Success Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						$('.disabledButton').prop('disabled', true);
						
							setTimeout(
								function() 
								{
									
									if(response.type == "backend")
									{
										window.location = site_url+'admin/offers';
									}
									else
									{
										window.location = site_url+'dashboard';
									}
								}, 2000);
					}
					if(response.success == false)
					{
						toastr.error(response.error_message, 'Error!', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						$('.disabledButton').prop('disabled', false);
					}
				}
			});
			jQuery('.publish-credit-offer').attr('disabled',false);
		}
		else
		{
			toastr.error('Validate your offer', 'Error Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			jQuery('.publish-credit-offer').attr('disabled',true);
		}
	});

		
	jQuery(document).on('click','.offer-history',function(){
		$('#offerHistory').modal('show');
		var id = $(this).attr('data-id');
		jQuery.ajax({			
			type: "POST",			
			url: site_url + 'offer-history',			
			data:{id:id},			
			dataType: "html",			
			success: function (data)			
			{				
				$('#offerHistory').html(data);			
			}		
		});	
	});
	jQuery(document).off('click','.purchaseOffers').on('click','.purchaseOffers',function(){
		var offerId = $('.offerId').val();
		var sellerCm = $('.sellerCm').val();
		var buyerCm = $('.buyerCm').val();
		var sellerCredit = $('.sellerCredit').val();
		var sellerRefId = $('.sellerRefId').val();
		var buyerDebit = $('.buyerDebit').val();
		jQuery.ajax({			
			type: "POST",			
			url: site_url + 'purchase-offer',			
			data:{offerId:offerId,sellerCm:sellerCm,buyerCm:buyerCm,sellerCredit:sellerCredit,sellerRefId:sellerRefId,buyerDebit:buyerDebit},			
			dataType: "json",			
			success: function (data)			
			{				
				if(data == true)	
				{
					$('#offerHistory').modal('hide');
					$('#offerHistory').css('display','none');
					$('#thankupopup').modal({backdrop: 'static', keyboard: false})
					$('#headingTitle').html('Thank you !');
				}
			}		
		});	
	});
	jQuery(document).on('click','.confirmCredit',function(){
		$('#thankupopup').modal({backdrop: 'static', keyboard: false})
		$('#headingTitle').html('Please confirm previous purchased offer');
	});







