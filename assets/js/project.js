
	var max_fields      = 5;
	var wrapper         = $(".input_fields_wrap");
	$(document).off('click','.addContactBtn').on('click','.addContactBtn',function(e){
		e.preventDefault();
		var x = 1;

		if(x < max_fields){
			x++;
			$(wrapper).append('<div class="form-hr clearfix"><div class="row"><div class="col-md-4"><div class="form-group clearfix"><label for="exampleInputPassword1">Name</label><input type="text" class="form-control alphabets" placeholder="Name" name="contact_name[]" value=""></div></div><div class="col-md-4"><div class="form-group clearfix"><label for="exampleInputPassword1">Designation</label><input type="text" id="firstname" class="form-control" placeholder="Designation" name="contact_designation[]" value=""></div></div><div class="col-md-4"><div class="form-group clearfix"><label for="exampleInputPassword1">Phone</label><input type="text" id="firstname" class="form-control" placeholder="Phone" name="contact_phone[]" value=""></div></div></div><button type="button" class="btn btn-primary remove_fields">Remove</button></div>');
			
		}
	});
	$(wrapper).on("click",".remove_fields", function(e)
	{
		e.preventDefault();
		$(this).parent('div').remove();
	});
	
	var max_fields1      = 5;
	var wrapper1         = $(".input_fields_wrap1");
	$(document).off('click','.addContractorBtn').on('click','.addContractorBtn',function(e){
		e.preventDefault();
		var x1 = 1;

		if(x1 < max_fields){
			x1++;
			$(wrapper1).append('<div class="form-hrs clearfix"><div class="row"><div class="col-md-4"><div class="form-group clearfix"><label for="exampleInputPassword1">Contractor</label><input type="text" id="firstname" class="form-control alphabets" placeholder="Contractor" name="contractor_name[]" value=""></div></div><div class="col-md-6"><div class="form-group clearfix"><label for="exampleInputPassword1">Scope of Work</label><textarea id="lastname" class="form-control" placeholder="Scope of Work" name="scope_of_work[]"></textarea></div></div></div><button type="button" class="btn btn-primary remove_fields1">Remove</button></div>');
			
		}
	});
	$(wrapper1).on("click",".remove_fields1", function(e)
	{
		e.preventDefault();
		$(this).parent('div').remove();
	});
	$('body').on('click','.assignAssetBtn',function(){
		$('#assignAsset').modal('show');
	});
	$(document).on('click','.assign-asset-project',function(){
		var form = $('#assign-asset-project');
			$.ajax({
				type: form.attr('method'),
				url: form.attr('action'),
				data: form.serialize(),
				beforeSend: function() {
					$('.overlay').css('display','block');
				},
				dataType:"json",
				success: function(response) {
					if(response.success == true)
					{
						getProjectAssetDetail(response.projectRefId);
					}
				}
			});
	});
	$(document).on('click','.getProjectCoordinator',function(){
		var projectRefId = $(this).val();
		if($(this).val() != '')
		{
			jQuery.ajax({			
				type: "POST",			
				url: site_url + 'admin/get-project-coordinator',			
				data:{projectRefId:projectRefId},			
				dataType: "json",			
				success: function (data)			
				{	
					$('#assignedTo').val(data.first_name+' '+data.last_name);
					$('#assignedRefId').val(data.userRefId);
				}		
			});	
		}
		else
		{
			$('#assignedTo').val('');
			$('#assignedRefId').val('');
		}
		
	});
	function getProjectAssetDetail(projectRefId)
	{
		jQuery.ajax({			
			type: "POST",			
			url: site_url + 'admin/project-asset-detail',			
			data:{projectRefId:projectRefId},			
			dataType: "html",			
			success: function (data)			
			{	
				if(data != '')
				{
					$('#assignAsset').modal('hide');
					$('.allResult').html('');
					$('.projectAssetDetail').css('display','block');
					$('.allResult').append(data);
					$('html, body').animate({ scrollTop: 0 }, 1200);
				}					
				
			}		
		});	
		
	}
	$(document).on('click','.assign-component-project',function(){
		var form = $('#assign-component');
			$.ajax({
				type: form.attr('method'),
				url: form.attr('action'),
				data: form.serialize(),
				beforeSend: function() {
					$('.overlay').css('display','block');
				},
				dataType:"json",
				success: function(response) {
					if(response.success == true)
					{
						getProjectComponentDetail(response.projectRefId);
					}
				}
			});
	});
	function getProjectComponentDetail(projectRefId)
	{
		jQuery.ajax({			
			type: "POST",			
			url: site_url + 'admin/project-component-detail',			
			data:{projectRefId:projectRefId,assetRefId:assetRefId},			
			dataType: "html",			
			success: function (data)			
			{	
				if(data != '')
				{
					$('#componentModal').modal('hide');
					$('.allResults').html('');
					$('.projectComponentDetail').css('display','block');
					$('.allResults').append(data);
					$('html, body').animate({ scrollTop: 0 }, 1200);
				}					
				
			}		
		});	
		
	}

	$(document).on('change','.selectProject',function(){
		var projectRefId = $(this).val();
		jQuery.ajax({			
			type: "POST",			
			url: site_url + 'admin/get-project-asset-detail',			
			data:{projectRefId:projectRefId},			
			dataType: "html",			
			success: function (data)			
			{	
				if(data != '')
				{
					$('.selectProjectAsset').html(data);
					$('.chosen1').trigger("chosen:updated");
					$('html, body').animate({ scrollTop: 0 }, 1200);
				}					
				
			}		
		});	
	});

	$(document).on('change','.selectProjectAsset',function(){
		var assetRefId = $('.selectProjectAsset').val();
		var projectRefId = $( ".selectProject option:selected" ).val();
		jQuery.ajax({			
			type: "POST",			
			url: site_url + 'admin/get-asset-component-detail',			
			data:{projectRefId:projectRefId,assetRefId:assetRefId},			
			dataType: "html",			
			success: function (data)			
			{	
				if(data != '')
				{
					$('.assetComponent').html(data);
					$('.chosen2').trigger("chosen:updated");
					$('html, body').animate({ scrollTop: 0 }, 1200);
				}				
				
			}		
		});	
	});
	$(document).on('click','.assignProjectBtn',function(){
		$('#assignProject').modal('show');
	});
	$(document).on('click','.assign-project-btn',function(){
		var form = $('#assign-project');
			$.ajax({
				type: form.attr('method'),
				url: form.attr('action'),
				data: form.serialize(),
				beforeSend: function() {
					$('.overlay').css('display','block');
				},
				dataType:"json",
				success: function(response) {
					if(response.success == true)
					{
						$('#assignProject').modal('hide');
					}
				}
			});
	});
	$(document).on('click','.view-asset-history',function(){
		$('#asset-history').modal('show');
		var assetRefId = $(this).attr('data-target');
		$.ajax({
			type: 'POST',
			url: site_url+'/get-project-assets',
			data: {assetRefId:assetRefId},
			beforeSend: function() {
				$('.overlay').css('display','block');
			},
			dataType:"html",
			success: function(response) {
				$('.assetHistory').html(response);
				
			}
		});
	});
	$(document).on('click','#relatedjob',function(){
		$('#create-asset-complaint')[0].reset();
		$("#create-asset-complaint").validate().resetForm();
	});
	$(document).on('click','#relatedasset',function(){
		$('#create-job-complaint')[0].reset();
		$("#create-job-complaint").validate().resetForm();
	});
	$(document).on('click','.getProjectTask',function(){
		var projectRefId = $(this).val();
		var option = $('option:selected', this).attr('data-attr');
		$('.complaintAssignedTo').val(option);
		$.ajax({
			type: 'POST',
			url: site_url+'/get-project-task',
			data: {projectRefId:projectRefId},
			beforeSend: function() {
				$('.overlay').css('display','block');
			},
			dataType:"html",
			success: function(response) {
				$('.projectTasks').html(response);
				
			}
		});
	});
	$(document).on('click','.getProjectsAssets',function(){
		var projectRefId = $(this).val();
		var option = $('option:selected', this).attr('data-attr');
		$('.complaintAssignedTo').val(option);
		$.ajax({
			type: 'POST',
			url: site_url+'/get-projects-assets',
			data: {projectRefId:projectRefId},
			beforeSend: function() {
				$('.overlay').css('display','block');
			},
			dataType:"html",
			success: function(response) {
				$('.projectAssets').html(response);
				
			}
		});
	});
	$(document).on('click','.completeComplaint',function(){
		$('#completed').modal('show');
	});
	$(document).on('click','.solvedComplaint',function(){
		$('#solved').modal('show');
	});
	$(document).on('click','.notCompleteModal',function(){
		$('#not-complete').modal('show');

	});
	$(document).on('click','.yesNotCompleteModal',function(){
		var complaintnumber = $('.complaint-number').val();
			jQuery.ajax({			
			type: "POST",			
			url: site_url + 'not-complete-complaint',			
			data:{complaintnumber:complaintnumber},			
			dataType: "json",			
			success: function (response)			
			{
				if(response.success == true)
				{
					toastr.success(response.success_message, 'Success Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					
					
						setTimeout(
						  	function() 
						  	{
						  		if(response.url)
						  		{
									window.location = response.url;
						  		}
						  		
									
						  	}, 2000);
					
				}
				if(response.success == false)
				{
					toastr.error(response.error_message, 'Error!', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					
					
				}
				}
			});

	});	
	$(document).on('click','.workerAssigned',function(){
		$('.task-page').css('display','block');
	});
	$(document).on('click','.styled-checkbox',function(){
		var siteurl = site_url;
		var workersRefId = $('#workersRefId').val();
		var workerAssigned = $('.workerAssigned').val();
		var userRefId = $(this).attr('data-target');
		if($('.checked'+userRefId).not(':checked').length)
		{
			$('#remove'+userRefId).remove();
			$('.allResult').html('<tr><td colspan="3">No task found...</td></tr>');
		}
		else
		{
			$('.checked'+userRefId).val('checked');
			$img = $('#img'+userRefId).html();
			var workername = $('#worker-name'+userRefId).html();
			
			$html = '<div class="usr-slct-slctn" id="remove'+userRefId+'"><figure>'+$img+'</figure> <a href="#" title="" class="close_icn"><img src="'+site_url+'assets/frontend/images/close_icn.png" alt="close_icn" id="closeimg'+userRefId+'" data-target="'+userRefId+'"></a></div>';
			
			$('.user-img').append($html);
			
			if($('#workersRefId').val() == '')
			{
				$('#workersRefId').val(userRefId);
				$('.workerAssigned').val(workername);
				
			}
			else
			{
				$('#workersRefId').val(workersRefId+','+userRefId);
				$('.workerAssigned').val(workerAssigned+','+workername);
			}
			getWorkersTaskDetail(userRefId);
			
		
		}
		
	});
	
	function getWorkersTaskDetail(userRefId)
	{
		$.ajax({
			type: 'POST',
			url: site_url+'/get-workers-task-detail',
			data: {userRefId:userRefId},
			beforeSend: function() {
				$('.overlay').css('display','block');
			},
			dataType:"html",
			success: function(response) {
				$('.allResult').html(response);
				
			}
		});
		
	}
	
	$(document).on('click','.signInvoice',function(){
		var invoiceno = $(this).attr('data-invoice');
		var assignedBy = $(this).attr('data-assign');
		$('.forwardedto').val(assignedBy);
		$('.invoiceno').val(invoiceno);
		$('#signedInvoice').modal('show');
	});
	$(document).on('click','.unsigninvoice',function(){
		var invoiceno = $(this).attr('data-invoice');
		var assignedBy = $(this).attr('data-assign');
		$('.forwardedto').val(assignedBy);
		$('.invoiceno').val(invoiceno);
		$('#unsignedInvoice').modal('show');
	});
	$(document).on('click','.collectionAssign',function(){
		$('#assignCollection').modal('show');
	});
	$(document).on('click','.recievedCollection',function(){
		var collectionno = $(this).attr('data-rel');
		$('.collectionno').val(collectionno);
		$('#rcvd').modal('show');
	});
	$(document).on('click','.postponeCollection',function(){
		var collectionno = $(this).attr('data-rel');
		$('.collectionno').val(collectionno);
		$('#postpone').modal('show');
	});
	$(document).on('click','.componentModal',function(){
	    var componentRefId = $(this).attr('data-target');
	    $('#components').modal('show');
	    $.ajax({
			type: 'POST',
			url: site_url+'/get-component-detail',
			data: {componentRefId:componentRefId},
			beforeSend: function() {
				$('.overlay').css('display','block');
			},
			dataType:"json",
			success: function(response) {
			    $('.component-name').html(response.component_name);
			    $('.part-no').html(response.part_no);
			    $('.serial-no').html(response.serial_no);
			    $('.description').html(response.description);
			    $('.componentImg').html('<img src="'+site_url+'/assets/upload/component/'+response.component_image+'"></img>');
				
			}
		});
	});
	$(document).on('click','.componentHistory',function(){
	    var componentRefId = $(this).attr('data-target');
	    $('#c-history').modal('show');
	    $.ajax({
			type: 'POST',
			url: site_url+'/get-component-history',
			data: {componentRefId:componentRefId},
			beforeSend: function() {
				$('.overlay').css('display','block');
			},
			dataType:"html",
			success: function(response) {
			   
				$('.component-history').html(response);
			}
		});
	});
	$(document).on('click','.viewtask',function(){
	    var projectRefId = $(this).attr('data-target');
	    $('#t-list').modal('show');
	    $.ajax({
			type: 'POST',
			url: site_url+'/get-project-task',
			data: {projectRefId:projectRefId},
			beforeSend: function() {
				$('.overlay').css('display','block');
			},
			dataType:"html",
			success: function(response) {
			   
				$('.project-task').html(response);
			}
		});
	});
	$(document).on('click','.completeTask',function(){
		$('#completedTask').modal('show');
	});
	
			
