jQuery(document).ready(function(){

/*===================common submit form function====================*/

	function submitHandler (form) 
	{
		$.ajax({
			url: form.action,
			type: form.method,
			data: new FormData(form),
			contentType: false,       
			cache: false,             
			processData:false, 
			dataType: "json",
			beforeSend: function() {
				$('.overlay').css('display','block');
			},
			success: function(response) {
               console.log(response.success);

				if(response.success == true)
				{
					toastr.success(response.success_message, 'Success Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					//$('.disabledButton').prop('disabled', true);
					
						setTimeout(
						  	function() 
						  	{
						  		if(response.userRefId)
						  		{
						  			jQuery('#userRefId').val(response.userRefId);
									$("#otpModal").modal({ backdrop: 'static',keyboard: false});
						  			jQuery('#login').modal('hide');
						  		}
						  		else if(response.otp)
						  		{
						  			jQuery('#otpModal').modal('hide');
						  			$("#login").modal({ backdrop: 'static',keyboard: false});
						  			jQuery('#pills-login-tab').attr('aria-selected',true);
									jQuery('#pills-register-tab').attr('aria-selected',false);
									jQuery('#pills-login-tab').addClass('active');
									jQuery('#pills-register-tab').removeClass('active');
									jQuery('#pills-login').addClass('active show');
									jQuery('#pills-register').removeClass('active show');
						  		}
						  		else if(response.url)
						  		{
									window.location = response.url;
						  		}
						  		
									
						  	}, 2000);
					
				}
				if(response.success == false)
				{
					toastr.error(response.error_message, 'Error!', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					$('.disabledButton').prop('disabled', false);
					
				}
			}
		});
	}

/*======================register user form===================*/

	
	jQuery("#add-user").validate({
		rules:
		{
			gender: 
			{
				required: true,
			},
			first_name: 
			{
				required: true,
			},
			last_name: 
			{
				required: true,
			},
			phone_number: 
			{
				required: true,
			},
			email: 
			{
				required: true,
			},
			password: 
			{
				required: true,
			},
			confirm_password:
			{
				required:true,
				equalTo: '#password'
			},
			role:
			{
				required:true,
			},
			address:
			{
				required:true,
			},
			dob:
			{
				required:true,
			},
			description:
			{
				required:true,
			},
			image:
			{
				required:true,
			},
		},

		messages: 
		{

		}
		,
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#addProject").validate({
		rules:
		{
			project_name: 
			{
				required: true,
			},
			demographic_information: 
			{
				required: true,
			},
			project_information: 
			{
				required: true,
			},
			project_requirements: 
			{
				required: true,
			},
			specification: 
			{
				required: true,
			},
			project_location: 
			{
				required: true,
			},
			gps_location:
			{
				required:true,
			},
			scheduled_visit:
			{
				required:true,
			},
			project_plan:
			{
				required:true,
			},
			project_plan_image:
			{
				required:true,
			}
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#addAsset").validate({
		rules:
		{
			asset: 
			{
				required: true,
			},
			asset_no: 
			{
				required: true,
			},
			serial_no: 
			{
				required: true,
			},
			asset_description: 
			{
				required: true,
			}
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});	
	
	jQuery("#notification").validate({
		rules:
		{
			message: 
			{
				required: true,
			},
			attachment: 
			{
				required: true,
			},
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#add-expenses").validate({
		rules:
		{
			expense_type: 
			{
				required: true,
			},
			expense_name: 
			{
				required: true,
			},
			attachment: 
			{
				required: true,
			},
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	
	
	
	jQuery("#add-invoice").validate({		rules:		{			projectRefId: 			{				required: true,			},			assignedName: 			{				required: true,			},			document: 			{				required: true,			},		},		messages: 		{		},		submitHandler: function(form) 		{			submitHandler(form);		}	});		jQuery("#addInvoiceSign").validate({		rules:		{			message:			{				required : true,			},		},		messages: 		{		},		submitHandler: function(form) 		{			submitHandler(form);		}	});	jQuery("#addCollection").validate({		rules:		{			collection_recieved_image:			{				required : true,			},		},		messages: 		{		},		submitHandler: function(form) 		{			submitHandler(form);		}	});		jQuery("#addCollection1").validate({		rules:		{			collection_postpone_message:			{				required : true,			},			collection_date:			{				required : true,			},		},		messages: 		{		},		submitHandler: function(form) 		{			submitHandler(form);		}	});		jQuery("#addSignInvoice").validate({		rules:		{			image: 			{				required: true,			},					},		messages: 		{		},		submitHandler: function(form) 		{			submitHandler(form);		}	});
	jQuery("#addComponent").validate({
		rules:
		{
			component_name: 
			{
				required: true,
			},
			part_no: 
			{
				required: true,
			},
			serial_no: 
			{
				required: true,
			},
			description: 
			{
				required: true,
			},
			component_image: 
			{
				required: true,
			}
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});

	jQuery("#assignTask").validate({
		rules:
		{
			task_date: 
			{
				required: true,
			},
			task_subject: 
			{
				required: true,
			},
			projectRefId: 
			{
				required: true,
			},
			assetRefId: 
			{
				required: true,
			},
			componentRefId: 
			{
				required: true,
			},
			description: 
			{
				required: true,
			},
			worker_assigned: 
			{
				required: true,
			}
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});

	jQuery("#create-job-complaint").validate({
		rules:
		{
			projectRefId: 
			{
				required: true,
			},
			taskRefId: 
			{
				required: true,
			},
			message: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#create-asset-complaint").validate({
		rules:
		{
			projectRefId: 
			{
				required: true,
			},
			assetRefId: 
			{
				required: true,
			},
			message: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#add-safety-content").validate({
		rules:
		{
			safety_document: 
			{
				required: true,
			},
			content: 
			{
				required: true,
			},
			
			message: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#complete-complaint").validate({
		rules:
		{
			document: 
			{
				required: true,
			},
			message: 
			{
				required: true,
			},
			
			message: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#add-comment").validate({
		rules:
		{
			comment: 
			{
				required: true,
			},
			message: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		},
		submitHandler: function(form)
		{
			
			$.ajax({
				url: form.action,
				type: form.method,
				data: new FormData(form),
				contentType: false,       
				cache: false,             
				processData:false, 
				dataType: "json",
				beforeSend: function() {
					$('.overlay').css('display','block');
				},
				success: function(response) {
				   $('#add-comment')[0].reset();

					if(response.success == true)
					{
						var comment = '<div class="cmnt-block btm-0"><h3>'+response.username.first_name+' '+response.username.last_name+'</h3><p>'+response.comment+'</p><figure><img src="'+site_url+'assets/upload/comment/'+response.attachment+'" height="100" width="100"></figure></div>';
						$('#commentDiv').append(comment);
					}
					
				}
			});
		}
	});
	jQuery("#complete-solved-complaint").validate({
		rules:
		{
			document: 
			{
				required: true,
			},
			message: 
			{
				required: true,
			},
			
			message: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});

	jQuery("#addProduct").validate({
		rules:
		{
			projectRefId: 
			{
				required: true,
			},
			capacity: 
			{
				required: true,
			},
			max_load: 
			{
				required: true,
			},
			max_reach: 
			{
				required: true,
			},
			tip_load: 
			{
				required: true,
			},
			description: 
			{
				required: true,
			},
			image: 
			{
				required: true,
			}
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});

	jQuery("#add-contact-us").validate({
		rules:
		{
			name: 
			{
				required: true,
			},
			email: 
			{
				required: true,
			},
			phone: 
			{
				required: true,
			},
			message: 
			{
				required: true,
			},
			
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	setTimeout(function(){ 
		$('.newsBtn').attr('disabled',false);
		jQuery("#add-news").validate({
		rules:
		{
			image: 
			{
				required: true,
			},
			news_title: 
			{
				required: true,
			},
			description: 
			{
				required: true,
			},
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});


	 }, 1000);
	

/*======================login user form===================*/

	jQuery("#user-login").validate({
		rules:
		{
			username: 
			{
				required: true,
			},
			password: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	
	jQuery("#admin-login").validate({
		rules:
		{
			tel_number: 
			{
				required: true,
				min:10,
			},
			password: 
			{
				required: true,
				min:4,
			}
		},

		messages: 
		{
		
			
		},
		submitHandler: function(form) 
		{
			$.ajax({
			url: form.action,
			type: form.method,
			data: new FormData(form),
			contentType: false,       
			cache: false,             
			processData:false, 
			dataType: "json",
			beforeSend: function() {
				$('.overlay').css('display','block');
			},
			success: function(response) {
               

				if(response.success == true)
				{
					toastr.success(response.success_message, 'Success Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					
						setTimeout(
						  	function() 
						  	{
						  		if(response.url)
						  		{
						  			window.location = response.url;
						  		}
						  		
									
						  	}, 2000);
					
				}
				if(response.success == false)
				{
					toastr.error(response.error_message, 'Error!', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					$('.disabledButton').prop('disabled', false);
					
				}
			}
		});

		}
	});

	jQuery("#check-otp").validate({
		rules:
		{
			otp: 
			{
				required: true,
			},
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});

	jQuery("#publish-credit-offer").validate({
		rules:
		{
			credits: 
			{
				required: true,
			},
			expiry_date: 
			{
				required: true,
			},
			promotion_type: 
			{
				required: true,
			},
			selling_price: 
			{
				required: true,
			},
			credit_network:
			{
				required: true,
			}
		},

		messages: 
		{
			
		}
	});
	
	jQuery("#internet-offer").validate({
		rules:
		{
			internet_volume: 
			{
				required: true,
			},
			network_expiry_date: 
			{
				required: true,
			},
			network_selling_price: 
			{
				required: true,
			},
			network_promotion_type: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		}
	});
	jQuery("#add-credit-network").validate({
		rules:
		{
			network: 
			{
				required: true,
			},
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#add-network-operator").validate({
		rules:
		{
			operator: 
			{
				required: true,
			},
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#add-credit-commission").validate({
		rules:
		{
			commission: 
			{
				required: true,
			},
			credit_percent: 
			{
				required: true,
			},
			selling_percent: 
			{
				required: true,
			},
			commisioin_paid_seller: 
			{
				required: true,
			},
			commisioin_paid_buyer: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#withdrawMoneyRequest").validate({
		rules:
		{
			withdraw_amount: 
			{
				required: true,
			},
		},

		messages: 
		{
			
		}
	});
	
});
