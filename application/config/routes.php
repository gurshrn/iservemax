<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Admin_controller/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin/add-user'] = 'Admin_controller/addUser';
$route['admin/invoice'] = 'Admin_controller/invoice';
$route['admin/add-invoice'] = 'Admin_controller/addInvoice';
$route['admin/get-project-coordinator'] = 'Admin_controller/getProjectCoordinator';
$route['admin/safety-content'] = 'Admin_controller/safetyContent';
$route['admin/admin-edit-user/(:any)'] = 'Admin_controller/editUser/$1';
$route['admin/project'] = 'Admin_controller/projectList';
$route['admin/add-project'] = 'Admin_controller/addProject';
$route['admin/asset'] = 'Admin_controller/assetList';
$route['admin/add-asset'] = 'Admin_controller/addAsset';
$route['admin/assign-asset'] = 'Admin_controller/assignAsset';
$route['admin/assign-component'] = 'Admin_controller/assignComponent';
$route['admin/project-asset-detail'] = 'Admin_controller/getProjectAssetDetail';
$route['admin/project-component-detail'] = 'Admin_controller/getProjectComponentDetail';
$route['admin/component'] = 'Admin_controller/getComponentDetail';
$route['admin/add-component'] = 'Admin_controller/addComponent';
$route['admin/task'] = 'Admin_controller/task';
$route['admin/create-task'] = 'Admin_controller/createTask';
$route['admin/assign-project'] = 'Admin_controller/assignProject';
$route['admin/get-project-asset-detail'] = 'Admin_controller/getProjectAssetDetails';
$route['admin/get-asset-component-detail'] = 'Admin_controller/getAssetComponentDetails';
$route['admin/product'] = 'Admin_controller/productList';
$route['admin/add-product'] = 'Admin_controller/addProduct';
$route['admin/news'] = 'Admin_controller/news';
$route['admin/add-news'] = 'Admin_controller/addNews';
$route['admin/edit-component/(:any)'] = 'Admin_controller/editComponent/$1';
$route['admin/edit-asset/(:any)'] = 'Admin_controller/editAsset/$1';
$route['admin/admin-view-project/(:any)'] = 'Admin_controller/viewProject/$1';
$route['admin/admin-edit-project/(:any)'] = 'Admin_controller/editProject/$1';
$route['admin/admin-edit-product/(:any)'] = 'Admin_controller/editProduct/$1';
$route['admin/edit-news/(:any)'] = 'Admin_controller/editNews/$1';
$route['admin/view-news/(:any)'] = 'Admin_controller/viewNews/$1';


$route['login'] = 'User_controller/index';
$route['get-project-task'] = 'Project_controller/getProjectTasks';
$route['user-logout'] = 'User_controller/logout';
$route['dashboard'] = 'Dashboard_controller/index';
$route['project'] = 'Project_controller/index';
$route['asset'] = 'Project_controller/assetDetail';
$route['task'] = 'Project_controller/taskDetail';
$route['create-task'] = 'Project_controller/createTask';
$route['pending-task'] = 'Project_controller/pendingTask';
$route['completed-task'] = 'Project_controller/completedTask';
$route['get-project-assets'] = 'Project_controller/getProjectAssets';
$route['worker'] = 'Project_controller/workerList';
$route['complaint'] = 'Project_controller/complaintList';
$route['safety-content'] = 'Project_controller/safetyContentList';
$route['technical-document'] = 'Project_controller/technicalDocumentList';
$route['complete-complaint'] = 'Project_controller/completeComplaint';
$route['not-complete-complaint'] = 'Project_controller/notCompleteComplaint';
$route['get-projects-assets'] = 'Project_controller/getProjectAssetsDetail';
$route['worker-detail/(:any)'] = 'Project_controller/getWorkerDetail/$1';
$route['complaint-detail/(:any)'] = 'Project_controller/getComplaintDetail/$1';
$route['create-complaint'] = 'Project_controller/createComplaint';
$route['product'] = 'Project_controller/product';
$route['profile'] = 'Project_controller/profile';
$route['contact-us'] = 'Project_controller/contactUs';
$route['collection'] = 'Project_controller/collection';
$route['news'] = 'Project_controller/news';
$route['invoice'] = 'Project_controller/invoice';
$route['add-invoice-sign'] = 'Project_controller/addInvoiceSign';
$route['add-collection'] = 'Project_controller/addCollection';
$route['notification'] = 'Project_controller/notification';
$route['add-comment'] = 'Project_controller/addComment';
$route['get-component-detail'] = 'Project_controller/getComponentDetail';
$route['get-component-history'] = 'Project_controller/getComponentHistory';
$route['get-project-task'] = 'Project_controller/getProjectTask';
$route['get-workers-task-detail'] = 'Project_controller/workerTaskDetail';
$route['checkin-checkout'] = 'Project_controller/checkinCheckout';
$route['add-notification'] = 'Project_controller/addNotification';
$route['add-expense'] = 'Project_controller/addExpense';
$route['news-detail/(:any)'] = 'Project_controller/newsDetail/$1';
$route['completed-task-detail/(:any)'] = 'Project_controller/completedTaskDetail/$1';


$route['product-detail/(:any)'] = 'Project_controller/productDeatilById/$1';
$route['pending-task-detail/(:any)'] = 'Project_controller/pendingtaskDetail/$1';
$route['asset-detail/(:any)'] = 'Project_controller/assetDetailById/$1';
$route['project-detail/(:any)'] = 'Project_controller/getProjectDetail/$1';
$route['edit-profile/(:any)'] = 'Project_controller/editUserDetail/$1';

/*==========Sale's person Api==============*/

$route['api/login'] = 'Api/User/login';
$route['api/getProjectList'] = 'Api/Webservice/getProjectList';
$route['api/getProjectListByMonth'] = 'Api/Webservice/getProjectListByMonth';
$route['api/addContractor'] = 'Api/Webservice/addContractor';
$route['api/editContractor'] = 'Api/Webservice/addContractor';
$route['api/getContractorList'] = 'Api/Webservice/getContractorList';
$route['api/deleteContractor'] = 'Api/Webservice/deleteContractor';
$route['api/addContactPerson'] = 'Api/Webservice/addContactPerson';
$route['api/editContactPerson'] = 'Api/Webservice/addContactPerson';
$route['api/deleteContactPerson'] = 'Api/Webservice/deleteContactPerson';
$route['api/getContactPersonList'] = 'Api/Webservice/getContactPersonList';
$route['api/addProject'] = 'Api/Webservice/addProject';
$route['api/getVisitList'] = 'Api/Webservice/getVisitList';
$route['api/getVisitDetail'] = 'Api/Webservice/getVisitDetail';
$route['api/addVisitDetail'] = 'Api/Webservice/addVisitDetail';
$route['api/getProjectDetail'] = 'Api/Webservice/getProjectDetailById';
$route['api/editVisitDetail'] = 'Api/Webservice/addVisitDetail';
$route['api/deleteVisitDetail'] = 'Api/Webservice/deleteVisitDetail';
$route['api/addProjectAdditionalInfo'] = 'Api/Webservice/addProjectAdditionalInfo';
$route['api/getProjectAdditionalInfo'] = 'Api/Webservice/getProjectAdditionalInfo';
$route['api/getProjectPhase'] = 'Api/Webservice/getProjectPhase';
$route['api/addAttendance'] = 'Api/Webservice/addAttendance';
$route['api/getTodayAttendance'] = 'Api/Webservice/getTodayAttendance';
$route['api/getAttendanceByDate'] = 'Api/Webservice/getTodayAttendance';
$route['api/projectSubmitToAdmin'] = 'Api/Webservice/projectSubmitToAdmin';
$route['api/deleteAdditionalInfo'] = 'Api/Webservice/deleteAdditionalInfo';
$route['api/getVisitListByUserId'] = 'Api/Webservice/getVisitListByUserId';
$route['api/getProjectListByDate'] = 'Api/Webservice/getProjectListByDate';
$route['api/getVisitProjectList'] = 'Api/Webservice/getVisitProjectList';

/*==============Field coordinator api===============*/

$route['api/createProjectTask'] = 'Api/Webservice/createProjectTask';
$route['api/getAssignedProjectList'] = 'Api/Webservice/getAssignedProjectList';
$route['api/getProjectAssetList'] = 'Api/Webservice/getProjectAssetList';
$route['api/getAssetComponentList'] = 'Api/Webservice/getAssetComponentList';
$route['api/getWorkerList'] = 'Api/Webservice/getWorkerList';
$route['api/getWorkerAttendance'] = 'Api/Webservice/getWorkerAttendance';
$route['api/getWorkerAttendanceByDate'] = 'Api/Webservice/getWorkerAttendance';
$route['api/getPendingTaskList'] = 'Api/Webservice/getPendingTaskList';
$route['api/getPendingTaskProjectList'] = 'Api/Webservice/getPendingTaskProjectList';
$route['api/getTaskDetail'] = 'Api/Webservice/getTaskDetail';
$route['api/getCompletedTaskProjectList'] = 'Api/Webservice/getCompletedTaskProjectList';
$route['api/getCompletedTaskList'] = 'Api/Webservice/getCompletedTaskList';
$route['api/getWorkerTaskDetail'] = 'Api/Webservice/getWorkerTaskDetail';
$route['api/getAssetList'] = 'Api/Webservice/getAssetList';
$route['api/getAssetDetail'] = 'Api/Webservice/getAssetDetail';
$route['api/getProjectAssetHistory'] = 'Api/Webservice/getProjectAssetHistory';
$route['api/getAssetComponents'] = 'Api/Webservice/getAssetComponents';
$route['api/getComponentHistory'] = 'Api/Webservice/getComponentHistory';
$route['api/getSafetyContentDetail'] = 'Api/Webservice/getSafetyContentDetail';
$route['api/getComplaintDetailList'] = 'Api/Webservice/getComplaintDetailList';
$route['api/getComplaintDetail'] = 'Api/Webservice/getComplaintDetail';
$route['api/getInvoiceList'] = 'Api/Webservice/getInvoiceList';
$route['api/getInvoiceDetail'] = 'Api/Webservice/getInvoiceDetail';
$route['api/getTechnicalDocumentProjectList'] = 'Api/Webservice/getTechnicalDocumentProjectList';
$route['api/getTechnicalDocuments'] = 'Api/Webservice/getTechnicalDocuments';
$route['api/addComment'] = 'Api/Webservice/addComment';
$route['api/getCommentList'] = 'Api/Webservice/getCommentList';

/*==================Client's api=================*/

$route['api/getNewsList'] = 'Api/Webservice/getNewsList';
$route['api/getNewsDetail'] = 'Api/Webservice/getNewsDetail';
$route['api/getProductList'] = 'Api/Webservice/getProductList';
$route['api/getProductDetail'] = 'Api/Webservice/getProductDetail';
$route['api/addComplaintDetail'] = 'Api/Webservice/addComplaintDetail';
$route['api/completeTask'] = 'Api/Webservice/completeTask';

