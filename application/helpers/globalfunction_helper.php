<?php

	if (!defined('BASEPATH'))
    exit('No direct script access allowed');

	function getUserDetail($userRefId = null) {
	    $ci = & get_instance();
		$ci->db->select('*,AES_DECRYPT(email,"/*awshp$*/") as email,AES_DECRYPT(tel_number,"/*awshp$*/") as tel_number');
	    $ci->db->from('credit_login_detials');
	    $ci->db->where('userRefId', $userRefId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getPaymentDetail($transactionId,$userrefId) {
	    $ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('credit_payment');
	    $ci->db->where('userRefId', $userrefId);
	    $ci->db->where('transaction_id', $transactionId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	
	
	function generateRef()
	{
		$lengths = 8;
		return $randomStrings = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $lengths);
	}
	function generateRef1()
	{
		$lengths = 4;
		return $randomStrings = substr(str_shuffle("0123456789"), 0, $lengths);
	}
	function getProjectAssetdetail($assetRefId)
	{
		$ci = & get_instance();
	    $ci->db->select('*');
	    $ci->db->from('iserve_project_asset');
	    $ci->db->where('assetrefId',$assetRefId);
	    $ci->db->where('status',0);
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}
	function getAssignedProject($projectRefId)
	{
		$ci = & get_instance();
	    $ci->db->select('userRefId');
	    $ci->db->from('iserve_assign_project');
	    $ci->db->where('projectRefId',$projectRefId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}	
	function getTaskWorkers($taskrefId)
	{
		$ci = & get_instance();
	    $ci->db->select('iserve_user_detail.first_name,iserve_user_detail.last_name');
	    $ci->db->from('iserve_workers_task');
	    $ci->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_workers_task.workerRefId','left');
	    $ci->db->where('iserve_workers_task.taskRefId',$taskrefId);
	    $query = $ci->db->get();
	    $result = $query->result();
		foreach($result as $val)
		{
			$workers[] = ucfirst($val->first_name).' '.$val->last_name;
		}
		$workersname = implode(", ",$workers);
		return $workersname;
	}
	function getAssetName($assetRefId)
	{
		foreach($assetRefId as $val)
		{
			$ci = & get_instance();
			$ci->db->select('asset');
			$ci->db->from('iserve_asset');
			$ci->db->where('assetRefId',$val);
			$query = $ci->db->get();
			$result = $query->result();
			foreach($result as $vals)
			{
				$assetname[] = ucfirst($vals->asset);
			}
		}
		$result = implode(", ",$assetname);
		return $result;
	}
	function getComponentName($componentRefId)
	{
		foreach($componentRefId as $val)
		{
			$ci = & get_instance();
			$ci->db->select('component_name');
			$ci->db->from('iserve_components');
			$ci->db->where('compoRefId',$val);
			$query = $ci->db->get();
			$result = $query->result();
			foreach($result as $vals)
			{
				$componentname[] = ucfirst($vals->component_name);
			}
		}
		$result = implode(", ",$componentname);
		return $result;
	}
	function userDetail($userRefId)
	{
		$ci = & get_instance();
	    $ci->db->select('first_name,last_name');
	    $ci->db->from('iserve_user_detail');
	    $ci->db->where('userRefId',$userRefId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getCommentDetail($refId,$type)
	{
		$ci = & get_instance();
	    $ci->db->select('iserve_comment.*,iserve_user_detail.first_name,last_name');
	    $ci->db->from('iserve_comment');
	    $ci->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_comment.commented_by','left');
	    $ci->db->where('iserve_comment.refId',$refId);
	    $ci->db->where('iserve_comment.type',$type);
		$ci->db->order_by('iserve_comment.id','ASC');
	    $query = $ci->db->get();
		$result = $query->result();
	    return $result;
	}
	function getAssetComponents($assetrefId)
	{
		$ci = & get_instance();
	    $ci->db->select('iserve_project_asset_component.*,iserve_components.component_name,iserve_components.part_no,iserve_components.serial_no,iserve_project_detail.project_name,iserve_project_detail.geo_location as project_location');
	    $ci->db->from('iserve_project_asset_component');
	    $ci->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_project_asset_component.projectRefId','left');
	    $ci->db->join('iserve_components','iserve_components.compoRefId = iserve_project_asset_component.componentRefId','left');
	    $ci->db->where('iserve_project_asset_component.assetRefId',$assetrefId);
		$ci->db->group_by('iserve_project_asset_component.componentRefId');
	    $query = $ci->db->get();
		$result = $query->result();
	    return $result;
	}


?>