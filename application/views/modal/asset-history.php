<table class="tbl-view">
	<thead>
		<tr>
			<th>Project Name</th>
			<th>Duration</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			if(isset($asset) && !empty($asset)){ 
				foreach($asset as $val){
					if($val->status == 0)
					{
						$useby = 'Currently Use';
					}
					else
					{
						$useby = date('d M',strtotime($val->asset_completed_date));
					}
		
		?>
				<tr>
					<td data-column="Project Name"><?php echo ucfirst($val->project_name);?></td>
					<td data-column="Duration"><?php echo date('d M',strtotime($val->addedondate)).' - '.$useby;?></td>
				</tr>
		
		<?php } } ?>
		
	</tbody>
</table>