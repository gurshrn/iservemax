<?php $this->load->view('backend/include/header.php');?>

<?php $this->load->view('backend/include/sidebar.php');?>



<div class="content-wrapper">

	<form id="add-invoice" action="<?php echo site_url('admin/add-invoice');?>" method="POST" enctype="multipart/form-data">

		<section class="content-header productTitle">

			<h1>Invoice</h1>

		</section>

		<section class="content backend-sec">

			<div class="row">

				<div class="col-md-12">

					<div class="box box-primary">

						<div class="box-body">

							<div class="row">

										

								<div class="col-md-6">

									<div class="form-group clearfix">

										<label for="exampleInputPassword1">Select Project</label>

								  

										<select class="form-control getProjectCoordinator" name="projectRefId">
											<option value="">Select Project...</option>
											<?php 
												if(isset($project) && !empty($project)){ 
													foreach($project as $val){
											?>
											
												<option value="<?php echo $val->projectRefId?>"><?php echo ucfirst($val->project_name);?></option>
											
											<?php } } ?>
										</select>

										

									</div>

								</div>

								<div class="col-md-6">

									<div class="form-group clearfix">

										<label for="exampleInputPassword1">Assigned To</label>

								  

										<input type="text" id="assignedTo" class="form-control" placeholder="Project Name" name="assignedName" readonly>
										<input type="hidden" id="assignedRefId" name="assigned_to">

										

										

									</div>

								</div>

								

							</div>

							

							

							<div class="row">

								<div class="col-md-4">

									<div class="form-group clearfix">

										

										<label for="exampleInputPassword1">Document</label>

										<input type="file" class="form-control projectImage" name="document" onchange="readURL(this)";>

										<img id="blah" src="" alt="your image" height="250px" width="300px" style="display:none;">

									

									</div>

								</div>

								

									

							</div>

						</div>

					</div>

				</div>
				<div class="btn-group pull-right row">

				<div class="col-md-3">

					<button class="btn btn-primary " type="submit">Submit</button>

				</div>

			</div>

			</div>

		</section>

	</form>	

</div>

	

<?php $this->load->view('include/footer.php');?>



<script type="text/javascript">

        function readURL(input) {

            if (input.files && input.files[0]) {

				$('#blah').css('display','block');

                var reader = new FileReader();



                reader.onload = function (e) {

                    $('#blah').attr('src', e.target.result);

                }



                reader.readAsDataURL(input.files[0]);

            }

        }

</script>















