<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

	<div class="content-wrapper">
		<section class="content-header productTitle">
			<h1>Add News</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
						<form id="add-news" action="<?php echo site_url('admin/add-news'); ?>" method="POST" autocomplete="off">
								
								<input type="hidden" name="id" value="<?php if(isset($editNews) && !empty($editNews)){ echo $editNews->id;}?>">
								
								
								<div class="row">
									<div class="col-md-6">
										<div class="form-group clearfix">
											
											<label for="exampleInputPassword1">News Image</label>
											
											
											<img id="blah" src="<?php if(isset($editNews) && !empty($editNews)){ echo site_url('assets/upload/news/'.$editNews->image);}?>" alt="your image" height="250px" width="300px" <?php if(isset($editNews) && !empty($editNews)){ echo "style='display:block'"; } else { echo "style='display:none'";}?>>
										
										</div>
									</div>
									<div class="col-md-6">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">News Title</label>
										
										<p><?php if(isset($editNews) && !empty($editNews)){ echo ucfirst($editNews->news_title);}?><p>
									
									</div>
								</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group clearfix">
											
											<label for="exampleInputPassword1">News Description</label>
											
											<p><?php if(isset($editNews) && !empty($editNews)){ echo $editNews->description;}?></p>
										
										</div>
									</div>
								</div>
							
							</form>
					
					</div>
					
	            	
	        	</div>
	    	</div>
		</div>
	</section>
</div>

	
<?php $this->load->view('include/footer.php');?>
<script src="<?php echo site_url()  ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		plugins: "link image"
	 });
	 
	</script>
<script>
jQuery("#datepicker1").datepicker({

            minDate: "+3",

            dateFormat: 'mm/dd/yy'

        });
</script>
<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
				$('.projectImage').attr('name','image');
				$('#blah').css('display','block');
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>








