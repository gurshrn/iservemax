<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				News
			</h1>
			<div class="pull-right">
				<a href="<?php echo site_url('admin/add-news');?>"><button type="button" class="btn btn-primary">Add News</button></a>
			</div>
      
		</section>
		

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>News Title</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    if(isset($newsdetail) && !empty($newsdetail)) {
                      foreach($newsdetail as $val){
                  ?>
						<tr>
							<td><img src="<?php echo site_url('assets/upload/news/'.$val->image);?>" alt="news-image" height="100" width="100"></td>
							<td><?php echo ucfirst($val->news_title);?></td>
							<td>
								<a href="<?php echo site_url('/admin/edit-news/'.$val->id);?>"><i class="fa fa-edit"></i></a>
								<a href="<?php echo site_url('/admin/view-news/'.$val->id);?>"><i class="fa fa-eye"></i></a>
							
							</td>
						</tr>
                        
                  <?php } } else{ ?>
					<tr>
						<td colspan="9">No record found...</td>
					</tr>
				  
				  <?php } ?>
                
                
               
                
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
    

 
  

 
 
 
  <div class="control-sidebar-bg"></div>
</div>



<!-- Modal1 -->
<div class="modal fade" id="user-approved" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle"><span class="statusType"></span> User</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" class="userid">
          <input type="hidden" class="status">
          <input type="hidden" class="statusTypes">
          <h4><span class="user-body">Are you sure you want to <span class="statusType"></span> this user?</span></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary updateUserStatus">Yes</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal2 -->
<div class="modal fade" id="user-approved" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Approve User</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" class="userid">
          <h4><span class="user-body">Are you sure you want to enable this user?</span></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary updateUserStatus">Yes</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>


<?php $this->load->view('backend/include/footer.php');?>

