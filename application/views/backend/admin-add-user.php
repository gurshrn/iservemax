<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>
	<div class="content-wrapper">		<section class="content-header productTitle">			<h1>Add User</h1>		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
						<form id="add-user" action="<?php echo site_url('admin/add-user'); ?>" method="POST" autocomplete="off">
								
								<input type="hidden" name="id" value="<?php if(isset($editUser) && !empty($editUser)){ echo $editUser->id;}?>">
								<input type="hidden" name="userRefId" value="<?php if(isset($editUser) && !empty($editUser)){ echo $editUser->userRefId;}?>">
								
								<div class="row">
									<div class="col-md-4">
										<div class="form-group clearfix">
											<label for="exampleInputPassword1">Gender</label>
									  
											<select class="form-control" name="gender">
												<option value="">Select Gender....</option>
												<option value="Female" <?php if(isset($editUser) && !empty($editUser)){ if($editUser->gender == 'Female'){ echo "selected='selected'";}} ?>>Female</option>
												<option value="Male" <?php if(isset($editUser) && !empty($editUser)){ if($editUser->gender == 'Male'){ echo "selected='selected'";}} ?>>Male</option>
											</select>
									
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group clearfix">
											<label for="exampleInputPassword1">First Name</label>
									  
											<input type="text" id="firstname" class="form-control alphabets" placeholder="First Name" name="first_name" value="<?php if(isset($editUser) && !empty($editUser)){ echo $editUser->first_name;}?>">
											
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group clearfix">
											<label for="exampleInputPassword1">Last Name</label>
									  
											<input type="text" id="lastname" class="form-control alphabets" placeholder="Last Name" name="last_name" value="<?php if(isset($editUser) && !empty($editUser)){ echo $editUser->last_name;}?>">
											
											
										</div>
									</div>

									<div class="col-md-4">
										<div class="form-group clearfix">
											<label for="exampleInputPassword1">D.O.B</label>
									  
											<input type="text" id="datepicker1" class="form-control alphabets" placeholder="D.O.B" name="dob" value="<?php if(isset($editUser) && !empty($editUser)){ echo $editUser->dob;}?>">
											
											
										</div>
									</div>
							
							
								
									<div class="col-md-4">
										<div class="form-group clearfix">
											
											<label for="exampleInputPassword1">Phone Number</label>
											<input type="text" id="phonenumber" class="form-control validNumber" placeholder="Phone Number" name="phone_number" value="<?php if(isset($editUser) && !empty($editUser)){ echo $editUser->phone_number;}?>">
										
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group clearfix">
											
											<label for="exampleInputPassword1">Email</label>
											<input type="text" id="email" class="form-control" placeholder="Email" name="email" value="<?php if(isset($editUser) && !empty($editUser)){ echo $editUser->email;}?>">
										
										</div>
									</div>
									
									<div class="col-md-4">
										<div class="form-group clearfix">
											
											<label for="exampleInputPassword1">Password</label>
											<input type="password" id="password" class="form-control promotionType" placeholder="******" name="password" value="<?php if(isset($editUser) && !empty($editUser)){ echo $editUser->password;}?>">
										
										</div>
									</div>
									
									<div class="col-md-4">
										<div class="form-group clearfix">
											
											<label for="exampleInputPassword1">Confirm Password</label>
											<input type="password" id="confirm-pwd" class="form-control promotionType" placeholder="******" name="confirm_password" value="<?php if(isset($editUser) && !empty($editUser)){ echo $editUser->password;}?>">
										
										</div>
									</div>
									
									
									 
									<div class="col-md-4">
										<div class="form-group clearfix">
										  <label>User Role</label>
											<select class="form-control" name="role">
												<option value="">Select....</option>
												<option value="1" <?php if(isset($editUser) && !empty($editUser)){ if($editUser->role == 1){ echo "selected='selected'";}} ?>>Client</option>
												<option value="2" <?php if(isset($editUser) && !empty($editUser)){ if($editUser->role == 2){echo "selected='selected'";}}?>>Field Workers</option>
												<option value="3" <?php if(isset($editUser) && !empty($editUser)){ if($editUser->role == 3){echo "selected='selected'";}}?>>Field Coordinator</option>
												<option value="4" <?php if(isset($editUser) && !empty($editUser)){ if($editUser->role == 4){echo "selected='selected'";}}?>>Collection Agent</option>
												<option value="5" <?php if(isset($editUser) && !empty($editUser)){ if($editUser->role == 5){echo "selected='selected'";}}?>>Sales person</option>
												<option value="6" <?php if(isset($editUser) && !empty($editUser)){ if($editUser->role == 6){echo "selected='selected'";}}?>>Secretary </option>
												
											</select>
										</div>
									</div>
									
									<div class="col-md-4">
										<div class="form-group clearfix">
											
											<label for="exampleInputPassword1">Address</label>
											<input type="textarea" class="form-control" placeholder="Address" name="address" value="<?php if(isset($editUser) && !empty($editUser)){ echo $editUser->address;}?>" >
										
										</div>
									</div>

									<div class="col-md-4">
										<div class="form-group clearfix">
											
											<label for="exampleInputPassword1">Description</label>
											<textarea class="form-control" placeholder="Description" name="description"><?php if(isset($editUser) && !empty($editUser)){ echo $editUser->description;}?></textarea>
										
										</div>
									</div>
								</div>
								<div class="row">
								
								
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Profile Image</label>
										
										<input type="file" class="form-control projectImage" name="<?php if(isset($editUser) && !empty($editUser)){ echo '';}else{ echo 'image'; }?>" onchange="readURL(this)";>
										<img id="blah" src="<?php if(isset($editUser) && !empty($editUser)){ echo site_url('assets/upload/profile/'.$editUser->image);}?>" alt="your image" height="250px" width="300px" <?php if(isset($editUser) && !empty($editUser)){ echo 'style="display:block"'; } else{ echo 'style="display:none"';} ?>>
									
									</div>
								</div>
							</div>
							
								<div class="btn-group pull-right row">
								   <div class="col-md-3"><button class="btn btn-primary " type="submit">Save</button></div>
								</div>
							</form>
					
					</div>
					
	            	
	        	</div>
	    	</div>
		</div>
	</section>
</div>
	
<?php $this->load->view('include/footer.php');?>
<script>
jQuery("#datepicker1").datepicker({

            minDate: "+3",

            dateFormat: 'mm/dd/yy'

        });
</script>
<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
				$('.projectImage').attr('name','image');
				$('#blah').css('display','block');
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>








