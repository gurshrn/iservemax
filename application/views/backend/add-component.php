<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

<div class="content-wrapper">
	<form id="addComponent" action="<?php echo site_url('admin/add-component');?>" method="POST" enctype="multipart/form-data">
		
		<input type="hidden" name="id" value="<?php if(isset($editComponent) && !empty($editComponent)){ echo $editComponent->id;}?>">
		<input type="hidden" name="compoRefId" value="<?php if(isset($editComponent) && !empty($editComponent)){ echo $editComponent->compoRefId;}?>">
		<section class="content-header productTitle">
			<h1>Components</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
										
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Name</label>
								  
										<input type="text" id="" class="form-control" placeholder="Name" name="component_name" value="<?php if(isset($editComponent) && !empty($editComponent)){ echo $editComponent->component_name;}?>">
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Part Number</label>
								  
										<input type="text" id="" class="form-control" placeholder="Part Number" name="part_no" value="<?php if(isset($editComponent) && !empty($editComponent)){ echo $editComponent->part_no;}?>">
										
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Serial Number</label>
										<input type="text" id="" class="form-control" placeholder="Serial Number" name="serial_no" value="<?php if(isset($editComponent) && !empty($editComponent)){ echo $editComponent->serial_no;}?>">
									
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Description</label>
										<textarea id="" class="form-control" placeholder="Description" name="description"><?php if(isset($editComponent) && !empty($editComponent)){ echo $editComponent->description;}?></textarea>
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Component Image</label>
										<input type="file" class="form-control projectImage" name="<?php if(isset($editComponent) && !empty($editComponent)){ echo '';} else{ echo 'component_image';}?>" onchange="readURL(this)";>
										<img id="blah" src="<?php if(isset($editComponent) && !empty($editComponent)){ echo site_url('assets/upload/component/'.$editComponent->component_image);}?>" alt="your image" height="250px" width="300px" >
									
									</div>
								</div>
							</div>
							<div class="btn-group pull-right row">
								<div class="col-md-3">
									<button class="btn btn-primary " type="submit">Submit</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</form>	
</div>
	
<?php $this->load->view('include/footer.php');?>

<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
				$('.projectImage').attr('name','component_image');
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>







