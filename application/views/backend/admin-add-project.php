<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

<div class="content-wrapper">
	<form id="addProject" action="<?php echo site_url('admin/add-project');?>" method="POST" enctype="multipart/form-data">
		<?php 
			if(isset($editProject) && !empty($editProject))
			{ 
				$ownerInformation = unserialize($editProject->owner_information);
				
				$contactInformation = unserialize($editProject->contact_information);
				$contactInformation1 = array_slice($contactInformation, 1);
				
				$contractorInformation = unserialize($editProject->contractor_information);
				$contractorInformation1 = array_slice($contractorInformation, 1);
			}
		
		
		?>
		<input type="hidden" name="id" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->id;}?>">
		<input type="hidden" name="projectRefId" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->projectRefId;}?>">
		<section class="content-header productTitle">
			<h1>Projects</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
										
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Project Name</label>
								  
										<input type="text" id="" class="form-control" placeholder="Project Name" name="project_name" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->project_name;}?>">
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Demographic Information</label>
								  
										<textarea id="" class="form-control" placeholder="Demographic Information" name="demographic_information"><?php if(isset($editProject) && !empty($editProject)){ echo $editProject->demographic_information;}?></textarea>
										
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Information</label>
										<textarea id="" class="form-control" placeholder="Project Information" name="project_information"><?php if(isset($editProject) && !empty($editProject)){ echo $editProject->project_information;}?></textarea>
									
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Requirements</label>
										<textarea id="" class="form-control" placeholder="Project Requirements" name="project_requirements"><?php if(isset($editProject) && !empty($editProject)){ echo $editProject->project_requirements;}?></textarea>
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Specification</label>
										<textarea id="" class="form-control" placeholder="Project Specification" name="specification"><?php if(isset($editProject) && !empty($editProject)){ echo $editProject->specification;}?></textarea>
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Location</label>
										<input type="text" id="" class="form-control" placeholder="Location" name="project_location" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->project_location;}?>">
									
									</div>
								</div>
							</div>
							<div class="row">
								
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">GPS Location</label>
										<input type="text" id="" class="form-control" placeholder="GPS Location" name="gps_location" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->gps_location;}?>">
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Required Field Visit</label>
										<input type="text" id="pwd" class="form-control" placeholder="Required Field Visit" name="scheduled_visit" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->scheduled_visit;}?>">
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Required Quotation</label>
										<input type="text" id="pwd" class="form-control" placeholder="Required Quotation" name="required_quotation" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->required_quotation;}?>">
									
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Plan</label>
										<textarea id="" class="form-control alphabets" placeholder="Project Plan" name="project_plan"><?php if(isset($editProject) && !empty($editProject)){ echo $editProject->project_plan;}?></textarea>
									
									</div>
								</div>
								
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Plan Image</label>
										<input type="file" class="form-control projectImage" name="<?php if(isset($editProject) && !empty($editProject)){ echo '';} else{ echo 'project_plan_image';}?>" onchange="readURL(this)";>
										<img id="blah" src="<?php if(isset($editProject) && !empty($editProject)){ echo site_url('assets/upload/project/'.$editProject->project_plan_image);}?>" alt="your image" height="250px" width="300px" >
									
									</div>
								</div>
								
									
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="content-header productTitle">
			<h1>Additional Information</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
									
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Image</label>
										<input type="file" class="form-control" name="<?php if(isset($editProject) && !empty($editProject)){ echo '';} else{ echo 'project_image';}?>" onchange="readURL(this)";>
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Video</label>
										<input type="file" class="form-control" name="<?php if(isset($editProject) && !empty($editProject)){ echo '';} else{ echo 'project_video';}?>" onchange="readURL(this)";>

										
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Document</label>
										<input type="file" class="form-control" name="<?php if(isset($editProject) && !empty($editProject)){ echo '';} else{ echo 'project_document';}?>" onchange="readURL(this)";>
										
									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="content-header productTitle">
			<h1>Owner Information</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
									
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Name</label>
								  
										<input type="text" id="firstname" class="form-control alphabets" placeholder="Name" name="owner_name" value="<?php if(isset($editProject) && !empty($editProject)){ echo $ownerInformation['owner_name'];}?>">
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Designation</label>
								  
										<input type="text" id="firstname" class="form-control" placeholder="Designation" name="owner_designation" value="<?php if(isset($editProject) && !empty($editProject)){ echo $ownerInformation['owner_designation'];}?>">
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Phone</label>
										<input type="text" id="firstname" class="form-control" placeholder="Phone" name="owner_phone" value="<?php if(isset($editProject) && !empty($editProject)){ echo $ownerInformation['owner_phone'];}?>">
									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="content-header productTitle">
			<h1>Contractor Information</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
			
				<div class="col-md-12">
				
					<div class="box box-primary">
					
						<div class="box-body">
							<button type="button" class="btn btn-primary pull-right addContractorBtn">Add</button>
							<div class="clearfix"></div>
							<div class="row">
									
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Contractor</label>
								  
										<input type="text" id="firstname" class="form-control alphabets" placeholder="Contractor" name="contractor_name[]" value="<?php if(isset($editProject) && !empty($editProject)){ echo $contractorInformation[0]['contractor_name'];}?>">
										
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Scope of Work</label>
								  
										<textarea id="lastname" class="form-control" placeholder="Scope of Work" name="scope_of_work[]"><?php if(isset($editProject) && !empty($editProject)){ echo $contractorInformation[0]['scope_of_work'];}?></textarea>
										
										
									</div>
								</div>
								
							</div>
							<div class="input_fields_wrap1">
								<?php 
									if(isset($editProject) && !empty($editProject)){ 
										foreach($contractorInformation1 as $key=>$val1){
								?>
									<div class="form-hr clearfix">
										<div class="row">
									
											<div class="col-md-4">
												<div class="form-group clearfix">
													<label for="exampleInputPassword1">Contractor</label>
											  
													<input type="text" id="firstname" class="form-control alphabets" placeholder="Contractor" name="contractor_name[]" value="<?php if(isset($editProject) && !empty($editProject)){ echo $val1['contractor_name'];}?>">
													
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group clearfix">
													<label for="exampleInputPassword1">Scope of Work</label>
											  
													<textarea id="lastname" class="form-control" placeholder="Scope of Work" name="scope_of_work[]"><?php if(isset($editProject) && !empty($editProject)){ echo $val1['scope_of_work'];}?></textarea>
													
													
												</div>
											</div>
											
										</div>
										<button type="button" class="btn btn-primary remove_fields1">Remove</button></div>
									
								
								<?php } } ?>
							
							
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</section>
		<section class="content-header productTitle">
			<h1>Contact Information</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
					
							
						<div class="box-body">
						<button type="button" class="btn btn-primary pull-right addContactBtn">Add</button>
						<div class="clearfix"></div>
							<div class="row">
										
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Name</label>
								  
										<input type="text" id="firstname" class="form-control alphabets" placeholder="Name" name="contact_name[]" value="<?php if(isset($editProject) && !empty($editProject)){ echo $contactInformation[0]['contact_name'];}?>">
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Designation</label>
								  
										<input type="text" id="firstname" class="form-control" placeholder="Designation" name="contact_designation[]" value="<?php if(isset($editProject) && !empty($editProject)){ echo $contactInformation[0]['contact_designation'];}?>">
										
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Phone</label>
										<input type="text" id="firstname" class="form-control" placeholder="Phone" name="contact_phone[]" value="<?php if(isset($editProject) && !empty($editProject)){ echo $contactInformation[0]['contact_phone'];}?>">
									
									</div>
								</div>
							</div>
							
							<div class="input_fields_wrap">
							
							<?php 
								if(isset($editProject) && !empty($editProject)){ 
									foreach($contactInformation1 as $key=>$vals){
							?>
							<div class="form-hr clearfix">
								<div class="row">
										
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Name</label>
								  
										<input type="text" id="firstname" class="form-control alphabets" placeholder="Name" name="contact_name[]" value="<?php if(isset($editProject) && !empty($editProject)){ echo $vals['contact_name'];}?>">
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Designation</label>
								  
										<input type="text" id="firstname" class="form-control" placeholder="Designation" name="contact_designation[]" value="<?php if(isset($editProject) && !empty($editProject)){ echo $vals['contact_designation'];}?>">
										
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Phone</label>
										<input type="text" id="firstname" class="form-control" placeholder="Phone" name="contact_phone[]" value="<?php if(isset($editProject) && !empty($editProject)){ echo $vals['contact_phone'];}?>">
									
									</div>
								</div>
							</div>
								<button type="button" class="btn btn-primary remove_fields">Remove</button></div>
									
							
								<?php } } ?>
						</div>
						</div>
					</div>
				</div>
				<div class="btn-group pull-right row">
				<div class="col-md-3">
					<button class="btn btn-primary " type="submit">Submit</button>
				</div>
			</div>
			</div>
			
		</section>
	</form>	
</div>
	
<?php $this->load->view('include/footer.php');?>

<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
				$('.projectImage').attr('name','project_plan_image');
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>







