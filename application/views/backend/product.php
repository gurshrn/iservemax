<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Product
			</h1>
			<div class="pull-right">
				<a href="<?php echo site_url('admin/add-product');?>"><button type="button" class="btn btn-primary">Add Product</button></a>
			</div>
      
		</section>
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
            
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>Project Name</th>
									  <th>Capacity</th>
									  <th>Max Load</th>
									  <th>Max Reach</th>
									  <th>Tip Load</th>
									  <th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										if(isset($product) && !empty($product)){
											foreach($product as $val){
									?>
											<tr>
												<td><?php echo ucfirst($val->project_name);?></td>
												<td><?php echo $val->capacity.' kg';?></td>
												<td><?php echo $val->max_load.' kg';?></td>
												<td><?php echo $val->max_reach.' kg';?></td>
												<td><?php echo $val->tip_load.' kg';?></td>
												<td>
													<a href="<?php echo site_url('/admin/admin-edit-product/'.$val->id);?>"><i class="fa fa-edit"></i></a>
										
												</td>
											</tr>
										<?php } } else{?>
										<tr>
											<td colspan="4">No record found...</td>
										</tr>
									
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
    </div>
	
<?php $this->load->view('include/footer.php');?>

