<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

<div class="content-wrapper">
	<form id="assignTask" action="<?php echo site_url('admin/create-task');?>" method="POST" enctype="multipart/form-data" autocomplete="off">
		<?php 
			if(isset($editProject) && !empty($editProject))
			{ 
				$ownerInformation = unserialize($editProject->owner_information);
				
				$contactInformation = unserialize($editProject->contact_information);
				$contactInformation1 = array_slice($contactInformation, 1);
				
				$contractorInformation = unserialize($editProject->contractor_information);
				$contractorInformation1 = array_slice($contractorInformation, 1);
			}
		
		
		?>
		<input type="hidden" name="id" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->id;}?>">
		<input type="hidden" name="projectRefId" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->projectRefId;}?>">
		<section class="content-header productTitle">
			<h1>Projects</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
										
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Date of the Task</label>
								  
										<input type="text" id="datepicker1" class="form-control" placeholder="Date of the task" name="task_date" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->task_date;}?>">
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Task Subject</label>
								  
										<textarea id="" class="form-control" placeholder="Task Subject" name="task_subject"><?php if(isset($editProject) && !empty($editProject)){ echo $editProject->demographic_information;}?></textarea>
										
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Select Project</label>
										<select class="form-control chosen selectProject" data-placeholder="Choose a project" name="projectRefId">
											<option value=""></option>
											<?php 
												if(isset($project) && !empty($project))
												{
													foreach($project as $val)
													{

														echo '<option value="'.$val->projectRefId.'">'.$val->project_name.'</option>';

													}
												}

											?>
										<select>
									
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Select Asset</label>
										<select class="form-control chosen1 selectProjectAsset" data-placeholder="Choose a project asset" name="assetRefId[]" multiple>
											<option value=""></option>
										</select>
									
									</div>
								</div>
								
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Select Component</label>
										<select class="form-control chosen2 assetComponent" data-placeholder="Choose an asset component" name="componentRefId[]" multiple>
											<option value=""></option>
											
										</select>
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Description</label>
										<textarea id="" class="form-control" placeholder="Enter Task Description" name="description"><?php if(isset($editProject) && !empty($editProject)){ echo $editProject->specification;}?></textarea>
									
									</div>
								</div>
							</div>
							<div class="row">
								
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Assign Worker</label>
										<select class="form-control chosen" name="worker_assign" multiple>
											<?php 
												if(isset($worker) && !empty($worker))
												{
													foreach($worker as $vals)
													{
														echo '<option value="'.$vals->userRefId.'">'.ucfirst($vals->first_name).' '.$vals->last_name.'</option>';

													}

												}
												
											?>
										</select>
									
									</div>
								</div>
								
								
							</div>
							
						</div>
					</div>
				</div>
				<div class="btn-group pull-right row">
				<div class="col-md-3">
					<button class="btn btn-primary " type="submit">Create Task</button>
				</div>
			</div>
			</div>
		</section>
	</form>	
</div>
	
<?php $this->load->view('include/footer.php');?>

<script type="text/javascript">
	$(function() {
	    $(".chosen").chosen({
			'width': "400px",
			'no_results_text' :'Oops, nothing found!',
		});
	});
</script>
<script type="text/javascript">
	$(function() {
	    $(".chosen1").chosen({
			'width': "400px",
			'no_results_text' :'Oops, nothing found!',
		});
	});
</script>
<script type="text/javascript">
	$(function() {
	    $(".chosen2").chosen({
			'width': "400px",
			'no_results_text' :'Oops, nothing found!',
		});
	});
</script>







