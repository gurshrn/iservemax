<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Add Invoice
			</h1>
			<div class="pull-right">
				<a href="<?php echo site_url('admin/add-invoice');?>"><button type="button" class="btn btn-primary">Add Invoice</button></a>
			</div>
      
		</section>
		

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>

					<th>Project Name</th>

					<th>Invoice No</th>

					<th>Assigned To</th>

					<th>Date</th>

					<th>Status</th>
					
					<th>Collection</th>

				</tr>
                </thead>
                <tbody>
					<?php 
						if(isset($invoice) && !empty($invoice)){
							foreach($invoice as $val){
					?>
						<tr>
							<td><?php echo ucfirst($val->project_name);?></td>

								<td><?php echo $val->invoice_no;?></td>

								<td><?php echo ucfirst($val->first_name).' '.$val->last_name;?></td>

								<td><?php echo date('d M Y',strtotime($val->created_date));?></td>

								<td>

									<?php 
									
										if($val->status == 0)
										{
											echo '<button type="button" class="btn btn-warning">Pending</button>';
										}
										if($val->status == 1)
										{
											echo '<button type="button" class="btn btn-primary">Signed</button>';
										}
										if($val->status == 2)
										{
											echo '<button type="button" class="btn btn-danger">Not Signed</button>';
										}
											
									?>
										

								</td>
								<td>
									<?php 
										if($val->status == 0 || $val->status == 2)
										{
											echo '<button type="button" class="btn btn-primary">Not assigned</button>';
										}
										if($val->status == 1)
										{
											echo '<a href="javascript:void(0)" class="collectionAssign"><button type="button" class="btn btn-primary">Assigned</button></a>';
										}
										
									?>
								</td>
						</tr>
							
						<?php } } else{?>
						<tr>
							<td colspan="5">No record found...</td>
						</tr>
					
					<?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
    

 
  

 
 
 
  <div class="control-sidebar-bg"></div>
</div>



<!-- Modal1 -->
<div class="modal fade in" id="assignCollection" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

		<div class="modal-dialog modal-dialog-centered" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title"><span id="modal-title"></span> Add Component</h3>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body get-offer-detail">

					<form id="assign-component" method="post" action="<?php echo site_url('admin/assign-component');?>">

						<input type="hidden" name="projectRefId" value="<?php echo $editProject->projectRefId; ?>">

						<div class="container-fluid">

							<div class="row">

								<div class="form-group col-md-4">

								<?php //print_r($componentDetail);die;?>

									<label for="recipient-name" class="col-form-label">Select Asset</label> 

									<select class="form-control chosen project-asset" data-placeholder="Choose an asset please" name="project_asset">                     

									  <option value=""></option>

										<?php 

											if(isset($projectAssetDetail) && !empty($projectAssetDetail)) { 

												foreach($projectAssetDetail as $val){

										?>

												<option value="<?php echo $val->assetRefId;?>"><?php echo $val->asset;?></option>

											<?php } }  ?>

									</select>

								</div>



								

								

							</div>

							<div class="row assign-compo" style="display:none;">

								<div class="form-group col-md-4">

								

									<label for="recipient-name" class="col-form-label">Assign Component</label> 

									<select class="form-control chosen" data-placeholder="Choose an component please" name="assign_component[]" multiple>                     

									  <option value=""></option>

										<?php 

											if(isset($componentDetail) && !empty($componentDetail)) { 

												foreach($componentDetail as $val1){

										?>

												<option value="<?php echo $val1->compoRefId;?>"><?php echo $val1->component_name;?></option>

											<?php } }  ?>

									</select>

								</div>

								

								

								

							</div>

						</div>

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-primary assign-component-project">Assign</button>

				</div>

				</form>

			</div>

		</div>

	</div>

<!-- Modal2 -->
<div class="modal fade" id="user-approved" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Approve User</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" class="userid">
          <h4><span class="user-body">Are you sure you want to enable this user?</span></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary updateUserStatus">Yes</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>


<?php $this->load->view('include/footer.php');?>

