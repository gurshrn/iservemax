<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

<div class="content-wrapper">
	<form id="addProduct" action="<?php echo site_url('admin/add-product');?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?php if(isset($editProduct) && !empty($editProduct)){ echo $editProduct->id;}?>">
		<section class="content-header productTitle">
			<h1>Product</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
										
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Select Product</label>
								  
										<select class="form-control" name="projectRefId">
											<option value="">Select Project</option>
											<?php 
												if(isset($project) && !empty($project)){ 
													foreach($project as $val){
														if(isset($editProduct) && !empty($editProduct)){
															if($editProduct->projectRefId == $val->projectRefId)
															{
																$sel = "selected='selected'";
															}
															else
															{
																$sel = '';
															}
														}
														else
														{
															$sel = '';
														}
											?>
													<option value="<?php echo $val->projectRefId; ?>" <?php echo $sel; ?>><?php echo ucfirst($val->project_name);?></option>
											
												<?php } } ?>
										</select>
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Capacity</label>
								  
										<input type="text" id="" class="form-control" placeholder="Capacity" name="capacity" value="<?php if(isset($editProduct) && !empty($editProduct)){ echo $editProduct->capacity;}?>">
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Max Load</label>
										
										<input type="text" id="" class="form-control" placeholder="Max Load" name="max_load" value="<?php if(isset($editProduct) && !empty($editProduct)){ echo $editProduct->max_load;}?>">
									
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Max Reach</label>
										
										<input type="text" id="" class="form-control" placeholder="Max Reach" name="max_reach" value="<?php if(isset($editProduct) && !empty($editProduct)){ echo $editProduct->max_reach;}?>">
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Tip Load</label>
										
										<input type="text" id="" class="form-control" placeholder="Tip Load" name="tip_load" value="<?php if(isset($editProduct) && !empty($editProduct)){ echo $editProduct->tip_load;}?>">
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Description</label>
										
										<textarea id="" class="form-control alphabets" placeholder="Description" name="description"><?php if(isset($editProduct) && !empty($editProduct)){ echo $editProduct->description;}?></textarea>
									
									</div>
								</div>
							</div>
							
							<div class="row">
								
								
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Image</label>
										
										<input type="file" class="form-control projectImage" name="<?php if(isset($editProduct) && !empty($editProduct)){ echo '';}else{ echo 'image'; }?>" onchange="readURL(this)";>
										<img id="blah" src="<?php if(isset($editProduct) && !empty($editProduct)){ echo site_url('assets/upload/product/'.$editProduct->image);}?>" alt="your image" height="250px" width="300px">
									
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="btn-group pull-right row">
						<div class="col-md-3">
							<button class="btn btn-primary " type="submit">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</section>
	</form>	
</div>
	
<?php $this->load->view('include/footer.php');?>

<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
				$('.projectImage').attr('name','image');
				$('#blah').css('display','block');
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>







