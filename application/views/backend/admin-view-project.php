<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

<div class="content-wrapper">
	<form id="addProject" action="<?php echo site_url('admin/add-project');?>" method="POST" enctype="multipart/form-data">
		<?php 
			if(isset($editProject) && !empty($editProject))
			{ 
				//$ownerInformation = unserialize($editProject->owner_information);
				
				/* $contactInformation = unserialize($editProject->contact_information);
				$contactInformation1 = array_slice($contactInformation, 1);
				
				$contractorInformation = unserialize($editProject->contractor_information);
				$contractorInformation1 = array_slice($contractorInformation, 1); */
			}
		
		
		?>
		<input type="hidden" name="id" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->id;}?>">
		<input type="hidden" name="projectRefId" value="<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->projectRefId;}?>">
		<section class="content-header productTitle">
			<h1>Projects</h1>
			<div class="pull-right">
				<button type="button" class="btn btn-primary assignProjectBtn">Assign Project</button>
			</div>
			<div class="pull-right">
				<button type="button" class="btn btn-primary assignAssetBtn">Assign Asset</button>
			</div>
			<div class="pull-right">
				<button type="button" class="btn btn-primary addComponentBtn">Add Component</button>
			</div>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
										
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Project Name</label>
								  
										<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->project_name;}?>
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Demographic Information</label>
								  
										<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->demographic_information;}?>
										
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Information</label>
										<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->project_information;}?>
									
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Requirements</label>
										<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->project_requirements;}?>
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Specification</label>
										<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->specification;}?>
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Location</label>
										<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->project_location;}?>
									
									</div>
								</div>
							</div>
							<div class="row">
								
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">GPS Location</label>
										<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->gps_location;}?>
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Scheduled Visit</label>
										<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->scheduled_visit;}?>
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Plan</label>
										<?php if(isset($editProject) && !empty($editProject)){ echo $editProject->project_plan;}?>
									
									</div>
								</div>
							</div>
							<div class="row">
								
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Project Plan Image</label>
										<img id="blah" src="<?php if(isset($editProject) && !empty($editProject)){ echo site_url('assets/upload/project/'.$editProject->project_plan_image);}?>" alt="your image" height="250px" width="300px" >
									
									</div>
								</div>
								
									
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		 <!--section class="content-header productTitle">
			<h1>Owner Information</h1>
		</section> 
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
									
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Name</label>
								  
										<?php if(isset($editProject) && !empty($editProject)){ echo $ownerInformation['owner_name'];}?>
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Designation</label>
								  
										<?php if(isset($editProject) && !empty($editProject)){ echo $ownerInformation['owner_designation'];}?>
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Phone</label>
										<?php if(isset($editProject) && !empty($editProject)){ echo $ownerInformation['owner_phone'];}?>
									
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Required Quotation</label>
										
									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="content-header productTitle">
			<h1>Contractor Information</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
			
				<div class="col-md-12">
				
					<div class="box box-primary">
					
						<div class="box-body">
							<div class="clearfix"></div>
							<div class="row">
									
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Contractor</label>
								  
										<?php if(isset($editProject) && !empty($editProject)){ echo $contractorInformation[0]['contractor_name'];}?>
										
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Scope of Work</label>
								  
										<?php if(isset($editProject) && !empty($editProject)){ echo $contractorInformation[0]['scope_of_work'];}?>
										
										
									</div>
								</div>
								
							</div>
							<div class="input_fields_wrap1">
								<?php 
									if(isset($editProject) && !empty($editProject)){ 
										foreach($contractorInformation1 as $key=>$val1){
								?>
									<div class="form-hr clearfix">
										<div class="row">
									
											<div class="col-md-4">
												<div class="form-group clearfix">
													<label for="exampleInputPassword1">Contractor</label>
											  
													<?php if(isset($editProject) && !empty($editProject)){ echo $val1['contractor_name'];}?>
													
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group clearfix">
													<label for="exampleInputPassword1">Scope of Work</label>
											  
													<?php if(isset($editProject) && !empty($editProject)){ echo $val1['scope_of_work'];}?>
													
													
												</div>
											</div>
											
										</div>
										
									
								
								<?php } } ?>
							
							
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</section>
		<section class="content-header productTitle">
			<h1>Contact Information</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
					
							
						<div class="box-body">
						<div class="clearfix"></div>
							<div class="row">
										
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Name</label>
								  
										<?php if(isset($editProject) && !empty($editProject)){ echo $contactInformation[0]['contact_name'];}?>
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Designation</label>
								  
										<?php if(isset($editProject) && !empty($editProject)){ echo $contactInformation[0]['contact_designation'];}?>
										
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Phone</label>
										<?php if(isset($editProject) && !empty($editProject)){ echo $contactInformation[0]['contact_phone'];}?>
									
									</div>
								</div>
							</div>
							
							<div class="input_fields_wrap">
							
							<?php 
								if(isset($editProject) && !empty($editProject)){ 
									foreach($contactInformation1 as $key=>$vals){
							?>
							<div class="form-hr clearfix">
								<div class="row">
										
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Name</label>
								  
										<?php if(isset($editProject) && !empty($editProject)){ echo $vals['contact_name'];}?>
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Designation</label>
								  
										<?php if(isset($editProject) && !empty($editProject)){ echo $vals['contact_designation'];}?>
										
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Phone</label>
										<?php if(isset($editProject) && !empty($editProject)){ echo $vals['contact_phone'];}?>
									
									</div>
								</div>
							</div>
								
									
							
								<?php } } ?>
						</div>
						</div>
					</div>
				</div>
				<div class="btn-group pull-right row">
				<div class="col-md-3">
					<a href=""><button class="btn btn-primary " type="button">Back</button></a>
				</div>
			</div>
			</div>
			
		</section-->
		<div class="projectAssetDetail" <?php if(isset($projectAssetDetail) && !empty($projectAssetDetail)){ echo 'style="display:block"';}else{ echo 'style="display:none"';}?>>
			<section class="content-header productTitle">
				<h1>Asset Information</h1>
			</section>
			<section class="content backend-sec">
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
						
								
							<div class="box-body">
							<div class="clearfix"></div>
							 <table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>Asset</th>
									  <th>Date</th>
									</tr>
								</thead>
								<tbody class="allResult">
									<?php 
										if(isset($projectAssetDetail) && !empty($projectAssetDetail)){
											foreach($projectAssetDetail as $vals){
									?>
										<tr>
											<td><?php echo $vals->asset;?></td>
											<td><?php echo $vals->addedondate;?></td>
											<td></td>
										</tr>
										<?php } } ?>
								</tbody>
							</table>
								
								
								
						</div>
					</div>
				</div>
				
			</section>
		</div>
		<div class="projectComponentDetail" <?php if(isset($projectComponentDetail) && !empty($projectComponentDetail)){ echo 'style="display:block"';}else{ echo 'style="display:none"';}?>>
			<section class="content-header productTitle">
				<h1>Asset Component</h1>
			</section>
			<section class="content backend-sec">
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
						
								
							<div class="box-body">
							<div class="clearfix"></div>
							 <table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>Component</th>
									  <th>Asset</th>
									  <th>Date</th>
									</tr>
								</thead>
								<tbody class="allResults">
									<?php 
										if(isset($projectComponentDetail) && !empty($projectComponentDetail)){
											foreach($projectComponentDetail as $vals1){
									?>
										<tr>
											<td><?php echo ucfirst($vals1->component_name);?></td>
											<td><?php echo ucfirst($vals1->asset);?></td>
											<td><?php echo $vals1->addedondate;?></td>
											<td></td>
										</tr>
										<?php } } ?>
								</tbody>
							</table>
								
								
								
						</div>
					</div>
				</div>
				
			</section>
		</div>
	</form>	
</div>
<div class="modal fade in" id="assignProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title"><span id="modal-title"></span> Assign Project</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body get-offer-detail">
					<form id="assign-project" method="post" action="<?php echo site_url('admin/assign-project');?>">
						<input type="hidden" name="projectRefId" value="<?php echo $editProject->projectRefId; ?>">
						<div class="container-fluid">
							<div class="row">
								<div class="form-group col-md-">
								
									<label for="recipient-name" class="col-form-label">Assign Project</label> 
									<select class="form-control chosen" data-placeholder="Choose field coordinator" name="userRefId">                     
									  <option value=""></option>
										<?php 
											if(isset($fieldCoordinator) && !empty($fieldCoordinator)) { 
												foreach($fieldCoordinator as $val){
										?>
												<option value="<?php echo $val->userRefId;?>"><?php echo $val->first_name.' '.$val->last_name;?></option>
											<?php } }  ?>
									</select>
								</div>
								
								
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary assign-project-btn">Assign</button>
				</div>
				</form>
			</div>
		</div>
	</div>
<div class="modal fade in" id="assignAsset" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title"><span id="modal-title"></span> Assign Asset</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body get-offer-detail">
					<form id="assign-asset-project" method="post" action="<?php echo site_url('admin/assign-asset');?>">
						<input type="hidden" name="projectRefId" value="<?php echo $editProject->projectRefId; ?>">
						<div class="container-fluid">
							<div class="row">
								<div class="form-group col-md-">
								
									<label for="recipient-name" class="col-form-label">Assign Asset</label> 
									<select class="form-control chosen" data-placeholder="Choose an asset please" name="assign_asset[]" multiple>                     
									  <option value=""></option>
										<?php 
											if(isset($assetDetail) && !empty($assetDetail)) { 
												foreach($assetDetail as $val){
													$results = getProjectAssetdetail($val->assetRefId,$editProject->projectRefId);
													if($results[0]->assetRefId != $val->assetRefId) {
										?>
												<option value="<?php echo $val->assetRefId;?>"><?php echo $val->asset;?></option>
											<?php } } } ?>
									</select>
								</div>
								
								
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary assign-asset-project">Assign</button>
				</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="componentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title"><span id="modal-title"></span> Add Component</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body get-offer-detail">
					<form id="assign-component" method="post" action="<?php echo site_url('admin/assign-component');?>">
						<input type="hidden" name="projectRefId" value="<?php echo $editProject->projectRefId; ?>">
						<div class="container-fluid">
							<div class="row">
								<div class="form-group col-md-4">
								<?php //print_r($componentDetail);die;?>
									<label for="recipient-name" class="col-form-label">Select Asset</label> 
									<select class="form-control chosen project-asset" data-placeholder="Choose an asset please" name="project_asset">                     
									  <option value=""></option>
										<?php 
											if(isset($projectAssetDetail) && !empty($projectAssetDetail)) { 
												foreach($projectAssetDetail as $val){
										?>
												<option value="<?php echo $val->assetRefId;?>"><?php echo $val->asset;?></option>
											<?php } }  ?>
									</select>
								</div>

								
								
							</div>
							<div class="row assign-compo" style="display:none;">
								<div class="form-group col-md-4">
								
									<label for="recipient-name" class="col-form-label">Assign Component</label> 
									<select class="form-control chosen" data-placeholder="Choose an component please" name="assign_component[]" multiple>                     
									  <option value=""></option>
										<?php 
											if(isset($componentDetail) && !empty($componentDetail)) { 
												foreach($componentDetail as $val1){
										?>
												<option value="<?php echo $val1->compoRefId;?>"><?php echo $val1->component_name;?></option>
											<?php } }  ?>
									</select>
								</div>
								
								
								
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary assign-component-project">Assign</button>
				</div>
				</form>
			</div>
		</div>
	</div>

	
<?php $this->load->view('include/footer.php');?>

<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
				$('.projectImage').attr('name','project_plan_image');
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
<script type="text/javascript">
	$(function() {
	    $(".chosen").chosen({
			'width': "500px",
			'no_results_text' :'Oops, nothing found!',
		});
	});
	$(document).on('change','.project-asset',function(){
		$('.assign-compo').css('display','block');
	});
</script>







