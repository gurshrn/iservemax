<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Safety Content
			</h1>
			<div class="pull-right">
				<a href="javascript:void(0)"><button type="button" class="btn btn-primary addSafetyContent">Add Safety Content</button></a>
			</div>
      
		</section>
		

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Safety Document</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
					<?php 
						if(isset($safetyContent) && !empty($safetyContent)){
							foreach($safetyContent as $val){
					?>
							<tr>
								<td>
									<?php if($val->type == 'video/mp4') { ?>

											<video width="150" height="150" controls><source src="<?php echo site_url('assets/upload/safetyDocument/'.$val->safety_document); ?>"></video>
									<?php } else{ ?>

											<a href="<?php echo site_url('assets/upload/safetyDocument/'.$val->safety_document);?>" class="btn" target="_blank"><?php echo $val->safety_document;?></a>

									<?php } ?>

								</td>
								<td><?php echo ucfirst($val->content);?></td>
							</tr>
							
					<?php } } else{?>
						<tr>
							<td colspan="4">No record found...</td>
						</tr>
					
					<?php } ?>
                  
                
               
                
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
    

 
  

 
 
 
  <div class="control-sidebar-bg"></div>
</div>



<!-- Modal1 -->
<div class="modal fade in" id="addSafetyContent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title"><span id="modal-title"></span> Add Safety Content</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body get-offer-detail">
					<form id="add-safety-content" method="post" action="<?php echo site_url('admin/safety-content');?>" enctype="multipart/form-data">
						<div class="container-fluid">
							<div class="row">
								<div class="form-group col-md-">
								
									<label for="recipient-name" class="col-form-label">Safety Content</label> 
									<input type="file" class="form-control" name="safety_document" onchange="readURL(this)";>
								</div>
								
								
							</div>
							<div class="row">
								<div class="form-group col-md-">
								
									<label for="recipient-name" class="col-form-label">Content</label> 
									<textarea class="form-control" name="content"></textarea>
								</div>
								
								
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				</form>
			</div>
		</div>
	</div>


<?php $this->load->view('include/footer.php');?>

