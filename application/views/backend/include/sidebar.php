<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php 
       
?>

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Credit Marche</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Credit</b>  Marche</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><?php //echo ucfirst($adminFirstname); ?></span>
            </a>
			<ul class="dropdown-menu">
              <li class="user-footer">
                <div class="pull-left">
                  <!--a href="#" class="btn btn-default btn-flat">Profile</a-->
                </div>
                <div class="pull-right">
                  <a href="<?php echo site_url('admin/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
            
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
     
      <ul class="sidebar-menu" data-widget="tree">
        
        <!--li class="<?php if($parenturl == 'Dashboard'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/dashboard');?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
         
        </li-->
		<li class="<?php if($parenturl == 'User'){ echo 'active';}?>">
          <a href="<?php echo site_url('/');?>">
            <i class="fa fa-user-circle-o"></i> <span>Users</span>
          </a>
         
        </li>
		
		<li class="<?php if($parenturl == 'Project'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/project');?>">
            <i class="fa fa-user-circle-o"></i> <span>Projects</span>
          </a>
         
        </li>
		<li class="<?php if($parenturl == 'Asset'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/asset');?>">
            <i class="fa fa-user-circle-o"></i> <span>Asset</span>
          </a>
         
        </li>
		<li class="<?php if($parenturl == 'Component'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/component');?>">
            <i class="fa fa-user-circle-o"></i> <span>Component</span>
          </a>
         
        </li>
      <li class="<?php if($parenturl == 'Task'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/task');?>">
            <i class="fa fa-user-circle-o"></i> <span>Task</span>
          </a>
         
        </li>

        <li class="<?php if($parenturl == 'Safety Content'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/safety-content');?>">
            <i class="fa fa-user-circle-o"></i> <span>Safety Content</span>
          </a>
         
        </li>

        <li class="<?php if($parenturl == 'Product'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/product');?>">
            <i class="fa fa-user-circle-o"></i> <span>Product</span>
          </a>
         
        </li>

        <li class="<?php if($parenturl == 'News'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/news');?>">
            <i class="fa fa-user-circle-o"></i> <span>News</span>
          </a>
         
        </li>				<li class="<?php if($parenturl == 'Invoice'){ echo 'active';}?>">          <a href="<?php echo site_url('/admin/invoice');?>">            <i class="fa fa-user-circle-o"></i> <span>Invoice</span>          </a>                 </li>
		
		
	
     </ul>
    </section>
   
  </aside>
