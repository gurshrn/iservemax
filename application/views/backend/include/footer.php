		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/jquery-ui/jquery-ui.min.js"></script>
		<script>
		  $.widget.bridge('uibutton', $.ui.button);
		</script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/moment/min/moment.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/fastclick/lib/fastclick.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/dist/js/adminlte.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/dist/js/pages/dashboard.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/dist/js/demo.js"></script>
		<script src="<?php echo site_url(); ?>assets/js/admin.js"></script>
	</body>
</html>