<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>AdminLTE 2 | Dashboard</title>
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/style.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/font-awesome/css/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/Ionicons/css/ionicons.min.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/dist/css/AdminLTE.min.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/dist/css/skins/_all-skins.min.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/morris.js/morris.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/jvectormap/jquery-jvectormap.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/bootstrap-daterangepicker/daterangepicker.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
      <script>
        var site_url = '<?php echo site_url(); ?>';
    </script>
    </head>