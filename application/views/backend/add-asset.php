<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

<div class="content-wrapper">
	<form id="addAsset" action="<?php echo site_url('admin/add-asset');?>" method="POST" enctype="multipart/form-data">
		<?php 
			if(isset($editAsset) && !empty($editAsset))
			{ 
				$additionalInformation = unserialize($editAsset->additional_information);
			}
		
		
		?>
		<input type="hidden" name="id" value="<?php if(isset($editAsset) && !empty($editAsset)){ echo $editAsset->id;}?>">
		<input type="hidden" name="assetRefId" value="<?php if(isset($editAsset) && !empty($editAsset)){ echo $editAsset->assetRefId;}?>">
		<section class="content-header productTitle">
			<h1>Asset</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
										
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Asset No:</label>
								  
										<input type="text" id="" class="form-control" placeholder="Asset No" name="asset_no" value="<?php if(isset($editAsset) && !empty($editAsset)){ echo $editAsset->asset_no;}?>">
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Asset:</label>
								  
										<input type="text" id="" class="form-control" placeholder="Asset" name="asset" value="<?php if(isset($editAsset) && !empty($editAsset)){ echo $editAsset->asset;}?>">
										
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Serial No:</label>
										<input type="text" id="" class="form-control" placeholder="Serial No" name="serial_no" value="<?php if(isset($editAsset) && !empty($editAsset)){ echo $editAsset->serial_no;}?>">
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Description</label>
										<textarea id="" class="form-control" placeholder="Description" name="asset_description"><?php if(isset($editAsset) && !empty($editAsset)){ echo $editAsset->asset_description;}?></textarea>
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Asset Image</label>
										<input type="file" class="form-control assetImage" name="<?php if(isset($editAsset) && !empty($editAsset)){ echo '';} else{ echo 'asset_image';}?>" onchange="readURL(this)";>
										<img id="blah" src="<?php if(isset($editAsset) && !empty($editAsset)){ echo site_url('assets/upload/asset/'.$editAsset->asset_image);}?>" alt="your image" height="250px" width="300px" >
									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="content-header productTitle">
			<h1>Additional Information</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
									
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Purshase Date</label>
								  
										<input type="text" id="datepicker1" class="form-control" placeholder="Name" name="purshase_date" value="<?php if(isset($editAsset) && !empty($editAsset)){ echo $additionalInformation['purshase_date'];}?>" readonly>
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Warranty Validity</label>
								  
										<input type="text" id="datepicker" class="form-control" placeholder="Designation" name="warranty_validity" value="<?php if(isset($editAsset) && !empty($editAsset)){ echo $additionalInformation['warranty_validity'];}?>" readonly>
										
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">P.O Number</label>
										<input type="text" id="" class="form-control" placeholder="Phone" name="po_number" value="<?php if(isset($editAsset) && !empty($editAsset)){ echo $additionalInformation['po_number'];}?>">
									
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Supplier</label>
										<input type="text" id="" class="form-control" placeholder="Supplier" name="supplier" value="<?php if(isset($editAsset) && !empty($editAsset)){ echo $additionalInformation['supplier'];}?>">
									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Inspected by Time & Date</label>
										<input type="text" id="datepicker2" class="form-control" placeholder="Name" name="inspected_date" value="<?php if(isset($editAsset) && !empty($editAsset)){ echo $additionalInformation['inspected_date'];}?>" readonly>
										<input type="text" id="" class="form-control" placeholder="Name" name="inspected_time" value="<?php if(isset($editAsset) && !empty($editAsset)){ echo $additionalInformation['inspected_time'];}?>">
									
									</div>
								</div>
							</div>
						</div>
					</div>
						<div class="btn-group pull-right row">
						<div class="col-md-3">
							<button class="btn btn-primary " type="submit">Submit</button>
						</div>
					</div>
				</div>
					
			</div>
		</section>
	</form>	
</div>
	
<?php $this->load->view('include/footer.php');?>

<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
				$('.assetImage').attr('name','asset_image');
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>










