<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
	
?>
        <div class="content-section">
            <div class="cntnt-head-bar">
                <h1>Create Task</h1>
            </div>
            <div class="checkin-out-chart create-task-bx">
                <form id="assignTask" action="<?php echo site_url('admin/create-task');?>" method="POST" enctype="multipart/form-data" autocomplete="off">
                   
				   <input type="hidden" name="id" value="">
                    <input type="hidden" name="projectRefId" value="">
                    
					<div class="form-group">
                        <input class="form-control" type="text" placeholder="Date of the Task" name="task_date" id="datepicker1">
                        <input class="form-control" type="text" placeholder="Task Subject" name="task_subject">
                        <!--select class="form-control">
                            <option>Task Subject</option>
                            <option>Task Subject1</option>
                            <option>Task Subject2</option>
                        </select-->
                    </div>
                    <div class="form-group">
                        <select class="form-control chosen selectProject" data-placeholder="Choose a project" name="projectRefId">
                            <option value=""></option>
                            <?php 
                                if(isset($project) && !empty($project))
                                {
                                    foreach($project as $val)
                                    {

                                        echo '<option value="'.$val->projectRefId.'">'.$val->project_name.'</option>';

                                    }
                                }

                            ?>
                       </select>
                        <select class="form-control chosen1 selectProjectAsset" data-placeholder="Choose a project asset" name="assetRefId[]" multiple>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control chosen2 assetComponent" data-placeholder="Choose an asset component" name="componentRefId[]" multiple>
                            <option value=""></option>
                       </select>
                        <div class="chs_fle">
                            <input name="r" class="file" type="file">
                            <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                <input class="brws_inpt form_custom_control form-control" name="task_image" placeholder="Upload Image" type="text"> <span class="input-group-btn"> 
									</span> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Enter Task Description" name="description"></textarea>
                    </div>
                    <div class="form-group">
						<input type="hidden" id="workersRefId" name="worker_assigned">
                        <input class="form-control workerAssigned" type="text" placeholder="Worker Assigned" name="worker_name" readonly>
                    </div>										
					
					<div class="checkin-out-chart task-page" style="display:none;">                					
						<div class="srch">                    					
							<input type="search" placeholder="Search...">                    					
							<i class="fa fa-search"></i>                					
						</div>                					
						<div class="task-full">                    					
							<div class="task-lft">                       					
								<div class="usr-slcted user-img">                            											
							</div>                        						
							<div class="usr-list">																				
							<?php 
								$i = 1;												
								if(isset($worker) && !empty($worker)){															
								foreach($worker as $val1){
														
							?>														
									<div class="usr-chk" data-target="<?php echo $val1->userRefId;?>">																					
										<input class="styled-checkbox checked<?php echo $val1->userRefId;?>" id="styled-checkbox-<?php echo $i;?>" type="checkbox" value="<?php echo $val1->userRefId;?>" data-target="<?php echo $val1->userRefId;?>">												
										
										<figure id="img<?php echo $val1->userRefId;?>">												
											<img src="<?php echo site_url(); ?>assets/frontend/images/usr1.jpg" alt="usr1">
										</figure>												
										
										<label id="worker-name<?php echo $val1->userRefId;?>" for="styled-checkbox-<?php echo $i;?>"><?php echo ucfirst($val1->first_name).' '.$val1->last_name;?></label>														
									</div>																			
							<?php $i++; } } ?>																	
						</div>                    					
					</div><!-- task-lft end -->                    					
					
					<div class="task-rght-main">                         					
						<h1>Pending Task</h1>                         					
						<div class="task-rght">                        					
							<table class="tbl-view">                            					
								<thead>                             					
									<tr>                                 					
										<th style="width:142px;">S no</th>                                  					
										<th style="width:337px;">Title Name</th>                                  					
										<th style="width:238px;">workers Assigned</th>                             					
									</tr>                            					
								</thead>                            					
								<tbody class="allResult">																				
									<tr>
										<td colspan="3">No worker selected...</td>
									</tr>                             					
									<!--tr>                                 					
										<td data-column="S no">1</td>                                 					
										<td data-column="Title Name">Lorem ipsum dolor sit amet</td>                                 					
										<td data-column="Workers Assigned">John Smith, Vicky Nbh, Jemes</td>                                                              					
									</tr-->                                												
									<!--tr>
										<td colspan="3" style="text-align:right;">
											<a href="#" title="" class="btn">Add Member</a>
										</td>
									</tr-->                            					
								</tbody>                        					
							</table>                                               					
						</div><!-- task-rght end -->                    					
					</div><!-- task-rght-main end -->                					
				</div>                					
			</div>
			<input type="submit" value="create task" class="btn-sbmit">
		</form>
	</div>						
</section>

<?php $this->load->view('include/footer.php');?>
<script type="text/javascript">
    $(function() {
        $(".chosen").chosen({
            'width': "400px",
            'no_results_text' :'Oops, nothing found!',
        });
    });
</script>
<script type="text/javascript">
    $(function() {
        $(".chosen1").chosen({
            'width': "400px",
            'no_results_text' :'Oops, nothing found!',
        });
    });
</script>
<script type="text/javascript">
    $(function() {
        $(".chosen2").chosen({
            'width': "400px",
            'no_results_text' :'Oops, nothing found!',
        });
    });
</script>
  