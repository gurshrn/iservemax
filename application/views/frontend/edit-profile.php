<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
?>
	<div class="content-section">
		<div class="cntnt-head-bar">
			<h1>Profile</h1>
		</div>
		<div class="checkin-out-chart create-task-bx">
			<form id="add-user" action="<?php echo site_url('admin/add-user'); ?>" method="POST" autocomplete="off">
				<input type="hidden" name="id" value="<?php echo $userdetail->id;?>">
				<input type="hidden" name="userRefId" value="<?php echo $userdetail->userRefId;?>">
				<input type="hidden" name="frontend" value="frontend">
				
				<div class="form-group">
					<input class="form-control" placeholder="First Name" name="first_name" type="text" value="<?php echo ucfirst($userdetail->first_name);?>">
					<input class="form-control" placeholder="Last Name" name="last_name" type="text" value="<?php echo ucfirst($userdetail->last_name);?>">
				</div>
				<div class="form-group">
					
					<input class="form-control" placeholder="Email" name="email" type="text" value="<?php echo $userdetail->email;?>">
					<input class="form-control" placeholder="Phone Number" name="phone_number" type="text" value="<?php echo $userdetail->phone_number;?>">
					
				</div>
				<div class="form-group">
					
					<input class="form-control" placeholder="Date of Birth" name="dob" type="text" id="datepicker1" value="<?php echo $userdetail->dob;?>">
					
					<input class="form-control" placeholder="Address" name="address" type="text" value="<?php echo ucfirst($userdetail->address);?>">
				</div>
				<div class="form-group">
					<select class="form-control" name="gender">
					<option value="">Select Gender</option>
					<option value="Female" <?php if(isset($userdetail) && !empty($userdetail)){ if($userdetail->gender == 'Female'){ echo "selected='selected'";}} ?>>Female</option>
					<option value="Male" <?php if(isset($userdetail) && !empty($userdetail)){ if($userdetail->gender == 'Male'){ echo "selected='selected'";}} ?>>Male</option>
					<textarea class="form-control" placeholder="Description" name="description"><?php echo ucfirst($userdetail->description);?></textarea>
				</div>
				<div class="form-group">
					<div class="chs_fle">
						<input name="image" class="file" type="file">
						<p> <i class="fa fa-paperclip" aria-hidden="true"></i>
							<input class="brws_inpt form_custom_control form-control" name="image" placeholder="profile image" type="text"> <span class="input-group-btn"> 
							</span> </p>
					</div>
				</div>
				<input value="Save" class="btn-sbmit" type="submit">
			</form>
		</div>
	</div>
	
<?php $this->load->view('include/footer.php');?>
   