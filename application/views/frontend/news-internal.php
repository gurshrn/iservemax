<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
?>
        <div class="content-section">
            <div class="cntnt-head-bar">
                <h1>News</h1>
                <!--div class="srch">
                    <input type="search" placeholder="Search...">
                    <i class="fa fa-search"></i>
                </div-->
            </div>
            <div class="checkin-out-chart">
                <div class="news-internal">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="full-news">
                                <figure><img src="<?php echo site_url('assets/upload/news/'.$newsdetail->image);?>" alt="news-img"></figure>
                                <h3><?php echo ucfirst($newsdetail->news_title);?></h3>
                                <p><?php echo ucfirst($newsdetail->description);?></p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="latest-news">
                                <h3>latest news</h3>
								<?php 
									if(isset($latestNews) && !empty($latestNews)){ 
										foreach($latestNews as $val){
											$string = substr($val->description, 0, 150);
								
								?>
										<div class="news-box">
											<figure><img src="<?php echo site_url('assets/upload/news/'.$val->image);?>" alt="news-image"></figure>
											<div class="description">
												<h3><a href="<?php echo site_url('news-detail/'.$val->id);?>"><?php echo ucfirst($val->news_title);?></a></h3>
												<p><?php echo ucfirst($string);?></p>
											</div>
										</div>
										
								<?php } } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('include/footer.php');?> 
   
