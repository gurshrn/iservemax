<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
?>
	<div class="content-section">
		<div class="cntnt-head-bar">
			<h1>Safety Contect</h1>

		</div>
		<div class="checkin-out-chart">
			<div class="safety-contect-main">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item active">
						<a class="nav-link btn" id="video-tab" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="true">Video</a>
					</li>
					<li class="nav-item">
						<a class="nav-link btn" id="document-tab" data-toggle="tab" href="#document" role="tab" aria-controls="document" aria-selected="false">Document</a>
					</li>

				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade active in" id="video" role="tabpanel" aria-labelledby="video-tab">
						<div class="video-main">

                            <?php 
                                if(isset($safetyContentList) && !empty($safetyContentList)) { 
                                    foreach($safetyContentList as $val) { 
                                        if($val->type == 'video/mp4'){
                            ?>
                                    <div class="video-bx">
                                        <a href="#" title=""><figure><video width="320" height="240" controls><source src="<?php echo site_url('assets/upload/safetyDocument/'.$val->safety_document); ?>"></video></figure>
                                         <span><?php echo ucfirst($val->content);?></span>
                                        </a>
                                    </div><!-- video-bx end -->

                            <?php } } } ?>
							
                            
						
						</div><!-- video-main end -->
					</div>
					<div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="document-tab">
					<table class="doc-tbl">
			  
        				<tbody>
                            <?php 
                                if(isset($safetyContentList) && !empty($safetyContentList)) { 
                                    foreach($safetyContentList as $val) { 
                                        if($val->type != 'video/mp4'){
                            ?>
        					
                            <tr>
        						<td data-column="Project Description"><?php echo ucfirst($val->content);?></td>
        					 
        						<td data-column="Status">
        							<a href="<?php echo site_url('assets/upload/safetyDocument/'.$val->safety_document);?>" class="btn" target="_blank">download</a>
        						  
        						</td>
        					</tr>

                        <?php } } } ?>
        					 
        					 
        					 
        				   
        				</tbody>
        			</table>
					</div>

				</div>
			</div>

		</div>
		
	</div>
</section>

<?php $this->load->view('include/footer.php');?>
    <!-------------------------------------- All Modals starts here ------------------------------------------->
    <div class="modal fade" id="c-history" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">components history</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Duration</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="asset-history" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Asset history</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Duration</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="unsigned" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Un - Signed</h2>
                </div>
                <div class="modal-body">
                    <form>

                        <div class="form-group">
                            <textarea class="form-control" placeholder="Message"></textarea>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="signed" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Signed</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="chs_fle">
                            <input name="r" class="file" type="file">
                            <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                <input class="brws_inpt form_custom_control form-control" placeholder="Upload Image" type="text"> <span class="input-group-btn"> 
									</span> </p>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="solved" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Solved</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <div class="chs_fle">
                                <input name="r" class="file" type="file">
                                <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    <input class="brws_inpt form_custom_control form-control" placeholder="Attach: photo, text, video, voice note" type="text"> <span class="input-group-btn"> 
									</span> </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Message"></textarea>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="t-list" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Task list</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view asset-tbl">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Task</th>
                                <th>Worker</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add-expense" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Add expenses</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <select class="form-control">
                            <option>Select Expense Type</option>
                            <option>Select Expense Type1</option>
                            <option>Select Expense Type2</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control">
                            <option>Expense Name</option>
                            <option>Expense Name1</option>
                            <option>Expense Name2</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group">
                            <div class="chs_fle">
                                <input name="r" class="file" type="file">
                                <p>
                                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    <input class="brws_inpt form_custom_control form-control" placeholder="Select Images" type="text"> <span class="input-group-btn"></span>
                                </p>
                            </div>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="components" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">components</h2>
                </div>
                <div class="modal-body">
                    <div class="comp-div">
                        <figure><img src="<?php echo site_url(); ?>assets/frontend/images/component-img.jpg"></figure>
                        <div class="labels-component">
                            <p><strong>Name : </strong>Abc</p>
                            <p><strong>Part Number : </strong>12232345</p>
                            <p><strong>Serial No : </strong>123132579565693</p>
                        </div>
                        <p><strong>Description : </strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

