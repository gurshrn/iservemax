<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
?>
        <div class="content-section">
            <div class="cntnt-head-bar">
                <h1>Product</h1>
                <div class="srch">
                    <input type="search" placeholder="Search...">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            <div class="checkin-out-chart">
                <div class="fltrs">
                    <h2>Filters <i class="fa fa-caret-down"></i></h2>
                    <div class="all-filters">
                        <form class="fltr-form">
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Equipment Category">
                            </div>
                            <div class="form-group">
                                <select class="form-control">
                            <option>Equipment 1 ( for capacity )</option>
                            </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control">
                            <option>Equipment 1 ( for max load )</option>
                            </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control">
                            <option>Equipment 1 ( for max reach )</option>
                            </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control">
                            <option>Equipment 1 ( for tip load )</option>
                            </select>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Search results">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="srch-rslt">
                    <h2>Search Results</h2>
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Capacity</th>
                                <th>Max load</th>
                                <th>Max reach</th>
                                <th>Tip load</th>
                                <th>Data Sheet</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php 
								if(isset($product) && !empty($product)){
									foreach($product as $val){
							?>	
									<tr>
										<td data-column="Project Name"><?php echo ucfirst($val->project_name);?></td>
										<td data-column="Capacity"><?php echo $val->capacity.' kg'; ?></td>
										<td data-column="Max load"><?php echo $val->max_load.' kg';?></td>
										<td data-column="Max reach"><?php echo $val->max_reach.' kg';?></td>
										<td data-column="Tip load"><?php echo $val->tip_load.' kg';?></td>
										<td data-column="Data Sheet"><a href="<?php echo site_url('product-detail/'.$val->id);?>" class="btn">view</a></td>
									</tr>
							<?php } } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
	
<?php $this->load->view('include/footer.php');?>



   