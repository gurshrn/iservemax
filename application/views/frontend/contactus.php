<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
?>
	<div class="content-section">
		<div class="cntnt-head-bar">
			<h1>Contact us</h1>
		</div>
		<div class="checkin-out-chart">
			<div class="row">
				<div class="col-md-6">
					<div class="abt-cmpny">
						<h3>about company</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue. Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit. Donec aliquam lobortis rhoncus. Integer ut vestibulum justo. Sed volutpat nulla libero.</p>
						<h3>Contact Information</h3>
						<ul>
							<li>
								<span><i class="fa fa-phone" aria-hidden="true"></i></span>
								<p><a href="tel:1234567890">123-456-7890</a></p>
							</li>
							<li>
								<span><i class="fa fa-envelope" aria-hidden="true"></i></span>
								<p><a href="mailto:info@iservemax.com">info@iservemax.com</a></p>
							</li>
							<li>
								<span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
								<p>500 Terry Francois Street<br>San Francisco, CA 94158</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-6">
					<div class="get-in-touch">
						<h3>get in touch</h3>
						<form id="add-contact-us" action="<?php echo site_url('contact-us');?>" method="POST">
							<div class="form-group">
								<input class="form-control" placeholder="Name" name="name" type="text">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Email" name="email" type="text">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Phone" name="phone" type="text">
							</div>
							<div class="form-group">
								<textarea class="form-control" placeholder="Message" name="message"></textarea>
							</div>
							<input value="Submit" class="btn-sbmit" type="submit">
						</form>
					</div>
				</div>
			</div>
			<div class="map-container">
				<div class="row">
					<div class="col-md-12">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3153.839057195348!2d-122.3893134842277!3d37.770371779760076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808f7fc4fe7ace37%3A0xfa1746dd4faeb818!2s500+Terry+A+Francois+Blvd%2C+San+Francisco%2C+CA+94158%2C+USA!5e0!3m2!1sen!2sin!4v1534751409951" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php $this->load->view('include/footer.php');?>

  
   