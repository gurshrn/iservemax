<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
	
?>
        <div class="content-section">
            <div class="cntnt-head-bar">
                <h1>Create Task</h1>

            </div>
            <div class="checkin-out-chart task-page">
                <div class="srch">
                    <input type="search" placeholder="Search...">
                    <i class="fa fa-search"></i>
                </div>
                <div class="task-full">
                    <div class="task-lft">
                        <div class="usr-slcted">
                            <div class="usr-slct-slctn">
                                <figure><img src="<?php echo site_url(); ?>assets/frontend/images/usr1.jpg" alt="usr1"></figure>
                                <a href="#" title="" class="close_icn"><img src="<?php echo site_url(); ?>assets/frontend/images/close_icn.png" alt="close_icn"></a>
                            </div>
                            <div class="usr-slct-slctn">
                                <figure><img src="<?php echo site_url(); ?>assets/frontend/images/usr1.jpg" alt="usr1"></figure>
                                <a href="#" title="" class="close_icn"><img src="<?php echo site_url(); ?>assets/frontend/images/close_icn.png" alt="close_icn"></a>
                            </div>
                            <div class="usr-slct-slctn">
                                <figure><img src="<?php echo site_url(); ?>assets/frontend/images/usr1.jpg" alt="usr1"></figure>
                                <a href="#" title="" class="close_icn"><img src="<?php echo site_url(); ?>assets/frontend/images/close_icn.png" alt="close_icn"></a>
                            </div>
                            <div class="usr-slct-slctn">
                                <figure><img src="<?php echo site_url(); ?>assets/frontend/images/usr1.jpg" alt="usr1"></figure>
                                <a href="#" title="" class="close_icn"><img src="<?php echo site_url(); ?>assets/frontend/images/close_icn.png" alt="close_icn"></a>
                            </div>
                        </div>
                        <div class="usr-list">
                            <div class="usr-chk">

                                <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value2" checked>
                                <figure><img src="<?php echo site_url(); ?>assets/frontend/images/usr1.jpg" alt="usr1"></figure>
                                <label for="styled-checkbox-1">Vicky Nbh</label>
                            </div>
                            <div class="usr-chk">

                                <input class="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" checked>
                                <figure><img src="<?php echo site_url(); ?>assets/frontend/images/usr1.jpg" alt="usr1"></figure>
                                <label for="styled-checkbox-2">Vicky Nbh</label>
                            </div>
                            <div class="usr-chk">

                                <input class="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value2" checked>
                                <figure><img src="<?php echo site_url(); ?>assets/frontend/images/usr1.jpg" alt="usr1"></figure>
                                <label for="styled-checkbox-3">Vicky Nbh</label>
                            </div>
                            <div class="usr-chk">

                                <input class="styled-checkbox" id="styled-checkbox-4" type="checkbox" value="value2" checked>
                                <figure><img src="<?php echo site_url(); ?>assets/frontend/images/usr1.jpg" alt="usr1"></figure>
                                <label for="styled-checkbox-4">Vicky Nbh</label>
                            </div>
                            <div class="usr-chk">

                                <input class="styled-checkbox" id="styled-checkbox-5" type="checkbox" value="value2">
                                <figure><img src="<?php echo site_url(); ?>assets/frontend/images/usr1.jpg" alt="usr1"></figure>
                                <label for="styled-checkbox-5">Vicky Nbh</label>
                            </div>
                            <div class="usr-chk">

                                <input class="styled-checkbox" id="styled-checkbox-6" type="checkbox" value="value2">
                                <figure><img src="<?php echo site_url(); ?>assets/frontend/images/usr1.jpg" alt="usr1"></figure>
                                <label for="styled-checkbox-6">Vicky Nbh</label>
                            </div>
                        </div>
                    </div><!-- task-lft end -->
                    <div class="task-rght-main">
                         <h1>Pending Task</h1>
                         <div class="task-rght">
                        <table class="tbl-view">
                            <thead>
                             <tr>
                                 <th style="width:142px;">S no</th>
                                  <th style="width:337px;">Title Name</th>
                                  <th style="width:238px;">workers Assigned</th>
                             </tr>
                            </thead>
                            <tbody>
                             <tr>
                                 <td data-column="S no">1</td>
                                 <td data-column="Title Name">Lorem ipsum dolor sit amet</td>
                                 <td data-column="Workers Assigned">John Smith, Vicky Nbh, Jemes</td>
                                 
                             </tr>
                                <tr>
                                 <td data-column="S no">2</td>
                                 <td data-column="Title Name">Lorem ipsum dolor sit amet</td>
                                 <td data-column="Workers Assigned">John Smith, Vicky Nbh, Jemes</td>
                                 
                             </tr>
                                <tr>
                                 <td data-column="S no">3</td>
                                 <td data-column="Title Name">Lorem ipsum dolor sit amet</td>
                                 <td data-column="Workers Assigned">John Smith, Vicky Nbh, Jemes</td>
                                 
                             </tr>
                                <tr>
                                 <td data-column="S no">4</td>
                                 <td data-column="Title Name">Lorem ipsum dolor sit amet</td>
                                 <td data-column="Workers Assigned">John Smith, Vicky Nbh, Jemes</td>
                                 
                             </tr>
                                <tr>
                                 <td data-column="S no">5</td>
                                 <td data-column="Title Name">Lorem ipsum dolor sit amet</td>
                                 <td data-column="Workers Assigned">John Smith, Vicky Nbh, Jemes</td>
                                 
                             </tr>
                                <tr>
                                 <td data-column="S no">6</td>
                                 <td data-column="Title Name">Lorem ipsum dolor sit amet</td>
                                 <td data-column="Workers Assigned">John Smith, Vicky Nbh, Jemes</td>
                                 
                             </tr>
                                <tr>
                                 <td data-column="S no">7</td>
                                 <td data-column="Title Name">Lorem ipsum dolor sit amet</td>
                                 <td data-column="Workers Assigned">John Smith, Vicky Nbh, Jemes</td>
                                 
                             </tr>
                                <tr><td colspan="3" style="text-align:right;"><a href="#" title="" class="btn">Add Member</a></td></tr>
                            </tbody>
                        </table>
                       
                        </div><!-- task-rght end -->
                    </div><!-- task-rght-main end -->
                </div>

            </div>

        </div>
    </section>

<?php $this->load->view('include/footer.php');?>
    <!-------------------------------------- All Modals starts here ------------------------------------------->
    <div class="modal fade" id="c-history" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">components history</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Duration</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="asset-history" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Asset history</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Duration</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="unsigned" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Un - Signed</h2>
                </div>
                <div class="modal-body">
                    <form>

                        <div class="form-group">
                            <textarea class="form-control" placeholder="Message"></textarea>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="signed" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Signed</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="chs_fle">
                            <input name="r" class="file" type="file">
                            <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                <input class="brws_inpt form_custom_control form-control" placeholder="Upload Image" type="text"> <span class="input-group-btn"> 
									</span> </p>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="solved" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Solved</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <div class="chs_fle">
                                <input name="r" class="file" type="file">
                                <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    <input class="brws_inpt form_custom_control form-control" placeholder="Attach: photo, text, video, voice note" type="text"> <span class="input-group-btn"> 
									</span> </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Message"></textarea>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="t-list" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Task list</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view asset-tbl">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Task</th>
                                <th>Worker</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add-expense" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Add expenses</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <select class="form-control">
                            <option>Select Expense Type</option>
                            <option>Select Expense Type1</option>
                            <option>Select Expense Type2</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control">
                            <option>Expense Name</option>
                            <option>Expense Name1</option>
                            <option>Expense Name2</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group">
                            <div class="chs_fle">
                                <input name="r" class="file" type="file">
                                <p>
                                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    <input class="brws_inpt form_custom_control form-control" placeholder="Select Images" type="text"> <span class="input-group-btn"></span>
                                </p>
                            </div>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="components" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">components</h2>
                </div>
                <div class="modal-body">
                    <div class="comp-div">
                        <figure><img src="images/component-img.jpg"></figure>
                        <div class="labels-component">
                            <p><strong>Name : </strong>Abc</p>
                            <p><strong>Part Number : </strong>12232345</p>
                            <p><strong>Serial No : </strong>123132579565693</p>
                        </div>
                        <p><strong>Description : </strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

 