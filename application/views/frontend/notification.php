<?php 	
	$this->load->view('include/header.php');	
	$this->load->view('include/sidebar.php');	
?>

        <div class="content-section">

            <div class="cntnt-head-bar">

                <h1>Notifications</h1>

                <div class="srch">

                    <input type="search" placeholder="Search...">

                    <i class="fa fa-search"></i>

                </div>

            </div>

            <div class="checkin-out-chart">
				
				<?php 
					if(isset($projectTask) && !empty($projectTask)){ 
						foreach($projectTask as $val){
				
				?>
							<div class="notification-bar">

								<div class="nf-bar">

									<h3>Added a new task in <span><?php echo ucfirst($val->project_name);?></span></h3>

									<h6>10 min ago</h6>

								</div>

								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. </p>

							</div>
				
				
				<?php } } ?>
                

            </div>

        </div>

    </section>
	
<?php $this->load->view('include/footer.php');?>



   
   