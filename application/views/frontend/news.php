<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
?>
        <div class="content-section">
            <div class="cntnt-head-bar">
                <h1>News</h1>
				
                <!--div class="srch">
                    <input type="search" placeholder="Search...">
                    <i class="fa fa-search"></i>
                </div-->
            </div>
            <div class="checkin-out-chart">
				<?php 
					if(isset($newsdetail) && !empty($newsdetail)){ 
						foreach($newsdetail as $val){
							$string = substr($val->description, 0, 400);
				?>
						<div class="news-section">
							<figure><img src="<?php echo site_url('assets/upload/news/'.$val->image);?>" alt="news-image"></figure>
							<div class="news-description">
								<h3><?php echo ucfirst($val->news_title);?></h3>
								<p><?php echo ucfirst($string);?></p>
								<a href="<?php echo site_url('news-detail/'.$val->id);?>">Read More</a>
							</div>
						</div>
				<?php } } ?>
            </div>
        </div>
    </section>
	
<?php $this->load->view('include/footer.php');?>

   