<?php $this->load->view('include/header.php');?>

<body>
    <div class="pk-loader">
        <svg id="triangle" viewBox="-3 -4 39 39">
             <polygon fill="#EFEFEF" stroke="#555" stroke-width="1" points="16,0 32,32 0,32"></polygon>
        </svg>
        <div class="loader-text">© 2018 iServe Max.</div>
    </div>
    <section class="Cover-bg" style="background: url(<?php echo site_url(); ?>assets/frontend/images/bg-img.jpg);">
        <div class="login-window">
            <a class="logo" href="#"><img src="<?php echo site_url(); ?>assets/frontend/images/logo.png" alt="logo"></a>
            <h2>Login</h2>
            <form id="user-login" method="POST" action="<?php echo site_url('login');?>">
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="Username" name="username">
                </div>
                <div class="form-group">
                    <input class="form-control" type="password" placeholder="Password" name="password">
                </div>
                <p><a class="frgt-pwd" href="#">forgot password?</a></p>
                <input class="sbmt-btn" type="submit" value="login">
            </form>
        </div>
        <div class="footer">
            <p>By Signing in you are agreeing to the <a href="#">Terms & Conditions</a> and <a href="#">Privacy Policy</a></p>
            <p class="copyright-txt">© 2018 iServe Max. All rights reserved.</p>
        </div>
    </section>

<?php $this->load->view('include/footer.php');?>


