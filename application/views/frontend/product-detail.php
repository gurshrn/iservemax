<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
?>
	<div class="content-section">
		<div class="cntnt-head-bar">
			<h1>Product</h1>
			<!--div class="srch">
				<input type="search" placeholder="Search...">
				<i class="fa fa-search"></i>
			</div-->
		</div>
		<div class="checkin-out-chart product-details-sec">
			<div class="img-info-block">
				<div class="row">
					<div class="col-md-3">
						<figure><img src="<?php echo site_url('assets/upload/product/'.$product->image);?>" alt="asset-img"></figure>
					</div>
					<div class="col-md-9">
						<div class="asset-info">
							<p><label>Project Name :</label><span><?php echo ucfirst($product->project_name);?></span></p>
							<p><label>Capacity :  </label><span><?php echo $product->capacity.' kg';?></span></p>
							<p><label>Max Load : </label><span><?php echo $product->max_load.' kg';?></span></p>
							<p><label>Max Reach :</label><span><?php echo $product->max_reach.' kg';?></span></p>
							<p><label>Tip Load :</label><span><?php echo $product->tip_load.' kg';?></span></p>
							<p><label>Description :</label><span><?php echo ucfirst($product->description);?></span></p>
							<a href="#" class="btn">inquiry</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

<?php $this->load->view('include/footer.php');?>


 
   
    
    
  
   

  