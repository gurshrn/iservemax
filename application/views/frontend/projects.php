<?php $this->load->view('include/header.php');?>
<?php $this->load->view('include/sidebar.php');?>

        <div class="content-section">
            <div class="cntnt-head-bar">
                <h1>Projects</h1>
                <div class="srch">
                    <input type="search" placeholder="Search...">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            <div class="checkin-out-chart">
                <table class="tbl-view">
                    <thead>
                        <tr>
                            <th>Project Name</th>
                            <th>Team Member</th>
                            <th>Complaints Tasks</th>
                            <th>Total Tasks</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php   if(isset($getProjectDetail) && !empty($getProjectDetail)) { 
                                    foreach($getProjectDetail as $val){
                        ?>
                            <tr>
                                <td data-column="Project Name"><?php echo ucfirst($val->project_name);?></td>
                                <td data-column="Team Member"></td>
                                <td data-column="Complaints Tasks"></td>
                                <td data-column="Total Tasks"><?php echo $val->totaltask;?></td>
                                <td data-column="Actions">
                                    <a href="<?php echo site_url('project-detail/'.$val->projectRefId);?>" class="viewbtn"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </td>
                            </tr>

                        <?php  }} ?>
                    </tbody>
                </table>
            </div>
            <div class="pagination">
                <img src="images/pagination.jpg">
            </div>
        </div>
    </section>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <?php $this->load->view('include/footer.php');?>
    <!-------------------------------------- All Modals starts here ------------------------------------------->
    <div class="modal fade" id="c-history" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">components history</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Duration</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="asset-history" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Asset history</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Duration</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="unsigned" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Un - Signed</h2>
                </div>
                <div class="modal-body">
                    <form>

                        <div class="form-group">
                            <textarea class="form-control" placeholder="Message"></textarea>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="signed" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Signed</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="chs_fle">
                            <input name="r" class="file" type="file">
                            <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                <input class="brws_inpt form_custom_control form-control" placeholder="Upload Image" type="text"> <span class="input-group-btn"> 
									</span> </p>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="solved" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Solved</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <div class="chs_fle">
                                <input name="r" class="file" type="file">
                                <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    <input class="brws_inpt form_custom_control form-control" placeholder="Attach: photo, text, video, voice note" type="text"> <span class="input-group-btn"> 
									</span> </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Message"></textarea>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="t-list" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Task list</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view asset-tbl">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Task</th>
                                <th>Worker</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Task">lorem ipsum dolar</td>
                                <td data-column="Worker">John, Smith, James</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add-expense" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Add expenses</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <select class="form-control">
                            <option>Select Expense Type</option>
                            <option>Select Expense Type1</option>
                            <option>Select Expense Type2</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control">
                            <option>Expense Name</option>
                            <option>Expense Name1</option>
                            <option>Expense Name2</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group">
                            <div class="chs_fle">
                                <input name="r" class="file" type="file">
                                <p>
                                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    <input class="brws_inpt form_custom_control form-control" placeholder="Select Images" type="text"> <span class="input-group-btn"></span>
                                </p>
                            </div>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="components" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">components</h2>
                </div>
                <div class="modal-body">
                    <div class="comp-div">
                        <figure><img src="images/component-img.jpg"></figure>
                        <div class="labels-component">
                            <p><strong>Name : </strong>Abc</p>
                            <p><strong>Part Number : </strong>12232345</p>
                            <p><strong>Serial No : </strong>123132579565693</p>
                        </div>
                        <p><strong>Description : </strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

   