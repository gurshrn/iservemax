<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
?>
	<div class="content-section">
		<div class="cntnt-head-bar">
			<h1>Profile</h1>
		</div>
		<div class="checkin-out-chart product-details-sec">
			<div class="img-info-block">
				<a class="edit-profile" href="<?php echo site_url('edit-profile/'.$userdetail->userRefId);?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
				<div class="row">
					<div class="col-md-3">
						<figure><img src="<?php if($userdetail->image != ''){ echo site_url('assets/upload/profile/'.$userdetail->image);}else{ echo site_url('assets/frontend/images/dummy.png');}?>" alt="profile-img"></figure>
					</div>
					<div class="col-md-9">
						<div class="asset-info">
							<p><label>Name :</label><span><?php echo ucfirst($userdetail->first_name).' '.$userdetail->last_name;?></span></p>
							<p><label>Phone No:</label><span><?php echo $userdetail->phone_number;?></span></p>
							<p><label>DOB:</label><span><?php echo date('d/M/Y', strtotime($userdetail->dob));?></span></p>
							<p><label>Email:</label><span><?php echo $userdetail->email;?></span></p>
							<p><label>Address:</label><span><?php echo ucfirst($userdetail->address);?></span></p>
							<p><label>Gender:</label><span><?php echo $userdetail->gender;?></span></p>
							<p><label>Status By</label><span><?php if($userdetail->status == 0){ echo 'Active';}else{ echo 'Not Active';}?></span></p>
							<p><label>Description :</label><span><?php echo ucfirst($userdetail->description);?></span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<?php $this->load->view('include/footer.php');?>
   

  