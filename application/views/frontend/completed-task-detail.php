<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
?>

        <div class="content-section">

            <div class="cntnt-head-bar">

                <h1>Completed Task Detail</h1>

                <a class="rght-btn cmplt-btn" href="#" data-toggle="modal" data-target="#solved">Finished</a>

            </div>

            <div class="checkin-out-chart">

                <div class="img-info-block">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="flex-cover">

                                <div class="asset-info">

                                    <p><label>Project Name :</label><span>Abc</span></p>

                                    <p><label>Task Id :</label><span>EM0017</span></p>

                                    <p><label>Task :</label><span>Lorem ipsum dolor</span></p>

                                    <p><label>Asset :</label><span>Bulldozer, Jcb, Truk</span></p>

                                    <p><label>Worker :</label><span>John, Smith, jemes, Vicky Nbh</span></p>

                                </div>

                                <div class="review-rating">

                                    <form>

                                        <ul>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>

                                        </ul>

                                        <h2>4.5</h2>

                                        <h3>Review Rating by Client</h3>

                                        <input type="submit" value="submit">

                                    </form>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="additional-info">

                    <h2>additional information<i class="fa fa-caret-down"></i></h2>

                   <table class="tbl-view asset-tbl info-list">

                    <thead>

                        <tr>

                            <th>File Name</th>

                            <th>Image</th>

                            <th>Video</th>

                            <th>Pdf</th>

                        </tr>

                    </thead>

                    <tbody>

                        <tr>

                            <td data-column="File Name">Abc</td>

                            <td data-column="Image"><img src="images/person-img.jpg"></td>

                            <td data-column="Video"><img src="images/video-file.jpg"></td>

                            <td data-column="Pdf">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>

                        </tr>

                        <tr>

                            <td data-column="File Name">Abc</td>

                            <td data-column="Image"><img src="images/person-img.jpg"></td>

                            <td data-column="Video"><img src="images/video-file.jpg"></td>

                            <td data-column="Pdf">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>

                        </tr>

                    </tbody>

                </table>

                </div>

                

                <div class="ratings-boxes">

                    <div class="box-rating">

                        <table>

                            <thead>

                                <tr>

                                    <th>Worker</th>

                                    <th>Image</th>

                                    <th>Rating</th>

                                    <th>&nbsp;</th>

                                </tr>

                            </thead>

                            <tbody>

                                <tr>

                                    <td>john smith</td>

                                    <td><img src="images/worker-image.jpg"></td>

                                    <td>

                                        <ul>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>

                                        </ul>

                                        <span>4.5</span>

                                    </td>

                                    <td><a href="#"><svg><use xlink:href="#enter"></use></svg></a></td>

                                </tr>

                                <tr>

                                    <td>john smith</td>

                                    <td><img src="images/worker-image.jpg"></td>

                                    <td>

                                        <ul>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>

                                        </ul>

                                        <span>4.5</span>

                                    </td>

                                    <td><a href="#"><svg><use xlink:href="#enter"></use></svg></a></td>

                                </tr>

                                <tr>

                                    <td>john smith</td>

                                    <td><img src="images/worker-image.jpg"></td>

                                    <td>

                                        <ul>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>

                                        </ul>

                                        <span>4.5</span>

                                    </td>

                                    <td><a href="#"><svg><use xlink:href="#enter"></use></svg></a></td>

                                </tr>

                                <tr>

                                    <td>john smith</td>

                                    <td><img src="images/worker-image.jpg"></td>

                                    <td>

                                        <ul>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>

                                        </ul>

                                        <span>4.5</span>

                                    </td>

                                    <td><a href="#"><svg><use xlink:href="#enter"></use></svg></a></td>

                                </tr>

                            </tbody>

                        </table>

                    </div>

                    <div class="box-rating">

                        <table>

                            <thead>

                                <tr>

                                    <th>Worker</th>

                                    <th>Image</th>

                                    <th>Rating</th>

                                    <th>&nbsp;</th>

                                </tr>

                            </thead>

                            <tbody>

                                <tr>

                                    <td>john smith</td>

                                    <td><img src="images/worker-image.jpg"></td>

                                    <td>

                                        <ul>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>

                                        </ul>

                                        <span>4.5</span>

                                    </td>

                                    <td><a href="#"><svg><use xlink:href="#enter"></use></svg></a></td>

                                </tr>

                                <tr>

                                    <td>john smith</td>

                                    <td><img src="images/worker-image.jpg"></td>

                                    <td>

                                        <ul>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>

                                        </ul>

                                        <span>4.5</span>

                                    </td>

                                    <td><a href="#"><svg><use xlink:href="#enter"></use></svg></a></td>

                                </tr>

                                <tr>

                                    <td>john smith</td>

                                    <td><img src="images/worker-image.jpg"></td>

                                    <td>

                                        <ul>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>

                                        </ul>

                                        <span>4.5</span>

                                    </td>

                                    <td><a href="#"><svg><use xlink:href="#enter"></use></svg></a></td>

                                </tr>

                                <tr>

                                    <td>john smith</td>

                                    <td><img src="images/worker-image.jpg"></td>

                                    <td>

                                        <ul>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>

                                            <li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>

                                        </ul>

                                        <span>4.5</span>

                                    </td>

                                    <td><a href="#"><svg><use xlink:href="#enter"></use></svg></a></td>

                                </tr>

                            </tbody>

                        </table>

                    </div>

                </div>

                <div class="comments">
				<h2>comments</h2>								
				<div id="commentDiv">
					<?php 
						$commentDetail = getCommentDetail($complaintDetail[0]->id,'complaint');
						if(!empty($commentDetail))
						{
							foreach($commentDetail as $val){
					?>
							<div class="cmnt-block btm-0">
								<h3><?php echo ucfirst($val->first_name).' '.$val->last_name;?></h3>
								<p><?php echo ucfirst($val->comment);?></p>
								<figure><img src="<?php echo site_url('assets/upload/comment/'.$val->attachment);?>" height="100" width="100"></figure>
							</div>
					<?php } } ?>								
				</div>								
				<form id="add-comment" action="<?php echo site_url('add-comment');?>" method="POST" enctype="multipart/form-data" autocomplete="off">
									
				<div class="txtarea_sctn">						
				<input type="hidden" name="type" value="complaint">
				<input type="hidden" name="refId" value="<?php echo $complaintDetail[0]->id;?>">	
															
				<textarea placeholder="Enter Your Comment" name="comment"></textarea>
										
				<input class="brws_inpt form_custom_control form-control" placeholder="Upload Image" type="file" name="attachment"> 								
										
				<div class="snd-btn">							
				<input type="submit" value="send" class="btn addCommentBtn">
				</div>
									
				</div>								
				</form>
			</div>

            </div>

        </div>

    </section>


<?php $this->load->view('include/footer.php');?>
    

    <!-------------------------------------- All Modals starts here ------------------------------------------->

    
    
    



    
    <div class="modal fade" id="solved" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

        <div class="modal-dialog">

            <div class="modal-content">

                <div class="modal-header">

                    <h2 class="modal-title">Completed</h2>

                </div>

                <div class="modal-body">

                    <form>

                        <div class="form-group">

                            <div class="chs_fle">

                                <input name="r" class="file" type="file">

                                <p> <i class="fa fa-paperclip" aria-hidden="true"></i>

                                    <input class="brws_inpt form_custom_control form-control" placeholder="Attach: photo, text, video, voice note" type="text"> <span class="input-group-btn"> 

									</span> </p>

                            </div>

                        </div>

                        <div class="form-group">

                            <textarea class="form-control" placeholder="Message"></textarea>

                        </div>

                        <input type="submit" value="submit" class="btn-sbmit">

                    </form>

                </div>

            </div>

        </div>

    </div>

    
  