<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
	$role = $this->session->userdata('role');
?>
	<div class="content-section">
		<div class="cntnt-head-bar">
			<h1>Complaint</h1>
			<?php if($role == 1 && $complaintDetail[0]->status == 1){?>
                <a class="rght-btn cmplt-btn completeComplaint" href="javscript:void(0)">Complete</a>
				<a class="rght-btn notCompleteModal" href="javscript:void(0)">No Complete</a>
			<?php }?>
			<?php if($role == 3 && $complaintDetail[0]->status == 0){?>
				<a class="rght-btn solvedComplaint" href="javascript:void(0)">Solved</a>
			<?php }?>
		</div>
		<div class="checkin-out-chart">
			<div class="img-info-block">
				<div class="row">

					<div class="col-md-12">
						<div class="asset-info">
						
							<p><label>Project Name :</label><span><?php echo ucfirst($complaintDetail[0]->project_name);?></span></p>
							<p><label>Complaint No. :</label><span><?php echo $complaintDetail[0]->complaint_number;?></span></p>
                            <?php if($complaintDetail[0]->assetRefId != '') { ?>
							     
                                 <p><label>Asset :</label><span><?php echo ucfirst($complaintDetail[0]->asset);?></span></p>

                                  <p><label>Serial No. :</label><span><?php echo $complaintDetail[0]->serial_no;?></span></p>
                            <?php } ?>
                            <?php if($complaintDetail[0]->taskRefId != '') { ?>
                                 
                                 <p><label>Task :</label><span><?php echo ucfirst($complaintDetail[0]->task_subject);?></span></p>

                                 <p><label>Task ID :</label><span><?php echo $complaintDetail[0]->taskId;?></span></p>

                            <?php } ?>
							
						</div>
					</div>
				</div>
			</div>
			<div class="comments">
				<h2>comments</h2>								
				<div id="commentDiv">
					<?php 
						$commentDetail = getCommentDetail($complaintDetail[0]->id,'complaint');
						if(!empty($commentDetail))
						{
							foreach($commentDetail as $val){
					?>
							<div class="cmnt-block btm-0">
								<h3><?php echo ucfirst($val->first_name).' '.$val->last_name;?></h3>
								<p><?php echo ucfirst($val->comment);?></p>
								<figure><img src="<?php echo site_url('assets/upload/comment/'.$val->attachment);?>" height="100" width="100"></figure>
							</div>
					<?php } } ?>								
				</div>								
				<form id="add-comment" action="<?php echo site_url('add-comment');?>" method="POST" enctype="multipart/form-data" autocomplete="off">
									
				<div class="txtarea_sctn">						
				<input type="hidden" name="type" value="complaint">
				<input type="hidden" name="refId" value="<?php echo $complaintDetail[0]->id;?>">	
															
				<textarea placeholder="Enter Your Comment" name="comment"></textarea>
										
				<input class="brws_inpt form_custom_control form-control" placeholder="Upload Image" type="file" name="attachment"> 								
										
				<div class="snd-btn">							
				<input type="submit" value="send" class="btn addCommentBtn">
				</div>
									
				</div>								
				</form>
			</div>
		</div>
	</div>
</section>

<?php $this->load->view('include/footer.php');?>
    <!-------------------------------------- All Modals starts here ------------------------------------------->
    <div class="modal fade" id="c-history" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">components history</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Duration</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="asset-history" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Asset history</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Duration</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="unsigned" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Un - Signed</h2>
                </div>
                <div class="modal-body">
                    <form>

                        <div class="form-group">
                            <textarea class="form-control" placeholder="Message"></textarea>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="signed" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Signed</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="chs_fle">
                            <input name="r" class="file" type="file">
                            <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                <input class="brws_inpt form_custom_control form-control" placeholder="Upload Image" type="text"> <span class="input-group-btn"> 
									</span> </p>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="solved" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Solved</h2>
                </div>
                <div class="modal-body">
                    <form id="complete-complaint" method="post" action="<?php echo site_url('complete-complaint');?>" enctype="multipart/form-data">
                        <?php $userBy = $this->session->userdata('userRefId');?>
						<input type="hidden" name="status" value="1">
						<input type="hidden" name="complaint_number" value="<?php echo $complaintDetail[0]->complaint_number;?>">
						<input type="hidden" name="userToRefId" value="<?php echo $complaintDetail[0]->addedby;?>">
						<input type="hidden" name="userByRefId" value="<?php echo $userBy;?>">
						
						<div class="form-group">
                            <div class="chs_fle">
                                <input name="r" class="file" type="file">
                                <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    <input class="brws_inpt form_custom_control form-control" placeholder="Attach: photo, text, video, voice note" type="text" name="document"> <span class="input-group-btn"> 
									</span> </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Message" name="message"></textarea>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
	<div class="modal fade" id="completed" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Solved</h2>
                </div>
                <div class="modal-body">
                    <form id="complete-solved-complaint" method="post" action="<?php echo site_url('complete-complaint');?>" enctype="multipart/form-data">
                        <input type="hidden" name="status" value="2">
                        <input type="hidden" name="complaint_number" value="<?php echo $complaintDetail[0]->complaint_number;?>">
						<input type="hidden" name="userByRefId" value="<?php echo $complaintDetail[0]->addedby;?>">
						<input type="hidden" name="userToRefId" value="<?php echo $complaintDetail[0]->complaint_assigned_to;?>">
						<div class="form-group">
                            <div class="chs_fle">
                                <input name="r" class="file" type="file">
                                <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    <input class="brws_inpt form_custom_control form-control" placeholder="Attach: photo, text, video, voice note" type="text" name="document"> <span class="input-group-btn"> 
									</span> </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Message" name="message"></textarea>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
     <div class="modal fade" id="not-complete" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Not Complete</h2>
                </div>
                <div class="modal-body">
                     <form id="not-complete-solved-complaint" method="post" action="<?php echo site_url('not-complete-complaint');?>">
                    <input type="hidden" class="complaint-number" name="complaint_number" value="<?php echo $complaintDetail[0]->complaint_number;?>">
                    <input type="button" class="yesNotCompleteModal" value="Yes" class="btn-sbmit">
                </div>
            </div>
        </div>
    </div>
   