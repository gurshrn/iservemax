<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
	
	$ownerInfo = unserialize($projectDetail->owner_information);
	$contactInfo = unserialize($projectDetail->contact_information);
?>


        <div class="content-section">
            <div class="cntnt-head-bar">
                <h1>Projects</h1>
                <a class="rght-btn viewtask" href="javascript:void(0)" data-target="<?php echo $projectDetail->projectRefId; ?>">View all task</a>
            </div>
			<div class="checkin-out-chart">
                <div class="img-info-block">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="info-list">
                                <li><label>Project Name : </label><span><?php echo ucfirst($projectDetail->project_name);?></span></li>
                                <li><label>Demographic Information : </label><span><?php echo ucfirst($projectDetail->demographic_information);?></span></li>
                                <li><label>Project Information :</label><span><?php echo ucfirst($projectDetail->project_information);?></span></li>
                                <li><label>Location :</label><span><?php echo ucfirst($projectDetail->project_location);?></span></li>
                                <li><label>Project Requirements :</label><span><?php echo ucfirst($projectDetail->project_requirements);?></span></li>
                                <li><label>Phone :</label><span><?php echo $ownerInfo['owner_phone'];?></span></li>
                                <li><label>Name :</label><span><?php echo ucfirst($ownerInfo['owner_name']);?></span></li>
                                <li><label>Specification :</label><span><?php echo ucfirst($projectDetail->specification);?></span></li>
                                <li><label>Designation :</label><span><?php echo ucfirst($ownerInfo['owner_designation']);?></span></li>
                                <li><label>Required Field Visit :</label><span><?php echo $projectDetail->scheduled_visit?></span></li>
                                <li><label>Required Quotation </label><span><?php echo $projectDetail->required_quotation?></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="additional-info">
                    <h2>additional information<i class="fa fa-caret-down"></i></h2>
                    <table class="tbl-view asset-tbl info-list">
						<thead>
							<tr>
								<th>File Name</th>
								<th>Image</th>
								<th>Video</th>
								<th>Pdf</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td data-column="File Name">Abc</td>
								<td data-column="Image"><img src="<?php echo site_url(); ?>assets/frontend/images/person-img.jpg"></td>
								<td data-column="Video"><img src="<?php echo site_url(); ?>assets/frontend/images/video-file.jpg"></td>
								<td data-column="Pdf">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
							</tr>
							<tr>
								<td data-column="File Name">Abc</td>
								<td data-column="Image"><img src="<?php echo site_url(); ?>assets/frontend/images/person-img.jpg';?>"></td>
								<td data-column="Video"><img src="<?php echo site_url(); ?>assets/frontend/images/video-file.jpg"></td>
								<td data-column="Pdf">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
							</tr>
						</tbody>
					</table>
                </div>
                
                <div class="cntct-info">
                    <h2>Contact information<i class="fa fa-caret-down"></i></h2>
					<div class="info-wrap">
						<?php if(!empty($contactInfo)){
								foreach($contactInfo as $vals){
						?>
								<ul class="info-list">
									<li><label>Name : </label><span><?php echo ucfirst($vals['contact_name']);?></span></li>
									<li><label>Designation : </label><span><?php echo ucfirst($vals['contact_designation']);?></span></li>
									<li><label>Phone : </label><span><?php echo $vals['contact_phone'];?></span></li>
								</ul>
						<?php } }?>
					</div>
                    
                </div>
                <div class="comments">					
					<h2>comments</h2>													
					<div id="commentDiv">						
						<?php 							
							$commentDetail = getCommentDetail($projectDetail->projectRefId,'project');							
							if(!empty($commentDetail))							
							{								
								foreach($commentDetail as $val){						
						?>								
									<div class="cmnt-block btm-0">									
										<h3><?php echo ucfirst($val->first_name).' '.$val->last_name;?></h3>									
										<p><?php echo ucfirst($val->comment);?></p>									
										<figure><img src="<?php echo site_url('assets/upload/comment/'.$val->attachment);?>" height="100" width="100"></figure>								
									</div>						
						<?php } } ?>													
					</div>													
					<form id="add-comment" action="<?php echo site_url('add-comment');?>" method="POST" enctype="multipart/form-data" autocomplete="off">																
						<div class="txtarea_sctn">													
							<input type="hidden" name="type" value="project">							
							<input type="hidden" name="refId" value="<?php echo $projectDetail->projectRefId;?>">																								
							<textarea placeholder="Enter Your Comment" name="comment"></textarea>																		
							<input class="brws_inpt form_custom_control form-control" placeholder="Upload Image" type="file" name="attachment"> 																										
							<div class="snd-btn">															
								<input type="submit" value="send" class="btn addCommentBtn">							
							</div>																
						</div>													
					</form>				
				</div>
            </div>
        </div>
    </section>

      <?php $this->load->view('include/footer.php');?>
    <!-------------------------------------- All Modals starts here ------------------------------------------->
    
    <div class="modal fade" id="asset-history" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Asset history</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Duration</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="unsigned" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Un - Signed</h2>
                </div>
                <div class="modal-body">
                    <form>

                        <div class="form-group">
                            <textarea class="form-control" placeholder="Message"></textarea>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="signed" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Signed</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="chs_fle">
                            <input name="r" class="file" type="file">
                            <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                <input class="brws_inpt form_custom_control form-control" placeholder="Upload Image" type="text"> <span class="input-group-btn"> 
									</span> </p>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="solved" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Solved</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <div class="chs_fle">
                                <input name="r" class="file" type="file">
                                <p> <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    <input class="brws_inpt form_custom_control form-control" placeholder="Attach: photo, text, video, voice note" type="text"> <span class="input-group-btn"> 
									</span> </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Message"></textarea>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="t-list" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Task list</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view asset-tbl">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Task</th>
                                <th>Worker</th>
                            </tr>
                        </thead>
                        <tbody class="project-task">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add-expense" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Add expenses</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <select class="form-control">
                            <option>Select Expense Type</option>
                            <option>Select Expense Type1</option>
                            <option>Select Expense Type2</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control">
                            <option>Expense Name</option>
                            <option>Expense Name1</option>
                            <option>Expense Name2</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group">
                            <div class="chs_fle">
                                <input name="r" class="file" type="file">
                                <p>
                                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    <input class="brws_inpt form_custom_control form-control" placeholder="Select Images" type="text"> <span class="input-group-btn"></span>
                                </p>
                            </div>
                        </div>
                        <input type="submit" value="submit" class="btn-sbmit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="components" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">components</h2>
                </div>
                <div class="modal-body">
                    <div class="comp-div">
                        <figure><img src="images/component-img.jpg"></figure>
                        <div class="labels-component">
                            <p><strong>Name : </strong>Abc</p>
                            <p><strong>Part Number : </strong>12232345</p>
                            <p><strong>Serial No : </strong>123132579565693</p>
                        </div>
                        <p><strong>Description : </strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

