<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
	
	$additionalInfo = unserialize($assetDetail->additional_information);
	
?>
        <div class="content-section">
            <div class="cntnt-head-bar">
                <h1>Assest</h1>
                <a class="rght-btn view-asset-history" href="javascript:void(0)" data-target="<?php echo $assetDetail->assetRefId;?>">view history</a>
				<?php //echo "<pre>"; print_r($additionalInfo);?>
            </div>
            <div class="checkin-out-chart">
                <div class="img-info-block">
                    <div class="row">
                        <div class="col-md-3">
                            <figure><img src="<?php echo site_url('assets/upload/asset/'.$assetDetail->asset_image); ?>" alt="asset-img"></figure>
                        </div>
                        <div class="col-md-9">
                            <div class="asset-info">
                                <p><label>Project Name :</label><span><?php echo ucfirst($assetDetail->project_name);?></span></p>
                                <p><label>Asset No. :  </label><span><?php echo $assetDetail->asset_no;?></span></p>
                                <p><label>Asset :</label><span><?php echo ucfirst($assetDetail->asset);?></span></p>
                                <p><label>Serial No :</label><span><?php echo $assetDetail->serial_no;?></span></p>
                                <p><label>Description :</label><span><?php echo ucfirst($assetDetail->asset_description);?></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="additional-info">
                    <h2>additional information<i class="fa fa-caret-down"></i></h2>
                    <ul class="info-list">
                        <li><label>Purchase Date : </label><span><?php echo date('d M Y',strtotime($additionalInfo['purshase_date']));?></span></li>
                        <li><label>Warranty Validity :</label><span><?php echo date('d M Y',strtotime($additionalInfo['warranty_validity']));?></span></li>
                        <li><label>P.O Number :</label><span><?php echo $additionalInfo['po_number'];?></span></li>
                        <li><label>Supplier :</label><span><?php echo ucfirst($additionalInfo['supplier']);?></span></li>
                        <li><label>Inspected by Time & Date  :</label><span><?php echo $additionalInfo['inspected_time'].'/'.date('d M Y',strtotime($additionalInfo['inspected_date']));?></span></li>
                    </ul>										<table class="tbl1-view asset-tbl info-list">                    <thead>                        <tr>                            <th>File Name</th>                            <th>Image</th>                            <th>Video</th>                            <th>Pdf</th>                        </tr>                    </thead>                    <tbody>                        <tr>                            <td data-column="File Name">Abc</td>                            <td data-column="Image"><img src="<?php echo site_url(); ?>assets/frontend/images/person-img.jpg"></td>                            <td data-column="Video"><img src="<?php echo site_url(); ?>assets/frontend/images/video-file.jpg"></td>                            <td data-column="Pdf">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>                        </tr>                        <tr>                            <td data-column="File Name">Abc</td>                            <td data-column="Image"><img src="<?php echo site_url(); ?>assets/frontend/images/person-img.jpg"></td>                            <td data-column="Video"><img src="<?php echo site_url(); ?>assets/frontend/images/video-file.jpg"></td>                            <td data-column="Pdf">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>                        </tr>                    </tbody>                </table>
                </div>
                
                <div class="pdf-sheet">
                    <h2>Pdf Datasheet<i class="fa fa-caret-down"></i></h2>
                    <ul class="sheets">
                        <li>Services Name <a href="#">View</a></li>
                        <li>Maintenance Manual <a href="#">View</a></li>
                    </ul>
                </div>
                <div class="components-sec">
                    <h2>components<i class="fa fa-caret-down"></i></h2>
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Components Code</th>
                                <th>Part number</th>
                                <th>Project Name</th>
                                <th>Current Location</th>
                                <th>serial no</th>
                                <th>View History</th>
                            </tr>
                        </thead>
                        <tbody>													<?php 								$getAssetComponents = getAssetComponents($assetDetail->assetRefId);								if(!empty($getAssetComponents))								{									foreach($getAssetComponents as $val1)									{							?>										<tr>											<td data-column="Name"><a href="javascript:void(0)" data-target="<?php echo $val1->componentRefId;?>" class="componentModal"><?php echo ucfirst($val1->component_name);?></a></td>											<td data-column="Components Code"><?php echo $val1->componentRefId;?></td>											<td data-column="Part number"><?php echo $val1->part_no;?></td>											<td data-column="Project Name"><?php echo ucfirst($val1->project_name);?></td>											<td data-column="Current Location"><?php echo $val1->project_location;?></td>											<td data-column="Serial no"><?php echo $val1->serial_no;?></td>											<td data-column="View History">												<a href="javscript:void(0)" class="viewbtn componentHistory" data-target="<?php echo $val1->componentRefId;?>"><i class="fa fa-eye" aria-hidden="true"></i></a>											</td>										</tr>																	<?php } } ?>
                            						</tbody>					</table>				</div>								<div class="comments">					<h2>comments</h2>													<div id="commentDiv">						<?php 							$commentDetail = getCommentDetail($assetDetail->assetRefId,'asset');							if(!empty($commentDetail))							{								foreach($commentDetail as $val){						?>								<div class="cmnt-block btm-0">									<h3><?php echo ucfirst($val->first_name).' '.$val->last_name;?></h3>									<p><?php echo ucfirst($val->comment);?></p>									<figure><img src="<?php echo site_url('assets/upload/comment/'.$val->attachment);?>" height="100" width="100"></figure>								</div>						<?php } } ?>													</div>													<form id="add-comment" action="<?php echo site_url('add-comment');?>" method="POST" enctype="multipart/form-data" autocomplete="off">																<div class="txtarea_sctn">													<input type="hidden" name="type" value="asset">							<input type="hidden" name="refId" value="<?php echo $assetDetail->assetRefId;?>">																								<textarea placeholder="Enter Your Comment" name="comment"></textarea>																		<input class="brws_inpt form_custom_control form-control" placeholder="Upload Image" type="file" name="attachment"> 																										<div class="snd-btn">															<input type="submit" value="send" class="btn addCommentBtn">							</div>																</div>													</form>				</div>			</div>		</div>	</section>
<?php $this->load->view('include/footer.php');?>
    <!-------------------------------------- All Modals starts here ------------------------------------------->
    <div class="modal fade" id="c-history" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">components history</h2>
                </div>
                <div class="modal-body">
                    <table class="tbl-view">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Duration</th>
                            </tr>
                        </thead>
                        <tbody class="component-history">
                            <tr>
                                <td data-column="Project Name">Abc</td>
                                <td data-column="Duration">05 Aug - 20 Sept</td>
                            </tr>						</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="asset-history" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Asset history</h2>
                </div>
                <div class="modal-body assetHistory">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="components" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">components</h2>
                </div>
                <div class="modal-body">													<div class="comp-div">
                        <figure class="componentImg"></figure>
                        <div class="labels-component">
                            <p><strong>Name : </strong><span class="component-name"></span></p>
                            <p><strong>Part Number : </strong><span class="part-no"></span></p>
                            <p><strong>Serial No : </strong><span class="serial-no"></span></p>
                        </div>
                        <p><strong>Description : </strong><span class="description"></span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

 