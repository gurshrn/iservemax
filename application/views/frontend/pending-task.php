<?php 	
	$this->load->view('include/header.php');	
	$this->load->view('include/sidebar.php');	
	$role = $this->session->userdata('role');?>
        <div class="content-section">
            <div class="cntnt-head-bar">
                <h1>Pending Task</h1>
                <div class="srch">
                    <input type="search" placeholder="Search...">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            <div class="checkin-out-chart">
                <table class="tbl-view">
                    <thead>
                        <tr>
                            <th>Project Name</th>
                            <th>Task Id</th>
                            <th>Task</th>
                            <th>Asset</th>																					
							<?php if($role == 1){?>															
								<th>Field Coordinator</th>														
							<?php } ?>
                            <th>Worker</th>
                            <th>Status</th>							                            							
							<th>Action</th>
                        </tr>
                    </thead>
                    <tbody>											
						<?php 												
							if(isset($pendingtask) && !empty($pendingtask)) 
							{ 													
								foreach($pendingtask as $val) 
								{ 														
									$assetRefId = unserialize($val->assetRefId);
									$assetname = getAssetName($assetRefId);	
									$workers = getTaskWorkers($val->taskRefId);
						?>
									<tr>
										<td data-column="Project Name"><?php echo ucfirst($val->project_name); ?></td>
										<td data-column="Task Id"><?php echo $val->taskId;?></td>
										<td data-column="Task"><?php echo ucfirst($val->task_subject);?></td>
										<td data-column="Asset"><?php echo $assetname;?></td>
										<?php if($role == 1){?>															
						
						<td data-column="Asset"><?php echo $val->first_name;?></td>														
										<?php } ?>
										<td data-column="Worker"><?php echo $workers;?></td>
										<td data-column="Status">
											<a href="javascript:void(0)" class="btn">Pending</a>
										</td>																											<td data-column="Actions">																			
										<a href="<?php echo site_url('pending-task-detail/'.$val->taskRefId);?>" class="viewbtn">									
										<i class="fa fa-eye" aria-hidden="true"></i></a>																		
										</td>
									</tr>																				
						<?php } } ?>													
					</tbody>
                </table>
            </div>
        </div>
    </section>

<?php $this->load->view('include/footer.php');?>

    
  