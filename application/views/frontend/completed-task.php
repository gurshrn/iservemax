<?php 
	$this->load->view('include/header.php');
	$this->load->view('include/sidebar.php');
	$role = $this->session->userdata('role');
	
?>
        <div class="content-section">
            <div class="cntnt-head-bar">
                <h1>Completed Task</h1>
                <div class="srch">
                    <input type="search" placeholder="Search...">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            <div class="checkin-out-chart">
				
                <table class="tbl-view">
                    <thead>
                        <tr>
                            <th>Project Name</th>
                            <th>Task Id</th>
                            <th>Task</th>
                            <th>Asset</th>
                            <th>Worker</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>						
						<?php 							
							if(isset($completetask) && !empty($completetask)) 
							{ 								
								foreach($completetask as $val) 
								{ 									
									$assetRefId = unserialize($val->assetRefId);
									$assetname = getAssetName($assetRefId);	
									$workers = getTaskWorkers($val->taskRefId);									
						?>
									<tr>
										<td data-column="Project Name"><?php echo ucfirst($val->project_name); ?></td>
										<td data-column="Task Id"><?php echo $val->taskId;?></td>
										<td data-column="Task"><?php echo ucfirst($val->task_subject);?></td>
										<td data-column="Asset"><?php echo $assetname;?></td>
										<td data-column="Worker"><?php echo $workers; ?></td>
										<td data-column="Status">
											<?php if($role == 1){ ?>
												<a href="<?php echo site_url('completed-task-detail/'.$val->taskRefId);?>" class="btn cmplt-btn">Complete</a>
											<?php } else{ ?>
												<a href="javascript:void(0)" class="btn cmplt-btn">Complete</a>
											<?php } ?>
										</td>
									</tr>												
						<?php } } else{?>														
							<tr>								
								<td colspan="6">No record found...</td>							
							</tr>												
						<?php } ?>					
					</tbody>
                </table>
			</div>
        </div>
    </section>

<?php $this->load->view('include/footer.php');?>
   