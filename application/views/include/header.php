<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <title>Home::MarcheCredit</title>
    <!-- Favicon -->
    <link rel="icon" href="<?php echo site_url(); ?>assets/frontend/images/favicon.png" type="image/x-icon">
    <!-- Custom - from scss/style.scss -->

    <link href="<?php echo site_url(); ?>assets/frontend/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">

    <link href="<?php echo site_url(); ?>assets/frontend/css/style.css" rel="stylesheet">

    <link href="<?php echo site_url(); ?>assets/frontend/css/clientstyle.css" rel="stylesheet">

    <link href="<?php echo site_url(); ?>assets/frontend/css/jquery.mCustomScrollbar.css" rel="stylesheet">
    
    <link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">

    <link href="<?php echo site_url(); ?>assets/frontend/css/owl.carousel.css" rel="stylesheet">
    
    <link href="<?php echo site_url(); ?>assets/frontend/css/owl.theme.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    
    <link href="<?php echo site_url(); ?>assets/css/intlTelInput.css" rel="stylesheet">
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <!-- google fonts -->
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    
    <script>
        var site_url = '<?php echo site_url(); ?>';
    </script>   
</head>
