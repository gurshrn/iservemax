<footer>
    <div class="container">
        <div class="wrap">
            <div class="footer-nav">
                <div class="row">
                    <div class="col-md-6">
                        <div class="foot-block">
                            <h3>Quick Links</h3>
                            <ul>
                                <li><a href="#">How Are We?</a></li>
                                <li><a href="faq.html">FAQ - Help</a></li>
                                <li><a href="term-condition.html">Terms and Conditions</a></li>
                                <li><a href="#">Security Advices</a></li>
                                <li><a href="#">Web TV</a></li>
                                <li><a href="blog.html">Blog</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="foot-block">
                            <h3>contact us</h3>
                            <div class="contct-info">
                                <div class="icon">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </div>
                                <div class="dtl">
                                    <p>
                                        <span>Street Address</span>123 lorem ipsu, South Africa
                                    </p>
                                </div>
                            </div>
                            <div class="contct-info">
                                <div class="icon">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                </div>
                                <div class="dtl">
                                    <p>
                                        <span>Fax phone Number</span>123 456 7899
                                    </p>
                                </div>
                            </div>
                            <div class="contct-info">
                                <div class="icon">
                                    <i class="fa fa-mobile" aria-hidden="true"></i>
                                </div>
                                <div class="dtl">
                                    <p>
                                        <span>Mobile Number</span><a href="tel:1234567899">123 456 7899</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-nav">
                <div class="get-in-touch">
                    <h2>get in touch</h2>
                    <form>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="First Name">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Last Name">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Phone">
                        </div>
                        <div class="form-group txt">
                            <textarea class="form-control" placeholder="Message"></textarea>
                        </div>
                        <input class="sbmt-btn" type="submit" value="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap">
        <div class="footer-nav">
            <div class="copy-right-sec">
                <p>Copyright © 2018 CreditMarche. Powered by: <a href="https://www.imarkinfotech.com/" target="_blank">iMarkInfotech</a></p>
            </div>
        </div>
    </div>
</footer>