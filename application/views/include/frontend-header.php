<body>
    <?php 
        if($this->session->userdata('userRefId'))
        {
            $userrefId = $this->session->userdata('userRefId');
            $firstname = $this->session->userdata('firstName');
            $userdetail = getUserDetail($userrefId);
            
        }
    ?>
    <div class="pk-loader">
        <svg id="triangle" viewBox="-3 -4 39 39">
            <polygon fill="#EFEFEF" stroke="#555" stroke-width="1" points="16,0 32,32 0,32"></polygon>
        </svg>
        <div class="loader-text">© 2018 CreditMarche.</div>
    </div>
    <!-- Header Starts Here -->
    <header>
        <div class="container">
            <nav>
                <ul class="nav-list">
                    <li class="<?php if($parentUrl == 'Home'){ echo 'current-menu-item';} ?>"><a href="<?php echo site_url('/');?>">Home</a></li>
                    
                    <li class="<?php if($parentUrl == 'View Offers'){ echo 'current-menu-item';} ?>"><a href="<?php echo site_url('/view-offer');?>">View Offers</a></li>
                    
					<li class="<?php if($parentUrl == 'Publish Offers'){ echo 'current-menu-item';} ?>">
					<a <?php if($this->session->userdata('userRefId') && $userdetail->status == 1 && $userdetail->user_active == 1) { echo ''; } elseif(!$this->session->userdata('userRefId')){ echo 'data-attr="login" id="frontend-register"'; } ?> href="<?php if($this->session->userdata('userRefId') && $userdetail->status == 1 && $userdetail->user_active == 1) { echo site_url('/publish-offer'); } else{ echo "javascript:void(0)"; } ?>">Publish an Offer</a></li>

                   

                </ul>
            </nav>
            <a class="logo" href="index.html"><img src="<?php echo site_url(); ?>assets/images/logo.png" alt="logo"></a>
            <?php if($this->session->userdata('userRefId')) { ?>
            <aside class="authentication-sec">
                <div class="user_view"><span class="user_icon"><img src="<?php echo site_url(); ?>assets/images/user_img.png"></span><span class="caret1"><?php echo ucfirst($firstname);?><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                    <ul class="view_links" style="display: none;">
                        <li>
                            <a href="#">My Profile</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('/dashboard');?>">Dashboard</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('/logout');?>">Logout</a>
                        </li>
                    </ul>
                </div>
            </aside>
        <?php } else{?>
            <aside class="authentication-sec">
                <a href="javascript:void(0)" class="login-btn" data-attr="login" id="frontend-register">login</a>
                <a href="javascript:void(0)" class="signup-btn" data-attr="sign-up" id="frontend-register">sign up</a>
            </aside>
        <?php } ?>
        </div>
    </header>