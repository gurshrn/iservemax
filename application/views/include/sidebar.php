<?php $role = $this->session->userdata('role'); ?>
<body>
    <div class="pk-loader">
        <svg id="triangle" viewBox="-3 -4 39 39">
             <polygon fill="#EFEFEF" stroke="#555" stroke-width="1" points="16,0 32,32 0,32"></polygon>
        </svg>
        <div class="loader-text">© 2018 iServe Max.</div>
    </div>
    <header class="d-header">
        <div class="container-fluid">
            <div class="hdr-wrap">
                <a class="logo" href="#"><img src="<?php echo site_url(); ?>assets/frontend/images/logo.png" alt="logo"></a>
                <div class="user_view"><span class="user_icon"><img src="<?php echo site_url(); ?>assets/frontend/images/user_img.jpg"></span><span class="caret1">Field Coordinator <i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    <ul class="view_links">
                        <li>
                            <a href="#">settings</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('user-logout')?>">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <section class="dashboard-content">
        <?php if($role == 3) { ?>
            <aside class="dashboard-nav">
                <ul>
                    <li <?php if($parentUrl == 'Checkin-checkout'){ echo 'class="active"';} ?>><a href="<?php echo site_url('checkin-checkout');?>"><span><svg><use xlink:href="#menu1"></use></svg></span> Checkin Checkout</a><span class="drop-down"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                        <ul class="submenu">
                            <li <?php if($parentUrl == 'Checkin-checkout'){ echo 'class="active"';} ?>><a href="<?php echo site_url('checkin-checkout');?>"><i class="fa fa-caret-right" aria-hidden="true"></i>view all</a></li>
                        </ul>
                    </li>
                    <li <?php if($parentUrl == 'Task'){ echo 'class="active"';} ?>>
                        <a href="<?php echo site_url('task');?>"><span><svg><use xlink:href="#menu2"></use></svg></span> Task</a>
                        <span class="drop-down  <?php if(isset($childUrl)){ echo 'open';}?>">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </span>
                        <ul class="submenu" <?php if(isset($childUrl)){ echo 'style="display:block"';}?>>
                            
                            <li <?php if(isset($childUrl) && $childUrl == 'Create Task'){ echo 'class="active"';} ?>><a href="<?php echo site_url('create-task');?>"><i class="fa fa-caret-right" aria-hidden="true"></i>create task</a></li>
                            
                            <li <?php if(isset($childUrl) && $childUrl == 'Pending Task'){ echo 'class="active"';} ?>><a href="<?php echo site_url('pending-task');?>"><i class="fa fa-caret-right" aria-hidden="true"></i>pending task</a></li>
                            
                            <li <?php if(isset($childUrl) && $childUrl == 'Completed Task'){ echo 'class="active"';} ?>><a href="<?php echo site_url('completed-task');?>"><i class="fa fa-caret-right" aria-hidden="true"></i>completed task</a></li>
                        </ul>
                    </li>
                    
					<li <?php if($parentUrl == 'Worker'){ echo 'class="active"';} ?>><a href="<?php echo site_url('worker');?>"><span><svg><use xlink:href="#menu3"></use></svg></span> Worker <small>15</small></a></li>
                    
                    <li <?php if($parentUrl == 'Asset'){ echo 'class="active"';} ?>><a href="<?php echo site_url('asset');?>"><span><svg><use xlink:href="#menu4"></use></svg></span> Asset <small>10</small></a></li>
                    
                    <li <?php if($parentUrl == 'Project'){ echo 'class="active"';} ?>><a href="<?php echo site_url('project');?>"><span><svg><use xlink:href="#menu5"></use></svg></span> Projects <small>25</small></a></li>
                    
                    <li <?php if($parentUrl == 'Complaint'){ echo 'class="active"';} ?>><a href="<?php echo site_url('complaint');?>"><span><svg><use xlink:href="#menu6"></use></svg></span> Complaint <small>18</small></a></li>
                    
                    <li <?php if($parentUrl == 'Invoice'){ echo 'class="active"';} ?>><a href="<?php echo site_url('invoice');?>"><span><svg><use xlink:href="#menu7"></use></svg></span> invoice <small>8</small></a></li>

                    <li <?php if($parentUrl == 'Collection'){ echo 'class="active"';} ?>><a href="<?php echo site_url('collection');?>"><span><svg><use xlink:href="#menu8"></use></svg></span> collection <small>11</small></a></li>
                   
                    <li <?php if($parentUrl == 'Safety Content'){ echo 'class="active"';} ?>><a href="<?php echo site_url('safety-content');?>"><span><svg><use xlink:href="#menu9"></use></svg></span> Safety Content</a></li>
                    
                    <li <?php if($parentUrl == 'Technical Document'){ echo 'class="active"';} ?>><a href="<?php echo site_url('technical-document');?>"><span><svg><use xlink:href="#menu10"></use></svg></span> Technical Document</a></li>

                    <li <?php if($parentUrl == 'Profile'){ echo 'class="active"';} ?>><a href="<?php echo site_url('profile');?>"><span><svg><use xlink:href="#menu14"></use></svg></span> Profile</a></li>
               
                </ul>
            </aside>
        <?php } if($role == 1){ ?>

            <aside class="dashboard-nav">
                <ul>
                    <li <?php if(isset($childUrl) && $childUrl == 'Pending Task'){ echo 'class="active"';} ?>><a href="<?php echo site_url('pending-task');?>"><span><svg><use xlink:href="#menu2"></use></svg></span> pending task <small>15</small></a></li>
                    
					<li <?php if(isset($childUrl) && $childUrl == 'Completed Task'){ echo 'class="active"';} ?>><a href="<?php echo site_url('completed-task');?>"><span><svg><use xlink:href="#menu11"></use></svg></span> completed task <small>15</small></a></li>
                    
                    <li <?php if($parentUrl == 'Project'){ echo 'class="active"';} ?>><a href="<?php echo site_url('project');?>"><span><svg><use xlink:href="#menu5"></use></svg></span> Projects <small>25</small></a></li>
                    
                    <li <?php if($parentUrl == 'Complaint'){ echo 'class="active"';} ?>><a href="<?php echo site_url('complaint');?>"><span><svg><use xlink:href="#menu6"></use></svg></span> Complaint <small>18</small></a></li>
                    
                    <li <?php if($parentUrl == 'Safety Content'){ echo 'class="active"';} ?>><a href="<?php echo site_url('safety-content');?>"><span><svg><use xlink:href="#menu9"></use></svg></span> Safety Content</a></li>
                   
                    <li <?php if($parentUrl == 'Technical Document'){ echo 'class="active"';} ?>><a href="<?php echo site_url('technical-document');?>"><span><svg><use xlink:href="#menu10"></use></svg></span> Technical Document</a></li>
                   
                    <li <?php if($parentUrl == 'News'){ echo 'class="active"';} ?>><a href="<?php echo site_url('news');?>"><span><svg><use xlink:href="#menu12"></use></svg></span> News</a></li>

                    <li <?php if($parentUrl == 'Product'){ echo 'class="active"';} ?>><a href="<?php echo site_url('product');?>"><span><svg><use xlink:href="#menu13"></use></svg></span> Product</a></li>

                    <li <?php if($parentUrl == 'Contact Us'){ echo 'class="active"';} ?>><a href="<?php echo site_url('contact-us');?>"><span><svg><use xlink:href="#menu14"></use></svg></span> Contact Us</a></li>

                    <li <?php if($parentUrl == 'Profile'){ echo 'class="active"';} ?>><a href="<?php echo site_url('profile');?>"><span><svg><use xlink:href="#menu14"></use></svg></span> Profile</a></li>
                    
                    <li <?php if($parentUrl == 'Notification'){ echo 'class="active"';} ?>><a href="<?php echo site_url('notification');?>"><span><svg><use xlink:href="#menu15"></use></svg></span> Notifications</a></li>
                
                </ul>
            </aside>

        <?php } ?>