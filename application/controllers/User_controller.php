<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_controller extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('login');
		$this->load->helper('globalfunction');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	/*=================Get all user detail=================*/
	
	public function index()	
	{
		$this->load->view('frontend/login.php');
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$results = $this->login->loginUser($_POST);
			if (count($results) > 0) 
			{
				$this->session->set_userdata('userRefId', $results[0]->userRefId);
	            $this->session->set_userdata('firstName', $results[0]->first_name);
	            $this->session->set_userdata('role', $results[0]->role);
				$this->session->set_userdata('logged_in_front', 'frontend');
	            $result['success'] = true;
	            $result['success_message'] = 'Login Successfully';
	            $result['url'] = 'complaint';
	       	}
			else
			{
				$result['success'] = false;
	            $result['error_message'] = 'Username or Password is wrong,Please check';
			
			}
			echo json_encode($result);exit;
			}			
	}
	public function logout()
	{
		$this->session->unset_userdata('userRefId');
        $this->session->unset_userdata('firstName');
		//if (!$this->session->userdata('logged_in_back')) 
		//{
			$this->session->sess_destroy();
		//}
        redirect(site_url('/login'));
	}
}
