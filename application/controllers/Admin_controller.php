<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('admin');
        $this->load->model('project');
		$this->load->helper('globalfunction');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	/*=================Get all user detail=================*/
	
	public function index()	
	{
		$output['parenturl'] = 'User';		
		$output['userdetail'] = $this->admin->getAllUsers();
		$this->load->view('backend/admin-users.php',$output);			
	}
	public function addUser()
	{
		$output['parenturl'] = 'User';		
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$postData = $this->input->post();
			if (!empty($_FILES['image']['size'])) {
                    $count = count($_FILES['image']['size']);
                    $path = 'assets/upload/profile/';
                    $imagename = $_FILES['image'];
                    $date = time();
                    $exps = explode('.', $imagename['name']);
                    
					$_FILES['image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
                    $_FILES['image']['type'] = $imagename['type'];
                    $_FILES['image']['tmp_name'] = $imagename['tmp_name'];
                    $_FILES['image']['error'] = $imagename['error'];
                    $_FILES['image']['size'] = $imagename['size'];
                    
					$config['upload_path'] = $path;
                    $config['allowed_types'] = '*';
                    $config['max_size'] = '100000';
                    $config['max_width'] = '100024';
                    $config['max_height'] = '100768';
                    
					$this->load->library('upload', $config);
                    $this->upload->do_upload('image');
                    $datas = $this->upload->data();
					$name_array = $datas['file_name'];
                    $names = $name_array;
					$postData['image'] = $names;
			}
			
			if($postData['id'] == '')
			{
				$data['userRefId'] = generateRef();
				$data['addedondate'] = date('Y-m-d');
			}
			$data['first_name'] = $postData['first_name'];
			if(isset($postData['role']))
			{
				$data['role'] = $postData['role'];
			}
			
			
			$results = $this->admin->addUser($data,$postData);

			if($results)
			{
				$result['success'] = $results['success'];
				$result['success_message'] = $results['success_message'];
				if(isset($postData['frontend']))
				{
					$result['url'] = site_url('profile');
				}
				else
				{
					$result['url'] = site_url('/');
				}
				
			}
			else
			{
				$output['success'] = $results['success'];
				$output['error_message'] = $results['error_message'];
			}
			echo json_encode($result);exit;
		}
		$this->load->view('backend/admin-add-user.php',$output);
	}
	public function editUser($id)
	{
		$output['parenturl'] = 'User';
		$output['editUser'] = $this->admin->getUserbyId($id);
		$this->load->view('backend/admin-add-user.php',$output);
	}
	public function projectList()
	{
		$output['parenturl'] = 'Project';	
		$output['projectDetail'] = $this->admin->getProjectDetail();
		$this->load->view('backend/admin-project.php',$output);
	}
	public function addProject()
	{
		$output['parenturl'] = 'Project';
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			 $postData = $this->input->post();
			if (!empty($_FILES['project_plan_image']['size'])) {
                    $count = count($_FILES['project_plan_image']['size']);
                    $path = 'assets/upload/project/';
                    $imagename = $_FILES['project_plan_image'];
                    $date = time();
                    $exps = explode('.', $imagename['name']);
                    
					$_FILES['project_plan_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
                    $_FILES['project_plan_image']['type'] = $imagename['type'];
                    $_FILES['project_plan_image']['tmp_name'] = $imagename['tmp_name'];
                    $_FILES['project_plan_image']['error'] = $imagename['error'];
                    $_FILES['project_plan_image']['size'] = $imagename['size'];
                    
					$config['upload_path'] = $path;
                    $config['allowed_types'] = '*';
                    $config['max_size'] = '100000';
                    $config['max_width'] = '100024';
                    $config['max_height'] = '100768';
                    
					$this->load->library('upload', $config);
                    $this->upload->do_upload('project_plan_image');
                    $data = $this->upload->data();
					$name_array = $data['file_name'];
                    $names = $name_array;
					$postData['project_plan_image'] = $names;
			}
			if (!empty($_FILES['project_video']['size'])) {
              	$count = count($_FILES['project_video']['size']);
                $path = 'assets/upload/project/';
                $imagename = $_FILES['project_video'];
                $date = time();
                $exps = explode('.', $imagename['name']);
                
				$_FILES['project_video']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['project_video']['type'] = $imagename['type'];
                $_FILES['project_video']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['project_video']['error'] = $imagename['error'];
                $_FILES['project_video']['size'] = $imagename['size'];
                
				$config['upload_path'] = $path;
                $config['allowed_types'] = '*';
                $config['max_size'] = '100000';
                $config['max_width'] = '100024';
                $config['max_height'] = '100768';
                
				$this->load->library('upload', $config);
                $this->upload->do_upload('project_video');
                $data = $this->upload->data();
				$name_array = $data['file_name'];
                $names = $name_array;
				$document['project_video'] = $names; 
			}
			if (!empty($_FILES['project_image']['size'])) {
              	$count = count($_FILES['project_image']['size']);
                $path = 'assets/upload/project/';
                $imagename = $_FILES['project_image'];
                $date = time();
                $exps = explode('.', $imagename['name']);
                
				$_FILES['project_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['project_image']['type'] = $imagename['type'];
                $_FILES['project_image']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['project_image']['error'] = $imagename['error'];
                $_FILES['project_image']['size'] = $imagename['size'];
                
				$config['upload_path'] = $path;
                $config['allowed_types'] = '*';
                $config['max_size'] = '100000';
                $config['max_width'] = '100024';
                $config['max_height'] = '100768';
                
				$this->load->library('upload', $config);
                $this->upload->do_upload('project_image');
                $data = $this->upload->data();
				$name_array = $data['file_name'];
                $names = $name_array;
				$document['project_image'] = $names; 
			}
			if (!empty($_FILES['project_document']['size'])) {
              	$count = count($_FILES['project_document']['size']);
                $path = 'assets/upload/project/';
                $imagename = $_FILES['project_document'];
                $date = time();
                $exps = explode('.', $imagename['name']);
                
				$_FILES['project_document']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['project_document']['type'] = $imagename['type'];
                $_FILES['project_document']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['project_document']['error'] = $imagename['error'];
                $_FILES['project_document']['size'] = $imagename['size'];
                
				$config['upload_path'] = $path;
                $config['allowed_types'] = '*';
                $config['max_size'] = '100000';
                $config['max_width'] = '100024';
                $config['max_height'] = '100768';
                
				$this->load->library('upload', $config);
                $this->upload->do_upload('project_document');
                $data = $this->upload->data();
				$name_array = $data['file_name'];
                $names = $name_array;
				$document['project_document'] = $names; 
			}
			
           
			if($postData['id'] == '')
			{
				$postData['projectRefId'] = generateRef();
				$postData['addedondate'] = date('Y-m-d');
			}
			
			$ownerInformation = array('owner_name'=>$postData['owner_name'],'owner_designation'=>$postData['owner_designation'],'owner_phone'=>$postData['owner_phone']);
			
			$postData['owner_information'] = serialize($ownerInformation);
			
			foreach($postData['contact_name'] as $key=>$vals)
			{
				$contactInfo[] = array(
								'contact_name' => $vals,
                                'contact_designation' => $postData['contact_designation'][$key],
                                'contact_phone' => $postData['contact_phone'][$key]
                            );
			}
			foreach($postData['contractor_name'] as $key=>$val1)
			{
				$contractorInfo[] = array(
								'contractor_name' => $val1,
                                'scope_of_work' => $postData['scope_of_work'][$key],
                            );
			}
			$postData['contact_information'] = serialize($contactInfo);
			$postData['contractor_information'] = serialize($contractorInfo);
			
			$result = $this->admin->addProjectDetail($postData,$document);
		}
		$this->load->view('backend/admin-add-project.php',$output);
	}
	public function editProject($id)
	{
		$output['parenturl'] = 'Project';
		$output['editProject'] = $this->admin->getProjectDetailById($id);
		$this->load->view('backend/admin-add-project.php',$output);
	}
	public function viewProject($id)
	{
		$output['parenturl'] = 'Project';
		$output['editProject'] = $this->admin->getProjectDetailById($id);
		$output['assetDetail'] = $this->admin->getAssestDetail();
		$output['projectAssetDetail'] = $this->admin->getProjectAssetDetailById($id);
		$output['projectComponentDetail'] = $this->admin->getProjectComponentDetailById($id);
		$output['componentDetail'] = $this->admin->geAllComponentDetail();
		$output['fieldCoordinator'] = $this->admin->getFieldCoordinatorDetail();
		$this->load->view('backend/admin-view-project.php',$output);
	}
	
	public function assetList()
	{
		$output['parenturl'] = 'Asset';	
		$output['assetDetail'] = $this->admin->getAssestDetail();
		$this->load->view('backend/asset.php',$output);
	}
	public function addAsset()
	{
		$output['parenturl'] = 'Asset';
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$postData = $this->input->post();
			if (!empty($_FILES['asset_image']['size'])) {
                    $count = count($_FILES['asset_image']['size']);
                    $path = 'assets/upload/asset/';
                    $imagename = $_FILES['asset_image'];
                    $date = time();
                    $exps = explode('.', $imagename['name']);
                    
					$_FILES['asset_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
                    $_FILES['asset_image']['type'] = $imagename['type'];
                    $_FILES['asset_image']['tmp_name'] = $imagename['tmp_name'];
                    $_FILES['asset_image']['error'] = $imagename['error'];
                    $_FILES['asset_image']['size'] = $imagename['size'];
                    
					$config['upload_path'] = $path;
                    $config['allowed_types'] = '*';
                    $config['max_size'] = '100000';
                    $config['max_width'] = '100024';
                    $config['max_height'] = '100768';
                    
					$this->load->library('upload', $config);
                    $this->upload->do_upload('asset_image');
                    $data = $this->upload->data();
					$name_array = $data['file_name'];
                    $names = $name_array;
                    $postData['asset_image'] = $names;
					
			}

			if($postData['id'] == '')
			{
				$postData['assetRefId'] = generateRef();
				$postData['addedondate'] = date('Y-m-d');
			}
			
			$data['purshase_date'] = $postData['purshase_date'];
			$data['warranty_validity'] = $postData['warranty_validity'];
			$data['po_number'] = $postData['po_number'];
			$data['supplier'] = $postData['supplier'];
			$data['inspected_date'] = $postData['inspected_date'];
			$data['inspected_time'] = $postData['inspected_time'];
			$postData['additional_information'] = serialize($data);
			$result = $this->admin->addAssetDetail($postData);
		}
		$this->load->view('backend/add-asset.php',$output);
	}
	public function editAsset($id)
	{
		$output['parenturl'] = 'Asset';
		$output['editAsset'] = $this->admin->getAssetDetailById($id);
		$this->load->view('backend/add-asset.php',$output);
	}
	public function assignAsset()
	{
		$postData = $this->input->post();
		$postData['refId'] = generateRef();
		$postData['addedondate'] = date('Y-m-d');
		$result = $this->admin->addAssignAsset($postData);
		echo json_encode($result);exit;
	}
	public function assignComponent()
	{
		$postData = $this->input->post();
		$postData['refId'] = generateRef();
		$postData['addedondate'] = date('Y-m-d');
		$result = $this->admin->addAssignComponent($postData);
		echo json_encode($result);exit;
	}
	public function assignProject()
	{
		$postData = $this->input->post();
		$postData['refId'] = generateRef();
		$postData['addedondate'] = date('Y-m-d');
		$result = $this->admin->assignProjectToUser($postData);
		echo json_encode($result);exit;
	}
	public function getProjectAssetDetail()
	{
		$output = $this->admin->getProjectAssetDetailById($_POST['projectRefId']);
		$html = '';
		$html .= '<tr>';
		foreach($output as $val)
		{
			$html .= '<td>'.ucfirst($val->asset).'</td><td>'.ucfirst($val->addedondate).'</td></tr>';
		}
		$html .= '<tr>';
		
		echo $html;
	}
	public function getProjectAssetDetails()
	{
		$output = $this->admin->getProjectAssetDetailById($_POST['projectRefId']);
		$html = '';
		$html .= '<option value=""></option>';
		foreach($output as $val)
		{
			$html .= '<option value="'.$val->assetRefId.'">'.ucfirst($val->asset).'</option>';
		}
		echo $html;
	}
	public function getAssetComponentDetails()
	{
		$output = $this->admin->getAssetComponentDetailById($_POST['projectRefId'],$_POST['assetRefId']);
		$html = '';
		$html .= '<option value=""></option>';
		foreach($output as $val)
		{
			$html .= '<option value="'.$val->componentRefId.'">'.ucfirst($val->component_name).'</option>';
		}
		echo $html;
	}
	public function getProjectComponentDetail()
	{
		$output = $this->admin->getProjectComponentDetailById($_POST['projectRefId']);
		$html = '';
		$html .= '<tr>';
		foreach($output as $val)
		{
			$html .= '<td>'.ucfirst($val->component_name).'</td><td>'.ucfirst($val->asset).'</td><td>'.$val->addedondate.'</td></tr>';
		}
		$html .= '<tr>';
		
		echo $html;

	}
	public function getComponentDetail()
	{
		$output['parenturl'] = 'Component';
		$output['componentDetail'] = $this->admin->geAllComponentDetail();
		$this->load->view('backend/component.php',$output);
	}
	public function addComponent()
	{
		$output['parenturl'] = 'Component';
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$postData = $this->input->post();
			if (!empty($_FILES['component_image']['size'])) {
                    $count = count($_FILES['component_image']['size']);
                    $path = 'assets/upload/component/';
                    $imagename = $_FILES['component_image'];
                    $date = time();
                    $exps = explode('.', $imagename['name']);
                    
					$_FILES['component_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
                    $_FILES['component_image']['type'] = $imagename['type'];
                    $_FILES['component_image']['tmp_name'] = $imagename['tmp_name'];
                    $_FILES['component_image']['error'] = $imagename['error'];
                    $_FILES['component_image']['size'] = $imagename['size'];
                    
					$config['upload_path'] = $path;
                    $config['allowed_types'] = '*';
                    $config['max_size'] = '100000';
                    $config['max_width'] = '100024';
                    $config['max_height'] = '100768';
                    
					$this->load->library('upload', $config);
                    $this->upload->do_upload('component_image');
                    $data = $this->upload->data();
					$name_array = $data['file_name'];
                    $names = $name_array;
					$postData['component_image'] = $names;
			}
			if($postData['id'] == '')
			{
				$postData['compoRefId'] = generateRef();
				$postData['addedondate'] = date('Y-m-d');
			}
			$result = $this->admin->addComponentDetail($postData);
		}
		$this->load->view('backend/add-component.php',$output);
	}
	public function editComponent($id)
	{
		$output['parenturl'] = 'Component';
		$output['editComponent'] = $this->admin->getComponentDetailById($id);
		$this->load->view('backend/add-component.php',$output);
	}
	public function task()
	{
		$output['parenturl'] = 'Task';	
		$output['projectDetail'] = $this->admin->getTaskDetail();
		$this->load->view('backend/task.php',$output);
	}
	public function createTask()
	{		
		$output['parenturl'] = 'Task';
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$postData = $this->input->post();
			if (!empty($_FILES['task_image']['size'])) {
                    $count = count($_FILES['task_image']['size']);
                    $path = 'assets/upload/task/';
                    $imagename = $_FILES['task_image'];
                    $date = time();
                    $exps = explode('.', $imagename['name']);
                    
					$_FILES['task_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
                    $_FILES['task_image']['type'] = $imagename['type'];
                    $_FILES['task_image']['tmp_name'] = $imagename['tmp_name'];
                    $_FILES['task_image']['error'] = $imagename['error'];
                    $_FILES['task_image']['size'] = $imagename['size'];
                    
					$config['upload_path'] = $path;
                    $config['allowed_types'] = '*';
                    $config['max_size'] = '100000';
                    $config['max_width'] = '100024';
                    $config['max_height'] = '100768';
                    
					$this->load->library('upload', $config);
                    $this->upload->do_upload('task_image');
                    $data = $this->upload->data();
					$name_array = $data['file_name'];
                    $names = $name_array;
					$postData['task_image'] = $names;
			}
			$userRefId = $this->session->userdata('userRefId');
			$postData['taskRefId'] = generateRef();
			$postData['taskId'] = generateRef1();
			$postData['assetRefId'] = serialize($postData['assetRefId']);
			if(isset($postData['componentRefId']))
			{
				$postData['componentRefId'] = serialize($postData['componentRefId']);
			}
			
			$postData['addedondate'] = date('Y-m-d');
			$postData['task_created_by'] = $userRefId;
			$workers = explode(",",$postData['worker_assigned']);
			$results = $this->project->addTaskDetail($postData,$workers);
			if($results)
			{
				$result['success'] = $results['success'];
				$result['success_message'] = $results['success_message'];
				$result['url'] = site_url('pending-task');
			}
			else
			{
				$output['success'] = $results['success'];
				$output['error_message'] = $results['error_message'];
			}
			echo json_encode($result);exit;
		}	
		$output['project'] = $this->admin->getProjectDetail();
		$output['worker'] = $this->admin->getWorkerDetail();
		$this->load->view('backend/create-task.php',$output);
	}
	public function safetyContent()
	{

		$output['parenturl'] = 'Safety Content';
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$postData = $this->input->post();
			if (!empty($_FILES['safety_document']['size'])) {
                $count = count($_FILES['safety_document']['size']);
                $path = 'assets/upload/safetyDocument/';
                $imagename = $_FILES['safety_document'];
                $date = time();
                $exps = explode('.', $imagename['name']);
                
				$_FILES['safety_document']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['safety_document']['type'] = $imagename['type'];
                $_FILES['safety_document']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['safety_document']['error'] = $imagename['error'];
                $_FILES['safety_document']['size'] = $imagename['size'];
                
				$config['upload_path'] = $path;
                $config['allowed_types'] = '*';
                $config['max_size'] = '100000';
                $config['max_width'] = '100024';
                $config['max_height'] = '100768';
                
				$this->load->library('upload', $config);
                $this->upload->do_upload('safety_document');
                $data = $this->upload->data();
				$name_array = $data['file_name'];
                $names = $name_array;
                $postData['safety_document'] = $names;
                $postData['type'] = $imagename['type'];

					
			}
			$postData['addedondate'] = date('Y-m-d');
			
			$results = $this->admin->addSafetyContent($postData);
			if($results)
			{
				$result['success'] = $results['success'];
				$result['success_message'] = $results['success_message'];
				$result['url'] = site_url('admin/safety-content');
			}
			else
			{
				$output['success'] = $results['success'];
				$output['error_message'] = $results['error_message'];
			}
			echo json_encode($result);exit;	
		}
		$output['safetyContent'] = $this->admin->getSafetyContentDetail();
		$this->load->view('backend/safety-content.php',$output);
	}

	public function productList()
	{
		$output['parenturl'] = 'Product';
		$output['product'] = $this->admin->getProductDetail();
		$this->load->view('backend/product.php',$output);
	}
	public function addProduct()
	{
		$output['parenturl'] = 'Product';
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$postData = $this->input->post();
			if (!empty($_FILES['image']['size'])) {
				$count = count($_FILES['image']['size']);
                $path = 'assets/upload/product/';
                $imagename = $_FILES['image'];
                $date = time();
                $exps = explode('.', $imagename['name']);
                
				$_FILES['image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['image']['type'] = $imagename['type'];
                $_FILES['image']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['image']['error'] = $imagename['error'];
                $_FILES['image']['size'] = $imagename['size'];
                
				$config['upload_path'] = $path;
                $config['allowed_types'] = '*';
                $config['max_size'] = '100000';
                $config['max_width'] = '100024';
                $config['max_height'] = '100768';
                
				$this->load->library('upload', $config);
                $this->upload->do_upload('image');
                $data = $this->upload->data();
				$name_array = $data['file_name'];
                $names = $name_array;
                $postData['image'] = $names;
           	}

           	if($postData['id'] == '')
           	{
           		$postData['addedondate'] = date('Y-m-d');
           	}
           	
			
			$results = $this->admin->addProductDetail($postData);
			if($results)
			{
				$result['success'] = $results['success'];
				$result['success_message'] = $results['success_message'];
				$result['url'] = site_url('admin/product');
			}
			else
			{
				$output['success'] = $results['success'];
				$output['error_message'] = $results['error_message'];
			}
			echo json_encode($result);exit;	
		}
		$output['project'] = $this->admin->getProjectDetail();
		$this->load->view('backend/add-product.php',$output);
	}
	public function editProduct($id)
	{
		$output['parenturl'] = 'Product';
		$output['editProduct'] = $this->admin->getProductDetailById($id);
		$output['project'] = $this->admin->getProjectDetail();
		$this->load->view('backend/add-product.php',$output);
	}
	public function news()
	{
		$output['parenturl'] = 'News';
		$output['newsdetail'] = $this->admin->getNewsDetail();
		$this->load->view('backend/news.php',$output);
	}
	public function addNews()
	{
		$output['parenturl'] = 'News';
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$postData = $this->input->post();
			if (!empty($_FILES['image']['size'])) {
				$count = count($_FILES['image']['size']);
                $path = 'assets/upload/news/';
                $imagename = $_FILES['image'];
                $date = time();
                $exps = explode('.', $imagename['name']);
                
				$_FILES['image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['image']['type'] = $imagename['type'];
                $_FILES['image']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['image']['error'] = $imagename['error'];
                $_FILES['image']['size'] = $imagename['size'];
                
				$config['upload_path'] = $path;
                $config['allowed_types'] = '*';
                $config['max_size'] = '100000';
                $config['max_width'] = '100024';
                $config['max_height'] = '100768';
                
				$this->load->library('upload', $config);
                $this->upload->do_upload('image');
                $data = $this->upload->data();
				$name_array = $data['file_name'];
                $names = $name_array;
                $postData['image'] = $names;
           	}

           	if($postData['id'] == '')
           	{
           		$postData['addedondate'] = date('Y-m-d');
           	}
           	$postData['description'] = $_POST['description'];

           	$results = $this->admin->addNewsDetail($postData);
			if($results)
			{
				$result['success'] = $results['success'];
				$result['success_message'] = $results['success_message'];
				$result['url'] = site_url('admin/news');
			}
			else
			{
				$output['success'] = $results['success'];
				$output['error_message'] = $results['error_message'];
			}
			echo json_encode($result);exit;	
		}
		$this->load->view('backend/add-news.php',$output);
	}
	public function editNews($id)
	{
		$output['parenturl'] = 'News';
		$output['editNews'] = $this->admin->getNewsDetailById($id);
		$this->load->view('backend/add-news.php',$output);
	}
	public function viewNews($id)
	{
		$output['parenturl'] = 'News';
		$output['editNews'] = $this->admin->getNewsDetailById($id);
		$this->load->view('backend/view-news.php',$output);
	}		public function invoice()	{		$output['parenturl'] = 'Invoice';		$output['invoice'] = $this->admin->getInvoiceDetail();		$this->load->view('backend/invoice.php',$output);	}	public function addInvoice()	{		$output['parenturl'] = 'Invoice';		if ($_SERVER['REQUEST_METHOD'] == 'POST')		{			$postData = $this->input->post();			if (!empty($_FILES['document']['size'])) {				$count = count($_FILES['document']['size']);                $path = 'assets/upload/invoice/';                $imagename = $_FILES['document'];                $date = time();                $exps = explode('.', $imagename['name']);                				$_FILES['document']['name'] = $exps[0] . '_' . $date . "." . $exps[1];                $_FILES['document']['type'] = $imagename['type'];                $_FILES['document']['tmp_name'] = $imagename['tmp_name'];                $_FILES['document']['error'] = $imagename['error'];                $_FILES['document']['size'] = $imagename['size'];                				$config['upload_path'] = $path;                $config['allowed_types'] = '*';                $config['max_size'] = '100000';                $config['max_width'] = '100024';                $config['max_height'] = '100768';                				$this->load->library('upload', $config);                $this->upload->do_upload('document');                $data = $this->upload->data();				$name_array = $data['file_name'];                $names = $name_array;                $postData['document'] = $names;           	}			$userRefId = $this->session->userdata('userRefId');			$postData['created_date'] = date('Y-m-d');			$postData['assigned_by'] = $userRefId;			$postData['invoice_no'] = generateRef1();			$results = $this->admin->addInvoice($postData);			if($results)			{				$result['success'] = $results['success'];				$result['success_message'] = $results['success_message'];				$result['url'] = site_url('admin/invoice');			}			else			{				$output['success'] = $results['success'];				$output['error_message'] = $results['error_message'];			}			echo json_encode($result);exit;			}		$output['project'] = $this->admin->getProjectDetail();				$this->load->view('backend/add-invoice.php',$output);	}	public function getProjectCoordinator()	{		$output = $this->admin->getCoordinator($_POST['projectRefId']);		echo json_encode($output);exit;	}
}
