<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('project');
        $this->load->model('admin');
        $this->load->helper('globalfunction');
    }
    
    /*=================Get all user detail=================*/
    public function index()
    {
        $userRefId                  = $this->session->userdata('userRefId');
        $output['parentUrl']        = 'Project';
        $output['getProjectDetail'] = $this->project->getProjectDetailByUserId($userRefId);
        $this->load->view('frontend/projects.php', $output);
    }
    
    /*==================Get task setail===================*/
    public function taskDetail()
    {
        $output['parentUrl'] = 'Task';
        $this->load->view('frontend/task', $output);
    }
    
    /*==================Create task by field coordinator===================*/
    public function createTask()
    {
        $output['parentUrl'] = 'Task';
        $output['childUrl']  = 'Create Task';
        $output['project']   = $this->admin->getProjectDetail();
        $output['worker']    = $this->admin->getWorkerDetail();
        $this->load->view('frontend/create-task', $output);
    }
    
    /*==================get pending task===================*/
    public function pendingTask()
    {
        $output['parentUrl']   = 'Task';
        $output['childUrl']    = 'Pending Task';
        $output['pendingtask'] = $this->project->getPendingTaskDetail();
        $this->load->view('frontend/pending-task', $output);
    }
    
    public function pendingtaskDetail($id)
    {
        $output['parentUrl']   = 'Task';
        $output['childUrl']    = 'Pending Task';
        $output['projectTask'] = $this->project->getPendingTaskDetailById($id);
        $this->load->view('frontend/pending-tasks-detail', $output);
    }
    /*==================get complete task===================*/
    public function completedTask()
    {
        $output['parentUrl']    = 'Task';
        $output['childUrl']     = 'Completed Task';
        $output['completetask'] = $this->project->getCompleteTaskDetail();
        $this->load->view('frontend/completed-task', $output);
    }
    
    public function completedTaskDetail($id)
    {
        $output['parentUrl'] = 'Task';
        $output['childUrl']  = 'Completed Task';
        //$output['completetask'] = $this->project->getCompleteTaskDetail();        
        $this->load->view('frontend/completed-task-detail', $output);
    }
    
    /*===================get worker detail====================*/
    
    public function workerList()
    {
        $output['parentUrl']  = 'Worker';
        $output['workerList'] = $this->project->getAllWorkers();
        $this->load->view('frontend/worker', $output);
    }
    
    /*===================get worker detail by id====================*/
    
    public function getWorkerDetail($id)
    {
        $output['parentUrl']    = 'Worker';
        $output['workerDetail'] = $this->project->getWorkerDetailById($id);
        $this->load->view('frontend/worker-detail', $output);
    }
    
    /*==================Get asset detail===================*/
    
    public function assetDetail()
    {
        $userRefId             = $this->session->userdata('userRefId');
        $output['parentUrl']   = 'Asset';
        $output['assetDetail'] = $this->project->getAssetDetail($userRefId);
        $this->load->view('frontend/asset', $output);
    }
    
    /*==================Get asset detail by id===================*/
    
    public function assetDetailById($id)
    {
        $output['parentUrl']   = 'Asset';
        $output['assetDetail'] = $this->project->getAssetDetailById($id);
        $this->load->view('frontend/asset-detail', $output);
    }
    
    /*==================Get project asset history===================*/
    
    public function getProjectAssets()
    {
        $output['asset'] = $this->project->projectassetDetail($_POST['assetRefId']);
        $this->load->view('modal/asset-history', $output);
    }
    
    /*==================Get project detail by project id===================*/
    
    public function getProjectDetail($id)
    {
        $output['parentUrl']     = 'Project';
        $output['projectDetail'] = $this->admin->getProjectDetailById($id);
        $this->load->view('frontend/project-detail', $output);
    }
    
    /*===================create  complaint by client====================*/
    
    public function createComplaint()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $userRefId                    = $this->session->userdata('userRefId');
            $postData                     = $this->input->post();
            $postData['complaint_number'] = generateRef1();
            $postData['addedondate']      = date('Y-m-d');
            $postData['addedby']          = $userRefId;
            $postData['status']           = 0;
            $results                      = $this->project->addComplaint($postData);
            
            if ($results) {
                $result['success']         = $results['success'];
                $result['success_message'] = $results['success_message'];
                $result['url']             = site_url('complaint');
            } else {
                $output['success']       = $results['success'];
                $output['error_message'] = $results['error_message'];
            }
            echo json_encode($result);
            exit;
        }
    }
    
    /*===================get complaint detail====================*/
    
    public function complaintList()
    {
        $userRefId                 = $this->session->userdata('userRefId');
        $output['parentUrl']       = 'Complaint';
        $output['complaintDetail'] = $this->project->complaintDetail($userRefId);
        $output['project']         = $this->admin->getProjectDetail();
        $this->load->view('frontend/complaint', $output);
    }
    
    /*===================get complaint detail by id====================*/
    
    public function getComplaintDetail($id)
    {
        $output['parentUrl']       = 'Complaint';
        $output['complaintDetail'] = $this->project->getComplaintDetailById($id);
        $this->load->view('frontend/complaint-details', $output);
    }
    
    /*==============complete complaint by client or field coordinator===================*/
    
    public function completeComplaint()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $postData = $this->input->post();
            if (!empty($_FILES['document']['size'])) {
                $count     = count($_FILES['document']['size']);
                $path      = 'assets/upload/document/';
                $imagename = $_FILES['document'];
                $date      = time();
                $exps      = explode('.', $imagename['name']);
                
                $_FILES['document']['name']     = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['document']['type']     = $imagename['type'];
                $_FILES['document']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['document']['error']    = $imagename['error'];
                $_FILES['document']['size']     = $imagename['size'];
                
                $config['upload_path']   = $path;
                $config['allowed_types'] = '*';
                $config['max_size']      = '100000';
                $config['max_width']     = '100024';
                $config['max_height']    = '100768';
                
                $this->load->library('upload', $config);
                $this->upload->do_upload('document');
                $data                 = $this->upload->data();
                $name_array           = $data['file_name'];
                $names                = $name_array;
                $postData['document'] = $names;
            }
            $postData['addedondate'] = date('Y-m-d');
            
            $results = $this->project->addCompleteComplaint($postData);
            if ($results) {
                $result['success']         = $results['success'];
                $result['success_message'] = $results['success_message'];
                $result['url']             = site_url('complaint');
            } else {
                $output['success']       = $results['success'];
                $output['error_message'] = $results['error_message'];
            }
            echo json_encode($result);
            exit;
        }
    }
    
    /*==================on click not complete complaint by client======================*/
    
    public function notCompleteComplaint()
    {
        $postData = $_POST['complaintnumber'];
        $results  = $this->project->notComplete($postData);
        if ($results) {
            $result['success']         = $results['success'];
            $result['success_message'] = $results['success_message'];
            $result['url']             = site_url('complaint');
        } else {
            $output['success']       = $results['success'];
            $output['error_message'] = $results['error_message'];
        }
        echo json_encode($result);
        exit;
    }
    
    /*==================get project task for complaint module===================*/
    
    public function getProjectTasks()
    {
        $output = $this->project->getProjectTaskDetailById($_POST['projectRefId']);
        $html   = '';
        $html .= '<option value="">Select Task';
        foreach ($output as $val) {
            $html .= '<option value="' . $val->taskRefId . '">' . $val->task_subject . '</option>';
        }
        $html .= '</option>';
        
        echo $html;
    }
    
    /*==================get project asset detail for complaint module===================*/
    
    public function getProjectAssetsDetail()
    {
        $output = $this->project->getProjectAssetsDetailById($_POST['projectRefId']);
        $html   = '';
        $html .= '<option value="">Select Asset';
        foreach ($output as $val) {
            $html .= '<option value="' . $val->assetRefId . '">' . $val->asset . '</option>';
        }
        $html .= '</option>';
        
        echo $html;
    }
    
    /*==================get safety content detail======================*/
    
    public function safetyContentList()
    {
        $output['parentUrl']         = 'Safety Content';
        $output['safetyContentList'] = $this->project->getSafetyContentDetail();
        $this->load->view('frontend/safety-content', $output);
    }
    
    /*==================get technical document detail======================*/
    
    public function technicalDocumentList()
    {
        $output['parentUrl']          = 'Technical Document';
        $output['technicalDocuments'] = $this->project->getDocumentDetail();
        $this->load->view('frontend/technical-documents', $output);
    }
    
    public function product()
    {
        $output['parentUrl'] = 'Product';
        $output['product']   = $this->admin->getProductDetail();
        $this->load->view('frontend/product', $output);
    }
    
    public function productDeatilById($id)
    {
        $output['parentUrl'] = 'Product';
        $output['product']   = $this->project->getProductDetailById($id);
        $this->load->view('frontend/product-detail', $output);
    }
    
    public function contactUs()
    {
        $output['parentUrl'] = 'Contact Us';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $userRefId                     = $this->session->userdata('userRefId');
            $postData                      = $this->input->post();
            $postData['addedondate']       = date('Y-m-d');
            $postData['notification_from'] = $userRefId;
            $results                       = $this->project->addContactUs($postData);
            
            if ($results) {
                $result['success']         = $results['success'];
                $result['success_message'] = $results['success_message'];
                $result['url']             = site_url('contact-us');
            } else {
                $output['success']       = $results['success'];
                $output['error_message'] = $results['error_message'];
            }
            echo json_encode($result);
            exit;
        }
        $this->load->view('frontend/contactus', $output);
    }
    
    public function profile()
    {
        $userRefId            = $this->session->userdata('userRefId');
        $output['parentUrl']  = 'Profile';
        $output['userdetail'] = $this->project->getUserDetailById($userRefId);
        $this->load->view('frontend/profile', $output);
    }
    public function editUserDetail($id)
    {
        $output['parentUrl']  = 'Profile';
        $output['userdetail'] = $this->project->getUserDetailById($id);
        $this->load->view('frontend/edit-profile', $output);
    }
    public function news()
    {
        $output['parentUrl']  = 'News';
        $output['newsdetail'] = $this->project->getNewsList();
        $this->load->view('frontend/news', $output);
    }
    public function newsDetail($id)
    {
        $output['parentUrl']  = 'News';
        $output['newsdetail'] = $this->project->getNewsDetailById($id);
        $output['latestNews'] = $this->project->getLatestNews();
        $this->load->view('frontend/news-internal', $output);
    }
    public function collection()
    {
        $output['parentUrl']  = 'Collection';
        $output['collection'] = $this->project->getCollectionDetail();
        //$output['latestNews'] = $this->project->getLatestNews();
        $this->load->view('frontend/collection', $output);
    }
    public function notification()
    {
        $output['parentUrl']   = 'Notification';
        $output['projectTask'] = $this->project->gettaskDetail();
        //print_r($output['projectTask']);die;
        //$output['latestNews'] = $this->project->getLatestNews();
        $this->load->view('frontend/notification', $output);
    }
    public function addComment()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $userRefId = $this->session->userdata('userRefId');
            $postData  = $this->input->post();
            if (!empty($_FILES['attachment']['size'])) {
                $count     = count($_FILES['attachment']['size']);
                $path      = 'assets/upload/comment/';
                $imagename = $_FILES['attachment'];
                $date      = time();
                $exps      = explode('.', $imagename['name']);
                
                $_FILES['attachment']['name']     = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['attachment']['type']     = $imagename['type'];
                $_FILES['attachment']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['attachment']['error']    = $imagename['error'];
                $_FILES['attachment']['size']     = $imagename['size'];
                
                $config['upload_path']   = $path;
                $config['allowed_types'] = '*';
                $config['max_size']      = '100000';
                $config['max_width']     = '100024';
                $config['max_height']    = '100768';
                
                $this->load->library('upload', $config);
                $this->upload->do_upload('attachment');
                $data                   = $this->upload->data();
                $name_array             = $data['file_name'];
                $names                  = $name_array;
                $postData['attachment'] = $names;
            } else {
                $postData['attachment'] = '';
            }
            $postData['commented_date'] = date('Y-m-d');
            $postData['commented_by']   = $userRefId;
            $results                    = $this->project->addCommentDetail($postData);
            
            if ($results) {
                $result['success']    = $results['success'];
                $result['comment']    = $postData['comment'];
                $result['attachment'] = $postData['attachment'];
                $result['username']   = userDetail($userRefId);
                
            } else {
                $output['success'] = $results['success'];
            }
            echo json_encode($result);
            exit;
        }
        
    }
    public function getComponentDetail()
    {
        $output = $this->project->componentDetail($_POST['componentRefId']);
        echo json_encode($output);
        exit;
    }
    public function getComponentHistory()
    {
        $output = $this->project->componentHistory($_POST['componentRefId']);
        $html   = '';
        foreach ($output as $val) {
            if ($val->completed_date == '') {
                $date = 'Currently Use';
            } else {
                $date = date('d M Y', strtotime($val->completed_date));
            }
            $html .= '<tr><td>' . ucfirst($val->project_name) . '</td><td>' . date('d M Y', strtotime($val->addedondate)) . ' - ' . $date . '</td></tr>';
        }
        echo $html;
    }
    public function getProjectTask()
    {
        $output = $this->project->projectTask($_POST['projectRefId']);
        $html   = '';
        foreach ($output as $val) {
            $workerDetail = getTaskWorkers($val->taskRefId);
            $html .= '<tr><td>' . ucfirst($val->project_name) . '</td><td>' . ucfirst($val->task_subject) . '</td><td>' . $workerDetail . '</td></tr>';
        }
        echo $html;
    }
    
    public function checkinCheckout()
    {
        $output['parentUrl'] = 'checkin-checkout';
        $this->load->view('frontend/checkin-checkout', $output);
    }
    public function workerTaskDetail()
    {
        $output = $this->project->getworkersTaskDetail($_POST['userRefId']);
        $html   = '';
        foreach ($output as $val) {
            $html .= '<tr><td>' . $val->taskId . '</td><td>' . ucfirst($val->task_subject) . '</td><td>' . ucfirst($val->first_name) . ' ' . $val->last_name . '</td>';
        }
        echo $html;
    }
    public function addNotification()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $userRefId = $this->session->userdata('userRefId');
            $postData  = $this->input->post();
            if (!empty($_FILES['attachment']['size'])) {
                $count     = count($_FILES['attachment']['size']);
                $path      = 'assets/upload/notification/';
                $imagename = $_FILES['attachment'];
                $date      = time();
                $exps      = explode('.', $imagename['name']);
                
                $_FILES['attachment']['name']     = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['attachment']['type']     = $imagename['type'];
                $_FILES['attachment']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['attachment']['error']    = $imagename['error'];
                $_FILES['attachment']['size']     = $imagename['size'];
                
                $config['upload_path']   = $path;
                $config['allowed_types'] = '*';
                $config['max_size']      = '100000';
                $config['max_width']     = '100024';
                $config['max_height']    = '100768';
                
                $this->load->library('upload', $config);
                $this->upload->do_upload('attachment');
                $data                   = $this->upload->data();
                $name_array             = $data['file_name'];
                $names                  = $name_array;
                $postData['attachment'] = $names;
            } else {
                $postData['attachment'] = '';
            }
            $postData['notification_date'] = date('Y-m-d');
            $postData['notification_from'] = $userRefId;
            $results                       = $this->project->addNotificationDetail($postData);
            
            if ($results) {
                $result['success']         = $results['success'];
                $result['success_message'] = $results['success_message'];
                $result['url']             = site_url('pending-task');
                
                
            } else {
                $output['success']       = $results['success'];
                $result['error_message'] = $results['error_message'];
            }
            echo json_encode($result);
            exit;
        }
    }
    public function addExpense()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $userRefId = $this->session->userdata('userRefId');
            $postData  = $this->input->post();
            if (!empty($_FILES['attachment']['size'])) {
                $count     = count($_FILES['attachment']['size']);
                $path      = 'assets/upload/expense/';
                $imagename = $_FILES['attachment'];
                $date      = time();
                $exps      = explode('.', $imagename['name']);
                
                $_FILES['attachment']['name']     = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['attachment']['type']     = $imagename['type'];
                $_FILES['attachment']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['attachment']['error']    = $imagename['error'];
                $_FILES['attachment']['size']     = $imagename['size'];
                
                $config['upload_path']   = $path;
                $config['allowed_types'] = '*';
                $config['max_size']      = '100000';
                $config['max_width']     = '100024';
                $config['max_height']    = '100768';
                
                $this->load->library('upload', $config);
                $this->upload->do_upload('attachment');
                $data                   = $this->upload->data();
                $name_array             = $data['file_name'];
                $names                  = $name_array;
                $postData['attachment'] = $names;
            } else {
                $postData['attachment'] = '';
            }
            $postData['addedondate'] = date('Y-m-d');
            $results                 = $this->project->addExpenseDetail($postData);
            
            if ($results) {
                $result['success']         = $results['success'];
                $result['success_message'] = $results['success_message'];
                $result['url']             = site_url('pending-task-detail/' . $postData['taskRefId']);
                
                
            } else {
                $output['success']       = $results['success'];
                $result['error_message'] = $results['error_message'];
            }
            echo json_encode($result);
            exit;
        }
    }
    
    public function invoice()
    {
        $userRefId           = $this->session->userdata('userRefId');
        $output['parentUrl'] = 'Invoice';
        $output['invoice']   = $this->project->getInvoioceDetail($userRefId);
        $this->load->view('frontend/invoice', $output);
    }
    public function addInvoiceSign()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $postData = $this->input->post();
            if (isset($_FILES['image']) && !empty($_FILES['image']['size'])) {
                $count                       = count($_FILES['image']['size']);
                $path                        = 'assets/upload/invoice/';
                $imagename                   = $_FILES['image'];
                $date                        = time();
                $exps                        = explode('.', $imagename['name']);
                $_FILES['image']['name']     = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['image']['type']     = $imagename['type'];
                $_FILES['image']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['image']['error']    = $imagename['error'];
                $_FILES['image']['size']     = $imagename['size'];
                $config['upload_path']       = $path;
                $config['allowed_types']     = '*';
                $this->load->library('upload', $config);
                $this->upload->do_upload('image');
                $data              = $this->upload->data();
                $name_array        = $data['file_name'];
                $names             = $name_array;
                $postData['image'] = $names;
            }
            $postData['signed_date'] = date('Y-m-d');
            if (isset($postData['message'])) {
                $postData['message'] = $postData['message'];
            }
            $results = $this->project->addInvoiceSign($postData);
            if ($results) {
                $result['success']         = $results['success'];
                $result['success_message'] = $results['success_message'];
                $result['url']             = site_url('invoice');
            } else {
                $output['success']       = $results['success'];
                $output['error_message'] = $results['error_message'];
            }
            echo json_encode($result);
            exit;
        }
    }
    public function addCollection()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $postData = $this->input->post();
            if (isset($_FILES['collection_recieved_image']) && !empty($_FILES['collection_recieved_image']['size'])) {
                $count                                           = count($_FILES['collection_recieved_image']['size']);
                $path                                            = 'assets/upload/invoice/';
                $imagename                                       = $_FILES['collection_recieved_image'];
                $date                                            = time();
                $exps                                            = explode('.', $imagename['name']);
                $_FILES['collection_recieved_image']['name']     = $exps[0] . '_' . $date . "." . $exps[1];
                $_FILES['collection_recieved_image']['type']     = $imagename['type'];
                $_FILES['collection_recieved_image']['tmp_name'] = $imagename['tmp_name'];
                $_FILES['collection_recieved_image']['error']    = $imagename['error'];
                $_FILES['collection_recieved_image']['size']     = $imagename['size'];
                $config['upload_path']                           = $path;
                $config['allowed_types']                         = '*';
                $config['max_size']                              = '100000';
                $config['max_width']                             = '100024';
                $config['max_height']                            = '100768';
                $this->load->library('upload', $config);
                $this->upload->do_upload('collection_recieved_image');
                $data                                  = $this->upload->data();
                $name_array                            = $data['file_name'];
                $names                                 = $name_array;
                $postData['collection_recieved_image'] = $names;
            }
            if (isset($postData['collection_postpone_message'])) {
                $postData['collection_postpone_message'] = $postData['collection_postpone_message'];
                $postData['collection_date']             = $postData['collection_date'];
            }
            $results = $this->project->addCollectionStatus($postData);
            if ($results) {
                $result['success']         = $results['success'];
                $result['success_message'] = $results['success_message'];
                $result['url']             = site_url('collection');
            } else {
                $output['success']       = $results['success'];
                $output['error_message'] = $results['error_message'];
            }
            echo json_encode($result);
            exit;
        }
    }
}