<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Webservice extends CI_Controller {

	public function __construct() {

        parent::__construct();

        $this->load->model('salesPerson');
		
		$this->load->model('project');
		
		$this->load->model('admin');
		
		$this->load->model('fieldCoordinator');

		$this->load->helper('globalfunction');

    }



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	

	/*=================Get all user detail=================*/

	public function getProjectList()	
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->getProjectDetail($result['userId'],'',$result['offset']);
		echo json_encode($data);exit;
	}
	public function getProjectListByDate()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->getProjectDetail($result['userId'],$result['date'],$result['offset']);
		echo json_encode($data);exit;
	}
	public function addContractor()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->addContractorDetail($result);
		echo json_encode($data);exit;
	}
	public function getContractorList()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->contractorList($result['userId']);
		echo json_encode($data);exit;
	}
	public function deleteContractor()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->deleteContractorById($result['contractorId']);
		echo json_encode($data);exit;
	}
	public function addContactPerson()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->addContactDetail($result);
		echo json_encode($data);exit;
	}
	public function deleteContactPerson()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->deleteContact($result['contactPersonId']);
		echo json_encode($data);exit;
	}
	public function getContactPersonList()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->getContactList($result);
		echo json_encode($data);exit;
	}
	public function addProject()
	{
		if (!empty($_FILES['projectImage']['size'])) 
		{
			$count = count($_FILES['projectImage']['size']);
			$path = 'assets/upload/project/';
			$imagename = $_FILES['projectImage'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['projectImage']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['projectImage']['type'] = $imagename['type'];
			$_FILES['projectImage']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['projectImage']['error'] = $imagename['error'];
			$_FILES['projectImage']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$config['max_size'] = '100000';
			$config['max_width'] = '100024';
			$config['max_height'] = '100768';
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('projectImage');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$imageName = $name_array;
		}
		else
		{
			$imageName = '';
		}
		$data = $this->salesPerson->addProjectDetail($_REQUEST,$imageName);
		echo json_encode($data);exit;
	}
	public function getVisitList()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->getAllVisitList($result['projectId']);
		echo json_encode($data);exit;
	}
	public function getVisitProjectList()
	{
		$data = $this->salesPerson->getAllProjectVisitList($_GET['userId']);
		echo json_encode($data);exit;
	}
	
	public function addVisitDetail()
	{
		if (!empty($_FILES['image']['size'])) 
		{
			$count = count($_FILES['image']['size']);
			$path = 'assets/upload/project/';
			$imagename = $_FILES['image'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['image']['type'] = $imagename['type'];
			$_FILES['image']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['image']['error'] = $imagename['error'];
			$_FILES['image']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$config['max_size'] = '100000';
			$config['max_width'] = '100024';
			$config['max_height'] = '100768';
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('image');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$imageName = $name_array;
		}
		else
		{
			$imageName = '';
		}
		$data = $this->salesPerson->addVisit($_REQUEST,$imageName);
		echo json_encode($data);exit;
	}
	public function getVisitDetail()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->getVisitDetailById($result['visitId']);
		echo json_encode($data);exit;
	}
	public function deleteVisitDetail()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->deleteVisit($result['visitId']);
		echo json_encode($data);exit;
	}
	public function getProjectDetailById()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->getProjectDetailId($result['projectId']);
		echo json_encode($data);exit;
	}
	public function addProjectAdditionalInfo()
	{
		if (!empty($_FILES['uploadDocument']['size'])) 
		{
			$count = count($_FILES['uploadDocument']['size']);
			$path = 'assets/upload/project/';
			$imagename = $_FILES['uploadDocument'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['uploadDocument']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['uploadDocument']['type'] = $imagename['type'];
			$_FILES['uploadDocument']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['uploadDocument']['error'] = $imagename['error'];
			$_FILES['uploadDocument']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->do_upload('uploadDocument');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$imageName = $name_array;
		}
		else
		{
			$imageName = '';
		}
		if (!empty($_FILES['thumbnail']['size'])) 
		{
			$count = count($_FILES['thumbnail']['size']);
			$path = 'assets/upload/project/';
			$imagename = $_FILES['thumbnail'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['thumbnail']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['thumbnail']['type'] = $imagename['type'];
			$_FILES['thumbnail']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['thumbnail']['error'] = $imagename['error'];
			$_FILES['thumbnail']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->do_upload('thumbnail');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$thumbnail = $name_array;
		}
		else
		{
			$thumbnail = '';
		}
		$data = $this->salesPerson->addAdditionalInfo($_REQUEST,$imageName,$thumbnail);
		echo json_encode($data);exit;
	}
	public function getProjectAdditionalInfo()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->getAdditionalInfo($result['projectId']);
		echo json_encode($data);exit;
	}
	public function deleteAdditionalInfo()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->deleteAdditionalInformation($result['additionalId']);
		echo json_encode($data);exit;
	}
	public function getProjectPhase()
	{
		$data = $this->salesPerson->getPhaseOfProject();
		echo json_encode($data);exit;
	}
	public function addAttendance()
	{
		if (!empty($_FILES['userImage']['size'])) 
		{
			$count = count($_FILES['userImage']['size']);
			$path = 'assets/upload/attendance/';
			$imagename = $_FILES['userImage'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['userImage']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['userImage']['type'] = $imagename['type'];
			$_FILES['userImage']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['userImage']['error'] = $imagename['error'];
			$_FILES['userImage']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			//$config['max_size'] = '100000';
			//$config['max_width'] = '100024';
			//$config['max_height'] = '100768';
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('userImage');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$imageName = $name_array;
		}
		else
		{
			$imageName = '';
		}
		$data = $this->salesPerson->addAttendanceDetail($_REQUEST,$imageName);
		echo json_encode($data);exit;
	}
	public function getTodayAttendance()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->getTodayAttendanceDetail($result);
		echo json_encode($data);exit;
	}
	public function projectSubmitToAdmin()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->salesPerson->submitProjectToAdmin($result['projectId']);
		echo json_encode($data);exit;
	}
	
/*=====================Field coordinator's api=========================*/

	public function createProjectTask()
	{
		$taskData = $_REQUEST;
		if ($taskData['taskDate'] == '') 
		{
            $error   = 'Name is required';
			$result = new stdClass();
            $success = 0;
        }
 
		else if ($taskData['taskSubject'] == '') 
		{
            $error   = 'Scope of work is required';
			$result = new stdClass();
            $success = 0;
        } 
		else if ($taskData['projectId'] == '') 
		{
            $error   = 'Email is required';
			$result = new stdClass();
            $success = 0;
        } 
		else if ($taskData['assetId'] == '') 
		{
            $error   = 'Name is required';
			$result = new stdClass();
            $success = 0;
        }
 
		else if ($taskData['componentId'] == '') 
		{
            $error   = 'Scope of work is required';
			$result = new stdClass();
            $success = 0;
        } 
		else if ($_FILES['taskAttach'] == '') 
		{
            $error   = 'Email is required';
			$result = new stdClass();
            $success = 0;
        } 
		else if ($taskData['taskDescription'] == '') 
		{
            $error   = 'Scope of work is required';
			$result = new stdClass();
            $success = 0;
        } 
		else if ($taskData['workerId'] == '') 
		{
            $error   = 'Scope of work is required';
			$result = new stdClass();
            $success = 0;
        }
		else
		{
			if (!empty($_FILES['taskAttach']['size'])) 
			{
				$count = count($_FILES['taskAttach']['size']);
				$path = 'assets/upload/task/';
				$imagename = $_FILES['taskAttach'];
				$date = time();
				$exps = explode('.', $imagename['name']);
				
				$_FILES['taskAttach']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
				$_FILES['taskAttach']['type'] = $imagename['type'];
				$_FILES['taskAttach']['tmp_name'] = $imagename['tmp_name'];
				$_FILES['taskAttach']['error'] = $imagename['error'];
				$_FILES['taskAttach']['size'] = $imagename['size'];
				
				$config['upload_path'] = $path;
				$config['allowed_types'] = '*';
				$config['max_size'] = '100000';
				$config['max_width'] = '100024';
				$config['max_height'] = '100768';
				
				$this->load->library('upload', $config);
				$this->upload->do_upload('taskAttach');
				$datas = $this->upload->data();
				$name_array = $datas['file_name'];
				$postData['task_image'] = $name_array;
			}
			else
			{
				$postData['task_image'] = '';
			}
			$userRefId = $taskData['userId'];
			$assetId = explode(",",$taskData['assetId']);
			$componentId = explode(",",$taskData['componentId']);
			$postData = array(
				'taskRefId' => generateRef(),
				'taskId' => generateRef1(),
				'assetRefId' => serialize($assetId),
				'componentRefId' => serialize($componentId),
				'addedondate' => date('Y-m-d'),
				'task_created_by' => $userRefId,
				'task_date' => $taskData['taskDate'],
				'task_subject' => $taskData['taskSubject'],
				'projectRefId' => $taskData['projectId'],
				'description' => $taskData['taskDescription'],
				'id' => $taskData['id']
			);
			
			$workers = explode(",",$taskData['workerId']);
			$data = $this->project->addTaskDetail($postData,$workers);
			if($data['success'] == 1)
			{
				$success = 1;
				$result = $data['success_message'];
				$error = 'No error found';
			}
			else
			{
				$success = 0;
				$result = $data['error_message'];
				$error = $data['error_message'];
			}
		}
		
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		echo json_encode($array);exit;
	}
	public function getAssignedProjectList()
	{
		$data = $this->project->getAssignedProjectById($_GET['userId']);
		if(!empty($data))
		{
			
			foreach($data as $val)
			{
				$results[] = (object)array(
					'projectId'=>$val->projectRefId,
					'projectName'=>$val->project_name,
					'date'=>$val->addedondate,
				);
			}
			$success = 1;
			$error = "No error found";
		}
		else
		{
			$success = 0;
			$results = array();
			$error = "No record found";
		}
		$array = array('success'=>$success,'result'=>$results,'error'=>$error);
		echo json_encode($array);exit;
	}
	public function getProjectAssetList()
	{
		$data = $this->project->getProjectAssetsDetailById($_GET['projectId']);
		if(!empty($data))
		{
			
			foreach($data as $val)
			{
				$results[] = (object)array(
					'assetId'=>$val->assetRefId,
					'assetName'=>$val->asset,
					'assetNo' => $val->asset_no
				);
			}
			$success = 1;
			$error = "No error found";
		}
		else
		{
			$success = 0;
			$results = array();
			$error = "No record found";
		}
		$array = array('success'=>$success,'result'=>$results,'error'=>$error);
		echo json_encode($array);exit;
	}
	public function getAssetComponentList()
	{
		$assetId = explode(',',$_GET['assetId']);
		$data = $this->admin->getAssetComponentDetailById($_GET['projectId'],$assetId);
		if(!empty($data))
		{
			
			foreach($data as $val)
			{
				$results[] = (object)array(
					'componentId'=>$val->componentRefId,
					'componentName'=>$val->component_name,
					'component_no'=>$val->part_no,
				);
			}
			$success = 1;
			$error = "No error found";
		}
		else
		{
			$success = 0;
			$results = array();
			$error = "No record found";
		}
		$array = array('success'=>$success,'result'=>$results,'error'=>$error);
		echo json_encode($array);exit;
	}
	public function getWorkerList()
	{
		$data = $this->admin->getWorkerDetail();
		if(!empty($data))
		{
			
			
			foreach($data as $val)
			{
				$workers = $this->fieldCoordinator->getWorkerTasks($val->userRefId);
				$workerPendingTask = count($workers['result']);
				
				
				if($val->image != '')
				{
					$image = site_url('/assets/upload/profile/'.$val->image);
				}
				else
				{
					$image = '';
				}
				$results[] = (object)array(
					'workerId'=>$val->userRefId,
					'workerName'=>$val->first_name.' '.$val->last_name,
					'image'=>$image,
					'workerPendingTask'=>$workerPendingTask,
				);
			}
			$success = 1;
			$error = "No error found";
		}
		else
		{
			$success = 0;
			$results = array();
			$error = "No record found";
		}
		$array = array('success'=>$success,'result'=>$results,'error'=>$error);
		echo json_encode($array);exit;
	}
	public function getWorkerAttendance()
	{
		$data = $this->fieldCoordinator->getWorkerAttendanceDetail($_GET);
		echo json_encode($data);exit;
	}
	public function getWorkerTaskDetail()
	{
		$data = $this->fieldCoordinator->getWorkerTasks($_GET['workerId']);
		echo json_encode($data);exit;
	}
	public function getPendingTaskProjectList()
	{
		$status = 0;
		$data = $this->fieldCoordinator->getTaskProjectLists($_GET['userId'],$status);
		echo json_encode($data);exit;
	}
	public function getPendingTaskList()
	{
		$status = 0;
		$data = $this->fieldCoordinator->getProjectTask($_GET['projectId'],$status);
		echo json_encode($data);exit;
	}
	public function getTaskDetail()
	{
		$data = $this->fieldCoordinator->getTaskDetailById($_GET['taskId']);
		echo json_encode($data);exit;
	}
	public function getCompletedTaskProjectList()
	{
		$status = 1;
		$data = $this->fieldCoordinator->getTaskProjectLists($_GET['userId'],$status);
		echo json_encode($data);exit;
	}
	public function getCompletedTaskList()
	{
		$status = 1;
		$data = $this->fieldCoordinator->getProjectTask($_GET['projectId'],$status);
		echo json_encode($data);exit;
	}
	public function getAssetList()
	{
		$data = $this->project->getAssetDetail($_GET['userId']);
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			foreach($data as $val)
			{
				$results[] = array(
					'assetId' => $val->assetRefId,
					'assetNo' => $val->asset_no,
					'assetName' => $val->asset,
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
	}
	public function getAssetDetail()
	{
		$data = $this->project->getAssetDetailById($_GET['assetId']);
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			if($data->asset_image != '')
			{
				$asset_image = site_url('/assets/upload/asset/'.$data->asset_image);
			}
			else
			{
				$asset_image = '';
			}
			$results = (object)array(
				'assetId' => $data->assetRefId,
				'projectName' => $data->project_name,
				'assetNo' => $data->asset_no,
				'serialNo'=>$data->serial_no,
				'location'=>$data->gps_location,
				'image'=>$asset_image,
				'assetName' => $data->asset,
				'assetDescription' => $data->asset_description,
				'assetDescription' => $data->asset_description,
			);
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = new stdClass();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
	}
	public function getProjectAssetHistory()
	{
		$data = $this->project->projectassetDetail($_GET['assetId']);
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			foreach($data as $val)
			{
				$results[] = array(
					'projectName' => $val->project_name,
					'assignedDate' => $val->addedondate,
					'assetCompletedDate' => $val->asset_completed_date,
				);
			}
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
	}
	public function getAssetComponents()
	{
		$data = getAssetComponents($_GET['assetId']);
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			foreach($data as $val)
			{
				$results[] = array(
					'projectName' => $val->project_name,
					'componentName' => $val->component_name,
					'componentId' => $val->componentRefId,
					'currentLocation' => $val->project_location,
					'componentCode' => $val->componentRefId,
					'partNumber' => $val->part_no,
					'serialNumber' => $val->serial_no,
				);
			}
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
		
	}
	public function getComponentHistory()
	{
		$data = $this->project->componentHistory($_GET['componentId']);
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			foreach($data as $val)
			{
				$results[] = array(
					'projectName' => $val->project_name,
					'assignedDate' => $val->addedondate,
					'componentCompletedDate' => $val->completed_date,
				);
			}
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
	}
	public function getSafetyContentDetail()
	{
		$data = $this->project->getSafetyContentDetail();
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			foreach($data as $val)
			{
				if($val->safety_document != '')
				{
					$safety_document = site_url('/assets/upload/safetyDocument/'.$val->safety_document);
				}
				else
				{
					$safety_document = '';
				}
				$results[] = array(
					'id'=> $val->id,
					'content' => $val->content,
					'type' => $val->type,
					'safetyDocument' => $safety_document
				);
			}
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
	}
	public function getComplaintDetailList()
	{
		$data = $this->project->complaintDetail($_GET['userId']);
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			foreach($data as $val)
			{
				if($val->assetRefId != '')
				{
					$type = 'asset';
				}
				if($val->taskRefId != '')
				{
					$type = 'task';
				}
				$results[] = array(
					'complaintId'=> $val->id,
					'projectName'=> $val->project_name,
					'complaintType' => $type,
				);
			}
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
	}
	public function getComplaintDetail()
	{
		$data = $this->project->getComplaintDetailById($_GET['complaintId']);
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			$results = (object)array(
				'projectName'=> $data[0]->project_name,
				'complaintNumber'=> $data[0]->complaint_number,
				'assetName' => $data[0]->asset,
				'serialNumber' => $data[0]->serial_no,
				'status' => $data[0]->status,
			);
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = new stdClass();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
	}
	public function getInvoiceList()
	{
		$data = $this->project->getInvoioceDetail($_GET['userId']);
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			foreach($data as $val)
			{
				$results[] = array(
					'projectName'=> $val->project_name,
					'invoiceNumber'=> $val->invoice_no,
					'invoiceId'=> $val->id,
				);
			}
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
	}
	public function getInvoiceDetail()
	{
		$data = $this->fieldCoordinator->getInvoiceDetailById($_GET['invoiceId']);
		echo json_encode($data);exit;
	}
	public function getTechnicalDocumentProjectList()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->fieldCoordinator->getTechnicalDocumentProject();
		echo json_encode($data);exit;
	}
	public function getTechnicalDocuments()
	{
		$data = $this->fieldCoordinator->getDocument($_GET['projectId']);
		echo json_encode($data);exit;
	}
	public function getNewsList()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->project->getNewsList();
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			foreach($data as $val)
			{
				if($val->image != '')
				{
					$newsImage = site_url('/assets/upload/news/'.$val->image);
				}
				else
				{
					$newsImage = '';
				}
				$string = substr($val->description, 0, 200);
				$results[] = array(
					'newsId'=> $val->id,
					'newsImage'=> $newsImage,
					'newsDescription'=> strip_tags($string)
				);
			}
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
	}
	public function getNewsDetail()
	{
		$data = $this->project->getNewsDetailById($_GET['newsId']);
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			if($data->image != '')
			{
				$newsImage = site_url('/assets/upload/news/'.$data->image);
			}
			else
			{
				$newsImage = '';
			}
			$results = (object)array(
				'newsId'=> $data->id,
				'newsImage'=> $newsImage,
				'newsDescription'=> strip_tags($data->description),
				'newsTitile'=> $data->news_title,
			);
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = new stdClass();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
	}
	
	public function getProductList()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->admin->getProductDetail();
		if(!empty($data))
		{
			$error = 'No error found';
			$success = 1;
			
			foreach($data as $val)
			{
				$results[] = array(
					'projectId'=> $val->projectRefId,
					'projectName'=> $val->project_name,
				);
			}
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        echo json_encode($array);exit;
	}
	public function getProductDetail()
	{
		$data = $this->fieldCoordinator->getProductDetailById($_GET['projectId']);
		echo json_encode($data);exit;
	}
	public function addComplaintDetail()
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->fieldCoordinator->getProductDetailById($result);
		echo json_encode($data);exit;
	}
	public function addComment()
	{
		$postdata = $_REQUEST;
		if (!empty($_FILES['attachment']['size'])) 
		{
			$count = count($_FILES['attachment']['size']);
			$path = 'assets/upload/comment/';
			$imagename = $_FILES['attachment'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['attachment']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['attachment']['type'] = $imagename['type'];
			$_FILES['attachment']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['attachment']['error'] = $imagename['error'];
			$_FILES['attachment']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$config['max_size'] = '100000';
			$config['max_width'] = '100024';
			$config['max_height'] = '100768';
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('attachment');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$image = $name_array;
		}
		else
		{
			$image = '';
		}
		$data = $this->fieldCoordinator->addCommentDetail($postdata,$image);
		echo json_encode($data);exit;
	}
	public function getCommentList()
	{
		$data = $this->fieldCoordinator->getCommentListById($_GET['id'],$_GET['commentType']);
		echo json_encode($data);exit;
	}
	public function completeTask()
	{
		$status = 1;
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->fieldCoordinator->taskCompleted($result['taskId'],$status);
		echo json_encode($data);exit;
	}
	public function getProjectListByMonth()
	{
		$data = $this->fieldCoordinator->getProjectByMonth($_GET['userId']);
		echo json_encode($data);exit;
	}
	

	

}

