<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {

        parent::__construct();

        $this->load->model('login');

		$this->load->helper('globalfunction');

    }



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	

	/*=================Get all user detail=================*/

	public function login()	
	{
		$result = json_decode(file_get_contents('php://input'), true);
		$data = $this->login->appLogin($result);
		echo json_encode($data);exit;
	}

	

}

