<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function loginUser($data)
    {
        $this->db->select('*');
        $this->db->from('iserve_login_detail');
        $this->db->where('email', "AES_ENCRYPT('{$data['username']}', '/*awshp$*/')", FALSE);
        $this->db->where('role', 1);
        $this->db->or_where('role', 3);
        $result = $this->db->get();
        $result = $result->result();
        return $result;
    }
    public function appLogin($param)
    {
		if ($param['emailId'] == '') 
		{
            $error   = 'Email is required';
            $success = 0;
            $result  = new stdClass();
		} 
		else if (filter_var($param['emailId'], FILTER_VALIDATE_EMAIL) === false) 
		{
            $error   = 'Email is not valid';
            $success = 0;
            $result  = new stdClass();
        } 
		else if ($param['password'] == '') 
		{
            $error   = 'Password is required';
            $success = 0;
            $result  = new stdClass();
        }
		else 
		{
            $this->db->select('AES_DECRYPT(iserve_login_detail.password,"/*awshp$*/") as password,iserve_login_detail.userRefId as userId,iserve_login_detail.first_name as userFirstName,iserve_user_detail.last_name as userLastName,iserve_user_detail.image as userImage,iserve_login_detail.role as userRole,iserve_login_detail.tokenId');
            $this->db->from('iserve_login_detail');
            $this->db->join('iserve_user_detail', 'iserve_user_detail.userRefId = iserve_login_detail.userRefId', 'left');
            $this->db->where('iserve_login_detail.email', "AES_ENCRYPT('{$param['emailId']}', '/*awshp$*/')", FALSE);
            $result = $this->db->get();
            $result = $result->row();
            if (empty($result)) 
			{
                $error   = 'Email not exist';
                $success = 0;
                $result  = new stdClass();
            } 
			else 
			{
                if ($param['password'] != $result->password) 
				{
                    $error   = 'Invalid password';
                    $success = 0;
                    $result  = new stdClass();
                } 
				else 
				{
					
					$result->userImage = site_url('assets/upload/profile/'.$result->userImage);
					$result->currency = '$';
					$result->timeInterval = 1;
					$result->checkinTime = '9:00';
					$result->checkoutTime = '18:00';
                    if ($param['tokenId'] != '') 
					{
                        $tokenId = array(
                            'tokenId' => $param['tokenId']
                        );
                        $this->db->where('userRefId', $result->userId);
                        $this->db->update('iserve_login_detail', $tokenId);
                    }
                    $error   = 'No error found';
                    $success = 1;
                }
            }
        }
        $array = array(
            'success' => $success,
            'result' => $result,
            'error' => $error
        );
        return $array;
    }
}