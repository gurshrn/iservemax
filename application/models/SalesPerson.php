<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class SalesPerson extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getProjectDetail($userRefId,$date=NULL,$offset=NULL)
	{
		$limit = 10;
		$offsets = $limit*$offset;
		
		$this->db->select('*');
    	$this->db->from('iserve_project_detail');
    	$this->db->where('added_by',$userRefId);
		if($date != '')
		{
			$this->db->where('iserve_project_detail.addedondate',$date);
		}
		$this->db->limit($limit,$offsets);
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				if($val->project_plan_image != '')
				{
					$projectImage = site_url('/assets/upload/project/'.$val->project_plan_image);
				}
				else
				{
					$projectImage = '';
				}
				$results[] = (object)array(
					'projectName'=>$val->project_name,
					'projectId'=>$val->projectRefId,
					'projectRequirement'=>$val->project_requirements,
					'projectImage'=>$projectImage,
					'status'=>$val->status,
					'date'=>$val->addedondate
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
		
	}
	public function addContractorDetail($param)
	{
		if ($param['contractorName'] == '') 
		{
            $error   = 'Name is required';
			$result = new stdClass();
            $success = 0;
        }
 
		else if ($param['scopeOfWork'] == '') 
		{
            $error   = 'Scope of work is required';
			$result = new stdClass();
            $success = 0;
        } 
		else if ($param['email'] == '') 
		{
            $error   = 'Email is required';
			$result = new stdClass();
            $success = 0;
        } 
		else if (filter_var($param['email'], FILTER_VALIDATE_EMAIL) === false) 
		{
            $error   = 'Email is not valid';
            $success = 0;
            $result  = new stdClass();
		}
		else if ($param['phoneNumber'] == '') 
		{
            $error   = 'Phone number is required';
			$result = new stdClass();
            $success = 0;
        } 
		else 
		{
			$data = array('email'=>$param['email'],'phone_number'=>$param['phoneNumber'],'contractor_name'=>$param['contractorName'],'scope_of_work'=>$param['scopeOfWork']);
			if($param['contractorId'] == '')
			{
				$data['added_by'] = $param['userId'];
				$data['addedondate'] = date('Y-m-d');
				$this->db->insert('iserve_contractor',$data);
				$lastId = $this->db->insert_id();
				$db_error = $this->db->error();
			}
			else
			{
				$this->db->where('id',$param['contractorId']);
				$this->db->update('iserve_contractor',$data);
				$db_error = $this->db->error();
			}
			if ($db_error['code'] != 0) 
			{
				$success = 0;
				$result = new stdClass();
				$error = $db_error['message'];
			} 
			else
			{
				$success = 1;
				if($param['contractorId'] == '')
				{
					$result = array(
						'contractorId'=>$lastId,
						'contractorName'=>$param['contractorName'],
						'email'=>$param['email'],
						'phoneNumber'=>$param['phoneNumber'],
						'scopeOfWork'=>$param['scopeOfWork'],
					);
				}
				else
				{
					$result = array(
						'contractorId'=>$param['contractorId'],
						'contractorName'=>$param['contractorName'],
						'email'=>$param['email'],
						'phoneNumber'=>$param['phoneNumber'],
						'scopeOfWork'=>$param['scopeOfWork'],
					);
				}
				
				$error = 'No error found';
			}
		}
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;
	}
	public function contractorList($userId)
	{
		$this->db->select('*');
    	$this->db->from('iserve_contractor');
    	$this->db->where('added_by',$userId);
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				$results[] = (object)array(
					'contractorName'=>$val->contractor_name,
					'scopeOfwork'=>$val->scope_of_work,
					'contractorId'=>$val->id,
					'email'=>$val->email,
					'phoneNumber'=>$val->phone_number,
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function deleteContractorById($contractorId)
	{
		$this->db->where('id',$contractorId);
		$this->db->delete('iserve_contractor');
		$db_error = $this->db->error();
		if ($db_error['code'] != 0) 
		{
			$success = 0;
			$result = $db_error['message'];
			$error = $db_error['message'];
		} 
		else
		{
			$success = 1;
			$result = "Contractor deleted successfully";
			$error = 'No error found';
		}
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;
	}
	public function deleteAdditionalInformation($additionalId)
	{
		$this->db->where('id',$additionalId);
		$this->db->delete('iserve_project_additional_information');
		$db_error = $this->db->error();
		if ($db_error['code'] != 0) 
		{
			$success = 0;
			$result = $db_error['message'];
			$error = $db_error['message'];
		} 
		else
		{
			$success = 1;
			$result = "Additional Information deleted successfully";
			$error = 'No error found';
		}
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;
	}
	public function addContactDetail($param)
	{
		if ($param['contactPersonName'] == '') 
		{
            $error   = 'Name is required';
            $result   = new stdClass();
			$success = 0;
        } 
		else if ($param['contactPersonDesignation'] == '') 
		{
            $error   = 'Designation is required';
            $result   = new stdClass();
            $success = 0;
        } 
		else if ($param['contactPersonPhone'] == '') 
		{
            $error   = 'Phone number is required';
            $result   = new stdClass();
            $success = 0;
        } 
		else if ($param['contactPersonEmail'] == '') 
		{
            $error   = 'Email is required';
            $result   = new stdClass();
            $success = 0;
        }
		else if (filter_var($param['contactPersonEmail'], FILTER_VALIDATE_EMAIL) === false) {
            $error   = 'Email is not valid';
            $result   = new stdClass();
            $success = 0;
        }	
		else
		{
			$data = array('name'=>$param['contactPersonName'],'designation'=>$param['contactPersonDesignation'],'phone'=>$param['contactPersonPhone'],'email'=>$param['contactPersonEmail']);
			if($param['contactPersonId'] == '')
			{
				$data['added_by'] = $param['userId'];
				$data['addedondate'] = date('Y-m-d');
				$data['projectRefId'] = $param['projectId'];
				$this->db->insert('iserve_contact_person',$data);
				$lastId = $this->db->insert_id();
				$db_error = $this->db->error();
			}
			else
			{
				$this->db->where('id',$param['contactPersonId']);
				$this->db->update('iserve_contact_person',$data);
				$db_error = $this->db->error();
			}
			if ($db_error['code'] != 0) 
			{
				$success = 0;
				$error = $db_error['message'];
				$result = new stdClass();
			} 
			else
			{
				$success = 1;
				if($param['contactPersonId'] == '')
				{
					$result = array(
						'contactPersonId'=>$lastId,
						'contactPersonName'=>$param['contactPersonName'],
						'contactPersonDesignation'=>$param['contactPersonDesignation'],
						'contactPersonPhone'=>$param['contactPersonPhone'],
						'contactPersonEmail'=>$param['contactPersonEmail'],
					);
				}
				else
				{
					$result = array(
						'contactPersonId'=>$param['contactPersonId'],
						'contactPersonName'=>$param['contactPersonName'],
						'contactPersonDesignation'=>$param['contactPersonDesignation'],
						'contactPersonPhone'=>$param['contactPersonPhone'],
						'contactPersonEmail'=>$param['contactPersonEmail'],
					);
				}
				$error = 'No error found';
			}
			
		}
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;
	}
	public function deleteContact($contactPersonId)
	{
		$this->db->where('id',$contactPersonId);
		$this->db->delete('iserve_contact_person');
		$db_error = $this->db->error();
		if ($db_error['code'] != 0) 
		{
			$success = 0;
			$error = $db_error['message'];
			$result = $db_error['message'];
		} 
		else
		{
			$success = 1;
			$result = 'Contact person deleted successfully';
			$error = 'No error found';
		}
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;
	}
	public function getContactList($data)
	{
		$this->db->select('*');
    	$this->db->from('iserve_contact_person');
    	$this->db->where('added_by',$data['userId']);
    	$this->db->where('projectRefId',$data['projectId']);
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				$results[] = (object)array(
					'contactPersonName'=>$val->name,
					'contactPersonDesignation'=>$val->designation,
					'contactPersonPhone'=>$val->phone,
					'contactPersonEmail'=>$val->email,
					'contactPersonId'=>$val->id,
					'projectId'=>$val->projectRefId,
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function addProjectDetail($param,$imageName)
	{
		if ($param['projectName'] == '') 
		{
            $error   = 'Project Name is required';
            $result   = new stdClass();
			$success = 0;
        } 
		else if ($param['projectLocation'] == '') 
		{
            $error   = 'Project Location is required';
            $result   = new stdClass();
            $success = 0;
        } 
		else if ($param['currentLocation'] == '') 
		{
            $error   = 'Current Location is required';
            $result   = new stdClass();
            $success = 0;
        } 
		else if ($param['consultant'] == '') 
		{
            $error   = 'Consultant is required';
            $result   = new stdClass();
            $success = 0;
        } 
		else if ($param['budget'] == '') 
		{
            $error   = 'Budget is required';
            $result   = new stdClass();
            $success = 0;
        } 
		else if ($param['scopeOfWork'] == '') 
		{
            $error   = 'Scope of Work is required';
            $result   = new stdClass();
            $success = 0;
        } 
		else if ($param['contractor'] == '') 
		{
            $error   = 'Contractor is required';
            $result   = new stdClass();
            $success = 0;
        } 
		else if ($param['phaseOfProject'] == '') 
		{
            $error   = 'Phase of Project is required';
            $result   = new stdClass();
            $success = 0;
        } 
		else if ($param['requirement'] == '') 
		{
            $error   = 'Requirement is required';
            $result   = new stdClass();
            $success = 0;
        } 
		else if ($param['projectPlan'] == '') 
		{
            $error   = 'Project Plan is required';
            $result   = new stdClass();
            $success = 0;
        }
		else if ($imageName == '') 
		{
            $error   = 'Project Image is required';
            $result   = new stdClass();
            $success = 0;
        }
		else if ($param['contactPerson'] == '') 
		{
            $error   = 'Contact Person is required';
            $result   = new stdClass();
            $success = 0;
        }
		else if ($param['scheduleVisit'] == '') 
		{
            $error   = 'Schedule Visit is required';
            $result   = new stdClass();
            $success = 0;
        }
		else
		{
			$data = array(
				'projectRefId'=>generateRef(),
				'project_name'=>$param['projectName'],
				'project_location'=>$param['projectLocation'],
				'geo_location'=>$param['currentLocation'],
				'latitude'=>$param['latitude'],
				'longitude'=>$param['longitude'],
				'project_requirements'=>$param['requirement'],
				'consultant'=>$param['consultant'],
				'bugdet'=>$param['budget'],
				'contact_information'=>$param['contactPerson'],
				'schedule_visit'=>$param['scheduleVisit'],
				'project_plan'=>$param['projectPlan'],
				'project_plan_image'=>$imageName,
				'contractor_information'=>$param['contractor'],
				'project_phase'=>$param['phaseOfProject'],
			);
			if(!isset($param['id']))
			{
				$data['added_by'] = $param['userId'];
				$data['addedondate'] = date('Y-m-d');
				$this->db->insert('iserve_project_detail',$data);
				$lastId  = $this->db->insert_id();
				$db_error = $this->db->error();
			}
			else
			{
				$this->db->where('id',$param['id']);
				$this->db->update('iserve_project_detail',$data);
				$db_error = $this->db->error();
			}
			if ($db_error['code'] != 0) 
			{
				$success = 0;
				$error = $db_error['message'];
				$result = new stdClass();
			} 
			else
			{
				$success = 1;
				if(!isset($param['id']))
				{
					$result = array('id'=>$lastId);
				}
				else
				{
					$result = array('id'=>$param['id']);
				}
				$error = 'No error found';
			}
			
		}
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;
	}
	public function getAllVisitListByUserId($userId)
	{
		$this->db->select('iserve_project_visit_detail.*,iserve_project_detail.project_name');
    	$this->db->from('iserve_project_visit_detail');
    	$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_project_visit_detail.projectRefId','left');
    	$this->db->where('iserve_project_visit_detail.added_by',$userId);
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				if($val->visit_image != '')
				{
					$image = site_url('/assets/upload/project/'.$val->visit_image);
				}
				else
				{
					$image = '';
				}
				$results[] = array(
					'visitId'=>$val->id,
					'description'=>$val->description,
					'image'=>$image,
					'projectId'=>$val->projectRefId,
					'projectName'=>$val->project_name,
					'visitDate'=>$val->visit_date,
					
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function getAllProjectVisitList($userId)
	{
		$this->db->select('iserve_project_visit_detail.projectRefId,iserve_project_detail.project_name');
    	$this->db->from('iserve_project_visit_detail');
    	$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_project_visit_detail.projectRefId','inner');
    	$this->db->where('iserve_project_visit_detail.added_by',$userId);
		$this->db->group_by('iserve_project_visit_detail.projectRefId');
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				$results[] = (object)array(
					'projectId'=>$val->projectRefId,
					'projectName'=>$val->project_name,
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function getAllVisitList($projectId)
	{
		$this->db->select('iserve_project_visit_detail.*,iserve_project_detail.project_name');
    	$this->db->from('iserve_project_visit_detail');
    	$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_project_visit_detail.projectRefId','left');
    	$this->db->where('iserve_project_visit_detail.projectRefId',$projectId);
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				if($val->visit_image != '')
				{
					$image = site_url('/assets/upload/project/'.$val->visit_image);
				}
				else
				{
					$image = '';
				}
				$results[] = array(
					'visitId'=>$val->id,
					'description'=>$val->description,
					'image'=>$image,
					'projectId'=>$val->projectRefId,
					'projectName'=>$val->project_name,
					'visitDate'=>$val->visit_date,
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function getVisitDetailById($visitId)
	{
		$this->db->select('iserve_project_visit_detail.*,iserve_project_detail.project_name');
    	$this->db->from('iserve_project_visit_detail');
    	$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_project_visit_detail.projectRefId','left');
    	$this->db->where('iserve_project_visit_detail.id',$visitId);
    	$result = $this->db->get();	
		$result = $result->row();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			if($result->visit_image != '')
			{
				$image = site_url('/assets/upload/project/'.$result->visit_image);
			}
			else
			{
				$image = '';
			}
			$results = array(
				'visitId'=>$result->id,
				'description'=>$result->description,
				'image'=>$image,
				'projectId'=>$result->projectRefId,
				'projectName'=>$result->project_name,
				'visitDate'=>$result->visit_date,
			);
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = new stdClass();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function addVisit($param,$imageName)
	{
		if ($imageName == '') 
		{
            $error   = 'Image is required';
            $result   = 'Image is required';
			$success = 0;
        } 
		else if ($param['description'] == '') 
		{
            $error   = 'Description is required';
            $result   = 'Description is required';
            $success = 0;
        } 
		else if ($param['projectId'] == '') 
		{
            $error   = 'Project is required';
            $result   = 'Project is required';
            $success = 0;
        }
		else if ($param['visitDate'] == '') 
		{
            $error   = 'Visit date is required';
            $result   = 'Visit date is required';
            $success = 0;
        }
		else
		{
			$data = array(
				'projectRefId'=>$param['projectId'],
				'visit_image'=>$imageName,
				'description'=>$param['description'],
				'visit_date'=>$param['visitDate'],
			);
			if($param['visitId'] == '')
			{
				$data['added_by'] = $param['userId'];
				$data['addedondate'] = date('Y-m-d');
				$this->db->insert('iserve_project_visit_detail',$data);
				$db_error = $this->db->error();
			}
			else
			{
				$this->db->where('id',$param['visitId']);
				$this->db->update('iserve_project_visit_detail',$data);
				$db_error = $this->db->error();
			}
			if ($db_error['code'] != 0) 
			{
				$success = 0;
				$error = $db_error['message'];
				$result = $db_error['message'];
			} 
			else
			{
				$success = 1;
				if($param['visitId'] == '')
				{
					$result = 'Visit added successfully';
				}
				else
				{
					$result = 'Visit updated successfully';
				}
				$error = 'No error found';
			}
			
		}
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;		
	}
	public function deleteVisit($visitId)
	{
		$this->db->where('id',$visitId);
		$this->db->delete('iserve_project_visit_detail');
		$db_error = $this->db->error();
		if ($db_error['code'] != 0) 
		{
			$success = 0;
			$result = $db_error['message'];
			$error = $db_error['message'];
		} 
		else
		{
			$success = 1;
			$result = "Visit deleted successfully";
			$error = 'No error found';
		}
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;
		
	}
	public function getProjectDetailId($projectId)
	{
		$this->db->select('iserve_project_detail.*,iserve_project_phase.project_phase as project_phase_name');
    	$this->db->from('iserve_project_detail');
    	$this->db->join('iserve_project_phase','iserve_project_phase.id = iserve_project_detail.project_phase','left');
    	$this->db->where('iserve_project_detail.projectRefId',$projectId);
		
    	$result = $this->db->get();	
		$result = $result->row();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			if($result->project_plan_image != '')
			{
				$projectImage = site_url('/assets/upload/project/'.$result->project_plan_image);
			}
			else
			{
				$projectImage = '';
			}
			
			$contactPerson = explode(",",$result->contact_information);
			
			$this->db->select('id as contactPersonId,name');
			$this->db->from('iserve_contact_person');
			$this->db->where_in('id',$contactPerson);
			$contactInfo = $this->db->get();	
			$contactInfo = $contactInfo->result();
			
			$contractor = explode(",",$result->contractor_information);
			
			$this->db->select('id as contractorId,contractor_name as name');
			$this->db->from('iserve_contractor');
			$this->db->where_in('id',$contractor);
			$contractorInfo = $this->db->get();	
			$contractorInfo = $contractorInfo->result();
			
			$results = array(
				'projectId'=>$result->projectRefId,
				'projectName'=>$result->project_name,
				'projectLocation'=>$result->project_location,
				'currentLocation'=>$result->geo_location,
				'latitude'=>$result->latitude,
				'longitude'=>$result->longitude,
				'projectRequirement'=>$result->project_requirements,
				'consultant'=>$result->consultant,
				'bugdet'=>$result->bugdet,
				'contactPerson'=>$contactInfo,
				'scheduleVisit'=>$result->schedule_visit,
				'projectPlan'=>$result->project_plan,
				'projectImage'=>$projectImage,
				'contractor'=>$contractorInfo,
				'projectPhaseId'=>$result->project_phase,
				'projectPhase'=>$result->project_phase_name,
			); 
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = new stdClass();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function addAdditionalInfo($param,$imageName,$thumbnail)
	{
		if ($imageName == '') 
		{
            $error   = 'Document is required';
            $result   = 'Document is required';
			$success = 0;
        } 
		else if ($param['projectId'] == '') 
		{
            $error   = 'Project is required';
            $result   = 'Project is required';
            $success = 0;
        } 
		else if ($param['uploadType'] == '') 
		{
            $error   = 'Upload Type is required';
            $result   = 'Upload Type is required';
            $success = 0;
        }
		else if ($param['documentTitle'] == '') 
		{
            $error   = 'Document Title is required';
            $result   = 'Document Title is required';
            $success = 0;
        }
		else
		{
			$data = array(
				'refId'=>$param['projectId'],
				'upload_document'=>$imageName,
				'upload_type'=>$param['uploadType'],
				'document_title'=>$param['documentTitle'],
				'thumbnail' => $thumbnail,
				'added_by'=>$param['userId'],
				'addedondate'=>date('Y-m-d')
			);
			$this->db->insert('iserve_project_additional_information',$data);
			$db_error = $this->db->error();
			
			if ($db_error['code'] != 0) 
			{
				$success = 0;
				$error = $db_error['message'];
				$result = $db_error['message'];
			} 
			else
			{
				$success = 1;
				$result = 'Additional information added successfully';
				$error = 'No error found';
			}
			
		}
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;
	}
	public function getAdditionalInfo($projectId)
	{
		$this->db->select('*');
    	$this->db->from('iserve_project_additional_information');
    	$this->db->where('refId',$projectId);
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				if($val->upload_document != '')
				{
					$image = site_url('/assets/upload/project/'.$val->upload_document);
				}
				else
				{
					$image = '';
				}
				if($val->thumbnail != '')
				{
					$thumbnail = site_url('/assets/upload/project/'.$val->thumbnail);
				}
				else
				{
					$thumbnail = '';
				}
				$results[] = array(
					'additionalId'=>$val->id,
					'projectId'=>$val->refId,
					'uploadType'=>$val->upload_type,
					'documentTitle'=>$val->document_title,
					'uploadDocument'=>$image,
					'thumbnail'=>$thumbnail,
					'date'=>$val->addedondate
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function getPhaseOfProject()
	{
		$this->db->select('*');
    	$this->db->from('iserve_project_phase');
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				$results[] = array(
					'projectPhaseId'=>$val->id,
					'projectPhase'=>$val->project_phase,
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function addAttendanceDetail($param,$image)
	{
		if ($param['time'] == '') 
		{
            $error   = 'Time is required';
            $result   = 'Time is required';
			$success = 0;
        } 
		else if ($image == '') 
		{
            $error   = 'Selfie is required';
            $result   = 'Selfie is required';
            $success = 0;
        } 
		else if ($param['currentAddress'] == '') 
		{
            $error   = 'Current location is required';
            $result   = 'Current location is required';
            $success = 0;
        } 
		else if ($param['latitude'] == '') 
		{
            $error   = 'Latitude is required';
            $result   = 'Latitude is required';
            $success = 0;
        } 
		else if ($param['longitude'] == '') 
		{
            $error   = 'Longitude is required';
            $result   = 'Longitude is required';
            $success = 0;
        }
		else
		{
			$data = array(
				'userRefId'=>$param['userId'],
				'attendance_date'=>$param['date'],
				'attendance_time'=>$param['time'],
				'user_selfie'=>$image,
				'addedondate'=>date('Y-m-d'),
				'latitude'=>$param['latitude'],
				'longitude'=>$param['longitude'],
				'current_address'=>$param['currentAddress'],
			);
			$attendanceTime = explode(":",$param['time']);
		
			$this->db->select('attendance_time');
			$this->db->from('iserve_user_attendance');
			$this->db->where('attendance_date',$param['date']);
			$result1 = $this->db->get();	
			$result = $result1->result();
			if(!empty($result))
			{
				foreach($result as $val)
				{
					$attTime[] = explode(":",$val->attendance_time);
				}
				foreach($attTime as $val1)
				{
					
					$attTime1[] = $val1[0];
				}
				if(!in_array($attendanceTime[0], $attTime1))
				{
					$this->db->insert('iserve_user_attendance',$data);
					$db_error = $this->db->error();
					
					if ($db_error['code'] != 0) 
					{
						$success = 0;
						$error = $db_error['message'];
						$result = $db_error['message'];
					} 
					else
					{
						$success = 1;
						$result = 'Selfie uploaded successfully';
						$error = 'No error found';
					}
				}
				else
				{
					$success = 0;
					$error = 'Selfie already uploaded';
					$result = 'Selfie already uploaded';
				}
			}
			else
			{
				$this->db->insert('iserve_user_attendance',$data);
				$db_error = $this->db->error();
				
				if ($db_error['code'] != 0) 
				{
					$success = 0;
					$error = $db_error['message'];
					$result = $db_error['message'];
				} 
				else
				{
					$success = 1;
					$result = 'Selfie uploaded successfully';
					$error = 'No error found';
				}
			}
			
		}
		
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;
	}
	public function getTodayAttendanceDetail($param)
	{
		$this->db->select('*');
    	$this->db->from('iserve_user_attendance');
    	$this->db->where('userRefId',$param['userId']);
		if($param['date'] == '')
		{
			$this->db->where('attendance_date',date('Y-m-d'));
		}
		else
		{
			$this->db->where('attendance_date',$param['date']);
		}
    	$this->db->order_by('id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				if($val->user_selfie != '')
				{
					$image = site_url('/assets/upload/attendance/'.$val->user_selfie);
				}
				else
				{
					$image = '';
				}
				$results[] = array(
					'time'=>$val->attendance_time,
					'userImage'=>$image,
					'latitude'=>$val->latitude,
					'longitude'=>$val->longitude,
					'currentAddress'=>$val->current_address,
					'status'=>$val->status,
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function submitProjectToAdmin($projectId)
	{
		$status = array('status'=>1);
		$this->db->where('projectRefId',$projectId);
		$this->db->update('iserve_project_detail',$status);
		$db_error = $this->db->error();
			
		if ($db_error['code'] != 0) 
		{
			$success = 0;
			$error = $db_error['message'];
			$result = $db_error['message'];
		} 
		else
		{
			$success = 1;
			$result = 'Project Submitted successfully';
			$error = 'No error found';
		}
		$array = array(
            'success' => $success,
            'result' => $result,
            'error' => $error
        );
        return $array;
	}
}