<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class FieldCoordinator extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getWorkerAttendanceDetail($param)
	{
		$this->db->select('*');
    	$this->db->from('iserve_user_attendance');
    	$this->db->where('userRefId',$param['workerId']);
		if($param['date'] == '')
		{
			$this->db->where('attendance_date',date('Y-m-d'));
		}
		else
		{
			$this->db->where('attendance_date',$param['date']);
		}
    	$this->db->order_by('id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				if($val->user_selfie != '')
				{
					$image = site_url('/assets/upload/attendance/'.$val->user_selfie);
				}
				else
				{
					$image = '';
				}
				$results[] = array(
					'time'=>$val->attendance_time,
					'userImage'=>$image,
					'status'=>$val->status,
					'currentAddress'=>$val->current_address,
					'latitude'=>$val->latitude,
					'longitude'=>$val->longitude,
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
		
	}
	public function getProjectTask($projectId,$status)
	{
		$this->db->select('iserve_task.taskRefId,iserve_task.task_subject,iserve_task.status');
    	$this->db->from('iserve_task');
    	$this->db->where('iserve_task.projectRefId',$projectId);
		if($status == 0)
		{
			$this->db->where('iserve_task.status',$status);
		}
		else
		{
			$this->db->where('iserve_task.status',1);
			$this->db->or_where('iserve_task.status',2);
		}
    	
		$this->db->order_by('iserve_task.id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				$workers = getTaskWorkers($val->taskRefId);
				$results[] = array(
					'taskId' => $val->taskRefId,
					'taskName' => $val->task_subject,
					'workerName' => $workers,
					'status' => $val->status,
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function getTaskProjectLists($userId,$status)
	{
		$this->db->select('iserve_project_detail.projectRefId,iserve_project_detail.project_name');
    	$this->db->from('iserve_task');
    	$this->db->join(' iserve_project_detail',' iserve_project_detail.projectRefId = iserve_task.projectRefId','left');
    	$this->db->where('iserve_task.task_created_by',$userId);
		if($status == 0)
		{
			$this->db->where('iserve_task.status',$status);
		}
		else
		{
			$this->db->where('iserve_task.status',1);
			$this->db->or_where('iserve_task.status',2);
		}
    	
    	$this->db->group_by('iserve_task.projectRefId');
		$this->db->order_by('iserve_task.id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				$results[] = array(
					'projectId'=>$val->projectRefId,
					'projectName'=>$val->project_name,
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function getTaskDetailById($taskId)
	{
		$this->db->select('iserve_task.*,iserve_project_detail.project_name');
    	$this->db->from('iserve_task');
		$this->db->join(' iserve_project_detail',' iserve_project_detail.projectRefId = iserve_task.projectRefId','left');
    	$this->db->where('iserve_task.taskRefId',$taskId);
    	$result = $this->db->get();	
		$result = $result->row();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			$workers = getTaskWorkers($result->taskRefId);
			$asset = getAssetName(unserialize($result->assetRefId));
			$component = getComponentName(unserialize($result->componentRefId));
			$results = (object)array(
				'taskId' => $result->taskRefId,
				'taskName' => $result->task_subject,
				'workerName' => $workers,
				'components' => $component,
				'asset' => $asset,
				'status' => $result->status,
				'description' => $result->description,
				'projectName' => $result->project_name,
			);
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = new stdClass();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function getWorkerTasks($workerId)
	{
		$this->db->select('iserve_task.task_subject,iserve_task.status,iserve_task.taskRefId');
    	$this->db->from('iserve_workers_task');
		$this->db->join(' iserve_task',' iserve_task.taskRefId = iserve_workers_task.taskRefId','left');
    	$this->db->where('iserve_workers_task.workerRefId',$workerId);    	
		$this->db->where('iserve_workers_task.status',0);
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				$results[] = array(
					'taskId' => $val->taskRefId,
					'taskName' => $val->task_subject,
					'status' => $val->status,
				);
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function getInvoiceDetailById($invoiceId)
	{
		$this->db->select('iserve_invoice.*,iserve_project_detail.project_name,iserve_user_detail.first_name,iserve_user_detail.last_name');		
		$this->db->from('iserve_invoice');		
		$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_invoice.projectRefId','inner');		
		$this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_invoice.assigned_by','left');		
		$this->db->order_by('iserve_invoice.id',$invoiceId);		
		$result = $this->db->get();			
		$result = $result->row();	
	
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			$results = array(
				'invoiceId' => $result->id,
				'invoiceNumber' => $result->invoice_no,
				'secretaryName' => $result->first_name.' '.$result->last_name,
				'assignedDate' => $result->created_date,
				'invoiceDate' => $result->created_date,
				'status' => $result->status,
			);
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = new stdClass();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function getTechnicalDocumentProject()
	{
		$this->db->select('iserve_project_detail.project_name,iserve_project_detail.projectRefId');
    	$this->db->from('project_document');
    	$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = project_document.projectRefId','left');
    	$this->db->group_by('iserve_project_detail.projectRefId');
    	$result = $this->db->get();	
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				$results[] = array(
					'projectId' => $val->projectRefId,
					'projectName' => $val->project_name,
				);
			}
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function getDocument($projectId)
	{
		$this->db->select('*');
    	$this->db->from('project_document');
    	$this->db->where('projectRefId',$projectId);
    	$result = $this->db->get();	
		$result = $result->row();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			if($result->document != '')
			{
				$technicalDocument = site_url('/assets/upload/project/'.$result->document);
			}
			else
			{
				$technicalDocument = '';
			}
			$results = (object)array(
				'technicalDocument'=>$technicalDocument,
			);
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = new stdClass();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function getProductDetailById($projectId)
	{
		$this->db->select('iserve_product.*,iserve_project_detail.project_name');
        $this->db->from('iserve_product');
        $this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_product.projectRefId','left');
        $this->db->where('iserve_product.projectRefId',$projectId);
        $result = $this->db->get();
		$result = $result->row();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			if($result->image != '')
			{
				$productImage = site_url('/assets/upload/product/'.$result->image);
			}
			else
			{
				$productImage = '';
			}
			$results = (object)array(
				'projectName'=>$result->project_name,
				'productImage'=>$productImage,
				'capacity'=>$result->capacity,
				'maxLoad'=>$result->max_load,
				'maxReach'=>$result->max_reach,
				'tipLoad'=>$result->tip_load,
				'description'=>$result->description,
			);
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = new stdClass();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function addCommentDetail($param,$image)
	{
		if ($param['comment'] == '') 
		{
            $error   = 'Comment is required';
            $result   = 'Comment is required';
            $success = 0;
        } 
		else
		{
			$data = array(
				'refId'=>$param['id'],
				'comment'=>$param['comment'],
				'attachment'=>$image,
				'commented_by'=>$param['userId'],
				'type'=>$param['commentType'],
				'commented_date'=>date('Y-m-d')
			);
			$this->db->insert('iserve_comment',$data);
			$db_error = $this->db->error();
			
			if ($db_error['code'] != 0) 
			{
				$success = 0;
				$error = $db_error['message'];
				$result = $db_error['message'];
			} 
			else
			{
				$success = 1;
				$result = 'Comment added successfully';
				$error = 'No error found';
			}
			
		}
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;
	}
	public function getCommentListById($refId,$type)
	{
		$this->db->select('*');
        $this->db->from('iserve_comment');
        $this->db->where('refId',$refId);
        $this->db->where('type',$type);
        $result = $this->db->get();
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				
				$userName = userDetail($val->commented_by);
				
				
				if($val->attachment != '')
				{
					$attachment = site_url('/assets/upload/comment/'.$val->attachment);
				}
				else
				{
					$attachment = '';
				}
				$results[] = array(
					'comment'=>$val->comment,
					'attachment'=>$attachment,
					'commentDate'=>$val->commented_date,
					'userName'=>$userName->first_name.' '.$userName->last_name,
				);
				
			}
			
			
			
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
	public function taskCompleted($taskId,$status)
	{
		$data = array('status'=>$status);
		$this->db->where('taskRefId',$taskId);
		$this->db->update('iserve_task',$data);
		$db_error = $this->db->error();
		if ($db_error['code'] != 0) 
		{
			$success = 0;
			$error = $db_error['message'];
			$result = $db_error['message'];
		} 
		else
		{
			$success = 1;
			$result = 'Task completed successfully';
			$error = 'No error found';
		}
		$array = array('success'=>$success,'result'=>$result,'error'=>$error);
		return $array;
		
	}
	public function getProjectByMonth($userId)
	{
		$this->db->select('*');
        $this->db->from('iserve_project_detail');
        $this->db->where('added_by',$userId);
        $this->db->like('addedondate',date('Y-m'));
        $this->db->or_like('addedondate',date('Y-m',strtotime("-1 month")));
        $this->db->or_like('addedondate',date('Y-m',strtotime("+1 month")));
        $result = $this->db->get();
		$result = $result->result();
		if(!empty($result))
		{
			$error = 'No error found';
			$success = 1;
			foreach($result as $val)
			{
				
				$results[] = array(
					'projectName'=>$val->project_name,
					'projectId'=>$val->projectRefId,
					'date'=>$val->addedondate,
				);
				
			}
		}
		else
		{
			$error = 'No record found';
			$success = 0;
			$results = array();
		}
		$array = array(
            'success' => $success,
            'result' => $results,
            'error' => $error
        );
        return $array;
	}
		
}