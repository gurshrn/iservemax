<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	/*=================Get all user detail=================*/
	
	public function addUser($data,$param)
	{
		$this->db->set('email', "AES_ENCRYPT('{$param['email']}','/*awshp$*/')", FALSE);
        $this->db->set('phone_number', "AES_ENCRYPT('{$param['phone_number']}','/*awshp$*/')", FALSE);
        if(isset($param['password']))
        {
        	$this->db->set('password', "AES_ENCRYPT('{$param['password']}','/*awshp$*/')", FALSE);
        }
        if($param['id'] == '')
		{
			$this->db->insert('iserve_login_detail',$data);
		}
		else
		{
			$this->db->where('userRefId',$param['userRefId']);
			$this->db->update('iserve_login_detail',$data);
		}
        
        $db_error = $this->db->error();
        if ($db_error['code'] != 0) 
        {
            $result['success'] = false;
            $result['error_message'] = $db_error['message'];
        } 
        else 
        {
        	if(isset($param['image']))
        	{
        		$userdata['image'] = $param['image'];
        	}
        	$userdata = array(
                    'gender' => $param['gender'],
                    'dob' => $param['dob'],
                    'description' => $param['description'],
                    'first_name' => $param['first_name'],
                    'last_name' => $param['last_name'],
                    'address' => $param['address'],
            );
			if($param['id'] == '')
			{
				$userdata['userRefId'] = $data['userRefId'];
				$userdata['created_date'] = date('Y-m-d');
				$this->db->insert('iserve_user_detail',$userdata);
			}
			else
			{
				$this->db->where('userRefId',$param['userRefId']);
				$this->db->update('iserve_user_detail',$userdata);
			}
            
            $result['success'] = true;
            if($param['id'] == '')
			{
				$result['success_message'] = 'User added successfully';
			}
			else
			{
				$result['success_message'] = 'User updated successfully';
			}
            
        }
        return $result;
	}
	
	public function getUserbyId($userRefId)
	{
		$this->db->select('iserve_login_detail.*,iserve_user_detail.*,AES_DECRYPT(iserve_login_detail.email,"/*awshp$*/") as email, AES_DECRYPT(iserve_login_detail.phone_number,"/*awshp$*/") as phone_number, AES_DECRYPT(iserve_login_detail.password,"/*awshp$*/") as password');
        $this->db->from('iserve_login_detail');
        $this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_login_detail.userRefId','left');
		$this->db->where('iserve_login_detail.userRefId',$userRefId);
        $result = $this->db->get();
        $result = $result->row();
        return $result;
	}
	
	public function getAllUsers()
    {
	 	$this->db->select('iserve_login_detail.*,iserve_user_detail.*,AES_DECRYPT(iserve_login_detail.email,"/*awshp$*/") as email, AES_DECRYPT(iserve_login_detail.phone_number,"/*awshp$*/") as phone_number');
        $this->db->from('iserve_login_detail');
        $this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_login_detail.userRefId','left');
        $result = $this->db->get();
        $result = $result->result();
        return $result;
    }
	
	public function getProjectDetail()
	{
		$this->db->select('*');
        $this->db->from('iserve_project_detail');
        $this->db->where('status','1');
        $result = $this->db->get();
        $result = $result->result();
        return $result;
	}
	
	public function addProjectDetail($param,$document)
	{
		unset($param['owner_name']);
		unset($param['owner_designation']);
		unset($param['owner_phone']);
		unset($param['last_name']);
		unset($param['contractor_name']);
		unset($param['scope_of_work']);
		unset($param['contact_name']);
		unset($param['contact_designation']);
		unset($param['contact_phone']);
		if($param['id'] == '')
		{
			$this->db->insert('iserve_project_detail',$param);
		}
		else
		{
			$this->db->where('projectRefId',$param['projectRefId']);
			$this->db->update('iserve_project_detail',$param);
		}
		
		$db_error = $this->db->error();
        if ($db_error['code'] == 0) 
        {
        	if($param['id'] == '')
			{
				$document['addedondate'] = date('Y-m-d');
				$document['projectRefId'] = $param['projectRefId'];
				$this->db->insert('project_document',$document);
			}
			else
			{
				$this->db->where('projectRefId',$param['projectRefId']);
				$this->db->update('project_document',$param);
			}
            return true;
        }
		else
		{
			return false;
		}	
		
	}
	public function getProjectDetailById($projectRefId)
	{
		$this->db->select('*');
        $this->db->from('iserve_project_detail');
		$this->db->where('projectRefId',$projectRefId);
		$result = $this->db->get();
        $result = $result->row();
        return $result;
	}
	public function getAssestDetail()
	{
		$this->db->select('*');
        $this->db->from('iserve_asset');
		$result = $this->db->get();
        $result = $result->result();
        return $result;
	}
	public function addAssetDetail($param)
	{
		unset($param['purshase_date']);
		unset($param['warranty_validity']);
		unset($param['po_number']);
		unset($param['inspected_date']);
		unset($param['inspected_time']);
		if($param['id'] == '')
		{
			$this->db->insert('iserve_asset',$param);
		}
		else
		{
			$this->db->where('assetRefId',$param['assetRefId']);
			$this->db->update('iserve_asset',$param);
		}
		$db_error = $this->db->error();
		if ($db_error['code'] != 0) 
        {
            return true;
        }
		else
		{
			return false;
		}	
		
	}
	public function getAssetDetailById($assetRefId)
	{
		$this->db->select('*');
        $this->db->from('iserve_asset');
		$this->db->where('assetRefId',$assetRefId);
		$result = $this->db->get();
        $result = $result->row();
        return $result;
	}
	public function addAssignAsset($data)
	{
		foreach($data['assign_asset'] as $val)
		{
			$param['addedondate'] = $data['addedondate'];
			$param['projectRefId'] = $data['projectRefId'];
			$param['refId'] = $data['refId'];
			$param['assetRefId'] = $val;
			$this->db->insert('iserve_project_asset',$param);
		}
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			$result['success'] = true;
			$result['projectRefId'] = $param['projectRefId'];
		}
		else
		{
			$result['success'] = false;
		}
		return $result;
	}
	public function getProjectAssetDetailById($projectRefId)
	{
		$this->db->select('iserve_project_asset.*,iserve_asset.asset');
        $this->db->from('iserve_project_asset');
        $this->db->join('iserve_asset','iserve_asset.assetRefId = iserve_project_asset.assetRefId','left');
		$this->db->where('iserve_project_asset.projectRefId',$projectRefId);
		$this->db->order_by('iserve_project_asset.id','Desc');
		$result = $this->db->get();
        $result = $result->result();
        return $result;
	}
	public function getProjectComponentDetailById($projectRefId)
	{
		$this->db->select('iserve_project_asset_component.*,iserve_asset.asset,iserve_components.component_name');
        $this->db->from('iserve_project_asset_component');
        $this->db->join('iserve_asset','iserve_asset.assetRefId = iserve_project_asset_component.assetRefId','left');
        $this->db->join('iserve_components','iserve_components.compoRefId = iserve_project_asset_component.componentRefId','left');
		$this->db->where('iserve_project_asset_component.projectRefId',$projectRefId);
		$this->db->order_by('iserve_project_asset_component.id','Desc');
		$result = $this->db->get();
        $result = $result->result();
        return $result;
	}
	public function getAssetComponentDetailById($projectRefId,$assetRefId)
	{
		//foreach($assetRefId as $val)
		//{
			$this->db->select('iserve_project_asset_component.*,iserve_asset.asset,iserve_components.component_name,iserve_components.part_no');
	        $this->db->from('iserve_project_asset_component');
	        $this->db->join('iserve_asset','iserve_asset.assetRefId = iserve_project_asset_component.assetRefId','left');
	        $this->db->join('iserve_components','iserve_components.compoRefId = iserve_project_asset_component.componentRefId','left');
			$this->db->where('iserve_project_asset_component.projectRefId',$projectRefId);
			$this->db->where_in('iserve_project_asset_component.assetRefId',$assetRefId);
			$this->db->order_by('iserve_project_asset_component.id','Desc');
			$result = $this->db->get();
	        $result = $result->result();
		//}
		return $result;
	}
	public function geAllComponentDetail()
	{
		$this->db->select('*');
        $this->db->from('iserve_components');
		$result = $this->db->get();
        $result = $result->result();
        return $result;
	}
	public function addComponentDetail($param)
	{
		if($param['id'] == '')
		{
			$this->db->insert('iserve_components',$param);
		}
		else
		{
			$this->db->where('compoRefId',$param['compoRefId']);
			$this->db->update('iserve_components',$param);
		}
		$db_error = $this->db->error();
		if ($db_error['code'] != 0) 
        {
            return true;
        }
		else
		{
			return false;
		}	
	}
	public function getComponentDetailById($compoRefId)
	{
		$this->db->select('*');
        $this->db->from('iserve_components');
		$this->db->where('compoRefId',$compoRefId);
		$result = $this->db->get();
        $result = $result->row();
        return $result;
	}
	public function addAssignComponent($data)
	{
		foreach($data['assign_component'] as $val)
		{
			$param['addedondate'] = $data['addedondate'];
			$param['projectRefId'] = $data['projectRefId'];
			$param['refId'] = $data['refId'];
			$param['componentRefId'] = $val;
			$param['assetRefId'] = $data['project_asset'];
			$this->db->insert('iserve_project_asset_component',$param);
		}
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			$result['success'] = true;
			$result['projectRefId'] = $param['projectRefId'];
			$result['assetRefId'] = $param['assetRefId'];
		}
		else
		{
			$result['success'] = false;
		}
		return $result;

	}
	public function assignProjectToUser($param)
	{
		$this->db->insert('iserve_assign_project',$param);
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			$result['success'] = true;
		}
		else
		{
			$result['success'] = false;
		}
		return $result;
	}
	public function getTaskDetail()
	{
		$this->db->select('*');
        $this->db->from('iserve_task');
		$result = $this->db->get();
        $result = $result->result();
        return $result;
	}
	
	public function getWorkerDetail()
	{
		$this->db->select('iserve_login_detail.*,iserve_user_detail.last_name as last_name,iserve_user_detail.image');
        $this->db->from('iserve_login_detail');
        $this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_login_detail.userRefId','left');
        $this->db->where('iserve_login_detail.role',2);
		$result = $this->db->get();
        $result = $result->result();
        return $result;
	}
	public function getFieldCoordinatorDetail()
	{
		$this->db->select('iserve_login_detail.*,iserve_user_detail.last_name as last_name');
        $this->db->from('iserve_login_detail');
        $this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_login_detail.userRefId','left');
        $this->db->where('iserve_login_detail.role',3);
		$result = $this->db->get();
        $result = $result->result();
        return $result;
	}
	public function addSafetyContent($param)
	{
		$this->db->insert('iserve_safety_content',$param);
		$db_error = $this->db->error();
        if ($db_error['code'] != 0) 
        {
            $result['success'] = false;
            $result['error_message'] = $db_error['message'];
        } 
        else
        {
        	$result['success'] = true;
            $result['success_message'] = 'Safety Content added successfully';
		}
		return $result;

	}
	public function getSafetyContentDetail()
	{
		$this->db->select('*');
        $this->db->from('iserve_safety_content');
        $this->db->order_by('id','DESC');
		$result = $this->db->get();
        $result = $result->result();
        return $result;
	}
	public function getProductDetail()
	{
		$this->db->select('iserve_product.*,iserve_project_detail.project_name');
        $this->db->from('iserve_product');
        $this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_product.projectRefId','inner');
        $this->db->order_by('iserve_product.id','DESC');
		$result = $this->db->get();
        $result = $result->result();
        return $result;
	}
	public function getProductDetailById($id)
	{
		$this->db->select('iserve_product.*,iserve_project_detail.project_name');
        $this->db->from('iserve_product');
        $this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_product.projectRefId','left');
        $this->db->where('iserve_product.id',$id);
        $this->db->order_by('iserve_product.id','DESC');
		$result = $this->db->get();
        $result = $result->row();
        return $result;

	}
	public function addProductDetail($param)
	{
		if($param['id'] == '')
		{
			$this->db->insert('iserve_product',$param);
		}
		else
		{
			$this->db->where('id',$param['id']);
			$this->db->update('iserve_product',$param);
		}
		$db_error = $this->db->error();
        if ($db_error['code'] != 0) 
        {
            $result['success'] = false;
            $result['error_message'] = $db_error['message'];
        } 
        else
        {
        	$result['success'] = true;
        	if($param['id'] == '')
        	{
        		$result['success_message'] = 'Complaint added successfully';
        	}
        	else
        	{
        		$result['success_message'] = 'Complaint updated successfully';
        	}
            
		}
		return $result;
	}
	public function addNewsDetail($param)
	{
		if($param['id'] == '')
		{
			$this->db->insert('iserve_news',$param);

		}
		else
		{
			$this->db->where('id',$param['id']);
			$this->db->update('iserve_news',$param);
		}
		$db_error = $this->db->error();
        if ($db_error['code'] != 0) 
        {
            $result['success'] = false;
            $result['error_message'] = $db_error['message'];
        } 
        else
        {
        	$result['success'] = true;
        	if($param['id'] == '')
        	{
        		$result['success_message'] = 'News added successfully';
        	}
        	else
        	{
        		$result['success_message'] = 'News updated successfully';
        	}
            
		}
		return $result;

	}

	public function getNewsDetail()
	{
		$this->db->select('*');
    	$this->db->from('iserve_news');
    	$this->db->order_by('id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;
	}
	public function getNewsDetailById($id)
	{
		$this->db->select('*');
    	$this->db->from('iserve_news');
    	$this->db->where('id',$id);
    	$result = $this->db->get();	
		$result = $result->row();
		return $result;
	}	public function getInvoiceDetail()	{		$this->db->select('iserve_invoice.*,iserve_project_detail.project_name,iserve_user_detail.first_name,iserve_user_detail.last_name');		$this->db->from('iserve_invoice');		$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_invoice.projectRefId','left');		$this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_invoice.assigned_to','left');		$this->db->order_by('iserve_invoice.id','DESC');		$result = $this->db->get();			$result = $result->result();		return $result;	}	public function addInvoice($param)	{		unset($param['assignedName']);		$this->db->insert('iserve_invoice',$param);		$db_error = $this->db->error();        if ($db_error['code'] != 0)         {            $result['success'] = false;            $result['error_message'] = $db_error['message'];        }         else        {        	$result['success'] = true;			$result['success_message'] = 'Invoice added successfully';		}		return $result;	}	public function getCoordinator($projectRefId)	{		$this->db->select('iserve_assign_project.*,iserve_user_detail.first_name,iserve_user_detail.last_name');		$this->db->from('iserve_assign_project');		$this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_assign_project.userRefId','left');		$this->db->where('iserve_assign_project.projectRefId',$projectRefId);		$result = $this->db->get();			$result = $result->row();		return $result;	}
}