<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Project extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getProjectDetailByUserId($userRefId)
    {
    	$this->db->select('iserve_assign_project.*,iserve_project_detail.project_name,count(iserve_task.projectRefId) as totaltask');
    	$this->db->from('iserve_assign_project');
    	$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_assign_project.projectRefId','left');
    	$this->db->join('iserve_task','iserve_task.projectRefId = iserve_assign_project.projectRefId','left');
    	$this->db->where('iserve_assign_project.userRefId',$userRefId);
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;		
	}
	
	public function getAssignedProjectById($userRefId)
	{
		$this->db->select('*');
    	$this->db->from('iserve_project_detail');
    	$this->db->where('assigned_to',$userRefId);
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;	
	}
	

	public function getAssetDetail($userRefId)
	{
		$this->db->select('iserve_project_detail.project_name,iserve_project_detail.geo_location as gps_location,iserve_asset.*');
    	$this->db->from('iserve_project_detail');
		$this->db->join('iserve_project_asset','iserve_project_asset.projectRefId = iserve_project_detail.projectRefId','inner');
		$this->db->join('iserve_asset','iserve_asset.assetRefId = iserve_project_asset.assetRefId','left');
    	$this->db->where('iserve_project_detail.assigned_to',$userRefId);
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;		
	}
	public function getAssetDetailById($assetRefId)
	{
		$this->db->select('iserve_asset.*,iserve_project_asset.projectRefId,iserve_project_detail.project_name,iserve_project_detail.geo_location as gps_location');
    	$this->db->from('iserve_asset');
    	$this->db->join('iserve_project_asset','iserve_project_asset.assetRefId = iserve_asset.assetRefId','left');
    	$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_project_asset.projectRefId','left');
    	$this->db->where('iserve_project_asset.status',0);
    	$this->db->where('iserve_project_asset.assetRefId',$assetRefId);
    	$result = $this->db->get();	
		$result = $result->row();
		return $result;	
	}
	public function projectassetDetail($assetRefId)
	{
		$this->db->select('iserve_project_asset.*,iserve_project_detail.project_name');
    	$this->db->from('iserve_project_asset');
    	$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_project_asset.projectRefId','left');
    	$this->db->where('iserve_project_asset.assetRefId',$assetRefId);
    	$this->db->order_by('iserve_project_asset.id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;	
	}
	public function addComplaint($param)
	{
		$this->db->insert('iserve_complaint',$param);
		$db_error = $this->db->error();
        if ($db_error['code'] != 0) 
        {
            $result['success'] = false;
            $result['error_message'] = $db_error['message'];
        } 
        else
        {
        	$result['success'] = true;
            $result['success_message'] = 'Complaint added successfully';
		}
		return $result;

	}
	public function getDocumentDetail()
	{
		$this->db->select('project_document.*,iserve_project_detail.project_name');
    	$this->db->from('project_document');
    	$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = project_document.projectRefId','left');
    	$this->db->order_by('project_document.id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;	
	}
	public function getSafetyContentDetail()
	{
		$this->db->select('*');
    	$this->db->from('iserve_safety_content');
    	$this->db->order_by('id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;

	}
	public function getAllWorkers()
	{
		$this->db->select('iserve_login_detail.*,iserve_user_detail.*');
    	$this->db->from('iserve_login_detail');
    	$this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_login_detail.userRefId','left');
    	$this->db->where('iserve_login_detail.role',2);
    	$this->db->order_by('iserve_login_detail.id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;
	}
	public function getWorkerDetailById($userRefId)
	{
		$this->db->select('iserve_login_detail.*,iserve_user_detail.*,iserve_attendance.*');
    	$this->db->from('iserve_login_detail');
    	$this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_login_detail.userRefId','left');
    	$this->db->join('iserve_attendance','iserve_attendance.userRefId = iserve_login_detail.userRefId','left');
    	$this->db->where('iserve_login_detail.role',2);
    	$this->db->where('iserve_login_detail.userRefId',$userRefId);
    	$this->db->order_by('iserve_login_detail.id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;

	}
	public function complaintDetail($userRefId)
	{
		$this->db->select('iserve_complaint.*,iserve_project_detail.project_name');
    	$this->db->from('iserve_complaint');
    	$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_complaint.projectRefId','inner');
    	$this->db->where('iserve_complaint.complaint_assigned_to',$userRefId);
    	$this->db->order_by('iserve_complaint.id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;	
	}
	public function getComplaintDetailById($complaintId)
	{
		$this->db->select('iserve_complaint.*,iserve_project_detail.project_name,iserve_asset.asset,iserve_asset.serial_no,iserve_task.task_subject,iserve_task.taskId');
    	$this->db->from('iserve_complaint');
    	$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_complaint.projectRefId','left');
    	$this->db->join('iserve_task','iserve_task.taskRefId = iserve_complaint.taskRefId','left');
    	$this->db->join('iserve_asset','iserve_asset.assetRefId = iserve_complaint.assetRefId','left');
    	$this->db->where('iserve_complaint.id',$complaintId);
    	$this->db->order_by('iserve_complaint.id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;	
	}
	public function getProjectTaskDetailById($projectRefId)
	{
		$this->db->select('*');
    	$this->db->from('iserve_task');
    	$this->db->where('projectRefId',$projectRefId);
    	$this->db->order_by('id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;	
	}
	public function addCompleteComplaint($param)
	{
		$this->db->insert('iserve_complaint_notification',$param);
		$db_error = $this->db->error();
        if ($db_error['code'] != 0) 
        {
            $result['success'] = false;
            $result['error_message'] = $db_error['message'];
        } 
        else
        {
        	$status = array('status'=>$param['status']);
        	$this->db->where('complaint_number',$param['complaint_number']);
        	$this->db->update('iserve_complaint',$status);
        	$result['success'] = true;
            $result['success_message'] = 'Complaint completed successfully';
		}
		return $result;
	}
	public function notComplete($param)
	{
		$status = array('status'=> 0);
		$this->db->where('complaint_number',$param);
		$this->db->update('iserve_complaint',$status);
		$db_error = $this->db->error();
		if ($db_error['code'] != 0) 
        {
            $result['success'] = false;
            $result['error_message'] = $db_error['message'];
        } 
        else
        {
        	$result['success'] = true;
            $result['success_message'] = 'Complaint not completed successfully';
		}
		return $result;
	}
	
/*================get project asset==============*/

	public function getProjectAssetsDetailById($projectRefId)
	{
		$this->db->select('iserve_project_asset.*,iserve_asset.asset,iserve_asset.asset_no');
    	$this->db->from('iserve_project_asset');
    	$this->db->join('iserve_asset','iserve_asset.assetRefId = iserve_project_asset.assetRefId','left');
    	$this->db->where('iserve_project_asset.projectRefId',$projectRefId);
    	$this->db->order_by('iserve_project_asset.id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;	
	}

	public function getProductDetailById($id)
	{
		$this->db->select('iserve_product.*,iserve_project_detail.project_name');
        $this->db->from('iserve_product');
        $this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_product.projectRefId','left');
        $this->db->where('iserve_product.id',$id);
        $result = $this->db->get();
        $result = $result->row();
        return $result;
	}
	public function addContactUs($param)
	{
		$this->db->insert(' iserve_contact_us',$param);
		$db_error = $this->db->error();
        if ($db_error['code'] != 0) 
        {
            $result['success'] = false;
            $result['error_message'] = $db_error['message'];
        } 
        else
        {
        	$result['success'] = true;
            $result['success_message'] = 'Message sent successfully';
		}
		return $result;
	}
	public function getUserDetailById($userRefId)
	{
		$this->db->select('iserve_user_detail.*,iserve_login_detail.*,AES_DECRYPT(iserve_login_detail.email,"/*awshp$*/") as email, AES_DECRYPT(iserve_login_detail.phone_number,"/*awshp$*/") as phone_number, AES_DECRYPT(iserve_login_detail.password,"/*awshp$*/") as password');
    	$this->db->from(' iserve_user_detail');
    	$this->db->join('iserve_login_detail','iserve_login_detail.userRefId = iserve_user_detail.userRefId','left');
    	$this->db->where('iserve_user_detail.userRefId',$userRefId);
    	$result = $this->db->get();	
		$result = $result->row();
		return $result;	
	}
	public function getNewsList()
	{
		$this->db->select('*');
    	$this->db->from('iserve_news');
    	$this->db->order_by('id','DESC');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;	
	}
	public function getNewsDetailById($id)
	{
		$this->db->select('*');
    	$this->db->from('iserve_news');
    	$this->db->where('id',$id);
    	$result = $this->db->get();	
		$result = $result->row();
		return $result;	
	}
	public function getLatestNews()
	{
		$this->db->select('*');
    	$this->db->from('iserve_news');
    	$this->db->order_by('id','DESC');
    	$this->db->limit('3');
    	$result = $this->db->get();	
		$result = $result->result();
		return $result;
	}
	public function gettaskDetail()
	{
		$this->db->select('iserve_task.*,iserve_project_detail.project_name');		
		$this->db->from('iserve_task');		
		$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_task.projectRefId','left');		
		$this->db->where('iserve_task.status',0);		
		$this->db->order_by('iserve_task.id','DESC');		
		$result = $this->db->get();
		$result = 	$result->result();
		
		return $result;	
	}		
	public function getPendingTaskDetail()	
	{
		
		$userRefId = $this->session->userdata('userRefId');		
		$userrole = $this->session->userdata('role');		
		$this->db->select('iserve_task.*,iserve_project_detail.project_name');		
		$this->db->from('iserve_task');		
		$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_task.projectRefId','left');		
		if($userrole == 3)		
		{			
			$this->db->where('iserve_task.task_created_by',$userRefId);		
		}		
		$this->db->where('iserve_task.status',0);		
		$this->db->order_by('iserve_task.id','DESC');		
		$result = $this->db->get();
		$result = 	$result->result();
		
		return $result;	
	}	
	public function getPendingTaskDetailById($taskRefId)	
	{
		
		$this->db->select('iserve_task.*,iserve_project_detail.project_name');		
		$this->db->from('iserve_task');		
		$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_task.projectRefId','left');		
		$this->db->where('iserve_task.taskRefId',$taskRefId);		
		$this->db->order_by('iserve_task.id','DESC');		
		$result = $this->db->get();
		$result = $result->row();
		return $result;	
	}	
	public function getCompleteTaskDetail()	
	{		
		$userRefId = $this->session->userdata('userRefId');		
		$userrole = $this->session->userdata('role');		
		$this->db->select('iserve_task.*,iserve_project_detail.project_name');		
		$this->db->from('iserve_task');		
		$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_task.projectRefId','left');		
		if($userrole == 3)		
		{			
			$this->db->where('iserve_task.task_created_by',$userRefId);		
		}		
		$this->db->where('iserve_task.status',1);		
		$this->db->order_by('iserve_task.id','DESC');		
		$result = $this->db->get();			
		$result = $result->result();		
		return $result;	
	}	
	public function getInvoioceDetail($userRefId)	
	{		
		
		$this->db->select('iserve_invoice.*,iserve_project_detail.project_name,iserve_user_detail.first_name,iserve_user_detail.last_name');		
		$this->db->from('iserve_invoice');		
		$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_invoice.projectRefId','inner');		
		$this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_invoice.assigned_by','left');		
		$this->db->where('iserve_invoice.assigned_to',$userRefId);		
		$this->db->order_by('iserve_invoice.id','DESC');		
		$result = $this->db->get();			
		$result = $result->result();		
		return $result;	
	}	
	public function addInvoiceSign($param)	
	{		
		$this->db->insert('iserve_invoice_notification',$param);		
		$db_error = $this->db->error();        
		if ($db_error['code'] != 0)         
		{            
			$result['success'] = false;            
			$result['error_message'] = $db_error['message'];        
		}         
		else        
		{        	
			$status = array('status'=>$param['status']);        	
			$this->db->where('invoice_no',$param['invoice_no']);        	
			$this->db->update('iserve_invoice',$status);        	
			$result['success'] = true;            
			$result['success_message'] = 'Invoice signed successfully';		
		}		
		return $result;	
	}	
	public function getCollectionDetail()	
	{		
		$userRefId = $this->session->userdata('userRefId');		
		$this->db->select('iserve_collection.*,iserve_project_detail.project_name,iserve_user_detail.first_name,iserve_user_detail.last_name');		$this->db->from('iserve_collection');		
		$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_collection.projectRefId','left');		
		$this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_collection.assigned_by','left');		
		$this->db->where('iserve_collection.assigned_to',$userRefId);		
		$this->db->order_by('iserve_collection.id','DESC');		
		$result = $this->db->get();			
		$result = $result->result();		
		return $result;	
	}	
	public function addCollectionStatus($param)	
	{		
		$this->db->where('collection_number',$param['collection_number']);		
		if(isset($param['collection_date']))		
		{			
			$collection = array('status'=>$param['status'],'collection_date'=> $param['collection_date'],'collection_postpone_message'=>$param['collection_postpone_message']);			
			$this->db->update('iserve_collection',$collection);		
		}		
		else		
		{			
			$this->db->update('iserve_collection',$param);		
		}						
		$db_error = $this->db->error();        
		if ($db_error['code'] != 0)         
		{            
			$result['success'] = false;            
			$result['error_message'] = $db_error['message'];        
		}         
		else        
		{        	
			$result['success'] = true;            
			$result['success_message'] = 'collection updated successfully';		
		}		
		return $result;	
	}
	public function getworkersTaskDetail($userRefId)
	{
		$this->db->select('iserve_workers_task.*,iserve_task.taskId,iserve_task.task_subject,iserve_user_detail.first_name,iserve_user_detail.last_name');
		$this->db->from('iserve_workers_task');
		$this->db->join('iserve_user_detail','iserve_user_detail.userRefId = iserve_workers_task.workerRefId','left');
		$this->db->join('iserve_task','iserve_task.taskRefId = iserve_workers_task.taskRefId','left');
		$this->db->where('iserve_workers_task.workerRefId',$userRefId);
		$this->db->group_by('iserve_workers_task.taskRefId');
		$result = $this->db->get();			
		$result = $result->result();		
		return $result;
	}
	public function addCommentDetail($param)
	{
		$this->db->insert('iserve_comment',$param);		
		$db_error = $this->db->error();        
		if ($db_error['code'] != 0)         
		{            
			$result['success'] = false;            
		}         
		else        
		{        	
			$result['success'] = true;            
		}		
		return $result;	
	}
	public function componentDetail($componentRefId)
	{
		$this->db->select('*');
		$this->db->from('iserve_components');
		$this->db->where('compoRefId',$componentRefId);
		$result = $this->db->get();			
		$result = $result->row();		
		return $result;
	}
	public function componentHistory($componentRefId)
	{
		$this->db->select('iserve_project_asset_component.*,iserve_project_detail.project_name');
		$this->db->from('iserve_project_asset_component');
		$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_project_asset_component.projectRefId','left');
		$this->db->where('iserve_project_asset_component.componentRefId',$componentRefId);
		$this->db->order_by('iserve_project_asset_component.id','DESC');
		$result = $this->db->get();			
		$result = $result->result();		
		return $result;
	}
	public function projectTask($projectRefId)
	{
		$this->db->select('iserve_task.*,iserve_project_detail.project_name');
		$this->db->from('iserve_task');
		$this->db->join('iserve_project_detail','iserve_project_detail.projectRefId = iserve_task.projectRefId','left');
		$this->db->where('iserve_task.projectRefId',$projectRefId);
		$this->db->order_by('iserve_task.id','DESC');
		$result = $this->db->get();			
		$result = $result->result();		
		return $result;
	}
	public function addNotificationDetail($param)
	{
		$this->db->insert('iserve_notification',$param);		
		$db_error = $this->db->error();        
		if ($db_error['code'] != 0)         
		{            
			$result['success'] = false;            
			$result['error_message'] = $db_error['message'];         
		}         
		else        
		{
			if($param['notification_type'] == 'Task')  
			{
				$status = array('status' => 1);
				$this->db->where('taskRefId',$param['refId']);
				$this->db->update('iserve_task',$status);
			
			}
			$result['success'] = true; 
			$result['success_message'] = 'Added successfully';			
		}		
		return $result;	
	}
	public function addExpenseDetail($param)
	{
		$this->db->insert('iserve_task_expense',$param);		
		$db_error = $this->db->error();        
		if ($db_error['code'] != 0)         
		{            
			$result['success'] = false;            
			$result['error_message'] = $db_error['message'];         
		}         
		else        
		{
			$result['success'] = true; 
			$result['success_message'] = 'Expense Added successfully';			
		}		
		return $result;
	}
	
/*===============Create task===================*/

	public function addTaskDetail($param,$workers)
	{
		if(isset($param['worker_assigned']))
		{
			unset($param['worker_assigned']);
		}
		if(isset($param['worker_name']))
		{
			unset($param['worker_name']);
		}
		if($param['id'] == '')
		{
			$this->db->insert('iserve_task',$param);
		}
		$db_error = $this->db->error();        
		if ($db_error['code'] != 0)         
		{            
			$result['success'] = false;            
			$result['error_message'] = $db_error['message'];        
		}         
		else        
		{ 
			foreach($workers as $val)
			{
				$data['taskRefId'] = $param['taskRefId'];
				$data['addedondate'] = date('Y-m-d');
				$data['workerRefId'] = $val;
				$this->db->insert('iserve_workers_task',$data);
			}
			$result['success'] = true;
			if($param['id'] == '')
			{
				$result['success_message'] = 'Task created successfully';
			}
			else
			{
				$result['success_message'] = 'Task updated successfully';
			}	
					
		}		
		return $result;	
	}
}