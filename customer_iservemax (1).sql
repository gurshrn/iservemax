-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 12, 2019 at 07:51 AM
-- Server version: 5.6.44
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customer_iservemax`
--

-- --------------------------------------------------------

--
-- Table structure for table `iserve_asset`
--

CREATE TABLE `iserve_asset` (
  `id` int(11) NOT NULL,
  `assetRefId` varchar(255) NOT NULL,
  `asset` varchar(255) NOT NULL,
  `asset_no` varchar(255) NOT NULL,
  `serial_no` varchar(255) NOT NULL,
  `supplier` varchar(255) NOT NULL,
  `asset_description` varchar(255) NOT NULL,
  `asset_image` varchar(255) NOT NULL,
  `additional_information` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_asset`
--

INSERT INTO `iserve_asset` (`id`, `assetRefId`, `asset`, `asset_no`, `serial_no`, `supplier`, `asset_description`, `asset_image`, `additional_information`, `addedondate`, `status`) VALUES
(1, 'bqVhZ1Ao', 'xyz', '123456', '123456', 'john', 'gfdgfdgfd', 'morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535369325.jpg', 'a:6:{s:13:\"purshase_date\";s:10:\"08/24/2018\";s:17:\"warranty_validity\";s:10:\"08/30/2018\";s:9:\"po_number\";s:7:\"4234342\";s:8:\"supplier\";s:4:\"john\";s:14:\"inspected_date\";s:10:\"08/31/2018\";s:14:\"inspected_time\";s:4:\"9:30\";}', '2018-08-22', 0),
(3, 'yZONzHj8', 'test', '123456', '5345345', '', 'fg', 'morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535369325.jpg', 'a:6:{s:13:\"purshase_date\";s:10:\"08/24/2018\";s:17:\"warranty_validity\";s:10:\"08/30/2018\";s:9:\"po_number\";s:7:\"4234342\";s:8:\"supplier\";s:4:\"john\";s:14:\"inspected_date\";s:10:\"08/31/2018\";s:14:\"inspected_time\";s:4:\"9:30\";}', '2018-08-22', 0),
(4, 'zS9exHJv', 'gsfgfgf', '453455', '43534543543', '', 'fdgfdgfd', 'morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535369325.jpg', 'a:6:{s:13:\"purshase_date\";s:10:\"08/24/2018\";s:17:\"warranty_validity\";s:10:\"08/30/2018\";s:9:\"po_number\";s:7:\"4234342\";s:8:\"supplier\";s:4:\"john\";s:14:\"inspected_date\";s:10:\"08/31/2018\";s:14:\"inspected_time\";s:4:\"9:30\";}', '2018-08-23', 0),
(5, 'ha8m3MLq', 'abv', '4435435', '43242342343', 'yuh', 'fgdfsg', 'morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535369325.jpg', 'a:6:{s:13:\"purshase_date\";s:10:\"08/24/2018\";s:17:\"warranty_validity\";s:10:\"08/30/2018\";s:9:\"po_number\";s:7:\"4234342\";s:8:\"supplier\";s:3:\"yuh\";s:14:\"inspected_date\";s:10:\"08/31/2018\";s:14:\"inspected_time\";s:4:\"9:30\";}', '2018-08-27', 0),
(6, 'x0j8gJlr', 'rttret', '123456', 't55435345', 'yuh', 'fdgfdgfd', 'morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535369325.jpg', 'a:20:{s:9:\"file_name\";s:59:\"morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535368845.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:46:\"C:/xampp/htdocs/iservemax/assets/upload/asset/\";s:9:\"full_path\";s:105:\"C:/xampp/htdocs/iservemax/assets/upload', '2018-08-27', 0),
(7, 'upnDX9Z6', 'rttret', '123456', 't55435345', 'yuh', 'fdgfdgfd', 'morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535369325.jpg', 'a:20:{s:9:\"file_name\";s:59:\"morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535368911.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:46:\"C:/xampp/htdocs/iservemax/assets/upload/asset/\";s:9:\"full_path\";s:105:\"C:/xampp/htdocs/iservemax/assets/upload', '2018-08-27', 0),
(8, 'aoOr6MTg', 'rttret', '123456', 't55435345', 'yuh', 'fdgfdgfd', 'morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535369325.jpg', 'a:20:{s:9:\"file_name\";s:59:\"morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535369325.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:46:\"C:/xampp/htdocs/iservemax/assets/upload/asset/\";s:9:\"full_path\";s:105:\"C:/xampp/htdocs/iservemax/assets/upload', '2018-08-27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_assign_project`
--

CREATE TABLE `iserve_assign_project` (
  `id` int(11) NOT NULL,
  `refId` varchar(255) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `userRefId` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_assign_project`
--

INSERT INTO `iserve_assign_project` (`id`, `refId`, `projectRefId`, `userRefId`, `addedondate`, `status`) VALUES
(1, 'KAHd1zmP', 'YfLnJX5m', 'qm07gWoK', '2018-08-24', 0),
(2, 'WTwreuOa', 'gUnay4tC', 'qm07gWoK', '2018-09-13', 0),
(3, 'mQAoHuDL', 'pzk8HhQ7', 'qm07gWoK', '2018-09-14', 0),
(4, 'Z6XIMtOa', 'pzk8HhQ7', 'qm07gWoK', '2018-09-14', 0),
(5, '7YgoiFqC', 'tb9in7M6', 'qm07gWoK', '2018-10-02', 0),
(6, 'kJgshGbM', '', '', '2018-10-02', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_collection`
--

CREATE TABLE `iserve_collection` (
  `id` int(11) NOT NULL,
  `invoice_no` varchar(255) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `collection_number` varchar(255) NOT NULL,
  `assigned_by` varchar(255) NOT NULL,
  `assigned_to` varchar(255) NOT NULL,
  `assigned_date` varchar(255) NOT NULL,
  `collection_date` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1. Recieved 2. Postpone',
  `collection_recieved_image` varchar(255) NOT NULL,
  `collection_postpone_message` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_collection`
--

INSERT INTO `iserve_collection` (`id`, `invoice_no`, `projectRefId`, `collection_number`, `assigned_by`, `assigned_to`, `assigned_date`, `collection_date`, `status`, `collection_recieved_image`, `collection_postpone_message`) VALUES
(1, '8419', 'YfLnJX5m', '123456', 'qm07gWoK', 'qm07gWoK', '2018-08-30', '09/01/2018', 1, 'morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535631473.jpg', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_comment`
--

CREATE TABLE `iserve_comment` (
  `id` int(11) NOT NULL,
  `refId` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `commented_by` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `commented_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_comment`
--

INSERT INTO `iserve_comment` (`id`, `refId`, `comment`, `attachment`, `commented_by`, `type`, `commented_date`) VALUES
(7, '8', 'test', 'crash-dummy-vector-icons_1535900285.jpg', 'qm07gWoK', 'complaint', '2018-09-02'),
(6, '8', 'hello', 'crash-dummy-vector-icons_1535900242.jpg', 'qm07gWoK', 'complaint', '2018-09-02'),
(5, '8', 'test', 'crash-dummy-vector-icons_1535898594.jpg', 'qm07gWoK', 'complaint', '2018-09-02'),
(8, 'yZONzHj8', 'test', 'crash-dummy-vector-icons_1535900642.jpg', 'qm07gWoK', 'asset', '2018-09-02'),
(9, 'YfLnJX5m', 'test', 'crash-dummy-vector-icons_1535908380.jpg', 'qm07gWoK', 'project', '2018-09-02'),
(10, 'VTMBUtI6', 'test', 'images_1535968589.jpg', 'qm07gWoK', 'task', '2018-09-03'),
(11, 'YfLnJX5m', 'test', '', 'qm07gWoK', 'project', '2018-09-03'),
(12, 'pzk8HhQ7', 'test', 'SampleVideo_1280x720_1mb_1538112703.mp4', 'qm07gWoK', 'project', '2018-09-28'),
(13, 'zrqHL4aJ', 'hi', '', 'qm07gWoK', 'task', '2018-10-01'),
(14, 'zrqHL4aJ', 'hi', '', 'qm07gWoK', 'task', '2018-10-01'),
(15, 'zrqHL4aJ', 'hi', '', 'qm07gWoK', 'task', '2018-10-01'),
(16, 'zrqHL4aJ', 'hi', '', 'qm07gWoK', 'task', '2018-10-01'),
(17, 'zrqHL4aJ', 'bsjs', '1539087617885_1539263373.jpg', 'qm07gWoK', 'task', '2018-10-11'),
(18, 'FPErtbdJ', 'bjkl', '', 'qm07gWoK', 'task', '2018-10-12'),
(19, 'FPErtbdJ', 'chjk', '', 'qm07gWoK', 'task', '2018-10-12'),
(20, 'FPErtbdJ', 'chjk', '', 'qm07gWoK', 'task', '2018-10-12'),
(21, 'FPErtbdJ', 'ghjk', '', 'qm07gWoK', 'task', '2018-10-12'),
(22, 'FPErtbdJ', 'ghjk', '', 'qm07gWoK', 'task', '2018-10-12'),
(23, 'FPErtbdJ', 'chi', '', 'qm07gWoK', 'task', '2018-10-12'),
(24, 'FPErtbdJ', 'fjk', '', 'qm07gWoK', 'task', '2018-10-12'),
(25, 'FPErtbdJ', 'chjj', '', 'qm07gWoK', 'task', '2018-10-12'),
(26, 'FPErtbdJ', 'chjfhjj', '', 'qm07gWoK', 'task', '2018-10-12'),
(27, 'FPErtbdJ', 'chjfhjjxhj', '', 'qm07gWoK', 'task', '2018-10-12'),
(28, 'FPErtbdJ', 'chjfhjjxhjhjk', '', 'qm07gWoK', 'task', '2018-10-12'),
(29, 'FPErtbdJ', 'cbk', '', 'qm07gWoK', 'task', '2018-10-12'),
(30, 'FPErtbdJ', 'fhji', '', 'qm07gWoK', 'task', '2018-10-12'),
(31, 'FPErtbdJ', 'This is the test message and is t dsjfhslhflhslvjhnljdhnsxl vnndsv', 'chat_1539332524.png', 'qm07gWoK', 'task', '2018-10-12'),
(32, 'FPErtbdJ', 'chjh', '', 'qm07gWoK', 'task', '2018-10-12'),
(33, 'FPErtbdJ', 'chjk', '', 'qm07gWoK', 'task', '2018-10-12'),
(34, 'FPErtbdJ', 'ngfk', '', 'qm07gWoK', 'task', '2018-10-12'),
(35, 'FPErtbdJ', 'njld', '', 'qm07gWoK', 'task', '2018-10-12'),
(36, 'FPErtbdJ', 'njld', '', 'qm07gWoK', 'task', '2018-10-12'),
(37, 'FPErtbdJ', 'chui', '', 'qm07gWoK', 'task', '2018-10-12'),
(38, 'FPErtbdJ', 'chui', '', 'qm07gWoK', 'task', '2018-10-12'),
(39, 'FPErtbdJ', 'chui', '1539339224044_1539339251.jpg', 'qm07gWoK', 'task', '2018-10-12'),
(40, 'FPErtbdJ', 'nhkl', '', 'qm07gWoK', 'task', '2018-10-12'),
(41, 'FPErtbdJ', 'nhdk', '', 'qm07gWoK', 'task', '2018-10-12'),
(42, 'FPErtbdJ', 'ngkl', '1539339999862_1539340004.jpg', 'qm07gWoK', 'task', '2018-10-12'),
(43, 'FPErtbdJ', 'ngld', '', 'qm07gWoK', 'task', '2018-10-12'),
(44, 'FPErtbdJ', 'fuik', '', 'qm07gWoK', 'task', '2018-10-12'),
(45, 'FPErtbdJ', 'nvjl', '', 'qm07gWoK', 'task', '2018-10-12'),
(46, 'FPErtbdJ', 'njlf', '1538654944161_1539346879.jpg', 'qm07gWoK', 'task', '2018-10-12'),
(47, 'FPErtbdJ', 'njlf', '1538991429057_1539347119.jpg', 'qm07gWoK', 'task', '2018-10-12'),
(48, 'FPErtbdJ', 'njlf', '', 'qm07gWoK', 'task', '2018-10-12'),
(49, 'FPErtbdJ', 'njgl', '1538995925969_1539347613.jpg', 'qm07gWoK', 'task', '2018-10-12');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_complaint`
--

CREATE TABLE `iserve_complaint` (
  `id` int(11) NOT NULL,
  `complaint_number` varchar(255) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `assetRefId` varchar(255) NOT NULL,
  `taskRefId` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `addedby` varchar(255) NOT NULL,
  `complaint_assigned_to` varchar(255) NOT NULL,
  `complaint_type` varchar(225) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_complaint`
--

INSERT INTO `iserve_complaint` (`id`, `complaint_number`, `projectRefId`, `assetRefId`, `taskRefId`, `message`, `addedby`, `complaint_assigned_to`, `complaint_type`, `addedondate`, `status`) VALUES
(5, '5386', 'YfLnJX5m', '', '0Qvy1PwI', 'fggfdgf', '2XioFA8m', '', '', '2018-08-28', 2),
(6, '2018', 'YfLnJX5m', '', 'PE4Y09TK', 'test', '2XioFA8m', '', '', '2018-08-28', 2),
(7, '6830', 'pzk8HhQ7', '', '0Qvy1PwI', 'tesyt', '2XioFA8m', 'qm07gWoK', '', '2018-08-28', 0),
(8, '8965', 'pzk8HhQ7', 'yZONzHj8', '', 'test', '2XioFA8m', 'qm07gWoK', '', '2018-08-28', 1),
(9, '2593', 'pzk8HhQ7', 'yZONzHj8', '', 'test', '1ZKIFOgw', 'qm07gWoK', '', '2018-09-18', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_complaint_notification`
--

CREATE TABLE `iserve_complaint_notification` (
  `id` int(11) NOT NULL,
  `complaint_number` varchar(255) NOT NULL,
  `userByRefId` varchar(255) NOT NULL,
  `userToRefId` varchar(255) NOT NULL,
  `document` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_complaint_notification`
--

INSERT INTO `iserve_complaint_notification` (`id`, `complaint_number`, `userByRefId`, `userToRefId`, `document`, `message`, `addedondate`, `status`) VALUES
(1, '5386', 'mg5BJI1l', '2XioFA8m', 'dummy-pdf_2.pdf', 'test', '2018-08-28', 1),
(2, '5386', '2XioFA8m', '2XioFA8m', 'SampleVideo_1280x720_1mb.mp4', '1234gdsf', '2018-08-28', 2),
(3, '2018', 'mg5BJI1l', '2XioFA8m', 'SampleVideo_1280x720_1mb.mp4', 'test', '2018-08-28', 1),
(6, '2018', '2XioFA8m', '2XioFA8m', 'SampleVideo_1280x720_1mb.mp4', 'test', '2018-08-28', 2),
(7, '6830', 'mg5BJI1l', '2XioFA8m', 'dummy-pdf_2.pdf', 'test', '2018-08-28', 1),
(8, '6830', 'mg5BJI1l', '2XioFA8m', 'SampleVideo_1280x720_1mb.mp4', 'fdgfd', '2018-08-28', 1),
(9, '8965', 'qm07gWoK', '2XioFA8m', 'images.jpg', 'test', '2018-09-03', 1),
(10, '8965', '2XioFA8m', 'mg5BJI1l', 'images.jpg', 'test', '2018-09-03', 2);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_components`
--

CREATE TABLE `iserve_components` (
  `id` int(11) NOT NULL,
  `compoRefId` varchar(255) NOT NULL,
  `component_name` varchar(255) NOT NULL,
  `part_no` varchar(255) NOT NULL,
  `serial_no` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `component_image` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_components`
--

INSERT INTO `iserve_components` (`id`, `compoRefId`, `component_name`, `part_no`, `serial_no`, `description`, `component_image`, `addedondate`, `status`) VALUES
(1, 'fE83IwgO', 'abc', '123456', '31231444434', 'test', 'up-cartoon-pixar-movie-hd-wallpaper-by-891952386-iltwmt_1535003154.jpg', '2018-08-23', 0),
(2, 'AHzOYWmF', 'vcdfd', '43244', '443434', 'fddfgfdgdf', 'images_(1)_1535608786.jpg', '2018-08-30', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_contact_person`
--

CREATE TABLE `iserve_contact_person` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `added_by` varchar(255) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_contact_person`
--

INSERT INTO `iserve_contact_person` (`id`, `name`, `designation`, `phone`, `email`, `addedondate`, `added_by`, `projectRefId`, `status`) VALUES
(6, 'abc', 'test', '123456789', 'abc@gmail.com', '2018-09-12', 'oIF7qD8R', '', 0),
(5, 'dummy1', 'testing1', '123456789', 'dummy1@gmail.com', '2018-09-06', 'oIF7qD8R', '', 0),
(3, 'hello13', 'test', '123456789', 'abc@gmail.com', '2018-09-06', 'oIF7qD8R', '', 0),
(7, 'abc1', 'test', '123456789', 'abc@gmail.com', '2018-09-13', 'oIF7qD8R', '', 0),
(8, 'tina', 'test', 'jasmine@gmail.com', 'jasmine@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(9, 'himansh', 'test', 'himansh@gmail.com', 'himansh@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(10, 'dheer', 'test', '7878787878', 'dheer@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(11, 'kinjal', 'test', '787878787878', 'kinjal@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(12, 'rohab', 'test', '78787878787878', 'rehab@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(13, 'dheer', 'test', '7878787878787', 'dheer@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(14, 'dheer', 'test', '7878787878', 'damni@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(15, 'dheer', 'test', '787878787878', 'dhee@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(16, 'urmila', 'test', '787878787878', 'urmila@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(17, 'dhanvi', 'test', '787878787878', 'dhanvi@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(18, 'denu', 'test', '787878787878', 'denu@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(19, 'ishn', 'test', '78787878787878', 'ishn@gmail.com', '2018-09-14', 'oIF7qD8R', '', 0),
(67, 'nisha', 'test', '7878787878', 'nisha@gmail.com', '2018-09-24', 'oIF7qD8R', 'RFMyrmoO', 0),
(21, 'kajol', 'test', '7878787878', 'kajol@gmail.com', '2018-09-15', 'oIF7qD8R', '', 0),
(63, 'denu', 'test', '78787878787878', 'denu@gmail.com', '2018-09-22', 'oIF7qD8R', '', 0),
(23, 'babinder', 'test', '787878787878', 'babinder@gmail.com', '2018-09-15', 'oIF7qD8R', '', 0),
(24, 'dheer', 'test', '787878787878', 'dheer@gmail.com', '2018-09-15', 'oIF7qD8R', '', 0),
(25, 'jina', 'test', '7878787878787878', 'jina@gmail.com', '2018-09-15', 'oIF7qD8R', '', 0),
(26, 'saloni', 'test', '7878787878', 'saloni@gmail.com', '2018-09-18', 'oIF7qD8R', '', 0),
(27, 'lina', 'test', '7878787878', 'lina@gmail.com', '2018-09-18', 'oIF7qD8R', '', 0),
(28, 'ankita', 'test', '787878787878', 'ankita@gmail.com', '2018-09-18', 'oIF7qD8R', '', 0),
(29, 'janes', 'test', '7878787878', 'jane@gmail.com', '2018-09-18', 'oIF7qD8R', '', 0),
(30, 'abc1', 'test', '123456789', 'abc@gmail.com', '2018-09-18', 'oIF7qD8R', '', 0),
(31, 'hello03', 'test', '123456789', 'abc@gmail.com', '2018-09-18', 'oIF7qD8R', 'pzk8HhQ7', 0),
(32, 'anima', 'test', '7878787878', 'anima@gmail.com', '2018-09-19', 'oIF7qD8R', '', 0),
(33, 'dinu', 'test', '7878787878', 'dinu@gmail.com', '2018-09-19', 'oIF7qD8R', '', 0),
(34, 'hello03', 'test', '123456789', 'abc@gmail.com', '2018-09-19', 'oIF7qD8R', 'pzk8HhQ7', 0),
(35, 'hello03', 'test', '123456789', 'abc@gmail.com', '2018-09-19', 'oIF7qD8R', 'pzk8HhQ7', 0),
(48, 'nilu', 'test', '7878787878', 'nilu@gmail.com', '2018-09-20', 'oIF7qD8R', '', 0),
(37, 'ani', 'test', '7878787878', 'ani@gmail.com', '2018-09-19', 'oIF7qD8R', '', 0),
(47, 'nisha', 'test', '7878787878', 'nisha@gmail.com', '2018-09-20', 'oIF7qD8R', '', 0),
(49, 'laila', 'test', '7878787878', 'laila@gmail.com', '2018-09-20', 'oIF7qD8R', '', 0),
(50, 'himani', 'test', '7878787878', 'himani@gmail.com', '2018-09-20', 'oIF7qD8R', '', 0),
(51, 'binu', 'test', '78787878787878', 'binu@gmail.com', '2018-09-21', 'oIF7qD8R', '', 0),
(52, 'binu', 'test', '7878787878787878', 'binu@gmail.com', '2018-09-21', 'oIF7qD8R', '', 0),
(53, 'nilino', 'test', '78787878787878', 'nilini@gmail.com', '2018-09-21', 'oIF7qD8R', '', 0),
(54, 'nisha', 'test', '787878787878', 'nisha@gmil.com', '2018-09-21', 'oIF7qD8R', '', 0),
(55, 'nisha', 'teat', '7878787878', 'nisha@gmail.com', '2018-09-21', 'oIF7qD8R', '', 0),
(56, 'niva', 'test', '78787878787878', 'niva@gmail.com', '2018-09-21', 'oIF7qD8R', '', 0),
(57, 'niya', 'test', '7878787878787878', 'niya@gmail.com', '2018-09-21', 'oIF7qD8R', '', 0),
(58, 'dhanu', 'test', '787878787878', 'dhanu@gmail.com', '2018-09-21', 'oIF7qD8R', '', 0),
(59, 'nabu', 'test', '78787878787878', 'nabu@gmail.com', '2018-09-21', 'oIF7qD8R', '', 0),
(61, 'nisha', 'test', '78787878787878', 'nisha@gmail.com', '2018-09-22', 'oIF7qD8R', '', 0),
(64, 'nisha', 'test', '787878787878', 'nisha@gmail.com', '2018-09-22', 'oIF7qD8R', 'cDIy9i2Y', 0),
(65, 'nisha', 'test', '787878787878', 'nisha@gmail.com', '2018-09-22', 'oIF7qD8R', 'RFMyrmoO', 0),
(66, 'nisha', 'test', '787878787878', 'nisha@gmail.com', '2018-09-22', 'oIF7qD8R', '', 0),
(68, 'z contact name', 'z designation', '3366990077', 'zcontact@test.com', '2018-09-25', 'oIF7qD8R', '', 0),
(69, 'Diljit', 'this project in best work', '9988997845', 'diljit@gmail.com', '2018-09-25', 'oIF7qD8R', '', 0),
(70, 'jdjd', 'yryr', '5656', 'sb@gd.hd', '2018-09-25', 'oIF7qD8R', '', 0),
(71, 'The contacts', 'Punjab', '9988992323', 'dheryyavier16@gmail.com', '2018-09-25', 'oIF7qD8R', 'Z7dOu1hK', 0),
(72, 'The contacts 2', 'Chandigarh', '7722336868', 'dvs@gmail.com', '2018-09-25', 'oIF7qD8R', 'Z7dOu1hK', 0),
(73, 'England.  test', 'manager', '3366633', 'tes@gmail.com', '2018-10-02', 'oIF7qD8R', '', 0),
(74, 'nisha', 'test', '7878787878', 'nisha@gmail.com', '2018-10-03', 'oIF7qD8R', '', 0),
(75, 'nisha', 'test', '7878787878', 'nisha@gmail.com', '2018-10-03', 'oIF7qD8R', '', 0),
(76, 'nisha', 'test', '7878787878', 'nisha@gmail.com', '2018-10-03', 'oIF7qD8R', '', 0),
(77, 'nisha', 'test', '7878787878', 'nisha@gmail.com', '2018-10-03', 'oIF7qD8R', '0oCVpOe7', 0),
(79, 'nisha', 'test', '7878787878', 'nisha@gmail.com', '2018-10-03', 'oIF7qD8R', 'tb9in7M6', 0),
(81, 'nisha', 'test', '7878787878', 'nisha@gmail.com', '2018-10-03', 'oIF7qD8R', '7XfvM4Rm', 0),
(82, 'nisha', 'test', '7878787878', 'nisha@gmail.com', '2018-10-04', 'oIF7qD8R', '', 0),
(83, 'nisha', 'test', '7878787878', 'nisha@gmail.com', '2018-10-04', 'oIF7qD8R', '', 0),
(84, 'nisha', 'test', '7878787878', 'nisha@gmail.com', '2018-10-04', 'oIF7qD8R', 'WQr8uRx7', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_contact_us`
--

CREATE TABLE `iserve_contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `notification_from` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_contact_us`
--

INSERT INTO `iserve_contact_us` (`id`, `name`, `email`, `phone`, `message`, `notification_from`, `addedondate`) VALUES
(1, 'tets', 'test@gmail.com', '231312321', 'test', '2XioFA8m', '2018-08-29');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_contractor`
--

CREATE TABLE `iserve_contractor` (
  `id` int(11) NOT NULL,
  `contractor_name` varchar(255) NOT NULL,
  `email` varchar(225) NOT NULL,
  `phone_number` varchar(225) NOT NULL,
  `scope_of_work` varchar(255) NOT NULL,
  `added_by` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_contractor`
--

INSERT INTO `iserve_contractor` (`id`, `contractor_name`, `email`, `phone_number`, `scope_of_work`, `added_by`, `addedondate`, `status`) VALUES
(5, 'abcd', 'abc8@gmail.com', '1234566781', 'test', 'oIF7qD8R', '2018-09-10', 0),
(6, 'Test2', 'abc7@gmail.com', '1234566782', 'test', 'oIF7qD8R', '2018-09-12', 0),
(4, 'abc', 'abc6@gmail.com', '1234566783', 'test2', 'oIF7qD8R', '2018-09-06', 0),
(7, 'Test26', 'abc5@gmail.com', '1234566784', 'test', 'oIF7qD8R', '2018-09-12', 0),
(8, 'testing', 'abc4@gmail.com', '1234566785', 'test', 'oIF7qD8R', '2018-09-12', 0),
(9, 'testing1', 'abc1@gmail.com', '1234566780', 'test', 'oIF7qD8R', '2018-09-12', 0),
(10, 'testing100', 'abc@gmail.com', '1234566789', 'test', 'oIF7qD8R', '2018-09-12', 0),
(11, 'Abc2', 'a@gmail.com', '3232313232', 'test', 'oIF7qD8R', '2018-09-13', 0),
(12, 'amit', 'amit@gmail.con', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(13, 'ajkl', 'ajkl@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(14, 'gurjeevan', 'ajkl@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(15, 'gurj', 'gurjeevan@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(16, 'netu', 'netu@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(17, 'teken', 'teken@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(18, 'neema', 'neema@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(19, 'ami', 'adi@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(20, 'atul', 'atul@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(21, 'rani', 'rani@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(22, 'reshmi', 'reshmi@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(23, 'james', 'james@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(24, 'jani', 'jane@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(25, 'jami', 'kajol@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(26, 'raji', 'raji@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(27, 'nandani', 'nandani@gmail.com', '787878787878', 'tesr', 'oIF7qD8R', '2018-09-13', 0),
(28, 'danji', 'dhanji@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(29, 'vini', 'vini@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(30, 'nidhi', 'nidhi@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(31, 'tina', 'tina@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-13', 0),
(32, 'runki', 'runki@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-14', 0),
(33, 'laila', 'laila@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-14', 0),
(34, 'dheer', 'dheer@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-14', 0),
(35, 'himanshu', 'himanshu@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-14', 0),
(36, 'dinu', 'dhanv@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-14', 0),
(37, 'demu', 'demu@gmail.com', '7878787878', 'test', 'oIF7qD8R', '2018-09-14', 0),
(38, 'azhi', 'azim@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-14', 0),
(39, 'himani', 'himani@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-15', 0),
(40, 'anisha', 'anisha@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-15', 0),
(41, 'dinu@gmail.com', 'dinu@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-15', 0),
(42, 'dheer', 'dheer@gmail.com', '7878787878', 'test', 'oIF7qD8R', '2018-09-15', 0),
(43, 'dheer', 'dheer@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-15', 0),
(44, 'dheer', 'dheer@gmail.com', '787878787878', 'tesr', 'oIF7qD8R', '2018-09-15', 0),
(45, 'jina', 'jina@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-15', 0),
(46, 'dira', 'devy@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-09-18', 0),
(47, 'arnica', 'arnica@gmail.com', '7878787878', 'test', 'oIF7qD8R', '2018-09-18', 0),
(48, 'anamika', 'anamika@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-18', 0),
(49, 'nisha', 'nisha@gmail.com', '7878787878', 'test', 'oIF7qD8R', '2018-09-20', 0),
(50, 'dinu', 'dinu@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-21', 0),
(51, 'danisha', 'tanisha@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-21', 0),
(52, 'deevu', 'deevu@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-21', 0),
(53, 'dami@gmail.com', 'dami@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-21', 0),
(54, 'binu', 'binu@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-21', 0),
(55, 'dhanvi', 'dhanvi@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-21', 0),
(56, 'nisha', 'nisha@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-21', 0),
(57, 'dhanu', 'dhanu@gmail.com', '78787878787878', 'trst', 'oIF7qD8R', '2018-09-21', 0),
(58, 'nisha', 'nisha@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-22', 0),
(59, 'nisha', 'nisha@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-22', 0),
(60, 'nisha', 'nisha@gmail.com', '78787878787878', 'test', 'oIF7qD8R', '2018-09-22', 0),
(61, 'gjjb', 'z@z.com', '55632868528', 'hhug', 'oIF7qD8R', '2018-09-24', 0),
(62, 'z test contractor', 'zcontractor@test.com', '6699887722', 'z contractor scope of work', 'oIF7qD8R', '2018-09-25', 0),
(63, 'Amit Singh', 'amit@gmail.com', '9988992323', 'Do the work.work work.', 'oIF7qD8R', '2018-09-25', 0),
(64, 'Aman', 'aman@gmail.com', '9988562321', 'Front end work', 'oIF7qD8R', '2018-09-25', 0),
(65, 'test contact.  name', 'test.iservemax@gmail.com', '55663322252', 'concerning', 'oIF7qD8R', '2018-10-02', 0),
(66, 'nisha', 'nisha@gmail.com', '787878787878', 'test', 'oIF7qD8R', '2018-10-03', 0),
(67, 'nisha', 'nisha@gmail.com', '7878787878', 'test', 'oIF7qD8R', '2018-10-03', 0),
(68, 'nisha', 'nisha@gmail.com', '7878787878', 'test', 'oIF7qD8R', '2018-10-03', 0),
(69, 'nisha', 'nisha@gmail.com', '7878787878', 'test', 'oIF7qD8R', '2018-10-03', 0),
(70, 'nisha', 'nisha@gmail.com', '7878787878', 'test', 'oIF7qD8R', '2018-10-03', 0),
(71, 'nisha', 'nisha@gmail.com', '7878787878', 'test', 'oIF7qD8R', '2018-10-04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_invoice`
--

CREATE TABLE `iserve_invoice` (
  `id` int(11) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `invoice_no` varchar(255) NOT NULL,
  `assigned_to` varchar(255) NOT NULL,
  `assigned_by` varchar(255) NOT NULL,
  `document` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `collection_status` int(11) NOT NULL,
  `created_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_invoice`
--

INSERT INTO `iserve_invoice` (`id`, `projectRefId`, `invoice_no`, `assigned_to`, `assigned_by`, `document`, `status`, `collection_status`, `created_date`) VALUES
(1, 'pzk8HhQ7', '8419', 'qm07gWoK', 'qm07gWoK', 'dummy-pdf_2_1535623615.pdf', 1, 0, '2018-08-30');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_invoice_notification`
--

CREATE TABLE `iserve_invoice_notification` (
  `id` int(11) NOT NULL,
  `invoice_no` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `signed_by` varchar(255) NOT NULL,
  `forwarded_to` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `signed_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_invoice_notification`
--

INSERT INTO `iserve_invoice_notification` (`id`, `invoice_no`, `image`, `signed_by`, `forwarded_to`, `message`, `status`, `signed_date`) VALUES
(1, '8419', '', 'qm07gWoK', 'qm07gWoK', 'test', 2, '2018-08-30'),
(4, '8419', 'images_1535626536.jpg', 'qm07gWoK', 'qm07gWoK', '', 1, '2018-08-30');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_login_detail`
--

CREATE TABLE `iserve_login_detail` (
  `id` int(11) NOT NULL,
  `userRefId` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `tokenId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_login_detail`
--

INSERT INTO `iserve_login_detail` (`id`, `userRefId`, `first_name`, `phone_number`, `email`, `password`, `role`, `status`, `addedondate`, `tokenId`) VALUES
(1, '1ZKIFOgw', 'Gur', '“N%žOôÃRJBi=F\"õÃ', 'éurzŽÒ´AIœ!Ðq', '/œ\nc®]^Öæ|—', 1, 0, '2018-08-29', ''),
(2, 'qm07gWoK', 'Gurjeevan', '©}áHÆ2êd½\\àŒê', ']åµ™V×¯|å&zê\"éÃ<\róØ\níMDÇÁùxPKŒ', '/œ\nc®]^Öæ|—', 3, 0, '2018-08-29', ''),
(3, 'XOIEmS0N', 'test', 'Ve}¡‘3®0Zýc{i\\', '/ç÷–%Ïv\Z—s¦ž½»', '/œ\nc®]^Öæ|—', 2, 0, '2018-08-29', ''),
(4, 'nswJkP7H', 'test', '\02;ýFð;,tuÙ', 'hŠh60Å×q½\nï8ž}', '/œ\nc®]^Öæ|—', 2, 0, '2018-08-29', ''),
(5, 'JMNV8Rab', 'test', '\02;ýFð;,tuÙ', 'hŠh60Å×q½\nï8ž}', '/œ\nc®]^Öæ|—', 1, 0, '2018-08-29', ''),
(6, 'HPqKbNWL', 'gfgfd', 'š×«wêÂ®¨N\Zó‡ëá', 'æƒ(xµ„‡yž¯:NÜ(', '/œ\nc®]^Öæ|—', 2, 0, '2018-08-30', ''),
(7, 'oIF7qD8R', 'prachi', '\'ÿP‹’à4~©\'ø2…Õ…D', '>aWz>}3g0zdÈ=ï§Î[ž:Ë‘Su¯', '/œ\nc®]^Öæ|—', 5, 0, '2018-09-05', ''),
(8, 'XEsYmkeg', 'dfs sdff ', 'l÷ò¿QL¼(ÂÖl', ' \Z¤mµŒ*¤¯‚:rfBtÒV¾»¨îËUÜý€Å', '/œ\nc®]^Öæ|—', 1, 0, '2018-09-26', '');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_news`
--

CREATE TABLE `iserve_news` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `news_title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `addedondate` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_news`
--

INSERT INTO `iserve_news` (`id`, `image`, `news_title`, `description`, `addedondate`) VALUES
(14, 'images_(1)_1535540811.jpg', 'dsadasdsa', '<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>', '2018-08-29'),
(22, 'morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1535542354.jpg', 'hgjhgjgh', '<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>\r\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>\r\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>\r\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>\r\n<p>&nbsp;</p>', '2018-08-29'),
(25, 'images_(1)_1535542610.jpg', 'test', '<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>\r\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>\r\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>\r\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>', '2018-08-29');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_notification`
--

CREATE TABLE `iserve_notification` (
  `id` int(11) NOT NULL,
  `refId` varchar(255) NOT NULL,
  `notification_from` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `notification_status` int(11) NOT NULL,
  `notification_type` varchar(255) NOT NULL,
  `notification_date` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_notification`
--

INSERT INTO `iserve_notification` (`id`, `refId`, `notification_from`, `message`, `attachment`, `notification_status`, `notification_type`, `notification_date`, `status`) VALUES
(3, 'kFRJAent', 'qm07gWoK', 'hello', 'images_1535973891.jpg', 1, 'Task', '2018-09-03', 0),
(2, 'VTMBUtI6', 'qm07gWoK', 'test', 'images_1535973891.jpg', 1, 'Task', '2018-09-03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_product`
--

CREATE TABLE `iserve_product` (
  `id` int(11) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `capacity` varchar(255) NOT NULL,
  `max_load` varchar(255) NOT NULL,
  `max_reach` varchar(255) NOT NULL,
  `tip_load` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `addedondate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_product`
--

INSERT INTO `iserve_product` (`id`, `projectRefId`, `capacity`, `max_load`, `max_reach`, `tip_load`, `description`, `image`, `status`, `addedondate`) VALUES
(1, 'pzk8HhQ7', '34', '324', '324', '32', 'sdffds', 'app_1538130953.php', 0, '2018-08-29');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_project_additional_information`
--

CREATE TABLE `iserve_project_additional_information` (
  `id` int(11) NOT NULL,
  `refId` varchar(255) NOT NULL,
  `upload_type` varchar(225) NOT NULL,
  `document_title` varchar(255) NOT NULL,
  `upload_document` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `added_by` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_project_additional_information`
--

INSERT INTO `iserve_project_additional_information` (`id`, `refId`, `upload_type`, `document_title`, `upload_document`, `thumbnail`, `added_by`, `addedondate`, `status`) VALUES
(4, 'hz7bxZC0', 'Image', 'ndfgfgh', 'line_1537276598.png', '', 'oIF7qD8R', '2018-09-18', 0),
(3, 'pzk8HhQ7', 'document', 'test', 'images_1537276579.jpg', '', 'oIF7qD8R', '2018-09-18', 0),
(5, 'RFMyrmoO', 'Image', 'ndjl', 'Aqua_Ring_20180915_112742_1537277633.jpg', '', 'oIF7qD8R', '2018-09-18', 0),
(6, 'RFMyrmoO', 'Image', 'najd', 'Aqua_Ring_20180915_112742_1537335276.jpg', '', 'oIF7qD8R', '2018-09-19', 0),
(7, 'RFMyrmoO', 'Image', 'najd', 'Aqua_Ring_20180915_112742_1537335638.jpg', '', 'oIF7qD8R', '2018-09-19', 0),
(8, 'RFMyrmoO', 'Document', 'test', '1537338994841_1537339002.jpg', '', 'oIF7qD8R', '2018-09-19', 0),
(9, 'RFMyrmoO', 'Image', 'cjko', '1537339178104_1537339182.jpg', '', 'oIF7qD8R', '2018-09-19', 0),
(10, 'tb9in7M6', 'Image', 'ndfgfgh', 'Asset_4_1537340368.png', '', 'oIF7qD8R', '2018-09-19', 0),
(11, 'tb9in7M6', 'Image', 'ndfgfgh', '_1537340438.gitignore', '', 'oIF7qD8R', '2018-09-19', 0),
(12, 'tb9in7M6', 'Image', 'tyghjytj', '_1537340445.gitignore', '', 'oIF7qD8R', '2018-09-19', 0),
(13, 'RFMyrmoO', 'Image', 'vujk', 'Aqua_Ring_20180915_104105_1537341061.jpg', '', 'oIF7qD8R', '2018-09-19', 0),
(14, 'RFMyrmoO', 'Image', 'byhj', 'Aqua_Ring_20180919_120933_1537341987.jpg', '', 'oIF7qD8R', '2018-09-19', 0),
(15, 'tb9in7M6', 'Image', 'gkkn', 'Aqua_Ring_20180919_120933_1537342528.jpg', '', 'oIF7qD8R', '2018-09-19', 0),
(16, 'tb9in7M6', 'Image', 'gkkn', 'Aqua_Ring_20180919_120933_1537342531.jpg', '', 'oIF7qD8R', '2018-09-19', 0),
(17, 'tb9in7M6', 'Image', 'gkkn', 'Aqua_Ring_20180919_120933_1537342554.jpg', '', 'oIF7qD8R', '2018-09-19', 0),
(18, 'tb9in7M6', 'Image', 'nfld', 'Aqua_Ring_20180919_120933_1537344547.jpg', '', 'oIF7qD8R', '2018-09-19', 0),
(19, 'tb9in7M6', 'Image', 'nfmd', 'Aqua_Ring_20180919_120933_1537350369.jpg', '', 'oIF7qD8R', '2018-09-19', 0),
(20, 'RFMyrmoO', 'Image', 'njld', 'Screenshot_20180914-111401_1537597753.png', '', 'oIF7qD8R', '2018-09-22', 0),
(21, 'RFMyrmoO', 'Image', 'nvjl', '1537516087293_1537601445.jpg', '', 'oIF7qD8R', '2018-09-22', 0),
(22, 'XvRkBaJr', 'Image', 'ngjl', 'Screenshot_20180911-162705_1537618257.png', '', 'oIF7qD8R', '2018-09-22', 0),
(23, 'XvRkBaJr', 'Video', 'njlv', 'Screenshot_20180911-162705_1537618290.png', '', 'oIF7qD8R', '2018-09-22', 0),
(24, 'cDIy9i2Y', 'Image', 'nltd', 'Screenshot_20180911-162659_1537623936.png', '', 'oIF7qD8R', '2018-09-22', 0),
(25, 'RFMyrmoO', 'Image', 'njlv', 'Screenshot_20180911-162659_1537624613.png', '', 'oIF7qD8R', '2018-09-22', 0),
(26, 'tb9in7M6', 'Image', 'ndfgfgh', 'AndroidManifest_1537772789.xml', '', 'oIF7qD8R', '2018-09-24', 0),
(27, 'RFMyrmoO', 'Document', 'nflv', 'imembufDBG_1537776939.txt', '', 'oIF7qD8R', '2018-09-24', 0),
(28, 'RFMyrmoO', 'Document', 'nflv', 'imembufDBG_1537776972.txt', '', 'oIF7qD8R', '2018-09-24', 0),
(29, 'RFMyrmoO', 'Document', 'nlhv', 'imembufDBG_1537782185.txt', '', 'oIF7qD8R', '2018-09-24', 0),
(30, 'RFMyrmoO', 'Document', 'nflv', 'IMG-20180924-WA0001_1537784843.jpg', '', 'oIF7qD8R', '2018-09-24', 0),
(31, 'RFMyrmoO', 'Image', 'nflv', 'IMG-20180924-WA0001_1537784863.jpg', '', 'oIF7qD8R', '2018-09-24', 0),
(32, 'RFMyrmoO', 'Video', 'nflv\n', 'VID-20180924-WA0002_1537794669.mp4', '', 'oIF7qD8R', '2018-09-24', 0),
(33, 'RFMyrmoO', 'Video', 'njlv', 'VID-20180831-WA0000_1537794751.mp4', '', 'oIF7qD8R', '2018-09-24', 0),
(34, 'RFMyrmoO', 'Video', 'nflv', 'VID-20180831-WA0000_1537794836.mp4', '', 'oIF7qD8R', '2018-09-24', 0),
(35, 'RFMyrmoO', 'Image', 'nflv', '1537528513389_1537854192.jpg', '', 'oIF7qD8R', '2018-09-25', 0),
(36, 'RFMyrmoO', 'Image', 'nflv', 'Aqua_Ring_20180925_153624_1537870061.jpg', '', 'oIF7qD8R', '2018-09-25', 0),
(37, 'RFMyrmoO', 'Document', 'nflv', 'Aqua_Ring_20180914_135129_11_1537870291.jpg', '', 'oIF7qD8R', '2018-09-25', 0),
(38, 'Z7dOu1hK', 'Image', 'image 1', '1537871461387_1537871468.jpg', '', 'oIF7qD8R', '2018-09-25', 0),
(39, 'Z7dOu1hK', 'Video', 'video 1', '1537871563519_1537871617.jpg', '', 'oIF7qD8R', '2018-09-25', 0),
(40, 'Z7dOu1hK', 'Document', 'the document', '1537871563519_1537871636.jpg', '', 'oIF7qD8R', '2018-09-25', 0),
(41, 'Z7dOu1hK', 'Document', 'the best document', '1537871563519_1537871694.jpg', '', 'oIF7qD8R', '2018-09-25', 0),
(42, 'RFMyrmoO', 'Image', 'nflv', '1537450199892_1537874752.jpg', '', 'oIF7qD8R', '2018-09-25', 0),
(43, 'RFMyrmoO', 'Image', 'nflv', '1537876005542_1537876010.jpg', '', 'oIF7qD8R', '2018-09-25', 0),
(44, 'ZvlDoLc9', 'Document', 'njlv', 'Aqua_Ring_20181003_111451_1538552395.jpg', '', 'oIF7qD8R', '2018-10-03', 0),
(45, 'jY7d1glF', 'Image', 'njlv', 'Aqua_Ring_20181003_111451_1538553300.jpg', '', 'oIF7qD8R', '2018-10-03', 0),
(46, 'tb9in7M6', 'Video', 'njlv', 'Aqua_Ring_20180702_130123_1538562008.3gp', '', 'oIF7qD8R', '2018-10-03', 0),
(47, '7XfvM4Rm', 'Video', 'njlv', 'Aqua_Ring_20180702_130123_1538630638.3gp', '', 'oIF7qD8R', '2018-10-04', 0),
(48, '7XfvM4Rm', 'Video', 'njlv', 'temp_1538630705.mp4', '', 'oIF7qD8R', '2018-10-04', 0),
(49, '7XfvM4Rm', 'Video', 'njlv', 'temp_1538632928.mp4', '', 'oIF7qD8R', '2018-10-04', 0),
(50, '7XfvM4Rm', 'Video', 'njlv', 'temp_1538633538.mp4', '', 'oIF7qD8R', '2018-10-04', 0),
(51, '7XfvM4Rm', 'Image', 'ndjk', '1537869372692_1538634120.jpg', '', 'oIF7qD8R', '2018-10-04', 0),
(52, '7XfvM4Rm', 'Document', 'ndje', 'imembufDBG_1538634152.txt', '', 'oIF7qD8R', '2018-10-04', 0),
(53, '7XfvM4Rm', 'Image', 'nfuk', '1538634205982_1538634209.jpg', '', 'oIF7qD8R', '2018-10-04', 0),
(54, '7XfvM4Rm', 'Image', 'njlv', '1537596618461_1538636430.jpg', '', 'oIF7qD8R', '2018-10-04', 0),
(55, '7XfvM4Rm', 'Document', 'njlv', 'imembufDBG_1538636451.txt', '', 'oIF7qD8R', '2018-10-04', 0),
(56, 'NDMehsWL', 'Document', 'njlc', 'imembufDBG_1538638503.txt', '', 'oIF7qD8R', '2018-10-04', 0),
(57, '7XfvM4Rm', 'Video', 'njlv', 'temp_1538647864.mp4', '', 'oIF7qD8R', '2018-10-04', 0),
(58, '7XfvM4Rm', 'Video', 'njlv', 'mobizen_20180922_191108_1538647940.mp4', '', 'oIF7qD8R', '2018-10-04', 0),
(59, '7XfvM4Rm', 'Video', 'njlv', 'mobizen_20180922_185847_1538649969.mp4', '', 'oIF7qD8R', '2018-10-04', 0),
(60, '7XfvM4Rm', 'Video', 'njlv', 'mobizen_20180922_191126_1538650692.mp4', '', 'oIF7qD8R', '2018-10-04', 0),
(61, '7XfvM4Rm', 'Video', 'njlv', 'mobizen_20180922_191108_1538650849.mp4', '', 'oIF7qD8R', '2018-10-04', 0),
(62, 'WQr8uRx7', 'Image', 'njlv', '1537869372692_1538655651.jpg', '', 'oIF7qD8R', '2018-10-04', 0),
(63, '7XfvM4Rm', 'Video', 'njlv', 'mobizen_20180922_185847_1538723104.mp4', '', 'oIF7qD8R', '2018-10-05', 0),
(64, '7XfvM4Rm', 'Video', 'njlv', 'mobizen_20180922_191108_1538723136.mp4', '', 'oIF7qD8R', '2018-10-05', 0),
(65, 'XzSHyOC2', 'Document', 'njlv', 'voice_instructions_unitless_1538726871.zip', '', 'oIF7qD8R', '2018-10-05', 0),
(66, 'XzSHyOC2', 'Image', 'njlv', 'IMG_20180922_102842_1538726902.jpg', '', 'oIF7qD8R', '2018-10-05', 0),
(67, 'XzSHyOC2', 'Document', 'njlv', 'IMG_20180922_104004_1538726924.jpg', '', 'oIF7qD8R', '2018-10-05', 0),
(68, '7XfvM4Rm', 'Video', 'njlv', 'mobizen_20180922_191126_1538744961.mp4', '', 'oIF7qD8R', '2018-10-05', 0),
(69, '7XfvM4Rm', 'Video', 'njlv', 'mobizen_20180922_185847_1538745016.mp4', '', 'oIF7qD8R', '2018-10-05', 0),
(70, '7XfvM4Rm', 'Document', 'njlv', 'imembufDBG_1539164831.txt', '', 'oIF7qD8R', '2018-10-10', 0),
(71, 'pzk8HhQ7', 'video', 'qwerty', 'mobizen_20180922_185847_1539177329.mp4', '', 'oIF7qD8R', '2018-10-10', 0),
(72, 'pzk8HhQ7', 'video', 'test', 'images_1539319817.jpg', '', 'oIF7qD8R', '2018-10-12', 0),
(73, 'pzk8HhQ7', 'video', 'test', 'images_1539329315.jpg', '', 'oIF7qD8R', '2018-10-12', 0),
(74, 'pzk8HhQ7', 'video', 'test', 'images_1539329396.jpg', '', 'oIF7qD8R', '2018-10-12', 0),
(75, 'pzk8HhQ7', 'video', 'test', 'images_1539329507.jpg', 'images_15393295071.jpg', 'oIF7qD8R', '2018-10-12', 0),
(76, 'pzk8HhQ7', 'Image', 'njlv', '1539345381431_1539345385.jpg', '', 'qm07gWoK', '2018-10-12', 0),
(77, 'pzk8HhQ7', 'Video', 'njlv', 'VID_20181012_172639_1539345815.mp4', '', 'qm07gWoK', '2018-10-12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_project_asset`
--

CREATE TABLE `iserve_project_asset` (
  `id` int(11) NOT NULL,
  `refId` varchar(255) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `assetRefId` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `asset_completed_date` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0. Assigned 1. Not assigned'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_project_asset`
--

INSERT INTO `iserve_project_asset` (`id`, `refId`, `projectRefId`, `assetRefId`, `addedondate`, `asset_completed_date`, `status`) VALUES
(1, 'dg90hscY', 'pzk8HhQ7', 'bqVhZ1Ao', '2018-09-14', '', 0),
(2, 'dg90hscY', 'pzk8HhQ7', 'yZONzHj8', '2018-09-14', '', 0),
(3, 'dFoVfr23', 'tb9in7M6', 'ha8m3MLq', '2018-10-02', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_project_asset_component`
--

CREATE TABLE `iserve_project_asset_component` (
  `id` int(11) NOT NULL,
  `refId` varchar(255) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `componentRefId` varchar(255) NOT NULL,
  `assetRefId` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `completed_date` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_project_asset_component`
--

INSERT INTO `iserve_project_asset_component` (`id`, `refId`, `projectRefId`, `componentRefId`, `assetRefId`, `addedondate`, `completed_date`, `status`) VALUES
(1, 'fgfdfdfyf', 'pzk8HhQ7', 'fE83IwgO', 'bqVhZ1Ao', '2018-09-14', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_project_detail`
--

CREATE TABLE `iserve_project_detail` (
  `id` int(11) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_location` varchar(255) NOT NULL,
  `geo_location` varchar(255) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `project_requirements` varchar(255) NOT NULL,
  `consultant` varchar(255) NOT NULL,
  `bugdet` int(11) NOT NULL,
  `contact_information` varchar(255) NOT NULL,
  `schedule_visit` varchar(255) NOT NULL,
  `project_plan` varchar(255) NOT NULL,
  `project_plan_image` varchar(255) NOT NULL,
  `contractor_information` varchar(255) NOT NULL,
  `project_phase` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1. Pending 2. Submitted',
  `added_by` varchar(255) NOT NULL,
  `assigned_to` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_project_detail`
--

INSERT INTO `iserve_project_detail` (`id`, `projectRefId`, `project_name`, `project_location`, `geo_location`, `latitude`, `longitude`, `project_requirements`, `consultant`, `bugdet`, `contact_information`, `schedule_visit`, `project_plan`, `project_plan_image`, `contractor_information`, `project_phase`, `status`, `added_by`, `assigned_to`, `addedondate`) VALUES
(9, 'pzk8HhQ7', 'test', 'chandigarh', 'manimazra', 198888, 28888880, 'test', 'abc', 500, '5,3', '2018-09-16,2018-09-18', 'test', '1.jpeg', '5,6', 1, 1, 'oIF7qD8R', 'qm07gWoK', '2018-09-12'),
(10, 'gUnay4tC', 'test2', 'chandigarh', 'manimazra', 1984374738.99, 2133231323.89, 'test', 'abc', 400, '5,3', '2018-09-16,2018-09-18', 'test', 'hello02_1536735541.jpg', '5,6', 1, 0, 'oIF7qD8R', 'qm07gWoK', '2018-09-12'),
(11, 'jY7d1glF', 'ndks', 'ejke', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270373, 76.8466577, 'hsjsk', 'neke', 2147483647, '22', '2018- 8- 28 ', 'ndks', 'Aqua_Ring_20180915_104105_1536990488.jpg', '40', 0, 1, 'oIF7qD8R', '', '2018-09-15'),
(12, 'iHGPN4et', 'ndks', 'ejke', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270373, 76.8466577, 'hsjsk', 'neke', 2147483647, '22', '2018- 8- 28 ', 'ndks', 'Aqua_Ring_20180915_104105_1536990669.jpg', '40', 0, 1, 'oIF7qD8R', '', '2018-09-15'),
(13, 'g2Rp48zE', 'bsjs', 'bsjn', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270381, 76.8466557, 'dnkd', 'bufk', 2147483647, '23', '2018- 8- 28 ', 'bsjs', '1536991064524_1536991151.jpg', '41', 0, 0, 'oIF7qD8R', '', '2018-09-15'),
(14, 'ByTl9tXp', 'bsjs', 'bsjn', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270381, 76.8466557, 'dnkd', 'bufk', 2147483647, '23', '2018- 8- 28 ', 'bsjs', '1536991064524_1536991155.jpg', '41', 0, 0, 'oIF7qD8R', '', '2018-09-15'),
(15, '6iP2w1Vj', 'test2', 'chandigarh', 'manimazra', 1984374738.99, 2133231323.89, 'test', 'abc', 400, '1,2,3,', '2018-09-16,2018-09-18', 'test', 'images_1536993580.jpg', '1,2,3', 1, 0, 'oIF7qD8R', '', '2018-09-15'),
(16, 'XvRkBaJr', 'test2', 'chandigarh', 'manimazra', 1984374738.99, 2133231323.89, 'test', 'abc', 400, '1,2,3,', '2018-09-16,2018-09-18', 'test', 'images_1536993640.jpg', '1,2,3', 1, 0, 'oIF7qD8R', '', '2018-09-15'),
(17, 'ioIYmwvJ', 'fdg', 'yhjm', 'fgh', 0, 0, 'fgnf', 'fb', 0, '14', '2018-09-16', 'fdbd', 'Asset_1_1536993710.png', '11', 1, 0, 'oIF7qD8R', '', '2018-09-15'),
(18, 'RFMyrmoO', 'test2', 'chandigarh', 'manimazra', 1984374738.99, 2133231323.89, 'test', 'abc', 400, '1,2,3,', '2018-09-16,2018-09-18', 'test', 'images_1536993729.jpg', '1,2,3', 1, 1, 'oIF7qD8R', '', '2018-09-15'),
(19, '2qFZJjrw', 'fdg', 'yhjm', 'fgh', 0, 0, 'fgnf', 'fb', 7000000, '14', '2018-09-16', 'fdbd', 'Asset_1_1536993754.png', '11', 1, 0, 'oIF7qD8R', '', '2018-09-15'),
(20, 'TOqo4Bpn', 'fdg', 'yhjm', 'fgh', 0, 0, 'fgnf', 'fb', 7000000, '14', '2018-09-16', 'fdbd', 'Asset_1_1536993990.png', '11', 1, 0, 'oIF7qD8R', '', '2018-09-15'),
(21, 'H3xCVJtE', 'hjghj', 'yhjm', 'gf', 0, 0, 'dfhg', 'fbjm', 7000000, '14', '2018-09-16', 'hkth', 'Asset_1_1536994255.png', '11', 1, 0, 'oIF7qD8R', '', '2018-09-15'),
(22, 'ZGDoWOKr', 'hjghj', 'yhjm', 'gf', 0, 0, 'dfhg', 'fbjm', 7000000, '14', '2018-09-16', 'hkth', 'Asset_1_1536994258.png', '11', 1, 0, 'oIF7qD8R', '', '2018-09-15'),
(23, 'RJsiWXTy', 'ndkr', 'nfld', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270396, 76.8466549, 'nkcd', 'bxkd', 2147483647, '24', '2018- 8- 28 ', 'ndkr', 'Aqua_Ring_20180915_112742_1536994637.jpg', '42, 43, 44', 0, 0, 'oIF7qD8R', '', '2018-09-15'),
(24, 'vbHeD85p', 'ndkr', 'nfld', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270396, 76.8466549, 'nkcd', 'bxkd', 2147483647, '24', '2018- 8- 28 ', 'ndkr', 'Aqua_Ring_20180915_112742_1536995034.jpg', '42, 43, 44', 0, 0, 'oIF7qD8R', '', '2018-09-15'),
(25, 'XzSHyOC2', 'ndkn', 'kdkd', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270349, 76.8466592, 'jdln', 'jfkc', 2147483647, '25', '2018- 8- 28 ', 'ndkn', 'Aqua_Ring_20180915_112742_1536996218.jpg', '45', 0, 0, 'oIF7qD8R', '', '2018-09-15'),
(26, 'lmNG2ohR', 'test2', 'chandigarh', 'manimazra', 1984374738.99, 2133231323.89, 'test', 'abc', 400, '1,2,3,', '2018-09-16,2018-09-18', 'test', 'images_1536996683.jpg', '1,2,3', 1, 0, 'oIF7qD8R', '', '2018-09-15'),
(27, 'hz7bxZC0', 'test2', 'chandigarh', 'manimazra', 1984374738.99, 2133231323.89, 'test', 'abc', 400, '1,2,3,', '2018-09-16,2018-09-18', 'test', 'images_1536996713.jpg', '1,2,3', 1, 0, 'oIF7qD8R', '', '2018-09-15'),
(28, 'HXokZSUA', 'test2', 'chandigarh', 'manimazra', 1984374738.99, 2133231323.89, 'test', 'abc', 400, '1,2,3,', '2018-09-16,2018-09-18', 'test', 'images_1536996731.jpg', '1,2,3', 1, 0, 'oIF7qD8R', '', '2018-09-15'),
(29, 'b4VDImCL', 'test2', 'chandigarh', 'manimazra', 1984374738.99, 2133231323.89, 'test', 'abc', 400, '1,2,3,', '2018-09-16,2018-09-18', 'test', 'images_1537251294.jpg', '1,2,3', 1, 0, 'oIF7qD8R', '', '2018-09-18'),
(30, 'i8SCklm2', 'hjghj', 'yhjm', 'gf', 0, 0, 'dfhg', 'fbjm', 7000000, '14', '2018-09-16', 'hkth', '7_1537251505._notification-01', '11', 1, 0, 'oIF7qD8R', '', '2018-09-18'),
(31, 'tb9in7M6', 'NFIL', 'hdksl', '3rd Floor, Tower D, Phase - I, 134112, India', 30.727037, 76.8466576, 'nhdk', 'gsjh', 2147483647, '28', '2018- 8- 28 ', 'NFIL', 'Aqua_Ring_20180915_112742_1537251813.jpg', '48', 0, 1, 'oIF7qD8R', '', '2018-09-18'),
(32, 'Nnzsd70U', 'NVDF', 'ndkdo', 'suskk', 0, 0, 'nskg', 'susjk', 2147483647, '59', '2018-8-28 ', 'NVDF', 'IMG_20180913_123005_5_1537525902.jpg', '57', 0, 0, 'oIF7qD8R', '', '2018-09-21'),
(33, 'zQIsAHw9', 'njdv', 'nsyjk', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270342, 76.8466758, 'vuivb', 'fhkk', 2147483647, '61', '2018-9-28 ', 'njdv', '1537596618461_1537596624.jpg', '58', 0, 0, 'oIF7qD8R', '', '2018-09-22'),
(34, 'eHu6IkJF', 'njdv', 'nsyjk', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270342, 76.8466758, 'vuivb', 'fhkk', 2147483647, '61', '2018-9-28 ', 'njdv', '1537596618461_1537596649.jpg', '58', 0, 0, 'oIF7qD8R', '', '2018-09-22'),
(35, 'cDIy9i2Y', 'najl', 'nellore', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270348, 76.8466761, 'test', 'test', 787878, '63', '2018-9-28 ', 'najl', 'Screenshot_20180827-163436_1537623882.png', '59', 0, 0, 'oIF7qD8R', '', '2018-09-22'),
(36, '0oCVpOe7', 'NGFJ', 'nellore', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270356, 76.8466761, 'test', 'test', 787878, '66', '2018-9-28 ', 'NGFJ', 'Screenshot_20180911-162659_1537624854.png', '60', 0, 0, 'oIF7qD8R', '', '2018-09-22'),
(37, 'aO0NlkgY', 'Testing Project', 'Chandigarh India', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270512, 76.8466805, 'Glass,water,food\n', 'Amrik Singh', 2131, '69', '2018-9-25  ,2018-9-25 ', 'Testing Project', '1537869532185_1537869566.jpg', '63, 64', 0, 0, 'oIF7qD8R', '', '2018-09-25'),
(38, 'oF49TOXp', 'Testing Project', 'Chandigarh India', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270512, 76.8466805, 'Glass,water,food\n', 'Amrik Singh', 2131, '69', '2018-9-25  ,2018-9-25 ', 'Testing Project', '1537869532185_1537869574.jpg', '63, 64', 0, 0, 'oIF7qD8R', '', '2018-09-25'),
(39, 'Z7dOu1hK', 'Testing Project', 'Chandigarh India', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270512, 76.8466805, 'Glass,water,food\n', 'Amrik Singh', 2131, '69', '2018-9-25  ,2018-9-25 ', 'Testing Project', '1537869532185_1537870733.jpg', '63, 64', 0, 1, 'oIF7qD8R', '', '2018-09-25'),
(40, '7XfvM4Rm', 'NJLV', 'nellore', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270381, 76.8466754, 'test', 'test', 78787878, '74', '2018-7-28  ,2018-7-29 ', 'NJLV', 'attachment_1538545750.jpg', '66, 67', 0, 0, 'oIF7qD8R', '', '2018-10-03'),
(41, 'LGltSKse', 'NJLV', 'nfkl', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270388, 76.8466748, 'test', 'ndkf', 2147483647, '75', '2018-09-28  ,2018-10-28 ,2018-10-24', 'NJLV', 'attachment_1538548919.jpg', '68', 0, 0, 'oIF7qD8R', '', '2018-10-03'),
(42, 'ZvlDoLc9', 'NJLB', 'gjgj', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270381, 76.846676, 'test', 'vjvn', 7878, '76', '2018-09-28  ,2018-09-28 ', 'NJLB', 'attachment_1538550194.jpg', '69', 0, 0, 'oIF7qD8R', '', '2018-10-03'),
(43, 'chsYyEuM', 'NJLB', 'gjgj', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270381, 76.846676, 'test', 'vjvn', 7878, '76', '2018-09-28  ,2018-09-28 ', 'NJLB', 'attachment_1538550228.jpg', '69', 0, 0, 'oIF7qD8R', '', '2018-10-03'),
(44, 'xmF0yDMg', 'NJLB', 'gjgj', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270381, 76.846676, 'test', 'vjvn', 7878, '76', '2018-09-28  ,2018-09-28 ', 'NJLB', 'attachment_1538550236.jpg', '69', 0, 0, 'oIF7qD8R', '', '2018-10-03'),
(45, 'NDMehsWL', 'NJLB', 'gjgj', '3rd Floor, Tower D, Phase - I, 134112, India', 30.7270381, 76.846676, 'test', 'vjvn', 7878, '76', '2018-09-28  ,2018-09-28 ', 'NJLB', 'attachment_1538550301.jpg', '69', 0, 0, 'oIF7qD8R', '', '2018-10-03'),
(46, 'WQr8uRx7', 'NJLG', 'jsls', 'DLF Tower D, Phase - I, Manimajra, Panchkula, Haryana 160101, India', 30.7270486, 76.8466898, 'hdkn', 'test', 7878, '82, 83', '2018-09-28  ,2018-09-29 ', 'NJLG', '1537596618461_1538655299.jpg', '71', 0, 1, 'oIF7qD8R', '', '2018-10-04');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_project_phase`
--

CREATE TABLE `iserve_project_phase` (
  `id` int(11) NOT NULL,
  `project_phase` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_project_phase`
--

INSERT INTO `iserve_project_phase` (`id`, `project_phase`, `addedondate`, `status`) VALUES
(1, 'tendering', '2018-09-13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_project_visit_detail`
--

CREATE TABLE `iserve_project_visit_detail` (
  `id` int(11) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `visit_image` varchar(225) NOT NULL,
  `visit_date` varchar(225) NOT NULL,
  `description` varchar(225) NOT NULL,
  `added_by` varchar(225) NOT NULL,
  `addedondate` varchar(225) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_project_visit_detail`
--

INSERT INTO `iserve_project_visit_detail` (`id`, `projectRefId`, `visit_image`, `visit_date`, `description`, `added_by`, `addedondate`, `status`) VALUES
(1, 'pzk8HhQ7', '1538207337223_1538544291.jpg', '2018-10-3', 'hello45', 'oIF7qD8R', '2018-09-12', 0),
(3, 'pzk8HhQ7', 'Aqua_Ring_20180929_131847_1538544303.jpg', '2018-10-3', 'hello', 'oIF7qD8R', '2018-09-12', 0),
(4, 'tb9in7M6', '1537516087293_1537528525.jpg', '2018-8-21', 'yhjntyghnj', 'oIF7qD8R', '2018-09-19', 0),
(5, 'fhio', '9A0FBF57-1158-4F8E-B074-727E54B3F695_1537421313.png', '2018-8-20', 'fhiodh', 'oIF7qD8R', '2018-09-19', 0),
(7, 'RFMyrmoO', '1537273160931_1537361001.jpg', '2018-8-19', 'chjbkb', 'oIF7qD8R', '2018-09-19', 0),
(19, 'tb9in7M6', '1537621073581_1537621097.jpg', '2018-8-22', 'njlv', 'oIF7qD8R', '2018-09-22', 0),
(21, 'Z7dOu1hK', 'attachment_1538657838.jpg', '2018-10-4', 'tgjg', 'oIF7qD8R', '2018-10-02', 0),
(11, 'RFMyrmoO', '963747-download-abercrombie-wallpaper-3840x2160-for-ipad_1537425642.jpg', '2018-8-20', 'nfhksgjk', 'oIF7qD8R', '2018-09-20', 0),
(24, 'iHGPN4et', 'Aqua_Ring_20180929_131847_1538559953.jpg', '2018-010-3', 'njlv', 'oIF7qD8R', '2018-10-03', 0),
(20, 'RFMyrmoO', 'IMG_20180921_131805_1537880307.jpg', '2018-09-25', 'nnnn', 'oIF7qD8R', '2018-09-22', 0),
(14, 'RFMyrmoO', '1537516087293_1537535521.jpg', '2018-8-21', 'sgjk', 'oIF7qD8R', '2018-09-20', 0),
(25, 'oF49TOXp', '1538560253446_1538560266.jpg', '2018-10-3', 'njlv', 'oIF7qD8R', '2018-10-03', 0),
(26, 'XzSHyOC2', '1537596618461_1538726521.jpg', '2018-10-5', 'njlf', 'oIF7qD8R', '2018-10-03', 0),
(29, 'Z7dOu1hK', '1536745876371_1538658889.jpg', '2018-10-4', 'njlv', 'oIF7qD8R', '2018-10-03', 0),
(32, '7XfvM4Rm', '1539087593847_1539166913.jpg', '2018-10-10', 'ngkl', 'oIF7qD8R', '2018-10-04', 0),
(35, '7XfvM4Rm', '1539087617885_1539166394.jpg', '2018-10-10', 'njlv', 'oIF7qD8R', '2018-10-04', 0),
(34, '7XfvM4Rm', 'attachment_1539245300.png', '2018-10-11', 'njlv', 'oIF7qD8R', '2018-10-04', 0),
(36, 'NDMehsWL', '1537596618461_1538654816.jpg', '2018-10-4', 'ncjlguui', 'oIF7qD8R', '2018-10-04', 0),
(37, 'WQr8uRx7', '1537596618461_1538657228.jpg', '2018-10-4', 'ndjn', 'oIF7qD8R', '2018-10-04', 0),
(38, 'WQr8uRx7', '1537518549713_1538655439.jpg', '2018-010-4', 'njdv', 'oIF7qD8R', '2018-10-04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_safety_content`
--

CREATE TABLE `iserve_safety_content` (
  `id` int(11) NOT NULL,
  `safety_document` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `addedondate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_safety_content`
--

INSERT INTO `iserve_safety_content` (`id`, `safety_document`, `content`, `type`, `status`, `addedondate`) VALUES
(1, 'SampleVideo_1280x720_1mb_1535448521.mp4', 'hjhgjg', 'video/mp4', 0, '2018-08-28'),
(2, 'dummy-pdf_2_1535449084.pdf', 'test test test', 'application/pdf', 0, '2018-08-28'),
(3, '', '', '', 0, '2018-10-02'),
(4, '', '', '', 0, '2018-10-02');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_task`
--

CREATE TABLE `iserve_task` (
  `id` int(11) NOT NULL,
  `taskRefId` varchar(255) NOT NULL,
  `taskId` int(11) NOT NULL,
  `task_date` varchar(255) NOT NULL,
  `task_subject` varchar(255) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `assetRefId` varchar(255) NOT NULL,
  `componentRefId` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `task_image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 Pending 1 completed 2 Finish',
  `task_created_by` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_task`
--

INSERT INTO `iserve_task` (`id`, `taskRefId`, `taskId`, `task_date`, `task_subject`, `projectRefId`, `assetRefId`, `componentRefId`, `description`, `task_image`, `status`, `task_created_by`, `addedondate`) VALUES
(3, 'HlVfdEy6', 4701, '08/30/2018', 'test', 'gUnay4tC', 'a:1:{i:0;s:8:\"yZONzHj8\";}', 'a:1:{i:0;s:8:\"AHzOYWmF\";}', 'test', '', 1, 'qm07gWoK', '2018-08-31'),
(4, 'YkNLu1CB', 1869, '08/25/2018', 'testing', 'gUnay4tC', 'a:1:{i:0;s:8:\"yZONzHj8\";}', 'a:1:{i:0;s:8:\"AHzOYWmF\";}', 'tsts', '', 2, 'qm07gWoK', '2018-08-31'),
(5, 'QVe6zktI', 2348, '08/25/2018', 'test', 'gUnay4tC', 'a:2:{i:0;s:8:\"zS9exHJv\";i:1;s:8:\"yZONzHj8\";}', 'a:1:{i:0;s:8:\"AHzOYWmF\";}', 'test', '', 1, 'qm07gWoK', '2018-08-31'),
(6, 'kFRJAent', 385, '08/25/2018', 'test', 'pzk8HhQ7', 'a:2:{i:0;s:8:\"zS9exHJv\";i:1;s:8:\"yZONzHj8\";}', 'a:1:{i:0;s:8:\"AHzOYWmF\";}', 'test', '', 1, 'qm07gWoK', '2018-08-31'),
(7, 'VTMBUtI6', 2410, '08/25/2018', 'test', 'pzk8HhQ7', 'a:2:{i:0;s:8:\"zS9exHJv\";i:1;s:8:\"yZONzHj8\";}', 'a:1:{i:0;s:8:\"AHzOYWmF\";}', 'test', '', 1, 'qm07gWoK', '2018-08-31'),
(14, 'zrqHL4aJ', 1405, '2018-09-12', 'Test', 'pzk8HhQ7', 'a:2:{i:0;s:8:\"bqVhZ1Ao\";i:1;s:8:\"yZONzHj8\";}', 'a:1:{i:0;s:8:\"fE83IwgO\";}', 'testing', '', 0, 'qm07gWoK', '2018-09-14'),
(15, '6T7WioDq', 9725, '2018-09-12', 'Test', 'pzk8HhQ7', 'a:1:{i:0;s:8:\"bqVhZ1Ao\";}', 'a:1:{i:0;s:8:\"fE83IwgO\";}', 'testing', '', 0, 'qm07gWoK', '2018-10-12'),
(16, 'HMxqS4ec', 465, '2018-09-12', 'Test', 'pzk8HhQ7', 'a:1:{i:0;s:8:\"bqVhZ1Ao\";}', 'a:1:{i:0;s:8:\"fE83IwgO\";}', 'testing', '', 0, 'qm07gWoK', '2018-10-12'),
(17, 'GPqvpUKH', 3462, '2018-09-12', 'Test', 'pzk8HhQ7', 'a:1:{i:0;s:8:\"bqVhZ1Ao\";}', 'a:1:{i:0;s:8:\"fE83IwgO\";}', 'testing', '', 0, 'qm07gWoK', '2018-10-12'),
(18, 'TiK3f5Ga', 4236, '2018-04-17', 'Test', 'pzk8HhQ7', 'a:2:{i:0;s:8:\"bqVhZ1Ao\";i:1;s:8:\"yZONzHj8\";}', 'a:1:{i:0;s:8:\"fE83IwgO\";}', 'testing', '', 1, 'qm07gWoK', '2018-10-12'),
(19, 'axEo2qLO', 379, '2018-04-17', 'nfjl', 'pzk8HhQ7', 'a:3:{i:0;s:8:\"yZONzHj8\";i:1;s:9:\" bqVhZ1Ao\";i:2;s:1:\" \";}', 'a:2:{i:0;s:8:\"fE83IwgO\";i:1;s:0:\"\";}', 'njlg', '', 1, 'qm07gWoK', '2018-10-12'),
(20, 'X4MSun8J', 2918, '17 April 2018', 'njklv', 'pzk8HhQ7', 'a:2:{i:0;s:8:\"bqVhZ1Ao\";i:1;s:1:\" \";}', 'a:2:{i:0;s:8:\"fE83IwgO\";i:1;s:0:\"\";}', 'ngkl', '', 0, 'qm07gWoK', '2018-10-12'),
(21, 'pRdy7GNU', 5697, '2018-04-17', 'Task1', 'pzk8HhQ7', 'a:2:{i:0;s:8:\"bqVhZ1Ao\";i:1;s:1:\" \";}', 'a:2:{i:0;s:8:\"fE83IwgO\";i:1;s:0:\"\";}', 'this is the njlv', '', 0, 'qm07gWoK', '2018-10-12'),
(22, 'FPErtbdJ', 4763, '2018-04-17', 'Nask1', 'pzk8HhQ7', 'a:2:{i:0;s:8:\"bqVhZ1Ao\";i:1;s:1:\" \";}', 'a:2:{i:0;s:8:\"fE83IwgO\";i:1;s:0:\"\";}', 'njlv', '', 0, 'qm07gWoK', '2018-10-12');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_task_expense`
--

CREATE TABLE `iserve_task_expense` (
  `id` int(11) NOT NULL,
  `taskRefId` varchar(255) NOT NULL,
  `expense_name` varchar(255) NOT NULL,
  `expense_type` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_task_expense`
--

INSERT INTO `iserve_task_expense` (`id`, `taskRefId`, `expense_name`, `expense_type`, `price`, `attachment`, `addedondate`) VALUES
(6, 'QVe6zktI', '1', '1', 23, 'images_1535975957.jpg', '2018-09-03');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_user_attendance`
--

CREATE TABLE `iserve_user_attendance` (
  `id` int(11) NOT NULL,
  `userRefId` varchar(255) NOT NULL,
  `attendance_date` varchar(255) NOT NULL,
  `attendance_time` varchar(255) NOT NULL,
  `user_selfie` varchar(255) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `current_address` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0. Pending 1. Approved 2. Disapproved'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_user_attendance`
--

INSERT INTO `iserve_user_attendance` (`id`, `userRefId`, `attendance_date`, `attendance_time`, `user_selfie`, `latitude`, `longitude`, `current_address`, `addedondate`, `status`) VALUES
(1, 'XOIEmS0N', '2018-09-21', '05:17:01', 'images_1536839365.jpg', 0, 0, '', '2018-09-13', 0),
(2, 'oIF7qD8R', '2018-09-13', '05:17:01', 'images_1536912114.jpg', 0, 0, '', '2018-09-14', 0),
(3, 'oIF7qD8R', '2018 - 10 - 20 ', '00:00', '1537438411058_1537438449.jpg', 0, 0, '', '2018-09-20', 0),
(4, 'oIF7qD8R', '2018 - 10 - 20 ', '00:00', 'Screenshot_20180915-180043_1537438568.png', 0, 0, '', '2018-09-20', 0),
(5, 'oIF7qD8R', '2018 - 10 - 20 ', '00:00', 'Screenshot_20180915-180043_1537438892.png', 0, 0, '', '2018-09-20', 0),
(6, 'oIF7qD8R', '2018 - 10 - 20 ', '00:00', 'Screenshot_20180915-180043_1537438983.png', 0, 0, '', '2018-09-20', 0),
(7, 'oIF7qD8R', '2018 - 10 - 20 ', '00:00', 'Screenshot_20180915-180043_1537439588.png', 0, 0, '', '2018-09-20', 0),
(8, 'oIF7qD8R', '2018 - 10 - 20 ', '00:00', '1537439644648_1537439681.jpg', 0, 0, '', '2018-09-20', 0),
(9, 'oIF7qD8R', '2018 - 10 - 20 ', '00:00', 'Screenshot_20180914-151514_1537439992.png', 0, 0, '', '2018-09-20', 0),
(10, 'oIF7qD8R', '2018 - 10 - 20 ', '00:00', '1537442478420_1537442764.jpg', 0, 0, '', '2018-09-20', 0),
(11, 'oIF7qD8R', '2018 - 10 - 20 ', '00:00', 'Screenshot_20180915-180043_1537444898.png', 0, 0, '', '2018-09-20', 0),
(12, 'oIF7qD8R', '2018-09-13', '00:00', 'images_1537446529.jpg', 0, 0, '', '2018-09-20', 0),
(13, 'oIF7qD8R', '2018 - 10 - 20 ', '00:00', 'Screenshot_20180915-180043_1537446574.png', 0, 0, '', '2018-09-20', 0),
(14, 'oIF7qD8R', '2018 - 09 - 13', '00:00', 'images_1537446621.jpg', 0, 0, '', '2018-09-20', 0),
(15, 'oIF7qD8R', '2018-09-17', '04:05:00', 'download_1537448203.jpg', 0, 0, '', '2018-09-20', 0),
(16, 'oIF7qD8R', '2018 - 10 - 21 ', '00:00', '1537516928849_1537516942.jpg', 0, 0, '', '2018-09-21', 0),
(17, 'oIF7qD8R', '2018 - 10 - 21 ', '00:00', '1537516087293_1537526145.jpg', 0, 0, '', '2018-09-21', 0),
(18, 'oIF7qD8R', '2018 - 10 - 21 ', '00:00', '1537516928849_1537526333.jpg', 0, 0, '', '2018-09-21', 0),
(19, 'oIF7qD8R', '2018 - 10 - 21 ', '00:00', '1537516928849_1537526418.jpg', 0, 0, '', '2018-09-21', 0),
(20, 'oIF7qD8R', '2018 - 09 - 13', '12:00', 'images_1537600103.jpg', 0, 0, '', '2018-09-22', 0),
(21, 'oIF7qD8R', '2018 - 09 - 13', '12:00', 'images_1537600131.jpg', 999.89999, 888.799, 'chandigarh', '2018-09-22', 0),
(22, 'oIF7qD8R', '2018-09-22', '12:00', 'images_1537600211.jpg', 999.89999, 888.799, 'chandigarh', '2018-09-22', 0),
(23, 'oIF7qD8R', '2018 - 10 - 22 ', '00:00', '1537516087293_1537613686.jpg', 0, 0, '', '2018-09-22', 0),
(24, 'oIF7qD8R', '2018 - 10 - 22 ', '00:00', 'IMG_20180913_123015_2_1537613737.jpg', 0, 0, '', '2018-09-22', 0),
(25, 'oIF7qD8R', '2018 - 10 - 22 ', '00:00', 'Screenshot_20180911-162705_1537614208.png', 30.7270361, 76.8466759, '', '2018-09-22', 0),
(26, 'oIF7qD8R', '2018 - 10 - 22 ', '00:00', 'Screenshot_20180827-163436_1537614288.png', 30.7270361, 76.8466759, '', '2018-09-22', 0),
(27, 'oIF7qD8R', '2018 - 10 - 22 ', '00:00', 'Screenshot_20180911-162705_1537614569.png', 30.7270361, 76.846676, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-22', 0),
(28, 'oIF7qD8R', '2018 - 10 - 22 ', '00:00', 'Screenshot_20180911-162705_1537614579.png', 30.7270361, 76.846676, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-22', 0),
(29, 'oIF7qD8R', '2018 - 10 - 22 ', '17:21', 'Screenshot_20180911-162705_1537617078.png', 30.7270347, 76.8466757, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-22', 0),
(30, 'oIF7qD8R', '2018 - 10 - 22 ', '18:43', 'Screenshot_20180911-162705_1537622033.png', 30.7270358, 76.8466757, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-22', 0),
(31, 'oIF7qD8R', '2018 - 10 - 22 ', '19:28', 'Screenshot_20180911-162659_1537624742.png', 30.7270356, 76.8466761, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-22', 0),
(32, 'oIF7qD8R', '2018 - 10 - 24 ', '11:04', 'Screenshot_20180911-162705_1537767261.png', 30.7270359, 76.8466762, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-24', 0),
(33, 'oIF7qD8R', '2018-09-13', '00:00', 'images_1537767679.jpg', 999, 88899, 'chandigarh', '2018-09-24', 0),
(34, 'oIF7qD8R', '2018-09-24', '00:00', 'images_1537767733.jpg', 999, 88899, 'chandigarh', '2018-09-24', 0),
(35, 'oIF7qD8R', '2018 - 10 - 24 ', '11:49', 'Screenshot_20180911-162705_1537769951.png', 30.7270371, 76.8466758, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-24', 0),
(36, 'oIF7qD8R', '2018-09-24', '00:00', 'images_1537791641.jpg', 88899, 88899, 'chandigarh', '2018-09-24', 0),
(37, 'oIF7qD8R', '2018 - 10 - 25 ', '15:25', '1537621073581_1537869342.jpg', 30.7270351, 76.8466762, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(38, 'oIF7qD8R', '2018 - 10 - 25 ', '15:25', '1537596618461_1537869353.jpg', 30.7270363, 76.8466761, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(39, 'oIF7qD8R', '2018 - 10 - 25 ', '15:28', '', 30.727037, 76.8466765, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(40, 'oIF7qD8R', '2018 - 10 - 25 ', '15:36', 'Screenshot_20180920-190439_1537869974.png', 30.727036, 76.8466771, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(41, 'oIF7qD8R', '2018 - 10 - 25 ', '15:36', '1537869986358_1537869989.jpg', 30.727036, 76.8466771, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(42, 'oIF7qD8R', '2018 - 10 - 25 ', '16:10', '1537871998702_1537872006.jpg', 30.7270517, 76.84668, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(43, 'oIF7qD8R', '2018 - 10 - 25 ', '16:29', '', 30.7270349, 76.8466756, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(44, 'oIF7qD8R', '2018 - 10 - 25 ', '16:30', '', 30.7270357, 76.8466763, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(45, 'oIF7qD8R', '2018 - 10 - 25 ', '16:30', '', 30.7270357, 76.8466763, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(46, 'oIF7qD8R', '2018 - 9 - 25 ', '16:33', '', 30.727035, 76.8466767, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(47, 'oIF7qD8R', '2018 - 9 - 25 ', '16:33', '', 30.727035, 76.8466767, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(48, 'oIF7qD8R', '2018-09-25 ', '16:36', '', 30.7270352, 76.8466767, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(49, 'oIF7qD8R', '2018-09-25 ', '16:37', '', 30.7270352, 76.8466767, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(50, 'oIF7qD8R', '2018-09-25 ', '16:37', '', 30.7270352, 76.8466767, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(51, 'oIF7qD8R', '2018-09-25 ', '16:45', '', 30.7270342, 76.8466759, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(52, 'oIF7qD8R', '2018-09-25 ', '16:45', '1537450199892_1537874147.jpg', 30.7270342, 76.8466759, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(53, 'oIF7qD8R', '2018-09-25 ', '16:46', '1537449287734_1537874176.jpg', 30.7270342, 76.8466759, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(54, 'oIF7qD8R', '2018-09-25 ', '16:51', '29-Browse-Proposal-Pending-Screen_1537874475.png', 30.7270353, 76.8466756, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-09-25', 0),
(55, 'XOIEmS0N', '2018-09-24', '00:00', 'images_1538203556.jpg', 88899, 88899, 'chandigarh', '2018-09-29', 0),
(56, 'XOIEmS0N', '2018-09-29', '00:00', 'images_1538203593.jpg', 88899, 88899, 'chandigarh', '2018-09-29', 0),
(57, 'XOIEmS0N', '2018-09-29', '00:00', 'images_1538203639.jpg', 88899.23, 88899.2432, 'chandigarh', '2018-09-29', 0),
(58, 'oIF7qD8R', '2018 - 11 - 2 ', '19:22', '', 29.3336987, 48.0556884, '24-26 Helal Al Mutairi St, Salmiya, Kuwait', '2018-10-02', 0),
(59, 'oIF7qD8R', '2018 - 11 - 2 ', '19:22', '', 29.3336987, 48.0556884, '24-26 Helal Al Mutairi St, Salmiya, Kuwait', '2018-10-02', 0),
(60, 'oIF7qD8R', '2018 - 11 - 2 ', '19:40', '', 29.3336987, 48.0556884, '24-26 Helal Al Mutairi St, Salmiya, Kuwait', '2018-10-02', 0),
(61, 'oIF7qD8R', '2018 - 11 - 2 ', '19:40', '', 29.3336987, 48.0556884, '24-26 Helal Al Mutairi St, Salmiya, Kuwait', '2018-10-02', 0),
(62, 'oIF7qD8R', '2018-10-3 ', '11:05', '1538544920440_1538544924.jpg', 30.727038, 76.8466754, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-10-03', 0),
(63, 'oIF7qD8R', '2018-10-3 ', '11:14', '1538545493447_1538545497.jpg', 30.7270402, 76.8466737, '3rd Floor, Tower D, Phase - I, 134112, India', '2018-10-03', 0),
(64, 'XOIEmS0N', '2018-09-29', '18:00', 'images_1538574311.jpg', 88899.23, 88899.2432, 'chandigarh', '2018-10-03', 0),
(65, 'XOIEmS0N', '2018-09-29', '18:00', 'images_1538574359.jpg', 88899.23, 88899.2432, 'chandigarh', '2018-10-03', 0),
(66, 'oIF7qD8R', '2018-09-17', '6:00', 'abuse-01_1538655846.png', 0, 0, 'fsdf', '2018-10-04', 0),
(67, 'XOIEmS0N', '2018-09-29', '20:00', 'images_1538656335.jpg', 54544, 454, 'chandigarh', '2018-10-04', 0),
(68, 'XOIEmS0N', '2018-11-11', '20:00', 'images_1538656689.jpg', 54544, 32423, 'chandigarh', '2018-10-04', 0),
(69, 'oIF7qD8R', '2018-10-4 ', '18:12', '1538656924683_1538656936.jpg', 30.7270492, 76.8466897, 'DLF Tower D, Phase - I, Manimajra, Panchkula, Haryana 160101, India', '2018-10-04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iserve_user_detail`
--

CREATE TABLE `iserve_user_detail` (
  `id` int(11) NOT NULL,
  `userRefId` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_user_detail`
--

INSERT INTO `iserve_user_detail` (`id`, `userRefId`, `dob`, `gender`, `first_name`, `last_name`, `address`, `description`, `image`, `created_date`) VALUES
(1, '1ZKIFOgw', '09/14/2018', 'Female', 'Gur', 'Kaur', 'Beas', 'Test', 'images_1535535751.jpg', '2018-08-29'),
(2, 'qm07gWoK', '09/13/2018', 'Female', 'Gurjeevan', 'Kaur', 'Beas', 'Test', '', '2018-08-29'),
(3, 'XOIEmS0N', '09/07/2018', 'Male', 'test', 'abc', 'test', 'test', 'images_1535535751.jpg', '2018-08-29'),
(4, 'nswJkP7H', '09/14/2018', 'Female', 'user', 'xyz', 'test', 'test', 'images_1535535751.jpg', '2018-08-29'),
(5, 'JMNV8Rab', '09/14/2018', 'Female', 'test', 'user', 'test', 'test', '', '2018-08-29'),
(6, 'HPqKbNWL', '09/21/2018', 'Female', 'john', 'test', 'test', 'test', 'images_1535535751.jpg', '2018-08-30'),
(7, 'oIF7qD8R', '09/12/2018', 'Female', 'prachi', 'gupta', 'test', 'test', 'images_1535535751.jpg', '2018-09-05'),
(8, 'XEsYmkeg', '09/29/2018', 'Male', 'dfs sdff ', 'h', 'hd', 'sffsdf ', '', '2018-09-26');

-- --------------------------------------------------------

--
-- Table structure for table `iserve_workers_task`
--

CREATE TABLE `iserve_workers_task` (
  `id` int(11) NOT NULL,
  `taskRefId` varchar(255) NOT NULL,
  `workerRefId` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 Pending 1 completed',
  `addedondate` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iserve_workers_task`
--

INSERT INTO `iserve_workers_task` (`id`, `taskRefId`, `workerRefId`, `status`, `addedondate`) VALUES
(1, 'HlVfdEy6', 'XOIEmS0N', 0, '2018-08-31'),
(2, 'HlVfdEy6', 'nswJkP7H', 0, '2018-08-31'),
(3, 'YkNLu1CB', 'XOIEmS0N', 0, '2018-08-31'),
(4, 'YkNLu1CB', 'nswJkP7H', 0, '2018-08-31'),
(5, 'YkNLu1CB', 'HPqKbNWL', 0, '2018-08-31'),
(6, 'QVe6zktI', 'XOIEmS0N', 0, '2018-08-31'),
(7, 'kFRJAent', 'XOIEmS0N', 0, '2018-08-31'),
(8, 'VTMBUtI6', 'XOIEmS0N', 0, '2018-08-31'),
(9, 'DnLN0QO9', 'fdgfdg', 0, '2018-09-14'),
(10, 'BIVi4Pk1', 'fdgfdg', 0, '2018-09-14'),
(11, 'ywZ3Td0z', 'fdgfdg', 0, '2018-09-14'),
(12, 'mXzgoSak', 'fdgfdg', 0, '2018-09-14'),
(13, 'ABYsVrjU', 'fdgfdg', 0, '2018-09-14'),
(14, 'O5ThmJ6f', 'fdgfdg', 0, '2018-09-14'),
(15, 'zrqHL4aJ', 'XOIEmS0N', 0, '2018-09-14'),
(16, 'zrqHL4aJ', 'nswJkP7H', 0, '2018-09-14'),
(17, '6T7WioDq', 'XOIEmS0N', 0, '2018-10-12'),
(18, 'HMxqS4ec', 'XOIEmS0N', 0, '2018-10-12'),
(19, 'GPqvpUKH', 'XOIEmS0N', 0, '2018-10-12'),
(20, 'TiK3f5Ga', 'XOIEmS0N', 0, '2018-10-12'),
(21, 'TiK3f5Ga', 'nswJkP7H', 0, '2018-10-12'),
(22, 'axEo2qLO', '0', 0, '2018-10-12'),
(23, 'X4MSun8J', '0', 0, '2018-10-12'),
(24, 'pRdy7GNU', '0', 0, '2018-10-12'),
(25, 'FPErtbdJ', '0', 0, '2018-10-12');

-- --------------------------------------------------------

--
-- Table structure for table `project_document`
--

CREATE TABLE `project_document` (
  `id` int(11) NOT NULL,
  `projectRefId` varchar(255) NOT NULL,
  `document` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `status` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_document`
--

INSERT INTO `project_document` (`id`, `projectRefId`, `document`, `addedondate`, `status`) VALUES
(1, 'pzk8HhQ7', 'hqdefault_15354434571.jpg', '2018-08-28', 0),
(2, 'pzk8HhQ7', 'SampleVideo_1280x720_1mb_1535443457.mp4', '2018-08-28', 0),
(3, 'pzk8HhQ7', 'dummy-pdf_2_1535445263.pdf', '2018-08-28', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `iserve_asset`
--
ALTER TABLE `iserve_asset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_assign_project`
--
ALTER TABLE `iserve_assign_project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_collection`
--
ALTER TABLE `iserve_collection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_comment`
--
ALTER TABLE `iserve_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_complaint`
--
ALTER TABLE `iserve_complaint`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_complaint_notification`
--
ALTER TABLE `iserve_complaint_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_components`
--
ALTER TABLE `iserve_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_contact_person`
--
ALTER TABLE `iserve_contact_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_contact_us`
--
ALTER TABLE `iserve_contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_contractor`
--
ALTER TABLE `iserve_contractor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_invoice`
--
ALTER TABLE `iserve_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_invoice_notification`
--
ALTER TABLE `iserve_invoice_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_login_detail`
--
ALTER TABLE `iserve_login_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_news`
--
ALTER TABLE `iserve_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_notification`
--
ALTER TABLE `iserve_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_product`
--
ALTER TABLE `iserve_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_project_additional_information`
--
ALTER TABLE `iserve_project_additional_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_project_asset`
--
ALTER TABLE `iserve_project_asset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_project_asset_component`
--
ALTER TABLE `iserve_project_asset_component`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_project_detail`
--
ALTER TABLE `iserve_project_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_project_phase`
--
ALTER TABLE `iserve_project_phase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_project_visit_detail`
--
ALTER TABLE `iserve_project_visit_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_safety_content`
--
ALTER TABLE `iserve_safety_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_task`
--
ALTER TABLE `iserve_task`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_task_expense`
--
ALTER TABLE `iserve_task_expense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_user_attendance`
--
ALTER TABLE `iserve_user_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iserve_user_detail`
--
ALTER TABLE `iserve_user_detail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `iserve_workers_task`
--
ALTER TABLE `iserve_workers_task`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_document`
--
ALTER TABLE `project_document`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `iserve_asset`
--
ALTER TABLE `iserve_asset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `iserve_assign_project`
--
ALTER TABLE `iserve_assign_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `iserve_collection`
--
ALTER TABLE `iserve_collection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `iserve_comment`
--
ALTER TABLE `iserve_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `iserve_complaint`
--
ALTER TABLE `iserve_complaint`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `iserve_complaint_notification`
--
ALTER TABLE `iserve_complaint_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `iserve_components`
--
ALTER TABLE `iserve_components`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `iserve_contact_person`
--
ALTER TABLE `iserve_contact_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `iserve_contact_us`
--
ALTER TABLE `iserve_contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `iserve_contractor`
--
ALTER TABLE `iserve_contractor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `iserve_invoice`
--
ALTER TABLE `iserve_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `iserve_invoice_notification`
--
ALTER TABLE `iserve_invoice_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `iserve_login_detail`
--
ALTER TABLE `iserve_login_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `iserve_news`
--
ALTER TABLE `iserve_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `iserve_notification`
--
ALTER TABLE `iserve_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `iserve_product`
--
ALTER TABLE `iserve_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `iserve_project_additional_information`
--
ALTER TABLE `iserve_project_additional_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `iserve_project_asset`
--
ALTER TABLE `iserve_project_asset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `iserve_project_asset_component`
--
ALTER TABLE `iserve_project_asset_component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `iserve_project_detail`
--
ALTER TABLE `iserve_project_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `iserve_project_phase`
--
ALTER TABLE `iserve_project_phase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `iserve_project_visit_detail`
--
ALTER TABLE `iserve_project_visit_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `iserve_safety_content`
--
ALTER TABLE `iserve_safety_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `iserve_task`
--
ALTER TABLE `iserve_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `iserve_task_expense`
--
ALTER TABLE `iserve_task_expense`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `iserve_user_attendance`
--
ALTER TABLE `iserve_user_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `iserve_user_detail`
--
ALTER TABLE `iserve_user_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `iserve_workers_task`
--
ALTER TABLE `iserve_workers_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `project_document`
--
ALTER TABLE `project_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
